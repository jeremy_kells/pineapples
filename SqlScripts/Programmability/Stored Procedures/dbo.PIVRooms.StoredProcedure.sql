SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 1 12 2007
-- Description:	data for toilets pivot
-- =============================================
CREATE PROCEDURE [dbo].[PIVRooms]
	@DimensionColumns nvarchar(20) = 'CORE',
	@DataColumns nvarchar(20) = null,
	@Group nvarchar(30) = 'Grounds',
	@SchNo nvarchar(50) = null,
	@SurveyYear int = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


Select @DataColumns = dbo.pivDefaultDataColumns(@DataColumns)

-- no branching yet on datacolumns
	if (@Group = 'Class')
		EXEC dbo.PIVRooms_Class
			@DimensionColumns ,
			@DataColumns,
			@Group,
			@SchNo ,
			@SurveyYear

	if (@Group in( 'Grounds', 'REC'))
		EXEC dbo.PIVRooms_Grounds
			@DimensionColumns ,
			@DataColumns,
			@Group,
			@SchNo ,
			@SurveyYear

	if (@Group = 'Dorm')
		EXEC dbo.PIVRooms_Dorm
			@DimensionColumns ,
			@DataColumns,
			@Group,
			@SchNo ,
			@SurveyYear


	if (@Group like 'Admin%') begin
		-- transform this group
		Select @group =
			case
				when @group like 'ADMIN/%' then replace(@group,'ADMIN/','')
				when @group like 'ADMIN_%' then replace(@group,'ADMIN_','')
				else nullif(replace(@group,'ADMIN',''),'')
			end
		EXEC dbo.PIVRooms_Admin
			@DimensionColumns ,
			@DataColumns,
			@Group,
			@SchNo ,
			@SurveyYear


	end

END
GO

