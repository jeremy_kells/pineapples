SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 13 03 2018
-- Description:	Exam filter
--              TODO - Add ability to search by schools returning exams they participated in
-- =============================================
CREATE PROCEDURE [pExamRead].[ExamFilterIDs]
	-- Add the parameters for the stored procedure here

	@NumMatches int OUTPUT,
	@PageSize int = 0,
	@PageNo int = 0,
	@SortColumn nvarchar(30) = null,
	@SortDir int = 0,

	@ExamID int = null,
    @ExamCode nvarchar(10) = null,
    @ExamYear int = null,
    @ExamDate datetime = null
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @keys TABLE
	(
	selectedExID int
	, recNo int IDENTITY PRIMARY KEY
	)

	INSERT INTO @keys
	(selectedExID)
	SELECT exID
	FROM Exams E
	WHERE
	(exID = @ExamID or @ExamID is null)
	AND (E.exCode = @ExamCode or @ExamCode is null)
	AND (E.exYear = @ExamYear or @ExamYear is null)
	AND (E.exDate = @ExamDate or @ExamDate is null)

	ORDER BY
		CASE @sortColumn			-- strings
			WHEN 'ExamID' then E.exID
			WHEN 'ExamCode' then E.exCode
			WHEN 'ExamYear' then E.exYear
			ELSE null
			END,
		CASE @sortColumn -- dates
			WHEN 'ExamDate' then E.exDate
			ELSE null
		END,

		exID


	SELECT @NumMatches = @@ROWCOUNT		-- this returns the total matches

	If @SortDir = 1 begin


		SELECT selectedExID
		, RecNo
		FROM
		(
			Select selectedExID
			, @NumMatches - RecNo + 1 RecNo
			FROM @Keys
		) S
		WHERE (@PageSize = 0
				or
					@PageSize >= @NumMatches
				or
						RecNo between @PageSize * (@PageNo - 1) + 1
						and @PageSize * (@PageNo)
				)
		ORDER BY RecNo
	end
	else begin

		SELECT selectedExID
		, RecNo
		FROM @Keys
		WHERE (@PageSize = 0
				or
					@PageSize >= @NumMatches
				or
						RecNo between @PageSize * (@PageNo - 1) + 1
						and @PageSize * (@PageNo)

				)
	end
END
GO

