SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Description: Warehouse Enrolment Ratios
-- create thet base table for reporting on enrolment ratios by single year of education
-- generates both the National and district tables
-- =============================================
CREATE PROCEDURE [warehouse].[buildClassLevelER]
	@StartFromYear int = null
AS
BEGIN
	SET NOCOUNT ON
	SET ANSI_WARNINGS OFF

DELETE from Warehouse.ClassLevelERDistrict
	WHERE SurveyYear >= @StartFromYEar or @StartFromYEar is null
print 'warehouse.ClassLevelERDistrict deletes - rows:' + convert(nvarchar(10), @@rowcount)

-- can delete everything becuase the whole table is rebult from ClassLevelERDistrict
DELETE from Warehouse.ClassLevelER

print 'warehouse.ClassLevelER deletes - rows:' + convert(nvarchar(10), @@rowcount)

	INSERT INTO warehouse.classLevelERDistrict
	Select S.*
	, POP.popM
	, POP.popF
	, POP.pop

	FROM
	(
		Select SurveyYEar
			, ClassLevel
			, lvlYear YearOfEd
			, svyPSAge - 1 + lvlYear OfficialAge
			, districtCode
			, sum(enrolM) enrolM
			, sum(enrolF) enrolF
			, sum(enrol) enrol

			, sum(repM) repM
			, sum(repF) repF
			, sum(rep) rep


			, sum(psaM) psaM
			, sum(psaF) psaF
			, sum(psa) psa

			, sum(intakeM) intakeM
			, sum(intakeF) intakeF
			, sum(intake) intake

			, sum(nEnrolM) nEnrolM
			, sum(nEnrolF) nEnrolF
			, sum(nEnrol) nEnrol

			, sum(nRepM) nRepM
			, sum(nRepF) nRepF
			, sum(nRep) nRep

			, sum(nIntakeM) nIntakeM
			, sum(nIntakeF) nIntakeF
			, sum(nIntake) nIntake
		FROM
		(
		Select SurveyYear
			, ClassLevel
			, districtCode
			, Enrol enrolM
			, null enrolF
			, Enrol enrol

			, Rep repM
			, null repF
			, Rep rep

			, psa psaM
			, null psaF
			, psa psa

			, isnull(Enrol,0) - isnull(Rep,0) intakeM
			, null intakeF
			, isnull(Enrol,0) - isnull(Rep,0) intake

			, case when svyPSAge - 1 + lvlYear = Age then Enrol end nEnrolM
			, null nEnrolF
			, case when svyPSAge - 1 + lvlYear = Age then Enrol end nEnrol

			, case when svyPSAge - 1 + lvlYear = Age then Rep end nRepM
			, null nRepF
			, case when svyPSAge - 1 + lvlYear = Age then Rep end nRep

			, case when svyPSAge - 1 + lvlYear = Age then isnull(Enrol,0) - isnull(Rep,0)  end nIntakeM
			, null nIntakeF
			, case when svyPSAge - 1 + lvlYear = Age then isnull(Enrol,0) - isnull(Rep,0)  end nIntake

			FROM warehouse.tableEnrol
			INNER JOIN lkpLevels L
				ON ClassLevel = L.codeCode
			INNER JOIN Survey S
				ON SurveyYEar = S.svyYear
			WHERE GenderCode = 'M'
			UNION ALL
			Select SurveyYear
			, ClassLevel
			, districtCode
			, null enrolM
			, Enrol enrolF
			, Enrol enrol

			, null repM
			, Rep repF
			, Rep rep

			, null psaM
			, psa psaF
			, psa psa

			, null intakeM
			, isnull(Enrol,0) - isnull(Rep,0) intakeF
			, isnull(Enrol,0) - isnull(Rep,0) intake

			, null nEnrolM
			, case when svyPSAge - 1 + lvlYear = Age then Enrol end nEnrolF
			, case when svyPSAge - 1 + lvlYear = Age then Enrol end nEnrol

			, null nRepM
			, case when svyPSAge - 1 + lvlYear = Age then Rep end nRepF
			, case when svyPSAge - 1 + lvlYear = Age then Rep end nRep

			, null nIntakeM
			, case when svyPSAge - 1 + lvlYear = Age then isnull(Enrol,0) - isnull(Rep,0)  end nIntakeF
			, case when svyPSAge - 1 + lvlYear = Age then isnull(Enrol,0) - isnull(Rep,0)  end nIntake


			FROM warehouse.tableEnrol
			INNER JOIN lkpLevels L
				ON ClassLevel = L.codeCode
			INNER JOIN Survey S
				ON SurveyYEar = S.svyYear
			WHERE GenderCode = 'F'
		) U
		INNER JOIN lkpLevels L
			ON U.ClassLevel = L.codeCode
		INNER JOIN Survey S
			ON U.SurveyYear = S.svyYear
		GROUP BY SurveyYear, ClassLevel, lvlYEar, svyPSAge, districtCode
	) S
	LEFT join
	(
		--population totals
		SELECT popYear
		, popAge
		, dID
		, sum(case when GenderCode = 'M' then pop end) PopM
		, sum(case when GenderCode = 'F' then pop end) PopF
		, sum(pop) Pop
		from warehouse.measurePopG
		GRoup by popYear, popAge, dID
	) POP
		ON S.SurveyYEar = POP.PopYear
		AND S.OfficialAge = POP.popAge
		AND isnull(s.DistrictCode,'') = isnull(POP.dID,'')
	WHERE (SurveyYear >= @StartFromYear or @StartFromYear is null)
	ORDER BY SurveyYear, YearOfEd


	print 'warehouse.ClassLevelERDistrict inserts - rows:' + convert(nvarchar(10), @@rowcount)

END

-- now aggregate the district totals into National Totals
INSERT INTO warehouse.classLevelER
SELECT
		SurveyYEar
		, classLevel
		, yearOfEd
		, OfficialAge

		, sum(enrolM) enrolM
		, sum(enrolF) enrolF
		, sum(enrol) enrol

		, sum(repM) repM
		, sum(repF) repF
		, sum(rep) rep

		, sum(psaM) psaM
		, sum(psaF) psaF
		, sum(psa) psa

		, sum(intakeM) intakeM
		, sum(intakeF) intakeF
		, sum(intake) intake

		, sum(nEnrolM) nEnrolM
		, sum(nEnrolF) nEnrolF
		, sum(nEnrol) nEnrol

		, sum(nRepM) nRepM
		, sum(nRepF) nRepF
		, sum(nRep) nRep

		, sum(nIntakeM) nIntakeM
		, sum(nIntakeF) nIntakeF
		, sum(nIntake) nIntake

		, sum(popM) popM
		, sum(popF) popF
		, sum(pop) pop


FROM warehouse.classLevelERDistrict
GROUP BY SurveyYEar
		, ClassLevel
		, yearOfEd
		, OfficialAge

ORDER BY SurveyYear
		, yearOfEd

		print 'warehouse.ClassLevelER inserts - rows:' + convert(nvarchar(10), @@rowcount)
GO

