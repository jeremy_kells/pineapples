SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 12 11 2011
-- Description:	modified to support toiltet group code
-- =============================================
CREATE PROCEDURE [pSchoolWrite].[updateToilets]
	-- Add the parameters for the stored procedure here
	@SurveyID int = 0,
	@toiXML xml
AS

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @toitbl table(
							ToiletType nvarchar(25),
							ToiletUse nvarchar(10),
							Num int,
							Condition nvarchar(1),
							YN nvarchar(1)
							)

	DECLARE @group nvarchar(25)

	declare @idoc int
	EXEC sp_xml_preparedocument @idoc OUTPUT, @toiXML

	INSERT @toitbl
	Select * from OPENXML (@idoc, '/toilets/toilet',2)
		WITH (
				ToiletType nvarchar(25) '@toiletType',
				ToiletUse nvarchar(10) '@toiletUse',
				Num int '@num',
				Condition nvarchar(1) '@condition',
				YN			nvarchar(1)		'@yn'
			  )

	SELECT @group = ttypgroup
	FROM OPENXML (@idoc, '/toilets',2)
		WITH (
				ttypgroup nvarchar(25) '@group'
			  )

    -- Insert statements for procedure here
	SELECT * from @toitbl
	select @group

begin transaction

begin try
	DELETE from Toilets
	FROM Toilets
		INNER JOIN lkpToiletTypes TT
			ON Toilets.toiType = TT.ttypName
	WHERE ssid = @SurveyID
	AND ( TT.ttypGroup = @group OR @group is null		)
	print @@ROWCOUNT

	INSERT INTO Toilets(ssID, toiType, toiUse,
			toiNum, toiCondition, toiYN)
	SELECT @SurveyID,
		ToiletType,
		ToiletUse,
		Num,
		Condition,
		YN
	From @toitbl
end try

begin catch

    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end

    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err
end catch
-- and commit

if @@trancount > 0
	commit transaction

return


END
GO

