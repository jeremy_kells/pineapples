SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[BuildRank4]
	(@SurveyYear int = 0)
AS


BEGIN
/*
	This procedure rebuilds the ranking table showing the rank, dectile and quartile for
	each school in each year, within its district and school type, and nationally within school type

*/
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	create table #ranktmp
(
	svyYear int not null,
	schNo nvarchar(50) not null,
	District nvarchar(10) null,
	SchoolType nvarchar(10) null,
	Enrol int,
	Estimate bit
	, rankD int
	, rankN int
	, countD int
	, countN int

)


	declare @ds TABLE
(

	svyYear int,
	District nvarchar(10),
	SchoolType nvarchar(10),
	numSchools int
)

	declare @dsn TABLE
(

	svyYear int,
	SchoolType nvarchar(10),
	numSchools int
)

select *
into #ebse
from dbo.tfnESTIMATE_BestSurveyEnrolments()

-- this temp table is the enrolment for each school in each life year
-- it includee the enroloment value, (now staged on Schoolsurvey)
-- and the estimate flag

insert into #ranktmp(svyYear, schNo, District, SchoolType, Enrol,Estimate, rankD, rankN, countD, countN)


select E.[LifeYear],
E.schNo,
I.iGroup,
bestSchType,
E.bestEnrol ssEnrol,
E.Estimate
, ROW_NUMBER()  OVER (PARTITION BY [LifeYear], bestSchType, iGRoup  ORDER BY bestEnrol DESC)
, ROW_NUMBER()  OVER (PARTITION BY [LifeYear], bestSchType  ORDER BY bestEnrol DESC)
, count(*) OVER (PARTITION BY [LifeYear], bestSchType, iGRoup)
, count(*) OVER (PARTITION BY [LifeYear], bestSchType)

from
#ebse E
INNER JOIN Schools S
on E.schNo = s.schNo
inner join Islands I on S.iCode = i.iCode
WHERE [LifeYear] = @SurveyYear or @SurveyYear = 0


delete from SurveyYEarRank
WHERE svyYear = @SurveyYear or @SurveyYear = 0

insert into SurveyYearRank(svyYear, schNo,
	rankDistrict, rankSchType,
	rankEnrol, rankEstimate,
	rankDistrictSchoolType, rankDistrictSchoolTypeDec, rankDistrictSchoolTypeQtl,
	rankSchoolType, rankSchoolTypeDec, rankSchoolTypeQtl
)
select E.svyYear, E.schNo, E.District, E.SchoolType, E.Enrol,E. Estimate
, E.rankD
, 		right('0' + convert(nvarchar(2),case
			when countD < 10 then rankD
			else floor((rankD-1) * 10 / countD)+1
		end),2)
,		'Q' + convert(nchar(1),case
			when countD < 4 then rankD
			else floor((rankD-1) * 4 / countD)+1
		end)


, E.rankN
, 		right('0' + convert(nvarchar(2),case
			when countN < 10 then rankN
			else floor((rankN-1) * 10 / countN)+1
		end),2)
,		'Q' + convert(nchar(1),case
			when countN < 4 then rankN
			else floor((rankN-1) * 4 / countN)+1
		end)

from
#rankTmp E


-- this writes the ranke, dectile and quartile values
-- the rank we can calculate from the id in the rank table, and the first id for the group
--- which is now in #DS


END
GO

