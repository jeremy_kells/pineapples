SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 1 6 2010
-- Description:	Create the required org units
-- =============================================
CREATE PROCEDURE [Aurion].[CreateOrgUnitHierarchy]
	-- Add the parameters for the stored procedure here
	@SendAll int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    declare @AurionModel int	-- we'll preserve support for the deep model (Auth Type/Auth/SchoolType/School/Sector)
								-- while adding support for the shallow model (Auth Type/Auth/Sector)
	Select @AurionModel = common.SysParamInt('Aurion_Model',0)

	if (@AurionModel = 0)
		exec Aurion.createAuthoritySectorCodeOrgUnits @SendAll

	if (@AurionModel = 1)
		exec Aurion.createSchoolOrgUnits @SendAll

END
GO

