SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 11 10 2017
-- Description:	accepts a set of school number and returns those that are not valid
-- the Xml is a collection of all the schools represented on any page of the data survey workbook
-- also reports on duplicates found on sheet named Schools or WASH
-- called from Controllers_Api/NdoeController/upload
-- =============================================
CREATE PROCEDURE [dbo].[loadNdoeValidateSchools]
	-- Add the parameters for the stored procedure here
	@xml xml
AS
BEGIN
	SET NOCOUNT ON;

	-- this is the structure of the xml argument:
	-- Note that Sheet_Name is taken from the @Sheet attribute on School_No, rather than the parent node @name
	-- this was found to be far more efficient in execution
/*
<?xml version="1.0"?>
<Schools>
	<Sheet name="Students">
		<School_No Sheet="Students" Index="0">KSA201</School_No>
		<School_No Sheet="Students" Index="1">KSA201</School_No>
		<School_No Sheet="Students" Index="0">KSA201</School_No>
		<School_No Sheet="Students" Index="1">KSA201</School_No>
		<School_No Sheet="Students" Index="2">KSA201</School_No>
	</Sheet>
	<Sheet name="WASH">
		<School_No Sheet="WASH" Index="0">KSA201</School_No>
		<School_No Sheet="WASH" Index="1">KSA203</School_No>
	</Sheet>
</Schools>
*/

	-- collect all school nos that appear on any sheet
	declare @all table
	(
		School_No nvarchar(50)
		, Sheet_Name nvarchar(50)
		, rowID int
	)
	declare @validations TABLE
	(
		School_No nvarchar(50)
		, Sheet_Name nvarchar(50)
		, rowID int
		, valmsg nvarchar(200)
	)

	INSERT INTO @all
	Select
		nullif(v.value('.', 'nvarchar(50)'),'') School_No
		--, null
		--, null
		, v.value('@Sheet', 'nvarchar(50)') Sheet_Name
		, v.value('@Index', 'int') rowID
	FROM @xml.nodes('/Schools/Sheet/School_No') as V(v)

-- not supplied
	INSERT INTO @validations
	SELECT '(missing)'
	, Sheet_Name
	, rowID
	, 'No school supplied'
	FROM @all
	WHERE School_No is null

-- supplied, but not valid
	INSERT INTO @validations
	SELECT School_No
	, Sheet_Name
	, rowID
	, 'unknown school'
	FROM @all A
		LEFT JOIN Schools S
			ON A.School_No = S.schNo
	WHERE School_No is not null
		AND S.schNo is null

-- duplicates
	INSERT INTO @validations
	Select * from
	(
	SELECT School_No
	, Sheet_Name
	, min(rowID) rowID
	, 'duplicate school' valMsg
	FROM @all A
	WHERE Sheet_Name in ('Schools', 'WASH')
	GROUP BY School_No, Sheet_Name
	HAVING count(*) > 1
	UNION ALL
	SELECT School_No
	, Sheet_Name
	, max(rowID)
	, 'duplicate school'
	FROM @all A
	WHERE Sheet_Name in ('Schools', 'WASH')
	GROUP BY School_No, Sheet_Name
	HAVING count(*) > 1
	) S
	ORDER BY School_No, rowID


	Select * from @validations
END
GO

