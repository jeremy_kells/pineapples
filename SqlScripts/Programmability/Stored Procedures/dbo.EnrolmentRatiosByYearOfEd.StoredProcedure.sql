SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 17 12 2010
-- Description:	Nation-level summary of enrolment ratios at the class level, rather than by level of education
-- =============================================
CREATE PROCEDURE [dbo].[EnrolmentRatiosByYearOfEd]
	-- Add the parameters for the stored procedure here
	@SendASXML int = 0
	, @xmlOut xml = null OUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


declare @schoolNo nvarchar(50)
declare @SurveyYear int

declare @checkPop int		-- number of population models
declare @popModCode nvarchar(10)			-- the pop model we will use
declare @popModName nvarchar(50)
declare @popModDefault int

Select @checkPop = count(*)
, @popModCode = min(popModCode)
From PopulationModel


if (@checkPop=0) begin
	raiserror('There is no Population Model to use. ',
				16,10)
end

if (@checkPop > 1) begin
	declare @checkEFA int
	Select @checkEFA = count(*)

	-- SRSI0045 20 9 2010 get the one that is the default
	, @popModCode = min(popModCode)
	From PopulationModel

	WHERE popModEFA = 1

	if (@checkEFA <> 1) begin
		raiserror('You must specify exactly one Population Model to use for EFA reporting. ',
					16,10)
	end
end


Select *
	into #ebse
	from dbo.tfnESTIMATE_BestSurveyenrolments()
	WHERE (schNo = @schoolNo or @SchoolNo is null)
	AND (LifeYear between @SurveyYear and @SurveyYEar + 1
			or @SurveyYEar is null)
	   -- NER 1 to 6 this can come from EnrolmentRatios

IF @SendAsXML = 0

   Select
   P.popYear [Survey Year]
   , P.popAge [Official Age]
   , P.popM
   , P.popF
   , P.pop
   , N.YearOfEd
   , DL.levelCode
   , enrolM
   , enrolF
   , enrol
   , nEnrolM
   , nEnrolF
   , nEnrol
   , isnull(enrolM,0)/popM gerM
   , isnull(enrolF,0)/popF gerF
   , isnull(enrol,0)/pop ger
   , isnull(nEnrolM,0)/popM nerM
   , isnull(nEnrolF,0)/popF nerF
   , isnull(nEnrol,0)/pop ner

   FROM
   (
	Select

		P.popYear
		, P.popAge
		, cast(sum(P.popM) as float) popM
		, cast(sum(P.popF) as float) popF
		, cast(sum(P.popM + P.popF) as float) pop
	FROM
		dbo.Population P
		WHERE popModCode = @PopModCode or @popModCode is null
		GROUP BY popYear, popAge
	) P
   INNER JOIN
   (
   Select
    LifeYear
   , N.YearOfEd
   , N.OfficialAge
   , sum(enrolM) enrolM
   , sum(enrolF) enrolF
   , sum(enrol)  enrol
   , sum(nEnrolM) nEnrolM
   , sum(nEnrolF) nEnrolF
   , sum(nEnrol)  nEnrol
	   , sum(repM) repM
	   , sum(repF) repF
	   , sum(rep)  rep
	   , sum(nrepM) nRepM
	   , sum(nrepF) nRepF
	   , sum(nrep)  nRep
	FROM #ebse
	INNER JOIN pEnrolmentRead.ssIDEnrolmentLevelN N
		On #ebse.bestssID = N.ssID
	LEFT JOIN pEnrolmentRead.ssIDRepeatersLevelN RN
		On N.ssID = RN.ssID
		AND N.YearOfEd = RN.YearOfEd

	group by LifeYear, N.YearOfEd, N.OfficialAge
   ) N
   On P.popYear = N.LifeYear
	AND P.popAge = N.OfficialAge
	INNER JOIN common.ListDefaultPathLevels DL
	ON N.YEarOFEd = DL.YearOfEd
	ORDER BY P.PopYear, P.popAge

if @SendASXML <> 0  begin

   declare @XML xml
   declare @XML2 xml
   SELECT @XML =
   (
   Select
   P.popYear		[@year]
   , P.popAge		[@officialAge]
   , N.YearOfEd		[@yearOfEd]
   , DL.levelCode	[@levelCode]
   , cast( P.popM as int) 	    popM
   , cast(P.popF as int)		popF
   , cast(P.pop as int)			pop
   , enrolM
   , enrolF
   , enrol
   , nEnrolM
   , nEnrolF
   , nEnrol
   , repM
   , repF
   , rep
   , nRepM
   , nRepF
   , nRep
   , intakeM
   , intakeF
   , intake
   , nIntakeM
   , nIntakeF
   , nIntake
   , cast(psaM as int) psaM
   , cast(psaF as int) psaF
   , cast(psa as int) psa

   , cast(isnull(enrolM,0)/popM as decimal(8,5)) gerM
   , cast(isnull(enrolF,0)/popF as decimal(8,5)) gerF
   , cast(isnull(enrol,0)/pop as decimal(8,5)) ger
   , cast(isnull(nEnrolM,0)/popM as decimal(8,5)) nerM
   , cast(isnull(nEnrolF,0)/popF as decimal(8,5)) nerF
   , cast(isnull(nEnrol,0)/pop as decimal(8,5)) ner

   , cast(isnull(intakeM,0)/popM as decimal(8,5)) girM
   , cast(isnull(intakeF,0)/popF as decimal(8,5)) girF
   , cast(isnull(intake,0)/pop as decimal(8,5)) gir
   , cast(isnull(nIntakeM,0)/popM as decimal(8,5)) nirM
   , cast(isnull(nIntakeF,0)/popF as decimal(8,5)) nirF
   , cast(isnull(nIntake,0)/pop as decimal(8,5)) nir

   , case when N.YearOfEd = 1 then
			case when isnull(nIntakeM,0) = 0 then 0 else cast(psaM/ IntakeM  as decimal(18,5)) end
		else null end psaPercM

   , case when N.YearOfEd = 1 then
			case when isnull(nIntakeF,0) = 0 then 0 else cast(psaF / IntakeF as decimal(8,5)) end
		else null end psaPercF

    , case when N.YearOfEd = 1 then
			case when isnull(nIntake,0) = 0 then 0 else cast(psa / Intake as decimal(8,5)) end
		else null end psaPerc
   FROM
   (
	Select

		P.popYear
		, P.popAge
		, cast(sum(P.popM) as float) popM
		, cast(sum(P.popF) as float) popF
		, cast(sum(P.popM + P.popF) as float) pop
	FROM
		dbo.Population P
		WHERE popModCode = @PopModCode or @popModCode is null
		GROUP BY popYear, popAge
	) P
   INNER JOIN
   (
   Select
    LifeYear
   , N.YearOfEd
   , N.OfficialAge
   , sum(enrolM) enrolM
   , sum(enrolF) enrolF
   , sum(enrol)  enrol
   , sum(nEnrolM) nEnrolM
   , sum(nEnrolF) nEnrolF
   , sum(nEnrol)  nEnrol
   , sum(repM) repM
   , sum(repF) repF
   , sum(rep)  rep
   , sum(nrepM) nRepM
   , sum(nrepF) nRepF
   , sum(nrep)  nRep
   , sum(isnull(enrolM,0) - isnull(repM,0)) intakeM
   , sum(isnull(enrolF,0) - isnull(repF,0)) intakeF
   , sum(isnull(enrol,0) - isnull(rep,0)) intake

   , sum(isnull(nEnrolM,0) - isnull(nRepM,0)) nIntakeM
   , sum(isnull(nEnrolF,0) - isnull(nRepF,0)) nIntakeF
   , sum(isnull(nEnrol,0) - isnull(nRep,0)) nIntake

   , sum(PSA.psaM) psaM
   , sum(PSA.psaF) psaF
   , sum(PSA.PSA) psa

   FROM #ebse
	INNER JOIN pEnrolmentRead.ssIDEnrolmentLevelN N
		On #ebse.bestssID = N.ssID
	LEFT JOIN pEnrolmentRead.ssIDRepeatersLevelN RN
		On N.ssID = RN.ssID
		AND N.YearOfEd = RN.YearOfEd
	LEFT JOIN
		(SELECT ssID
		, cast(sum(vtblPSA.PSAF) as decimal(18,5)) psaF
		, cast(sum(vtblPSA.psaM)  as decimal(18,5)) psaM
		, cast(sum(isnull(vtblPSA.psaF,0) + isnull(vtblPSA.psaM,0))  as decimal(18,5)) psa
		FROM vtblPSA
		GROUP BY ssID
		) PSA
			ON N.ssID = PSA.ssID
			AND N.YearOfEd = 1  --- ??? why ???
			--ON #ebse.bestssID = PSA.ssID

	group by LifeYear, N.YearOfEd, N.OfficialAge
   ) N
   On P.popYear = N.LifeYear
	AND P.popAge = N.OfficialAge
	INNER JOIN common.ListDefaultPathLevels DL
	ON N.YEarOFEd = DL.YearOfEd
	ORDER BY P.PopYear, N.YearOfEd
	FOR XML PATH('LevelER')
	)
	SElect @xmlOut =
		(Select @xml
			FOR XML PATH('LevelERs')
		)
	-- send the xml as the result set
	if @SendAsXML = 1
		SELECT @xmlOut LevelNers
end
   drop table #ebse
END
GO

