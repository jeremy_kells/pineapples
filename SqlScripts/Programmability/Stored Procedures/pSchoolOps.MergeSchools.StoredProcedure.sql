SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 11 03 2010
-- Description:	Merge schools
-- =============================================
CREATE PROCEDURE [pSchoolOps].[MergeSchools]
	-- Add the parameters for the stored procedure here
	@Source nvarchar(50),
	@MergeTo nvarchar(50)
	with EXECUTE AS 'pineapples'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	--SET ANSI_WARNINGS OFF;
	-- we go through all the related tables, changing the source school to the dest school
    -- Insert statements for procedure here
	declare @TableName sysname
	declare @FK_Column sysname
	declare @key sysname
	declare @Update int
	declare @Delete int
    declare @sql nvarchar(2000)

	CREATE TABLE #rules
	(
		TableName sysname,
		mergeRule	nvarchar(50),
		rulekey sysname NULL			-- partial key e.g. svyYear if we are trying to interleave records
	)


	CREATE TABLE #results
	(
		TableName sysname,
		schNo nvarchar(50),
		num		int
	)
	CREATE TABLE #resultsA
	(
		TableName sysname,
		sourceCount int,
		targetCount int

	)


-- this is the table returned by collectrelatedtables
	CREATE TABLE #rellys
	(
		FK_Table  sysname,
		FK_Column sysname,
		PK_Table sysname,
		PK_Column sysname,
		Constraint_Name sysname,
		UPDATE_RULE nvarchar(30),
		DELETE_RULE nvarchar(30),
		CascadeUpdate int,
		CascadeDelete int
	)

/*
varAlwaysMove = Array("SchoolNotes", "SchoolLinks", "SchoolAlias")
varDeleteIfConflict = Array("SurveyControl")
varAlwaysDelete = Array("SurveyYearRank")
*/

BEGIN TRY


-- set up specific rules
	INSERT INTO #rules VALUES( 'SchoolSurvey','INSERTX', 'svyYear')		-- will move from the source to tagrte if there is not target record
														-- in the survey year. If there is a dup for a survey year, will error

	INSERT INTO #rules VALUES( 'SchoolNotes', 'MERGE', null)
	INSERT INTO #rules VALUES( 'SchoolLinks', 'MERGE', null)
	INSERT INTO #rules VALUES( 'SchoolAlias', 'MERGE', null)

	INSERT INTO #rules VALUES( 'SurveyControl', 'MOVE', null)

	INSERT INTO #rules VALUES( 'SurveyYearRank', 'DELETE', null)

	INSERT INTO #rules VALUES( 'EnrolmentProjection', 'MOVE', null)
	INSERT INTO #rules VALUES( 'SchoolYearHistory','INSERT', 'syYear')

	INSERT INTO #rules VALUES ( 'Establishment', 'MERGE', null)

	INSERT INTO #rules VALUES ( 'ListSchools', 'INSERT', 'lstName')			--SRSI0060


-- get all the tables that are related to schools

	INSERT INTO #rellys
	EXEC common.CollectRelatedTables 'Schools'		-- 3 6 2010 common


	BEGIN TRANSACTION

-- examine whether there are contents for each table

	If Not exists(Select schNo from Schools WHERE SchNo = @Source)
		exec common.RaiseValidationError
				'The source school %s does not exist', null, @source

	If Not exists(Select schNo from Schools WHERE SchNo = @MergeTo)
		exec common.RaiseValidationError
				'The target school %s does not exist', null, @MergeTo

	DECLARE @MatchYear int
	Select @MatchYear = S.svyYear FROM
				(Select svyYEar from SchoolSurvey WHERE schno= @Source) S
				INNER JOIN
				(Select svyYEar from SchoolSurvey WHERE schno= @MergeTo) T
				ON S.svyYEar = T.svyYear
	if @MatchYear is not null

		exec common.RaiseValidationError
				'Both schools have a survey in year %s. For any year in which both schools have a survey, you must delete one of these surveys', null, @MatchYear


	DECLARE relCursor CURSOR FOR
	Select FK_Table, CascadeUpdate, CascadeDelete, FK_Column from #rellys

	OPEN relCursor

	-- Perform the first fetch and store the values in variables.
	-- Note: The variables are in the same order as the columns
	-- in the SELECT statement.

	FETCH NEXT FROM relCursor
	INTO @TableName, @Update, @Delete, @FK_Column

	-- Check @@FETCH_STATUS to see if there are any more rows to fetch.
	WHILE @@FETCH_STATUS = 0
	BEGIN

	   -- Concatenate and display the current values in the variables.

	   -- This is executed as long as the previous fetch succeeds.
	   select @sql = 'Select ''%%%%'', [@@@@], count([@@@@]) from %%%% WHERE [@@@@] in (''$$$$'',''####'') GROUP BY [@@@@]'
	   select @sql = replace(@sql,'%%%%', @TableName)
	   select @sql = replace(@sql,'$$$$', @Source)
	   select @sql = replace(@sql,'####', @MergeTo)
	   select @sql = replace(@sql,'@@@@', @FK_Column)


	   INSERT INTO #results
	   EXEC sp_executesql @sql

		FETCH NEXT FROM relCursor
		INTO @TableName, @Update, @Delete, @FK_Column
	END

	close relCursor
	deallocate relCursor

	-- pt the results side by side for easier handling
	INSERT INTO #resultsA
	SELECT
		TableName
		, sum( case when schNo = @Source then num else null end)
		, sum( case when schNo = @MergeTo then num else null end)
	FROM #results
	GROUP BY TableName


	--if there is anything in this list, we can't proceed
	if exists (Select TableName
				from #resultsA
				WHERE sourceCount is not null and targetCount is not null
				AND TableName not in (Select TableName from #rules)
				) begin
			Select @sql = 'This school has data that cannot be merged:'
			Select @sql = @sql + '/n ' + TableName
			from #resultsA
				WHERE sourceCount is not null and targetCount is not null
				AND TableName not in (Select TableNAme from #rules)

			RAISERROR(@sql,16,1);
	end

	-- we'll have to treat SchoolEstablishmentRoles separately now that
	-- we have SchoolYearHistory combining SurveyControl and SchoolEstablishment
	-- we may have both records in a year . What if positions were assigned to both schools int he past?
	-- We can;t just delete these - safest to aggregate and comment

	Select @MergeTo schNo
		, syYear
		, estrRoleGrade
		, sum(case when schNo = @MergeTo then estrQuota else null end) estrQuota
		, sum(estrCountUser) estrCountUser
		, sum(case when schNo = @Source then 1 else 2 end) SourceSchool
	INTO #tmpSER
	FROM SchoolEstablishmentRoles  SER
	INNER JOIN SchoolYearHistory SYH
		ON SYH.syID = SER.estID
	WHERE schNo in (@Source, @MergeTo)
	GROUP BY syYear, estrRoleGRade

	-- remove the existing ones
	DELETE FROM SchoolEstablishmentRoles
	FROM SchoolEstablishmentRoles  SER
	INNER JOIN SchoolYearHistory SYH
		ON SYH.syID = SER.estID
	WHERE schNo in (@Source, @MergeTo)

	declare @MergeRule nvarchar(10)
	declare @SourceCount int
	declare @targetCount int

	-- now set up a cursor to allow us to process through these
	DECLARE tblCursor CURSOR FOR
	Select R.TableName, mergeRule , sourceCount, targetCount, FK_Column, rulekey
	from #resultsA R
		LEFT JOIN #rellys
			ON #rellys.FK_Table = R.TableName
	LEFT JOIN #rules
		ON R.TableName = #rules.TableName


	OPEN tblCursor

	-- Perform the first fetch and store the values in variables.
	-- Note: The variables are in the same order as the columns
	-- in the SELECT statement.

	FETCH NEXT FROM tblCursor
	INTO @TableName, @MergeRule, @sourceCount, @targetCount, @FK_Column, @key

	WHILE @@FETCH_STATUS = 0
	BEGIN


	   -- Concatenate and display the current values in the variables.

		If (@MergeRule = 'MERGE') begin
			-- move all the records from the source to the target
		   select @sql = 'UPDATE %%%% SET SchNo = ''####'' WHERE SchNo = ''$$$$'''
		end


		If (@MergeRule = 'MOVE') begin
			if @targetCount > 0
				-- there are already some so delete
				select @sql = 'DELETE FROM  %%%% WHERE SchNo = ''$$$$'''
			else
				-- move only if the target doesn't already have any
				select @sql = 'UPDATE %%%% SET SchNo = ''####'' WHERE SchNo = ''$$$$'''


		end

		If (@MergeRule = 'DELETE') begin

		   -- always delete the source records
		   select @sql = 'DELETE FROM  %%%% WHERE SchNo = ''$$$$'''
		end


		if (@MergeRule = 'INSERT') begin
			-- move all the records from the source to the target that do not have a corresponding record
			-- already - ie this is for 2 part keys
			select @sql = 'UPDATE %%%% SET [@@@@] = ''####'' WHERE [@@@@] = ''$$$$''
				AND [****] not in (Select [****] FROM %%%% WHERE [@@@@] = ''####'');
				DELETE FROM  %%%% WHERE [@@@@] = ''$$$$'''

		end

		if (@MergeRule = 'INSERTX') begin
			-- move all the records from the source to the target that do not have a corresponding record
			-- already - ie this is for 2 part keys BUT - ALLOW TO ERROR IF THERE IS A DUP
			select @sql = 'UPDATE %%%% SET [@@@@] = ''####'' WHERE [@@@@] = ''$$$$''
				AND [****] not in (Select [****] FROM %%%% WHERE [@@@@] = ''####'')'

		end
		If (@MergeRule is null) begin
			if (@targetCount is not null and @sourceCount is not null) begin
					-- there are already some so delete
					-- this is an error but should not happen
					Select @sql = 'There is already data in table ' + @TableName
					RAISERROR( @sql,16,1);
				end
			else

				select @sql = 'UPDATE %%%% SET [@@@@] = ''####'' WHERE [@@@@] = ''$$$$'''

		   -- This is executed as long as the previous fetch succeeds.
		end


	   select @sql = replace(@sql,'%%%%', @TableName)
	   select @sql = replace(@sql,'$$$$', @Source)
	   select @sql = replace(@sql,'####', @MergeTo)
	   select @sql = replace(@sql,'@@@@', @FK_Column)
	   select @sql = replace(@sql,'****', isnull(@key,''))


	   EXEC sp_executesql @sql


		FETCH NEXT FROM tblCursor
		INTO @TableName,  @MergeRule , @sourceCount, @targetCount, @FK_Column, @key


	END
	close tblCursor
	deallocate tblCursor


	-- fix up SchoolYearHistory for the target school, if it is missing any records

	DECLARE @MaxSurvey int
	DECLARE @MinSurvey int

	SELECT @MaxSurvey = max(svyYear)
		, @MinSurvey = min(svyYEar)
	FROM Survey

	INSERT INTO SchoolYearHistory
	(schNo
	, syYear
	, systCode
	, syAuth
	, syParent
	, syDormant
	)
	SELECT
		S.schNo
		, num
		, schType
		, schAuth
		, schParent
		, schDormant
	FROM Schools S
		INNER JOIN MetaNumbers
			On MetaNumbers.num >=schEst
			AND (MetaNumbers.num < schClosed or schClosed = 0)
		LEFT JOIN SchoolYearHistory SYH
			ON S.schNo = SYH.schNo
			and MetaNumbers.num = SYH.syYear
	WHERE s.schNo = @MergeTo
		AND (
				(num between @MinSurvey and @MaxSurvey)
			OR
				( num in (Select syYear from #tmpSER))
			)
		AND SYH.syID is null


	-- now reattach all the establishment plans
	INSERT INTO SchoolEstablishmentRoles
		(estID
		, estrRoleGrade
		, estrQuota
		, estrCountUser
		, estrNote
		)
	Select syID
		, estrRoleGrade
		, estrQuota
		, estrCountUser
		, case SourceSchool when 1 then 'merge ' when 3 then 'sum merge' end + @Source
	FROM schoolYearHistory SYH
	INNER JOIN #tmpSER
		ON  SYH.schNo = #tmpSER.schNo
		AND SYH.syYear = #tmpSER.syYear

	-- ad the alias to the new school

	INSERT INTO SchoolAlias
	( schNo
		,saAlias
		, saSrc
	)
	VALUES
	( @MergeTo
		, @Source
		,'MERGE'
	)

	-- alias the old name, if different from the target
	INSERT INTO SchoolAlias
	( schNo
		,saAlias
		, saSrc
	)
	Select
		@MergeTo
		, SRC.schName
		, 'MERGE'
	FROM Schools SRC
		CROSS JOIN Schools dest
	WHERE dest.schNo = @MergeTo
		AND src.schNo = @Source
		AND dest.schNAme <> src.schName

	-- write a school note recording the merge
	INSERT INTO SchoolNotes
	(schNo
		, ntDate
		, ntAuthor
		, ntSubject
		, ntNote
	)
	Select @MergeTo
		, common.Today()
		, original_login()
		, 'School Merged'
		, 'School ' + schNo + ' (' + schName + ') merged with ' + @MergeTo
	FROM Schools
	WHERE schno = @Source

	-- now we can delete the school
	DELETE FROM Schools
	WHERE schno = @Source


	COMMIT TRANSACTION
END TRY

/****************************************************************
Generic catch block
****************************************************************/
begin catch

    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end

    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err
end catch


DROP TABLE #results
DROP TABLE #rellys

END
GO

