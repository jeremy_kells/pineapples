SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [pEnrolmentWrite].[updateSubjects]
	-- Add the parameters for the stored procedure here
	@surveyID int,
	@SubjectCodeType nvarchar(10) = 'SUBJECTS',

	@grid xml
AS
BEGIN
	exec updateGrid @SurveyID , @SubjectCodeType, null,@grid


	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @codeType nvarchar(20)
	declare @page int
	declare @tchtbl table(	Row nvarchar(20),
							seq int,
							F int)

	declare @whtbl table(	Row nvarchar(20),
							seq int,
							F int)
	-- this is the set of row tags for the grid
	declare @rowSet table( row nvarchar(20)
							)

	declare @idoc int
	EXEC sp_xml_preparedocument @idoc OUTPUT, @grid

-- populate the table of grid values
	INSERT @tchtbl
	Select * from OPENXML (@idoc, '/Grid/TeacherReq/row/data',2)
		WITH (
				Row nvarchar(20) '../@Tag',
				seq int '../../@seq',
				F int '@value')

	INSERT @whtbl
	Select * from OPENXML (@idoc, '/Grid/WeeklyHours/row/data',2)
		WITH (
				Row nvarchar(20) '../@Tag',
				seq int '../../@seq',
				F int '@value')

-- get the complete set of rows
	INSERT @rowSet
	Select row from OPENXML(@idoc, '/Grid/rowSet/row',2)
		WITH (
				row nvarchar(20) '@Tag'
			)

begin transaction

begin try
	DELETE from PupilTables
	WHERE ssID = @SurveyID
	AND ptCode = 'TEACHERREQ' or ptCode = 'WEEKLYHOURS'


	-- insert teacherreq
	INSERT INTO PupilTables(ssID, ptCode,ptRow,  ptF)
	SELECT @surveyID, 'TEACHERREQ', G.Row, G.F
	FROM @tchtbl G
	-- insert teacherreq
	INSERT INTO PupilTables(ssID, ptCode,ptRow,  ptF)
	SELECT @surveyID, 'WEEKLYHOURS', G.Row, G.F
	FROM @whtbl G
end try

begin catch

    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end

    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err
end catch
-- and commit

if @@trancount > 0
	commit transaction


END
GO

