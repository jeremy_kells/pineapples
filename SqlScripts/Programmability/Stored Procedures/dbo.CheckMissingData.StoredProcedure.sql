SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Name
-- Create date: 5 11 2007
-- Description:	updates ssMissingData flag
-- =============================================
CREATE PROCEDURE [dbo].[CheckMissingData]
	-- Add the parameters for the stored procedure here
	@SurveyYear int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

-- first clear the ssMissingData flag everywhere
UPDATE SchoolSurvey
SET ssMissingData = 0
WHERE svyYear = @SurveyYear or @SurveyYear = 0

-- set the flag if enrolments are missing
UPDATE SchoolSurvey
SET ssMissingData = 1
WHERE ssID not in (Select ssID from Enrollments)


-- set the flag if repeaters, trin or trout are missing
UPDATE SchoolSurvey
SET ssMissingData = 1
WHERE ssID not in (Select ssID from PupilTables
					WHERE ptCode in ('REP','TRIN','TROUT' )
					)
-- set the flag if Classrrooms are missing
UPDATE SchoolSurvey
SET ssMissingData = 1
WHERE ssID not in (Select ssID from Rooms
					WHERE rmType = 'CLASS')

-- set the flag if communications equipment missing
UPDATE SchoolSurvey
SET ssMissingData = 1
WHERE ssID not in (Select ssID from Resources
					WHERE resName = 'Communications')


END
GO

