SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [pSchoolWrite].[updateResourceList]
	@surveyID int,
	@ResourceCategory nvarchar(50),
	@grid xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	declare @listtbl table(	ResSplit nvarchar(50),
							Available int,
							Adequate int,
							ResNumber int,
							Quantity int,
							ResLevel nvarchar(10),
							RoomID int,
							Condition smallint,
							Functioning smallint,
							Materials nvarchar(1),
							Location nvarchar(50),
							Note nvarchar(100)

							)

	declare @idoc int
	EXEC sp_xml_preparedocument @idoc OUTPUT, @grid

-- populate the table of grid values

	INSERT @listtbl
	Select * from OPENXML (@idoc, '/Resources/Resource/data',2)
		WITH (
				ResSplit nvarchar(50) '../@item',
				Available int '@available',
				Adequate int '@adequate',
				ResNumber int '@number',
				Quantity int '@qty',
				ResLevel nvarchar(10) '@classLevel',
				RoomID int '../@roomID',
				Condition smallint '@condition',
				Functioning smallint '@functioning',
				Materials nvarchar(1) '@material',
				Location nvarchar(50) '../../@location',
				Note nvarchar(100) '@note'
				)

-- this delete statement changed 4 6 2008
	if (@ResourceCategory is null)
		select @ResourceCategory = Category
		from OPENXML (@idoc, '/Resources',2)
		WITH (
				Category nvarchar(50) '@category'
				)
begin transaction

begin try

	DELETE from Resources
	WHERE ssID = @SurveyID
		AND resName = @ResourceCategory
		and resSplit in (Select resSplit from @listtbl)
		and (resLevel is null or  resLevel in (Select resLevel from @listtbl))
		and (resLocation is null or resLocation in (select Location from @listtbl))


	Insert into Resources
			(ssID,
			resName,
			resLevel,
			rmID,
			resSplit,
			resAvail,
			resAdequate,
			resNumber,
			resQty,
			resCondition,
			resFunctioning,
			resMaterial,
			resLocation,
			resNote)
	Select @SurveyID, @ResourceCategory,
		ResLevel, roomID, resSplit,
		Available, Adequate, ResNumber, Quantity,
		Condition, Functioning, Materials, Location, Note
	from @listtbl
	WHERE Available <> 0 or
			Adequate <> 0 or
			ResNumber <> 0 or
			(Condition is not null)

	-- 14 03 2010
	declare @BuildingType nvarchar(10)

	Select @BuildingType = mresCatBuildingType
	FROM metaResourceCategories
	WHERE mresCAtCode = @ResourceCategory

	If (@BuildingType is not null) begin
		-- We won;t do this if there is a later Survey mapped building inspection
		declare @SurveyYear int
		select @SurveyYear = svyYEar
		from schoolSurvey
		WHERE ssID = @SurveyID

		declare @LastInspectionYear int

		Select @LastInspectionYEar = max(SIS.svyYear)
		from SchoolInspection SI
			INNER JOIN SurveyInspectionSet SIS
				ON SI.inspSetID = SIS.inspsetID
			INNER JOIN SchoolSurvey SS
				ON SIS.svyYear = SS.svyYear
				AND SI.schNo = SS.schNo
			WHERE SS.ssID = @SurveyID
				AND SIS.intyCode = 'BLDG'
		print @LastInspectionYear

		If (@LastInspectionYear is null or @LastInspectionYear <= @SurveyYear )
			exec pSurveyWrite.applyTeacherHouseResourceToBuildings  @SurveyID, @ResourceCategory
	end


end try

begin catch

    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end

    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err
end catch
-- and commit

if @@trancount > 0
	commit transaction

return


END
GO

