SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 08/11/2015
-- Description:	Perf Assessment
-- =============================================
CREATE PROCEDURE [pPARead].[PerfAssessFilterIDs]
	-- Add the parameters for the stored procedure here
	@NumMatches int OUTPUT,
	@PageSize int = 0,
	@PageNo int = 0,
	@SortColumn nvarchar(30) = null,
	@SortDir int = 0,

	@PerfAssessID int = null,
	@TeacherID int = null,
	@ConductedBy nvarchar(50) = null,

	@SchoolNo nvarchar(50) = null,
	@District nvarchar(10) = null,
	@Island nvarchar(10) = null,

	@StartDate datetime = null,
	@EndDate datetime = null,

	@subScoreCode nvarchar(15) = null,
	@subScoreMin float = null,
	@subScoreMax float = null,


	@xmlFilter xml = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @keysAll TABLE
	(
	selectedID int
	, recNo int
	)

	DECLARE @s TABLE
	(
		paID int
	)

	declare @Framework nvarchar(10)
	declare @subScoreLevel nvarchar(1)
	declare @subScoreID int
	-- if processing for a subscore, determine the level
	if @subScoreCode is not null begin
		select @subScoreLevel = Level
		, @subScoreID = ID
		from paHierarchy
		WHERE FullId = @subScoreCode
	end

	print @subScoreLevel
	print @subScoreID


	if @subScoreLevel = 'I' begin

	-- need to translate the
		INSERT INTO @s
		Select paID
		from paAssessmentLine_ PAL
			INNER JOIN paIndicatorLevels_ IL
				ON PAL.paIndID = IL.paIndID
		WHERE PAL.paindID = @subScoreID
			AND ( @subScoreMin is null OR paplValue >= @subScoreMin)
			AND ( @subScoreMax is null OR paplValue <= @subScoreMax)


	end


	if @subScoreLevel = 'E' begin
		INSERT INTO @s
		Select paID
		from paElementAvg
		WHERE elmFullId = @subScoreCode
			AND ( @subScoreMin is null OR RawElmAvg >= @subScoreMin)
			AND ( @subScoreMax is null OR RawElmAvg <= @subScoreMax)
	end

	if @subScoreLevel = 'C' begin
		INSERT INTO @s
		Select paID
		from paCompetencyAvg
		WHERE ComFullId = @subScoreCode
			AND ( @subScoreMin is null OR RawComAvg >= @subScoreMin)
			AND ( @subScoreMax is null OR RawComAvg <= @subScoreMax)
	end

	if @subScoreLevel = 'F'
		select @Framework = @subScoreCode

	INSERT INTO @KeysAll
	SELECT PA.paID
	, row_number() OVER ( ORDER BY
						case @SortColumn
							when 'framework' then PA.pafrmCode
						end
						, paID
					) RecNo
	FROM PAAssessmentRpt PA

	WHERE
		(paID = @PerfAssessID or @PerfAssessID is null)
		AND (tID = @TeacherID or @TeacherID is null)
		AND (pafrmCode = @Framework or @Framework is null)
		AND (paConductedBy like @ConductedBy  + '%' or @ConductedBy is null)
		AND (
			(@StartDate is null and @EndDate is null)
			OR
			(paDate between isnull(@StartDate, '1901-01-01') and isnull(@EndDate, getdate()))
			)
		AND (@District is null or dID = @District)
		AND (@Island is null or iCode = @Island)
		AND (@schoolNo is null or paSchNo = @schoolNo)

		AND ( @subScoreLevel is null
				OR
				( @subScoreLevel = 'F' AND paScore between isnull(@subScoreMin,0) and isnull(@subScoreMax,9999))

				OR
				paId in (Select paID FROM @s)
			)


	SELECT @NumMatches = @@ROWCOUNT		-- this returns the total matches

	If @SortDir = 1 begin


		SELECT selectedID
		, RecNo
		FROM
		(
			Select selectedID
			, @NumMatches - RecNo + 1 RecNo
			FROM @KeysAll
		) S
		WHERE (@PageSize = 0
				or
					@PageSize >= @NumMatches
				or
						RecNo between @PageSize * (@PageNo - 1) + 1
						and @PageSize * (@PageNo)

				)
		ORDER BY RecNo

	end
	else begin


		SELECT selectedID
		, RecNo
		FROM @KeysAll
		WHERE (@PageSize = 0
				or
					@PageSize >= @NumMatches
				or
						RecNo between @PageSize * (@PageNo - 1) + 1
						and @PageSize * (@PageNo)

				)
	end END
GO

