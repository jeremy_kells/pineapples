SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 20 12 2007
-- Description:	NIS Room Required placeholder
-- =============================================
CREATE PROCEDURE [pInfrastructureRead].[NISRoomReq]
	-- Add the parameters for the stored procedure here
	@SurveyYear int = null,
	@SchNo nvarchar(50) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
-- we gein by collecting the 3 estimate queries
-- for anyrooms, enrolments and teachers
Select *
INTO #ebse
from dbo.tfnESTIMATE_BestSurveyEnrolments()

Select *
INTO #ebsr
from dbo.tfnESTIMATE_BestSurveyAnyRooms(@SurveyYear, @SchNo)

Select *
INTO #ebst
from dbo.tfnESTIMATE_BestSurveyTeachers(@SurveyYear, @SchNo)

Select subq2.*,
case when roomSize is null then null
	 when roomSize < EffectiveMinSize then 0
	when roomSize > isnull([MaxSize],99999999) then 0
	else 1
end 	AS SizeComplies, -- we got there!!!
case when roomSize < EffectiveMinSize then 'too small'
	when roomSize>[MaxSize] then 'too big'
	else null
end
 AS NonComplianceReason,
case when roomSize>[MaxSize] then 1 else 0 end  AS TooLarge,
case when roomSize<[EffectiveMinSize ]then 1 else 0 end AS TooSmall

from
(
Select subQ.*,
case
	when isnull(ProRataSize,0) < isnull(MinSize,0) then MinSize
	else ProRataSize
end EffectiveMinSize


from
(
Select
ER.LifeYear [Survey Year],
ER.SchNo,
ER.bestssID [roomDatassID],
ER.surveyDimensionssID,
EE.bestssID [enroldatassID],
ET.bestssID [teacherdatassID],
STD.stdName,
STD.stdItem,
STD.stdYear AS [Standard Year],
STD.stdValue AS MinSize,
std.stdValueMax AS [MaxSize],
std.stdperPupil AS PerPupil,
std.stdperTeacher AS PerTeacher,

R.rmType RoomType,
R.rmSize RoomSize,
R.rmNo AS RoomNo,


ER.bestYear AS [Room Data Year],
1 AS [Number Of Rooms],
--lkpConditionCodes.codeDescription AS RoomCondition,
case when R.rmSize is null then 0 else 1 end SizeSupplied,

EE.bestYear AS [Year of Enrolment Data],
SS.ssEnrol Enrol,
TT.NumTeachers,
[stdPerPupil]*[ssEnrol] AS MinByEnrol,
[stdPerTeacher]*[numTeachers] AS MinByTeacher,

isnull(stdPerPupil,0) * isnull(ssEnrol,0) + isnull(stdPerTeacher,0)*isnull(NumTeachers,0) ProRataCalc,
stdProRateCeiling AS ProRataMax,
case when stdProRateCeiling is null then
		-- use the raw calculated pro rate
		isnull(stdPerPupil,0) * isnull(ssEnrol,0) + isnull(stdPerTeacher,0)*isnull(NumTeachers,0)
	else
		case when stdProRateCeiling < isnull(stdPerPupil,0) * isnull(ssEnrol,0) + isnull(stdPerTeacher,0)*isnull(NumTeachers,0)
				then stdProRateCeiling
				-- the calculated pro-rate
				else isnull(stdPerPupil,0) * isnull(ssEnrol,0) + isnull(stdPerTeacher,0)*isnull(NumTeachers,0)
		end
end ProRataSize
FROM #ebsr ER
INNER JOIN #ebse EE
	ON ER.schNo = EE.schNo AND ER.LifeYear = EE.LifeYear
INNER JOIN #ebst ET
	ON ER.schNo = ET.schNo AND ER.LifeYear = ET.LifeYear
INNER JOIN schoolyearApplicableStandards STD
	ON ER.LifeYear = STD.LifeYear AND ER.schNo = STD.schNo
INNER JOIN Rooms R
	ON ER.bestssID = R.ssID AND STD.stdItem = R.rmType
INNER JOIN SchoolSurvey SS
	ON EE.bestssID = SS.ssID	-- to get teh aggregate enrolment
INNER JOIN (Select ssID, count(tchsID) numTeachers from TeacherSurvey group by ssID) TT
	ON ET.bestssID = TT.ssID
WHERE STD.stdName = 'ROOM_SIZE'
	-- don;t consider classrooms in this calculation
	AND STD.stdItem <> 'CLASS'
) subq
) subq2


--
    -- Insert statements for procedure here
	SELECT @SurveyYear, @SchNo
END
GO

