SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 11 10 2009
-- Description:	Return a list of all the organisation unit Roles defined in the current Pineapples system
-- =============================================
CREATE PROCEDURE [dbo].[getCurrentUserMemberships]
	-- Add the parameters for the stored procedure here
	@GroupOrRole nchar(1) = null
	,@PineapplesRoleType nvarchar(20) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
		Select u.Name
			, u.type
			from sys.database_principals u
			LEFT join ( select * from sys.extended_properties WHERE [name] = 'PineapplesUser') P
				on u.principal_id = P.Major_ID and P.class = 4
		cross apply common.ParsePineapplesRoleName(U.name) RN
		WHERE ( P.value = @PineapplesRoleType or @PineapplesRoleType is null)
			AND (@groupOrRole is null
					or (@groupOrRole = u.type)
				)
			AND is_member(u.name) = 1

    -- Insert statements for procedure here
END
GO
GRANT EXECUTE ON [dbo].[getCurrentUserMemberships] TO [public] AS [dbo]
GO

