SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 30 5 2010
-- Description:	Totals for supernumeraries in Establishment Plan
-- =============================================
CREATE PROCEDURE [pEstablishmentRead].[PlannedEstablishmentSupersYear]
	-- Add the parameters for the stored procedure here
	@EstYear int
	, @UseAuthorityDate bit = 0
	, @Authority nvarchar(10) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
/*********************************************
Temporary Table Definitions
***********************************************/


-- rates applicable to each paylevel at the date fo the establishment
-- copied from [pEstablishmentRead].[PlannedEstablishmentSummary]
create table #rates
(
	spCode nvarchar(10)
	, salLevel nvarchar(5)
	, spSalary money
	, spHAllow money
	, AnnualTotal money
	, spEffective datetime
	, pointOrder int
)


DECLARE @SUP TABLE
(
	EstYear int
	, Scope nvarchar(1)
	, authCode  nvarchar(10)
	, quota int NULL
	, FinalPositions int NULL
	, CurrentPositions int NULL
	, CurrentFilled int NULL
	, CurrentFilledCost money NULL
)
/**************************************************************************
constants
***************************************************************************/
declare @AsAtDate datetime

Select @AsAtDate =
	case  @UseAuthorityDate
		when 1 then estcAuthorityDate
		else estcSnapshotDate
	end
FROM EstablishmentControl
WHERE estcYEar = @EstYear

declare @PayslipDate datetime

Select @PayslipDate = min(tpsPeriodEnd)
FRom TeacherPayslips
WHERE tpsPeriodEnd >= @AsAtDate

declare @NominalSalaryPoint nvarchar(10)
declare @NominalRate money


/**************************************************************************
Populate temporary Tables
***************************************************************************/
-- get the salary rates that will be in effect for the establishment date

-- #rates

INSERT INTO #rates
SELECT
	spCode
	, salLevel
	, spSalary
	, spHAllow
	, isnull(spSalary,0) + isnull(spHAllow,0) AnnualTotal
	, spEffective
	-- this row number based on sort is so we can do an increment
	, row_number() OVER (ORDER BY spSort)
FROM
	(
	SELECT
		SalaryPointRates.*
		, spSort
		, salLevel
		-- row number to get the first pay effective date desc
		-- RowNum = 1 for the last Effective date before the Authority Date
		, row_number() over( PARTITION BY lkpSalaryPoints.spCode ORDER BY spEffective DESC)  RowNum
	FROM
		lkpSalaryPoints
		INNER JOIN SalaryPointRates
			ON LkpSalaryPoints.spCode = SalaryPointRates.spCode
		, EstablishmentControl
	WHERE
		EstablishmentControl.estcYear = @EstYear
		AND spEffective <= estcAuthorityDate
	) sub
	WHERE RowNum = 1


	Select @NominalSalaryPoint = estcAuthorityPositionSalaryPoint
	, @NominalRAte = AnnualTotal
	FROM EstablishmentControl
		LEFT JOIN #rates
			ON EstablishmentControl.estcAuthorityPositionSalaryPoint = #rates.spCode
	WHERE estcYear = @EstYear


-- supernumeraries


	INSERT INTO @SUP
	( EstYear
	 , Scope
	 , authCode
	 , quota
	 , FinalPositions
	 )
	Select estcYear
	, eppScope
	, eppAuth
	, eppQuota
	, eppCount
	FROM EstablishmentSuperPos ESP
		WHERE estcYear = @EstYear
		AND (eppAuth = @Authority OR @Authority is null)

	INSERT INTO @SUP
	( EstYear
	 , Scope
	 , authCode
	 , CurrentPositions
	 , CurrentFilled
	 )
	 Select @EstYear
	 , E.estpScope
	 , E.estpAuth
	 , count(E.estpNo)
	 , count(taID)
	 FROM Establishment E
		LEFT JOIN TeacherAppointment TA
			ON E.estpNo = TA.estpNo
			AND TA.taDate <= @AsAtDate
			AND (TA.taEndDate >= @AsAtDate or TA.taEndDate is null)
		LEFT JOIN TeacherIdentity TI
			ON TA.tID = TI.tID
		LEFT JOIN TeacherPayslips PSLIP
			ON TI.tPayroll = PSLIP.tpsPayroll
			AND tpsPeriodEnd = @PayslipDate
	 WHERE E.estpScope is not null
		AND (E.estpAuth = @Authority or @Authority is null)
		AND estpActiveDate<= @AsAtDate
		AND (estpClosedDate >= @AsAtDate or estpClosedDate is null)
	GROUP BY E.estpScope, E.estpAuth


	Select
	 Scope
	, S.authCode
	, authName
	, sum(CurrentPositions) CurrentPositions
	, sum(CurrentFilled) CurrentFilled
	, sum(Quota) Quota
	, sum(FinalPositions) FinalPositions
	, sum(FinalPositions) * @NominalRate FinalCost
	FROM @SUP S
		INNER JOIN Authorities
			ON S.authCode = Authorities.authCode
	GROUP BY Scope, S.AuthCode, Authorities.authName

	--Select
	--sum(CurrentPositions) CurrentPositions
	--, sum(CurrentFilled) CurrentFilled
	--, sum(Quota) Quota
	--, sum(FinalPositions) FinalPositions
	--, sum(FinalPositions) * @NominalRate FinalCost
	--FROM @SUP S

END
GO

