SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis and Ghislain Hachey
-- Create date: 27 04 2017
-- Description:	Filter school accreditation IDs
-- =============================================
CREATE PROCEDURE [pSurveyRead].[SchoolAccreditationFilterIDs]
	-- Add the parameters for the stored procedure here

	@NumMatches int OUTPUT,
	@PageSize int = 0,
	@PageNo int = 1,
	@SortColumn nvarchar(30) = null,
	@SortDir int = 0,

	@SAID int = null,
    @School nvarchar(50) = null, -- e.g. 'Geek High School'
	@SchoolNo nvarchar(50) = null, --'e.g. GHS100'
	@InspYear nvarchar(50) = null, --- e.g. '2016'

	-- adding district and authority in order to enforce security on these
	@District nvarchar(10) = null,
	@Authority nvarchar(10) = null,

	@xmlFilter xml = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

	SET NOCOUNT ON;

-- if filter params come from XML
if (@xmlFilter is not null) begin

	declare @idoc int
	EXEC sp_xml_preparedocument @idoc OUTPUT, @xmlFilter

	Select
	    @SAID = isnull(@SAID, saID),
		@School = isnull(@School, School),
		@SchoolNo = isnull(@SchoolNo, SchoolNo),
		@InspYear = isnull(@InspYear, InspYear),
		@District = isnull(@District, District),
		@Authority = isnull(@Authority, Authority)
	FROM OPENXML(@idoc,'//Filter',2)
	WITH
	(
	saID int '@SAID',
	School nvarchar(50) '@School',
	SchoolNo nvarchar(50) '@SchoolNo',
	InspYear nvarchar(50) '@InspYear',
	District nvarchar(10) '@District',
	Authority nvarchar(10) '@Authority'
	)
end

	DECLARE @keysAll TABLE
	(
	ID int,
	recNo int IDENTITY PRIMARY KEY
	)

INSERT INTO @KeysAll (ID)
SELECT saID
from pInspectionRead.SchoolAccreditations SA -- view
	LEFT JOIN Schools S
		ON SA.schNo = S.schNo
	LEFT JOIN lkpIslands I
		ON S.iCode = I.iCode
WHERE
(saID = @SAID OR @SAID IS NULL)
AND (InspYear = @InspYear OR @InspYear IS NULL)
AND (SA.schNo = @SchoolNo OR @SchoolNo IS NULL)
AND (S.schName like '%' +  @School + '%' OR @School IS NULL)
AND (S.schAuth = @Authority OR @authority is null)
AND (I.iGroup = @District OR @District is null)

OPTION(RECOMPILE)


SELECT @NumMatches = @@ROWCOUNT

-- now return the page, sorted in the right sequence and direction

If @SortDir = 1 begin


		SELECT ID
		, RecNo
		FROM
		(
			Select ID
			, @NumMatches - RecNo + 1 RecNo
			FROM @KeysAll
		) S
		WHERE (@PageSize = 0
				or
					@PageSize >= @NumMatches
				or
						RecNo between @PageSize * (@PageNo - 1) + 1
						and @PageSize * (@PageNo)

				)
		ORDER BY RecNo

	end
	else begin


		SELECT ID
		, RecNo
		FROM @KeysAll
		WHERE (@PageSize = 0
				or
					@PageSize >= @NumMatches
				or
						RecNo between @PageSize * (@PageNo - 1) + 1
						and @PageSize * (@PageNo)

				)
	end

END
GO

