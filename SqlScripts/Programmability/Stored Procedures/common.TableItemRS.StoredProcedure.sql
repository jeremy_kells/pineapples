SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 1 11 2011
-- Description:	get a Table Item RS
-- =============================================
CREATE PROCEDURE [common].[TableItemRS]
(
	@TableItemCode nvarchar(20),
	@SchoolNo nvarchar(20) = null,
	@paramYear int = null,
	@SchoolType nvarchar(10) = null
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @sqlSrc nvarchar(1000)

if (@SchoolType is null) begin
	Select @SchoolType = ssSchType from SchoolSurvey WHERE schNo = @SchoolNo AND svyYear = @paramYear
	if @SchoolType is null
		Select @SchoolType = schType from Schools WHERE schNo = @SchoolNo
end

Select @sqlSrc = isnull(tdiSrcSQL,tdiSrc)
from metaSchoolTableITems
WHERE tdiCode = @TableITemCode

print @sqlSrc

	select @sqlSrc = replace(@sqlSrc,'[paramSchoolNo]',quotename(isnull(@SchoolNo,''),''''))

	select @sqlSrc = replace(@sqlSrc,'[paramYear]',@paramYear)
	select @sqlSrc = replace(@sqlSrc,'[paramSchoolType]',quotename(@SchoolType,''''))
print @sqlSRc
	declare  @codeTmp table
	(
		codeCode nvarchar(20),
		codeDescription nvarchar(1000)
	)
	insert into @codeTmp
	exec (@sqlSrc)


SELECT *
from @Codetmp
END
GO

