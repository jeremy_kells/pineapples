SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 05 05 2010
-- Description:	Repair data redundancies
-- =============================================
CREATE PROCEDURE [common].[DataIntegrity]
	-- Add the parameters for the stored procedure here
WITH EXECUTE AS 'pineapples'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


 declare @output table
 (
	msg nvarchar(400)
 )


	 -- we know the enSum is a claulated field now - so it's safe to do this


 declare @Count int

	Select @count = count(*)
	FROM SchoolSurvey SS
	LEFT JOIN
	(
		Select
		ssID
		,sum(enM) sumM
		, sum(enF) sumF
		, sum(enSum) sumAll
		from Enrollments
		GROUP BY ssID
	) E
	ON SS.ssID = E.ssID
	WHERE ssEnrolM <> sumM
	OR ssEnrolF <> sumF
	OR ssEnrol <> sumAll
	OR (ssEnrol is null and sumAll is not null)

	INSERT INTO @output
	VALUES(cast(@Count as nvarchar(5)) + ' errors found in SchoolSurvey cached enrolment totals')


	UPDATE SchoolSurvey
	set ssEnrolM = sumM
	, ssEnrolF = sumF
	, ssEnrol = SumAll
	FROM SchoolSurvey SS
	LEFT JOIN
	(
		Select
		ssID
		,sum(enM) sumM
		, sum(enF) sumF
		, sum(enSum) sumAll
		from Enrollments
		GROUP BY ssID
	) E
	ON SS.ssID = E.ssID

		Select @count = count(*)
	FROM SchoolSurvey SS
	LEFT JOIN
	(
		Select
		ssID
		,sum(enM) sumM
		, sum(enF) sumF
		, sum(enSum) sumAll
		from Enrollments
		GROUP BY ssID
	) E
	ON SS.ssID = E.ssID
	WHERE ssEnrolM <> sumM
	OR ssEnrolF <> sumF
	OR ssEnrol <> sumAll
	OR (ssEnrol is null and sumAll is not null)

	INSERT INTO @output
	VALUES(cast(@Count as nvarchar(5)) + ' errors found in SchoolSurvey cached enrolment totals after repair')

Select * from @output
END
GO

