SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date:
-- Description:
-- =============================================
CREATE PROCEDURE [dbo].[SetDataVersion]
	@TableName nvarchar(50) ,
	@TableGroup int = 0
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @sql nvarchar(200)
	declare @params nvarchar(200)
	UPDATE TableDataVersion
		SET TableName = @TableName,
			TableGroup = @TableGroup,
			LastModifiedUser = system_user,
			LastModifiedDate = getdate()
		WHERE TableName = @TableName
	INSERT INTO TableDataVersion(TableName, TableGroup,
				LastModifiedUser, LastModifiedDate)
		Select @TableName , @TableGroup, system_user, getdate()
		WHERE @TableName NOT IN (Select TableName from TableDataVersion)

    -- Insert statements for procedure here

END
GO

