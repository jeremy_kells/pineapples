SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2/11/2015
-- Description:	Read a performance assessment by ID
-- =============================================
CREATE PROCEDURE [pPARead].[PerfAssessRead]
@paID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

Select
PA.paID Id
, PA.pafrmCode Framework
, PA.tID TeacherId
, PA.tFullName TeacherName
, PA.paSchNo SchoolNo
, PA.schName SchoolName
, PA.paDate [Date]
, PA.paConductedBy ConductedBy
, PA.pCreateUser
, PA.pCreateDateTime
, PA.pEditUser
, PA.pEditDateTime
, PA.pRowversion

from paAssessment PA
WHERE PA.paID = @paID

-- outer join all the indicators for this assessment type

Select indFullID IndicatorCode
, I.paindID IndicatorId
, L.paplCode PerformanceLevel
, L.palComment Comment
from paIndicators I
INNER JOIN paAssessment_ A
	ON I.pafrmCode = A.pafrmCode
LEFT JOIN paAssessmentLine_ L
	ON I.paindID = L.paindID
	AND L.paID = @paID
WHERE A.paID = @paID
ORDER BY indFullID

-- get all the possible levels for each indicator

Select L.paindID IndicatorId
, L.paplCode [Level]
, L.paplCriterion Criterion
, L.paplDescription [Description]
, L.paplValue Value

from paIndicators I
INNER JOIN paIndicatorLevels_ L
	ON I.paindID = L.paIndID
INNER JOIN paAssessment_ A
	ON I.pafrmCode = A.pafrmCode
WHERE A.paID = @paID
ORDER BY L.paIndID, paplSort, paplValue


END
GO

