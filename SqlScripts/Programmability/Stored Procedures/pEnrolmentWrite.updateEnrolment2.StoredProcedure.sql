SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 12 Jan 2016
-- Description:	Update of enrlment grid from Web site
-- =============================================
CREATE PROCEDURE [pEnrolmentWrite].[updateEnrolment2]
	-- this version is used in the web site, and takes slightly different
	-- Add the parameters for the stored procedure here
	@SchoolNo nvarchar(50),
	@SurveyYear int,
	@grid xml,
	@user nvarchar(100) = null
	WITH EXECUTE AS 'pineapples'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @surveyID int

	-- get the survey ID
	Select @SurveyId = ssId
	from SchoolSurvey SS
	WHERE schNo = @SchoolNo
	AND svyYear = @SurveyYear

	declare @grdtbl table(	Age int,
							EnLevel nvarchar(20) collate database_default,
							M int NULL,
							F int NULL)
	-- this is the set of column tags for the grid
	declare @colSet table( colLevel nvarchar(20) collate database_default
							)
	-- this is the set of row tags for the grid
	declare @rowSet table( rowAge nvarchar(20)  collate database_default
							)

	declare @idoc int
	EXEC sp_xml_preparedocument @idoc OUTPUT, @grid

-- populate the table of grid values
	INSERT @grdtbl
	Select * from OPENXML (@idoc, '/PupilTable/Data/RowColData',2)
		WITH (
				Age int 'Row',
				EnLevel nvarchar(20) 'Col',
				M int 'M/text()',
				F int 'F/text()')
-- get the complete set of columns
	INSERT @colSet
	Select colLevel from OPENXML(@idoc, '/PupilTable/Cols/LookupTable',2)
		WITH (
				colLevel nvarchar(20) 'codeCode'
			)

-- get the complete set of rows
	INSERT @rowSet
	Select rowAge from OPENXML(@idoc, '/PupilTable/Rows/LookupTable',2)
		WITH (
				rowAge int 'codeCode'
			)
-- 13 12 2007 this supports pineapples @school, which will send grids without colSet
-- or rowSet, assuming that all data will be supplied in one grid
declare @rowRange int;
select @rowRange = count(*) from @rowSet

declare @colRange int;
select @colRange = count(*) from @colSet

-- delete
-- if column and rowset specified, restrict the delete to that range
-- otherwise delete all

-- transaction control

select @user = isnull(@user, original_login())

begin transaction

begin try

	DELETE from Enrollments
	from Enrollments
	WHERE ssID = @SurveyID
		AND (
			enAge = any (Select rowAge from @rowSet)
			or @rowRange = 0
			)
		AND (
			enLevel = any (Select colLevel from @colSet)
			or @colRange = 0
			)

	-- insert from the grid

	INSERT INTO Enrollments(ssID, enAge, enLevel, enOrigin, enM, enF)
	SELECT @surveyID, Age, EnLevel,
		null,
	-- values
	G.M, G.F

	FROM @grdtbl G

	declare @auditID int

	exec @auditID = audit.logAudit
			'SchoolSurvey'
			, 'Enrolments (web)'
			, 'U'
			, @@rowcount
			, @user

	INSERT INTO Audit.auditRow
	(
			auditID
			, arowKey
	)
	VALUES ( @auditID, @SurveyID)

end try

begin catch

    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end

    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err
end catch
-- and commit

if @@trancount > 0
	commit transaction

exec LogActivity 'updateEnrolment', @SurveyID
return


END
GO

