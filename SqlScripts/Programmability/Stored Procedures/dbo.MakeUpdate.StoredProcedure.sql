SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[MakeUpdate]
@Table_Name nvarchar(100)
, @xml xml
, @testMode int = 0
AS
BEGIN
SET NOCOUNT ON;

declare @nn nvarchar(max)
Select @nn = replace(convert(nvarchar(max), @xml),'''','''''')
----Select @nn = replace(@nn,'&lt;','<')
----Select @nn = replace(@nn,'&gt;','>')

declare @pos int

declare @n1 nvarchar(max)
declare @n2 nvarchar(max)
declare @n3 nvarchar(max)


if len(@nn) > 4000 begin

	if len(@nn) > 8000 begin

		SELECT  @pos=CHARINDEX('<r>',@nn,3000)
		Select @n1 = SUBSTRING(@nn,1,@Pos - 1)


		Select @nn = right(@nn,len(@nn) - @pos)
		SELECT  @pos=CHARINDEX('<r>',@nn,3000)
		Select @n2 = SUBSTRING(@nn,1,@Pos - 1)
		select @n3 = SUBSTRING(@nn,@pos,4000)


	end

	else begin
		SELECT  @pos=CHARINDEX('<r>',@nn,3000)


		Select @n1 = SUBSTRING(@nn,1,@Pos - 1)


		Select @n2 = SUBSTRING(@nn,@pos,4000)
		Select @n3 = ''
	end

end
else begin
	Select @n1 = @nn
	Select @n2 = ''
	Select @n3 = ''
end

if @TestMode = 1
	print 'declare @xmlT xml'
print '--------------------------------------------------------------'
print '-- ' + @Table_Name
print '--------------------------------------------------------------'
print 'print ' + '''' + @Table_Name + ''''
print 'SELECT @xmlT = convert(xml,' +
	''''
print @n1
print @n2
print @n3
print ''')'


if @testMode = 1
	print 'Select @xmlT'


--print 'declare @xml xml'

print 'Select @xml = (Select @xmlT FOR XML PATH(''t''))'


Declare @sql nvarchar(4000)= ''
declare @sql2 nvarchar(4000) = ''
declare @sql3 nvarchar(4000) = ''
declare @sql4 nvarchar(4000) = ''


Select @Sql = @sql + char(13) + char(10) +
	case when ordinal_position > 1 then ', ' else '' end +
	'T.c.value(' +
	quotename(column_Name + '[1]','''') + ', '
	+ quotename(Data_Type + isnull(quotename(Character_Maximum_length, '('), ''),'''')
	+ ') ' + column_Name

from INFORMATION_Schema.Columns
WHERE Table_NAME = @Table_Name

--print @sql

Select @sql2 = @sql2 + char(13) + char(10) +
	case when ordinal_position > 1 then ', ' else 'SET ' end +
	Column_Name + ' = SUB.' + Column_Name
from INFORMATION_Schema.Columns
WHERE Table_NAME = @Table_Name

Select @sql3 = @sql3 + char(13) + char(10) +
	case when ordinal_position > 1 then ', ' else '' end + Column_Name
	, @sql4 = @sql4 + char(13) + char(10) +
	case when ordinal_position > 1 then ', SUB.' else '  SUB.' end + Column_Name
from INFORMATION_Schema.Columns
WHERE Table_NAME = @Table_Name


--print @sql2
declare @pk nvarchar(50)
Select @pk = Column_Name
from INFORMATION_Schema.Columns
WHERE Table_NAME = @Table_Name
AND ordinal_position = 1

print 'UPDATE ' + @table_name  +
	@sql2 + char(13) + char(10) +
	'FROM ' + @Table_Name + char(13) + char(10) +
	'INNER JOIN ' + char(13) + char(10) +
	'( Select * FROM ( SELECT ' + char(13) + char(10) +
	@sql + char(13) + char(10) +
	'FROM @xml.nodes(''//r'') T(c) ) x ) SUB' + char(13) + char(10) +
	'    ON SUB.' + @pk + ' = ' + @Table_Name + '.' + @pk


print 'INSERT INTO ' + @Table_Name + '(' +
	@sql3 + ' )' + char(13) + char(10) +
	'SELECT ' +
	@sql4 + char(13) + char(10) +
	'FROM ' + + char(13) + char(10) +
	'( Select * FROM ( SELECT ' + char(13) + char(10) +
	@sql + char(13) + char(10) +
	'FROM @xml.nodes(''//r'') T(c) ) x ) SUB' + char(13) + char(10) +

	'LEFT JOIN ' + @Table_Name + char(13) + char(10) +
	'    ON SUB.' + @pk + ' = ' + @Table_Name + '.' + @pk + char(13) + char(10) +
	'WHERE ' + @Table_Name + '.' + @pk + ' IS NULL'

print 'print ' + '''' + @Table_Name + ' record count:'''
print 'declare @' + @Table_Name + 'count int'
print 'Select @' + @Table_Name + 'count = Count(*) from ' + @Table_NAme
print 'print @' + @Table_Name + 'count'

END
GO

