SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 16 12 2017
-- Description:	Refresh the warehouse school counts
-- =============================================
CREATE PROCEDURE [warehouse].[buildSchoolCounts]
	-- Add the parameters for the stored procedure here
	@StartFromYear int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET ANSI_WARNINGS OFF;
---- school level flow model

		begin transaction
		IF OBJECT_ID('warehouse.measureSchoolCount', 'U') IS NULL begin
			Select count(DISTINCT E.SchNo) NumSchools
				, SurveyYear
				, AuthorityCode
				, [District Code] DistrictCode
				, SchoolTypeCode
				, Region
				INTO warehouse.measureSchoolCount
				FROM
				(
				Select DISTINCT schNo
					, SurveyYear
					, SurveyDimensionID
				from measureEnrolSchool
				) E
				INNER JOIN warehouse.DimensionSchoolSurvey DSS
				ON E.SurveyDimensionID = DSS.[Survey ID]
				GROUP BY
					SurveyYear
					, AuthorityCode
					, [District Code]
					, SchoolTypeCode
					, Region
				print 'warehouse.measureSchoolCount created - rows:' + convert(nvarchar(10), @@rowcount)
		end else begin
			DELETE
			FROM warehouse.measureSchoolCount
			WHERE (SurveyYear >= @StartFromYear or @StartFromYear is null)
			print 'warehouse.measureSchoolCount deletes - rows:' + convert(nvarchar(10), @@rowcount)

			INSERT INTO warehouse.measureSchoolCount
			Select count(DISTINCT E.SchNo) NumSchools
				, SurveyYear
				, AuthorityCode
				, [District Code] DistrictCode
				, SchoolTypeCode
				, Region

				FROM
				(
				Select DISTINCT schNo
					, SurveyYear
					, SurveyDimensionID
				from measureEnrolSchool
				) E
				INNER JOIN warehouse.DimensionSchoolSurvey DSS
				ON E.SurveyDimensionID = DSS.[Survey ID]
				WHERE (SurveyYear >= @StartFromYear or @StartFromYEar is null)
				GROUP BY
					SurveyYear
					, AuthorityCode
					, [District Code]
					, SchoolTypeCode
					, Region
				print 'warehouse.measureSchoolCount inserts - rows:' + convert(nvarchar(10), @@rowcount)
		end
		commit transaction

END
GO

