SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 21 10 2009
-- Description:	Generate an enrolment projection from the parameters on the EstablishmentControl
-- =============================================
CREATE PROCEDURE [pEstablishmentWrite].[unusedGenerateEnrolmentProjectionForYear]
	-- Add the parameters for the stored procedure here
	@EstYear int
	, @SchoolNo nvarchar(50) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- get the parameters from the EstablishmentControl

	declare @BaseYear int
	declare @ScenarioID int
	declare @SizeFactor float

	begin try

		Select @BaseYear = estcBaseYear
			, @ScenarioID = escnID
			, @SizeFactor = estcSizeFactor
		FROM
			EstablishmentControl
		WHERE
			estcYear = @EstYear


		exec [pEstablishmentWrite].[generateProjection]
				@BaseYear
				,@EstYear
				,@ScenarioID
				,@SizeFactor
				,@SchoolNo

	end try

	begin catch
	    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;

		Select @err = @@error,
			 @ErrorMessage = ERROR_MESSAGE(),
			 @ErrorSeverity = ERROR_SEVERITY(),
			 @ErrorState = ERROR_STATE()


		if @@trancount > 0
			begin
				rollback transaction
				select @errorMessage = @errorMessage + ' The transaction was rolled back.'
			end

		RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
		return @err
	end catch
END
GO

