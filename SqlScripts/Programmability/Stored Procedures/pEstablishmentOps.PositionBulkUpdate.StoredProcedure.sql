SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 11 04 2010
-- Description:	Bulk Set the flag or end date for positions
-- =============================================
CREATE PROCEDURE [pEstablishmentOps].[PositionBulkUpdate]
 	-- Add the parameters for the stored procedure here
 	  @NewFlag nvarchar(10)
	, @asAtDate datetime = null
	, @SchoolNo nvarchar(50) = null
	, @Authority nvarchar(10) = null
	, @SchoolType nvarchar(10) = null
	, @RoleGrade nvarchar(10) = null
	, @Role nvarchar(10) = null
	, @Sector nvarchar(10) = null
	, @Unfilled bit = 0
	, @PayslipExceptions bit = 0
	, @SurveyExceptions bit = 0
	, @SalaryPointExceptions bit = 0
	, @PositionFlag nvarchar(10) = null
	, @PositionFlagOp bit = 1

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

	SET NOCOUNT ON;

    -- Insert statements for procedure here

BEGIN TRY

	BEGIN TRANSACTION

	UPDATE Establishment
	SET estpFlag = @newFlag
	WHERE estpNo = any
	(SElect estpNo
	from pEstablishmentRead.readManpower(
		  @asAtDate
		, @SchoolNo
		, @Authority
		, @SchoolType
		, @RoleGrade
		, @Role
		, @Sector
		, @Unfilled
		, @PayslipExceptions
		, @SurveyExceptions
		, @SalaryPointExceptions
		, @PositionFlag
		, @PositionFlagOp
	))
	Select @@ROWCOUNT
	COMMIT TRANSACTION


END TRY
begin catch
    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end

    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err
end catch


END
GO

