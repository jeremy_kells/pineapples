SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 5 10 2017
-- Description:	Validate table of teacher data from ndoe (fsm) excel workbook
-- returns a table of validation errors
-- if this table is present in the response to the upload, the data is not processed
-- =============================================
CREATE PROCEDURE [dbo].[loadNdoeValidateStaff]
@xml xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	-- totals by education level

declare @ndoeStaff TABLE (
	rowIndex int
    , SchoolYear                 nvarchar(100) NULL
    , State                      nvarchar(100) NULL
    , School_Name                nvarchar(100) NULL
    , School_No                  nvarchar(100) NULL
    , School_Type                nvarchar(100) NULL
    , Office                     nvarchar(100) NULL
    , First_Name                 nvarchar(100) NULL
    , Middle_Name                nvarchar(100) NULL
    , Last_Name                  nvarchar(100) NULL
    , Full_Name                  nvarchar(100) NULL
    , Gender                     nvarchar(100) NULL
    , inDate_of_Birth            nvarchar(100) NULL
	, Age                        nvarchar(100) NULL
    , Citizenship                nvarchar(100) NULL		-- => tchCitizenship
    , Ethnicity                  nvarchar(100) NULL		--
    , FSM_SSN                    nvarchar(100) NULL		-- => tPayroll
    , OTHER_SSN                  nvarchar(100) NULL		-- => tchProvident ?
    , Highest_Qualification      nvarchar(100) NULL
    , Field_of_Study                           nvarchar(100) NULL
    , Year_of_Completion                       nvarchar(100) NULL
    , Highest_Ed_Qualification                 nvarchar(100) NULL
    , Year_Of_Completion2                      nvarchar(100) NULL
    , Employment_Status                        nvarchar(100) NULL
    , Reason                     nvarchar(100) NULL
    , Job_Title                  nvarchar(100) NULL
    , Organization               nvarchar(100) NULL					-- to tchSponsor, used for organisation that employs/pays the teacher
    , Staff_Type                 nvarchar(100) NULL
    , Teacher_Type               nvarchar(100) NULL			--- send to tchStatus, which has tradionally been used for e.g. 'Probationary' TRainee, etc
    , inDate_of_Hire             nvarchar(100) NULL
    , inDate_Of_Exit             nvarchar(100) NULL
    , Annual_Salary              nvarchar(100) NULL
    , Funding_Source             nvarchar(100) NULL
    , ECE                        nvarchar(1) NULL
    , Grade_1                    nvarchar(1) NULL
    , Grade_2                    nvarchar(1) NULL
    , Grade_3                    nvarchar(1) NULL
    , Grade_4                    nvarchar(1) NULL
    , Grade_5                    nvarchar(1) NULL
    , Grade_6                    nvarchar(1) NULL
    , Grade_7                    nvarchar(1) NULL
    , Grade_8                    nvarchar(1) NULL
    , Grade_9                    nvarchar(1) NULL
    , Grade_10                   nvarchar(1) NULL
    , Grade_11                   nvarchar(1) NULL
    , Grade_12                   nvarchar(1) NULL
    , Admin                      nvarchar(1) NULL
    , Other                      nvarchar(1) NULL
    , Total_Days_Absence         int NULL
    , Maths                      int NULL
    , Science                    int NULL
    , Language                   int NULL
    , Competency                 int NULL
    , fileReference				 uniqueidentifier
	, rowXml					 xml					-- this stores the entire raw xml of the row for posterity
	--
	, ssID						 int
	, tID						 int
	--
    , Date_of_Birth              date NULL
    , Date_of_Hire               date NULL
    , Date_Of_Exit               date NULL
	, minYearTaught			     int
	, maxYearTaught				 int
	, classMin					 nvarchar(10)
	, classMax					 nvarchar(10)
	, TAM						 nvarchar(1)		-- teacher / admin / mixed => tchTAM
	, teacherRole				 nvarchar(50)		-- from job title, via lkpTeacherRole => tchRole
	, sector					 nvarchar(10)		-- calculated from the school type, and the levels taught => tchSector
)

declare @validations TABLE
(
	field nvarchar(50),
	rowIndex int,
	errorValue nvarchar(100),
	message nvarchar(200),
	severity nvarchar(20)
)
-- use try / catch
begin try

declare @counter int
declare @debug int = 1

declare @SurveyYear int
declare @districtName nvarchar(50)

declare @districtID nvarchar(10)

-- derive the integer value of the survey year survey year as passed in looks like SY2017-2018
-- note the convention is that the year recorded is the FINAL year of the range ie 2018 in the above
declare @YearStartPos int = 8

-- startpos = 8
Select @DistrictName = v.value('@state', 'nvarchar(50)')
, @SurveyYear = cast(substring(v.value('@schoolYear','nvarchar(50)'),@YearStartPos,4) as int)
From @xml.nodes('ListObject') as V(v)

/*-----------------------------------------------------------
VALIDATIONS

validation errors will be reported back to the client, and no processing will take place
*/-----------------------------------------------------------

--- first off, check that there is a survey control record for year @SurveyYear
Select @SurveyYear = svyYear
FROM Survey
WHERE svyYEar = @SurveyYear

if (@@RowCount = 0) begin
	print 'No survey record created for survey year ' + cast(@SurveyYear as nvarchar(10))
	return
end


Select @districtID = dID
from Districts WHERE dName = @districtName
if (@@RowCount = 0) begin
	print 'District name not available ' + cast(@SurveyYear as nvarchar(10))
	return
end

print @SurveyYear
print @DistrictID

INSERT INTO @ndoeStaff
(
rowIndex
, SchoolYear
, State
, School_Name
, School_No
, School_Type
, Office
, First_Name
, Middle_Name
, Last_Name
, Full_Name
, Gender
, inDate_of_Birth
, Age
, Citizenship
, Ethnicity
, FSM_SSN
, OTHER_SSN
, Highest_Qualification
, Field_of_Study
, Year_of_Completion
, Highest_Ed_Qualification
, Year_Of_Completion2
, Employment_Status
, Reason
, Job_Title
, Organization
, Staff_Type
, Teacher_Type
, inDate_of_Hire
, inDate_Of_Exit
, Annual_Salary
, Funding_Source
, ECE
, Grade_1
, Grade_2
, Grade_3
, Grade_4
, Grade_5
, Grade_6
, Grade_7
, Grade_8
, Grade_9
, Grade_10
, Grade_11
, Grade_12
, Admin
, Other
, Total_Days_Absence
, Maths
, Science
, Language
, Competency
, filereference
, rowXml
)
SELECT
nullif(ltrim(v.value('@Index', 'int')),'')                           [Index]

, nullif(ltrim(v.value('@SchoolYear', 'nvarchar(100)')),'')                           [SchoolYear]
, nullif(ltrim(v.value('@State', 'nvarchar(100)')),'')                              [State]
, nullif(ltrim(v.value('@School_Name', 'nvarchar(100)')),'')                        [School_Name]
, nullif(ltrim(v.value('@School_No', 'nvarchar(100)')),'')                          [School_No]
, nullif(ltrim(v.value('@School_Type', 'nvarchar(100)')),'')                        [School_Type]
, nullif(ltrim(v.value('@Office', 'nvarchar(100)')),'')                             [Office]
, nullif(ltrim(v.value('@First_Name', 'nvarchar(100)')),'')                         [First_Name]
, nullif(ltrim(v.value('@Middle_Name', 'nvarchar(100)')),'')                        [Middle_Name]
, nullif(ltrim(v.value('@Last_Name', 'nvarchar(100)')),'')                          [Last_Name]
, nullif(ltrim(v.value('@Full_Name', 'nvarchar(100)')),'')                          [Full_Name]
, nullif(ltrim(v.value('@Gender', 'nvarchar(100)')),'')                             [Gender]
, nullif(ltrim(v.value('@Date_of_Birth', 'nvarchar(100)')),'')                      [Date_of_Birth]
, nullif(ltrim(v.value('@Age', 'nvarchar(100)')),'')                                [Age]
, nullif(ltrim(v.value('@Citizenship', 'nvarchar(100)')),'')                        [Citizenship]
, nullif(ltrim(v.value('@Ethnicity', 'nvarchar(100)')),'')                          [Ethnicity]
, nullif(ltrim(v.value('@FSM_SSN', 'nvarchar(100)')),'')                            [FSM_SSN]
, nullif(ltrim(v.value('@OTHER_SSN', 'nvarchar(100)')),'')                          [OTHER_SSN]
, nullif(ltrim(v.value('@Highest_Qualification', 'nvarchar(100)')),'')              [Highest_Qualification]
, nullif(ltrim(v.value('@Field_of_Study', 'nvarchar(100)')),'')                     [Field_of_Study]
, nullif(ltrim(v.value('@Year_of_Completion', 'nvarchar(100)')),'')                 [Year_of_Completion]
, nullif(ltrim(v.value('@Highest_Ed_Qualification', 'nvarchar(100)')),'')           [Highest_Ed_Qualification]
, nullif(ltrim(v.value('@Year_Of_Completion2', 'nvarchar(100)')),'')                [Year_Of_Completion2]
, nullif(ltrim(v.value('@Employment_Status', 'nvarchar(100)')),'')                  [Employment_Status]
, nullif(ltrim(v.value('@Reason', 'nvarchar(100)')),'')                             [Reason]
, nullif(ltrim(v.value('@Job_Title', 'nvarchar(100)')),'')                          [Job_Title]
, nullif(ltrim(v.value('@Organization', 'nvarchar(100)')),'')                       [Organization]
, nullif(ltrim(v.value('@Staff_Type', 'nvarchar(100)')),'')                         [Staff_Type]
, nullif(ltrim(v.value('@Teacher_Type', 'nvarchar(100)')),'')                       [Teacher_Type]
, nullif(ltrim(v.value('@Date_of_Hire', 'nvarchar(100)')),'')                       [Date_of_Hire]
, nullif(ltrim(v.value('@Date_of_Exit', 'nvarchar(100)')),'')                       [Date_Of_Exit]
, nullif(ltrim(v.value('@Annual_Salary', 'nvarchar(100)')),'')                      [Annual_Salary]
, nullif(ltrim(v.value('@Funding_Source', 'nvarchar(100)')),'')                     [Funding_Source]
, nullif(ltrim(v.value('@ECE', 'nvarchar(100)')),'')                                [ECE]
, nullif(ltrim(v.value('@Grade_1', 'nvarchar(1)')),'')                                          [Grade_1]
, nullif(ltrim(v.value('@Grade_2', 'nvarchar(1)')),'')                                          [Grade_2]
, nullif(ltrim(v.value('@Grade_3', 'nvarchar(1)')),'')                                          [Grade_3]
, nullif(ltrim(v.value('@Grade_4', 'nvarchar(1)')),'')                                          [Grade_4]
, nullif(ltrim(v.value('@Grade_5', 'nvarchar(1)')),'')                                          [Grade_5]
, nullif(ltrim(v.value('@Grade_6', 'nvarchar(1)')),'')                                          [Grade_6]
, nullif(ltrim(v.value('@Grade_7', 'nvarchar(1)')),'')                                          [Grade_7]
, nullif(ltrim(v.value('@Grade_8', 'nvarchar(1)')),'')                                          [Grade_8]
, nullif(ltrim(v.value('@Grade_9', 'nvarchar(1)')),'')                                          [Grade_9]
, nullif(ltrim(v.value('@Grade_10', 'nvarchar(1)')),'')                                         [Grade_10]
, nullif(ltrim(v.value('@Grade_11', 'nvarchar(1)')),'')                                         [Grade_11]
, nullif(ltrim(v.value('@Grade_12', 'nvarchar(1)')),'')                                         [Grade_12]
, nullif(ltrim(v.value('@Admin', 'nvarchar(1)')),'')									[Admin]
, nullif(ltrim(v.value('@Other', 'nvarchar(1)')),'')									[Other]
, nullif(ltrim(v.value('@Total_Days_Absence', 'int')),'')                               [Total_Days_Absence]
, nullif(ltrim(v.value('@Maths', 'int')),'')											[Maths]
, nullif(ltrim(v.value('@Science', 'int')),'')											[Science]
, nullif(ltrim(v.value('@Language', 'int')),'')                                         [Language]
, nullif(ltrim(v.value('@Competency', 'int')),'')                                       [Competency]
, null --@filereference
, v.query('.')																			[rowXml]
from @xml.nodes('ListObject/row') as V(v)


----- DEal with the DOC staff up front
----- Delete them!
----- to do - further discussion about handling these

-- find a better way to do this...
-- the teacher location on this page is a 'pay point', more general than a school

DELETE
FROM @ndoeStaff
WHERE School_No like '%%%DOE'


--- check any teacher qualification values
-- look for the qualification in both the Code and Description for flexibility in future
INSERT INTO @validations
Select
'Highest Qualification'
, rowIndex
, highest_qualification
, ' undefined code'
, 'Critical'

FROM @ndoeStaff ndoe
	LEFT JOIN lkpTeacherQual Q
		ON NDOE.Highest_Qualification in (codeCode, codeDescription)
WHERE highest_qualification is not null
AND Highest_Qualification <> 'None'			-- none is a special value
AND Q.codeCode is null

INSERT INTO @validations
Select
'Highest Qualification'
, rowIndex
, highest_qualification
, ' Classified as an Education Qualification (codeTeach=1)'
, 'Critical'
FROM @ndoeStaff ndoe
	INNER JOIN lkpTeacherQual Q
		ON NDOE.Highest_Qualification in (codeCode, codeDescription)
WHERE highest_qualification is not null
AND Highest_Qualification <> 'None'
AND Q.codeTeach = 1

--- check any teacher qualification values
INSERT INTO @validations
Select
'Highest Ed Qualification'
, rowIndex
, highest_ed_qualification
, ' undefined code'
, 'Critical'
FROM @ndoeStaff ndoe
	LEFT JOIN lkpTeacherQual Q
		ON NDOE.Highest_ed_Qualification in (codeCode, codeDescription)
WHERE highest_ed_qualification is not null
AND Highest_Ed_Qualification <> 'None'
AND Q.codeCode is null

INSERT INTO @validations
Select
'Highest Ed Qualification'
, rowIndex
, highest_ed_qualification
, ' not classified as an Education Qualification (codeTeach=0)'
, 'Critical'

FROM @ndoeStaff ndoe
	INNER JOIN lkpTeacherQual Q
		ON NDOE.Highest_ed_Qualification in (codeCode, codeDescription)
WHERE highest_ed_qualification is not null
AND Highest_Ed_Qualification <> 'None'
AND Q.codeTeach = 0

-- citizenship, ethinicity and gender, role should get validated

INSERT INTO @validations
Select
'Teacher Role'
, rowIndex
, Job_Title
, ' role not found in teacher roles'
, 'Information'

FROM @ndoeStaff ndoe
	INNER JOIN lkpTeacherRole R
		ON NDOE.Job_Title in (R.codeCode, R.codeDescription)
WHERE job_title is not null
AND job_title <> 'None'
AND R.codeCode is null


INSERT INTO @validations
SELECT
'Duplicate Row'
, rowIndex
, first_name + ' ' + last_name
, 'duplicate must be removed before loading'
, 'Information'
FROM @ndoeStaff ndoe
WHERE rowIndex in
(
Select min(rowIndex) RI
From @ndoeStaff ndoe
WHERE employment_status = 'Active'
GROUP BY school_No
, first_name
, last_name
, Date_of_Birth
, Job_Title
HAVING count(*) > 1
)

-- put the max as well
INSERT INTO @validations
SELECT
'Duplicate Row'
, rowIndex
, first_name + ' ' + last_name
, 'duplicate must be removed before loading'
, 'Information'
FROM @ndoeStaff ndoe
WHERE rowIndex in
(
Select max(rowIndex) RI
From @ndoeStaff ndoe
WHERE employment_status = 'Active'
GROUP BY school_No
, first_name
, last_name
, Date_of_Birth
, Job_Title
HAVING count(*) > 1
)

-- different school, but the same teacher
INSERT INTO @validations
SELECT
'Multiple Teacher Record'
, rowIndex
, first_name + ' ' + last_name
, 'duplicate row must be resolved before loading'
, 'Information'
FROM @ndoeStaff ndoe
WHERE rowIndex in
(
Select min(rowIndex) RI
From @ndoeStaff ndoe
WHERE employment_status = 'Active'
GROUP BY first_name
, last_name
, Date_of_Birth
, isnull(FSM_SSN,'')
HAVING count(*) > 1
)
-- different school, but the same teacher
-- the max record as well
INSERT INTO @validations
SELECT
'Multiple Teacher Record'
, rowIndex
, first_name + ' ' + last_name
, 'duplicate row must be resolved before loading'
, 'Information'
FROM @ndoeStaff ndoe
WHERE rowIndex in
(
Select max(rowIndex) RI
From @ndoeStaff ndoe
WHERE employment_status = 'Active'
GROUP BY first_name
, last_name
, Date_of_Birth
, isnull(FSM_SSN,'')
HAVING count(*) > 1
)


print 'at end'
Select * from @Validations

end try

begin catch
    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end

    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err
end catch
END
GO

