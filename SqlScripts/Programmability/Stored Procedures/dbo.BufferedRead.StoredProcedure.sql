SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date:
-- Description:
-- =============================================
CREATE PROCEDURE [dbo].[BufferedRead]
	-- Add the parameters for the stored procedure here
	@Key1 nvarchar(50) =null,
	@SourceTable1 nvarchar(50) = null,
	@TS1 timestamp= null,
	@SQL1 nvarchar(1000)
AS
BEGIN
	declare @xmlout nvarchar(3000)
	declare @sql nvarchar(3000)
	declare @currentTS timestamp
	set @sql = 'select max(ts) maxts into #tmp from ' + @SourceTable1
	exec sp_executesql @sql
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	-- select * from #tmp
	select @sql1 = @sql1+ ' FOR XML AUTO'
	exec sp_executesql @sql1
END
GO

