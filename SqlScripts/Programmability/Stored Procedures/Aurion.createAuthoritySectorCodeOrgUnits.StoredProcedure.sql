SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 1 6 2010
-- Description:	To support a thin org unit structure, Auth Type=>Auth=>Sector
--
-- =============================================
CREATE PROCEDURE [Aurion].[createAuthoritySectorCodeOrgUnits]
	-- Add the parameters for the stored procedure here
	@SendAll int = 0
	, @Nested int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

begin try

	declare @AurionModel int	-- we'll preserve support for the deep model (Auth Type/Auth/SchoolType/School/Sector)
								-- while adding support for the shallow model (Auth Type/Auth/Sector)
	declare @OrgLevel int		-- the level in the hierarchy for these units . Determined by the model.
								-- In fact, these units are only in the Hierarchy if Model = 0 (thin)
	declare @TopOrgUnit int
	declare @TopOrgLevel int

	Select @TopOrgUnit = common.SysParamInt('Aurion_TopLevelOrgNumber',0)
	Select @TopOrgLevel = common.SysParamInt('Aurion_TopLevelOrgLevel',4)
	Select @AurionModel = common.SysParamInt('Aurion_Model',0)


	Select @OrgLevel =
		@TopOrgLevel +
			Case @AurionModel
				when 0 then 2
				when 1 then 5
			end


	If @Nested = 0
		create table #OrgUnits
		(
				OrgUnitNumber int
				, OrgUnitLevel int
				, OrgUnitName nvarchar(50)
				, OrgUnitShortName nvarchar(50)
				, OrgUnitLongDescription nvarchar(100)
				, OrgUnitParent int
				, OrgUnitReference nvarchar(12)
		)

	exec Aurion.createAuthorityOrgUnits @SendAll, 1   -- nested

	begin transaction
	-- list the authority/sectors that need an org unit number
		Select DISTINCT schAuth
			, R.secCode
		INTO #tmpSec
		FROM Establishment E
			INNER JOIN Schools S
				ON E.schNo = S.SchNo
			LEFT JOIN RoleGrades
				ON E.estpRoleGRade = RoleGrades.RGCode
			LEFT JOIN lkpTeacherRole R
				On RoleGRades.roleCode = R.codeCode

			LEFT JOIN Aurion.AuthoritySectorOrgUnits ORGU
				ON R.secCode = ORGU.secCode
				AND S.schAuth = ORGU.authCode
		WHERE
			R.secCode is not null
			AND OrgU.OrgUnitNumber is null
			AND schAuth is not null


		declare @ToAllocate int

		-- this is the number of org unit numbers we need
		Select @ToAllocate = count(*)
		from #tmpSec

		-- this is where we'll put them
		declare @counters table
		(
		counter int
		, maxCounters int
		, seq int
		, char nvarchar(6)
		)

		IF (@ToAllocate > 0 ) begin


		-- get the numbers

			insert into @counters
			exec getCounter 'Aurion_OrgUnit', @ToAllocate


			INSERT INTO Aurion.AuthoritySectorOrgUnits
			SELECT
				schAuth
				, secCode
				, C.counter
			FROM
						(Select row_number() over (ORDER BY schAuth, secCode) Pos
						, schAuth, secCode
						from #tmpSec
						) ORDERED
					INNER JOIN @counters C
						ON ORDERED.Pos = C.seq


		end


	commit transaction


	INSERT INTO #OrgUnits
	(
		OrgUnitNumber
		, OrgUnitLevel
		, OrgUnitName
		, OrgUnitShortName
		, ORgUnitLongDescription
		, OrgUnitParent
		, OrgUnitReference
	)
	SELECT
		ORGU.OrgUnitNumber
		, @OrgLevel
		, secDesc
		, ORGU.secCode
		, secDesc
		, AUTH.authOrgUnitNumber
		, ORGU.secCode

	FROM Aurion.AuthoritySectorOrgUnits ORGU
		LEFT JOIN Authorities AUTH
			on ORGU.authCode = AUTH.authCode
		LEFT JOIN EducationSectors SEC
			ON ORGU.secCode = SEC.secCode
		LEFT JOIN #tmpSec
			ON ORGU.authCode = #tmpSec.schAuth
			AND ORGU.secCode = #tmpSEc.secCode
	WHERE
		(#tmpSec.schAuth is not null
			OR @SendAll = 1)


	-- return the ones we added
	-- return the ones we added
	If (@Nested = 0) begin
		Select *
		FROM
			#OrgUnits


		IF (@AurionModel) = 0

			SELECT GLSA.*
			FROM
				Aurion.EffectiveGLSalaryAccount0 GLSA
			WHERE
				GLSA.orgUnitNumber in (Select OrgUnitNumber FROM #OrgUnits)


		IF (@AurionModel) = 1

			SELECT GLSA.*
			FROM
				Aurion.EffectiveGLSalaryAccount GLSA
			WHERE
				GLSA.orgUnitNumber in (Select OrgUnitNumber FROM #OrgUnits)

		drop table #OrgUnits


	end

	drop table #tmpSec

end try

--- catch block
	begin catch
		DECLARE @err int,
			@ErrorMessage NVARCHAR(4000),
			@ErrorSeverity INT,
			@ErrorState INT;

		Select @err = @@error,
			 @ErrorMessage = ERROR_MESSAGE(),
			 @ErrorSeverity = ERROR_SEVERITY(),
			 @ErrorState = ERROR_STATE()


		if @@trancount > 0
			begin
				rollback transaction
				select @errorMessage = @errorMessage + ' The transaction was rolled back.'
			end

		RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
		return @err
	end catch


END
GO

