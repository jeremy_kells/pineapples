SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_PIVEstablishmentData]
AS
	/* SET NOCOUNT ON */
-- this definitiion must acord with the output from the stored proc
--Establishmentcheck
create table #establishmentcheck
(
	SchNo nvarchar(20),
	schName nvarchar(50),
	svyYear int,
	[Role] nvarchar(10),
	SurveyCount int,
	PayrollCount int,
	EstCount int,
	Apptcount int,
	EstStatus nvarchar(20),
	Unfilled int,
	OverFilled int,
	NumUnfilled int,
	NumOverfilled int,
	NetUnfilled int
)

	Select *
INTO #ebse
FROM dbo.tfnESTIMATE_BestSurveyenrolments()


insert into #EstablishmentCheck
EXEC  EstablishmentCheck

SELECT CHK.svyYear AS [Establishment Year],
CHK.[Role] AS RoleCode,
lkpTeacherRole.codeDescription AS [Role],
CHK.Surveycount AS [Teachers In Survey],
CHK.PayrollCount AS [Payroll ID Supplied],
CHK.EstCount AS [Establishment Positions],
CHK.ApptCount AS Appointments,
CHK.svyYear,
CHK.Unfilled,
CHK.Overfilled,
CHK.NumUnfilled,
CHK.NumOverfilled,
CHK.NetUnfilled,
#ebse.surveyDimensionssID
FROM #EstablishmentCheck CHK
	INNER JOIN lkpTeacherRole ON CHK.[Role] = lkpTeacherRole.codeCode
	 LEFT JOIN #ebse ON CHK.schNo = #ebse.schNo
WHERE
(((CHK.svyYear)=#ebse.[LifeYear])) OR
	(((CHK.svyYear)>#ebse.LifeYear) AND ((#ebse.LifeYear) In (Select max(svyYear) from Survey))) OR (((#ebse.LifeYear) Is Null))

RETURN
GO
GRANT EXECUTE ON [dbo].[sp_PIVEstablishmentData] TO [pEstablishmentRead] AS [dbo]
GO

