SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 30 10 2014
-- Description:	update parent committe / school council
-- 2016 08 02 Modified to allow for decimals in site sizes - these are rounded for storage
-- =============================================
CREATE PROCEDURE [pSurveyWrite].[xfdfSite]
	@SurveyID int
	, @xml xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	declare @idoc int
	-- the xfdf file has the default adobe namespace
	-- we have to alias this namespace ( as 'x' ) so as to be able to use
	-- the xpath expressions
	declare @xmlns nvarchar(500) = '<root xmlns:x="http://ns.adobe.com/xfdf/"/>'
	EXEC sp_xml_preparedocument @idoc OUTPUT, @xml, @xmlns


/*
Format of the Site block

	<field name="Site">
		<field name="Site">
			<field name="Size">
				<value>230</value>
			</field>
			<field name="L"/>
			<field name="W"/>			-- support Siexe ecplicitly or L and W
		</field>
		<field name="Playground">
			<field name="Size">
				<value>230</value>
			</field>
		</field>
		<field name="Garden">
			<field name="Size">
				<value>100</value>
			</field>
		</field>
		<field name="Services">
			<value>N</value>
		</field>
		<field name="Secure">
			<value>N</value>
		</field>
		<field name="Rating"/>
		<field name="Rubbish">
			freq... method
		</field>
		<field name="RandM">
			freq... method
		</field>
	</field>


*/
begin try
UPDATE schoolSurvey
	SET ssSizeSite = round(SiteSize,0)
	, ssSizePlayground = round(PlaygroundSize,0)
	, ssSizeFarm = round(GardenSize,0)
	, ssSizeFarmUsed = round(GardenUsedSize,0)

	, ssSizeRating = SiteSizeRating
	, ssSiteSecure = case SiteSecure when 'Y' then -1 when 'N' then 0 else null end
	, ssISClass = case SiteServices when 'Y' then 'URBAN' when 'N' then 'RURAL' else null end

	, ssRubbishFreq = RubbishFreq
	, ssRubbishMethod = RubbishMethod
	, ssRubbishNote = RubbishNote

	, ssRandM = case RandM when 'Y' then -1 when 'N' then 0 else null end
	, ssRandMResp = RandMResp

-- L and W variation (vanuatu)
	, ssPlaygroundL = round(PlaygroundL,0)
	, ssPlaygroundW = round(PlaygroundW,0)

	, ssFarmL = round(GardenL,0)
	, ssFarmW = round(GardenW,0)

	, ssFarmUsedL = round(GardenUsedL,0)
	, ssFarmUsedW = round(GardenUsedW,0)

	-- vanuatu specific
	, ssSiteHost = SiteHost
	, ssSiteHostDetail = SiteHostDetail

FROM OPENXML(@idoc, '/x:field',2)		-- base is the Site Node
WITH
(

	SiteSize				float			'x:field[@name="Site"]/x:field[@name="Size"]/x:value'
	, SiteL					float			'x:field[@name="Site"]/x:field[@name="L"]/x:value'
	, SiteW					float			'x:field[@name="Site"]/x:field[@name="W"]/x:value'
	, SiteSizeRating		float			'x:field[@name="Rating"]/x:value'

	, SiteSecure			nvarchar(1)		'x:field[@name="Secure"]/x:value'		-- Y/N
	, SiteServices			nvarchar(1)		'x:field[@name="Services"]/x:value'		-- Y/N

	, SiteHost				nvarchar(10)	'x:field[@name="Host"]/x:field[@name="Type"]/x:value'
	, SiteHostDetail		nvarchar(400)	'x:field[@name="Host"]/x:field[@name="Detail"]/x:value'

	, GardenSize			float			'x:field[@name="Garden"]/x:field[@name="Size"]/x:value'
	, GardenL				float			'x:field[@name="Garden"]/x:field[@name="L"]/x:value'
	, GardenW				float			'x:field[@name="Garden"]/x:field[@name="W"]/x:value'

	-- also known as 'Farm Used'
	, GardenUsedSize		float			'x:field[@name="GardenUsed"]/x:field[@name="Size"]/x:value'
	, GardenUsedL			float			'x:field[@name="GardenUsed"]/x:field[@name="L"]/x:value'
	, GardenUsedW			float			'x:field[@name="GardenUsed"]/x:field[@name="W"]/x:value'

	, PlaygroundSize		float			'x:field[@name="Playground"]/x:field[@name="Size"]/x:value'
	, PlaygroundL			float			'x:field[@name="Playground"]/x:field[@name="L"]/x:value'
	, PlaygroundW			float			'x:field[@name="Playground"]/x:field[@name="W"]/x:value'

	, RubbishFreq			nvarchar(1)		'x:field[@name="Rubbish"]/x:field[@name="Freq"]/x:value'
	, RubbishMethod			nvarchar(1)		'x:field[@name="Rubbish"]/x:field[@name="Method"]/x:value'
	, RubbishNote			nvarchar(50)	'x:field[@name="Rubbish"]/x:field[@name="Note"]/x:value'


	, RandM					nvarchar(1)     'x:field[@name="RandM"]/x:field[@name="Plan"]/x:value'
	, RandMResp				nvarchar(10)    'x:field[@name="RandM"]/x:field[@name="Resp"]/x:value'
) X
WHERE SchoolSurvey.ssID = @SurveyID

exec audit.xfdfInsert @SurveyID, 'Survey updated','Site',@@rowcount
end try

begin catch

    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end

	exec audit.xfdfError @SurveyID, @ErrorMessage,'Site'

    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err
end catch
END
GO

