SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 9 12 2009
-- Description:	create a family of coreRoles for a dataArea
-- =============================================
CREATE PROCEDURE [dbo].[setupPermissionRoles]
	-- Add the parameters for the stored procedure here
	@dataArea nvarchar(50),
	@Levels int = 1, -- can be 0,1,2,3
	@HasOps int = 1,
	@HasAdmin int = 1

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @roleNameR nvarchar(50)
	declare @roleNameW nvarchar(50)
	declare @roleNameRX nvarchar(50)
	declare @roleNameWX nvarchar(50)
	declare @roleNameRXX nvarchar(50)
	declare @roleNameWXX nvarchar(50)
	declare @roleNameOps nvarchar(50)
	declare @roleNameAdmin nvarchar(50)

	declare @roleOwner sysname

	select @roleOwner = 'dbo'

	Select @roleNameR = 'p' + @DataArea + 'Read'
	Select @roleNameW = 'p' + @DataArea + 'Write'
	Select @roleNameRX = 'p' + @DataArea + 'ReadX'
	Select @roleNameWX = 'p' + @DataArea + 'WriteX'
	Select @roleNameRXX = 'p' + @DataArea + 'ReadXX'
	Select @roleNameWXX = 'p' + @DataArea + 'WriteXX'

	Select @roleNameOps = 'p' + @DataArea + 'Ops'
	Select @roleNameAdmin = 'p' + @DataArea + 'Admin'


	if @Levels >= 1 begin

		exec setUpPermissionRole @roleNameR, @RoleOwner


		exec setUpPermissionRole @roleNameW, @RoleOwner
		exec sp_addrolemember @roleNameR,  @roleNameW

		if @levels = 1
			exec sp_addrolemember @roleNameW, 'pineapples'


	end

	if @Levels >= 2 begin

		exec setUpPermissionRole @roleNameRX, @RoleOwner
		--RX=> R
		exec sp_addrolemember @roleNameR,  @roleNameRX

		exec setUpPermissionRole @roleNameWX, @RoleOwner
		-- WX=>RX and WX=> W
		exec sp_addrolemember @roleNameRX,  @roleNameWX
		exec sp_addrolemember @roleNameW,  @roleNameWX

		if @levels = 2
			exec sp_addrolemember @roleNameWX, 'pineapples'

	end

	if @Levels >= 3 begin
		exec setUpPermissionRole @roleNameRXX, @RoleOwner

		--RXX=> RX
		exec sp_addrolemember @roleNameRX,  @roleNameRXX

		exec setUpPermissionRole @roleNameWXX, @RoleOwner
		-- WXX=>RXX and WXX=> WX
		exec sp_addrolemember @roleNameRXX,  @roleNameWXX
		exec sp_addrolemember @roleNameWX,  @roleNameWXX

		exec sp_addrolemember @roleNameWXX,  'pineapples'

	end

	if @HasOps = 1 begin

		exec setUpPermissionRole @roleNameOps,@RoleOwner
		-- Ops => W
		if @Levels >= 1
			exec sp_addrolemember @roleNameW,   @roleNameOps

		exec sp_addrolemember @roleNameOps, 'pineapples'

	end


	if @HasAdmin = 1 begin

		exec setUpPermissionRole @roleNameAdmin, @RoleOwner
		-- Admin => R
		if @Levels >=1
			exec sp_addrolemember @roleNameR,   @roleNameAdmin

		exec sp_addrolemember @roleNameAdmin, 'pineapples'


	end

END
GO

