SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 4 10 2009
-- Description:	Insert a new position into the establishment
-- =============================================
CREATE PROCEDURE [pEstablishmentWriteX].[redeployEstablishmentPosition]
	-- Add the parameters for the stored procedure here
	@PositionNo nvarchar(20) ,
	@NewSchNo nvarchar(50),
	@NewRoleGrade nvarchar(20),
	@NewActiveDate datetime,
	@Title nvarchar(50),
	@Description nvarchar(65),
	@Subject nvarchar(50),
	@RedeployAction int,		-- 0 = terminate 1 == assign to the new position
	@TransferPositionNo nvarchar(20)	-- if not null, the position to transfer to

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

	SET NOCOUNT ON;
begin try


	declare @ApptTID as int
	declare @NewPositionNo nvarchar(20)
	declare @SrcAuth nvarchar(10)
	declare @SrcSchool nvarchar(50)
	declare @SrcScope nvarchar(1)
	declare @TargetAuth nvarchar(10)
	declare @CloseDate datetime

	Select @srcScope = estpScope
		, @srcAuth = schAuth
		, @SrcSchool = E.schNo
		, @CloseDate = estpClosedDate
	FROM Establishment E
		LEFT JOIN Schools S
			ON E.SchNo = S.schNo
		WHERE estpNo = @PositionNo

	if (@CloseDate is not null)
			RAISERROR('Cannot redeploy this position because it is already closed' ,16,0);

	if (@srcScope is not null)
			RAISERROR('You can only redeploy school-based positions, not Authority scope positions',16,0);


	Select @targetAuth = schAuth
		FROM Schools WHERE
			SchNo = @NewSchNo


	if (@TargetAuth is null)
			RAISERROR('No Authority defined for target school',16,0);

	if (@SrcAuth is null)
			RAISERROR('No Authority defined for source school',16,0);

	if (@TargetAuth <> @SrcAuth)
			RAISERROR('Cannot redeploy a position to a different authority',16,0);


	begin transaction

	Select @ApptTID = tID
	FROM TeacherAppointment
	WHERE taDate < @NewActiveDate
		AND (taEndDate >= @NewActiveDate or taEndDate is null)
		AND estpNo = @PositionNo

	UPDATE TeacherAppointment
		SET taEndDate = @NewActiveDate - 1
	WHERE taDate < @NewActiveDate
		AND (taEndDate >= @NewActiveDate or taEndDate is null)
		AND estpNo = @PositionNo

	-- delete anything in the future
	DELETE FROM TeacherAppointment
	WHERE (taEndDate >= @NewActiveDate or taEndDate is null)
		AND estpNo = @PositionNo


	INSERT INTO Establishment
	( schNo, estpRoleGrade, estpActiveDate, estpTitle, estpDescription, estpSubject, estpFlag, estpNote)
	VALUES
		(@NewschNo
		,@NewRoleGrade
		,@NewActiveDate
		,@Title
		,@Description
		,@Subject
		, 'R'
		, 'redeploy from ' + @SrcSchool + ' (' + @PositionNo + ')')


	-- note that scope_IDENTITY() doesn't work
	-- becuase Establishment is inserted by an INSTEAD OF trigger,
	-- so the insert above is not in this scope!!!

   Select @newPositionNo = estpNo
   FROM Establishment
   WHERE estpID = @@IDENTITY


 	-- now close the original position, since we can fill in the note details
	UPDATE Establishment
	SET estpClosedDate = @NewActiveDate - 1
		, estpClosedReason = 'Redeployed'
		, estpFlag = 'RX'
		, estpNote = isnull(estpNote,'') + 'Redeploy to ' + @NewSchNo + ' (' + @NewPositionNo + ')'
	WHERE estpNo = @PositionNo

		-- return something - do this before the appointment post which generates recordsets
	Select * from Establishment
	WHERE estpNo = @NewPositionNo

   If (@AppttID is not null) begin

		if @RedeployAction = 1 begin
			Select @TransferPositionNo = isnull(@TransferPositionNo, @NewPositionNo)
			exec pEstablishmentWrite.CreateTeacherAppointment
				@TeacherID = @apptTID
				, @PositionNo = @TransferPositionNo
				, @AppointmentDate = @NewActiveDate
				, @SalaryPoint = null
				, @Verify = 0
		end
	end

	commit transaction
end try
/****************************************************************
Generic catch block
****************************************************************/
begin catch

    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end

    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err
end catch
END
GO

