SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 18 1 2016
-- Description:	for web site - read a pupil table
-- =============================================
CREATE PROCEDURE [pEnrolmentRead].[PupilTableRead]
	-- Add the parameters for the stored procedure here
	@SchoolNo nvarchar(50),
	@SurveyYear int,
	@TableName nvarchar(50) = 'ENROL'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		-- Insert statements for procedure here


	declare @ssID int
	declare @paramSchoolType nvarchar(20)
	declare @schName nvarchar(100)

	declare @paramMinAge nvarchar(3)
	declare @paramMaxAge nvarchar(3)

	Select @paramSchooltype = isnull(ssSchType, schType)
	, @ssID = ssID
	, @schName = schName
	from Schools S
		LEFT JOIN SchoolSurvey SS
			ON SS.schNo = S.schNo
			AND SS.svyYear = @SurveyYear
	WHERE S.schNo = @SchoolNo

	declare @tdiRow nvarchar(50)
	declare @tdiCol nvarchar(50)
	declare @tdiRowSql nvarchar(max)
	declare @tdiColSql nvarchar(max)

	if (@tableName <> 'Enrol')
		Select @tdiRow = isnull(tdefRows,'AGE')
		, @tdiCol = isnull(tdefCols,'LEVEL')
		from metaPupilTableDefs
		WHERE tdefCode = @tableName
	else
		Select @tdiRow = 'AGE'
		, @tdiCol = 'LEVEL'

	if (@tdiRow = 'DATA')
		select @tdiRowSql = 'Select ''DATA'' codeCode
		, tdefName codeDescription
		FROM metaPupilTableDefs
		WHERE tdefCode = ''' + @tableName + ''''
	else if (@tdiRow like 'AGE(%,%)') begin
		select @tdiRowSql = tdiSrc
		from metaSchoolTableItems
		WHERE tdiCode = 'AGEPARAM'	-- hardcoded, must exists
		declare @comma int, @b int
		select @comma = charindex(',', @tdiRow)
		, @b = charindex(')', @tdiRow)

		select substring(@tdiRow, 5, @comma - 5)
		select @paramMinAge = substring(@tdiRow, 5, @comma - 5)
		select @paramMaxAge = substring(@tdiRow, @comma+1, @b - @comma)
		Select @tdiRowSql = replace(@tdiRowSql, '[paramMin]',@paramMinAge)
		Select @tdiRowSql = replace(@tdiRowSql, '[paramMax]',@paramMaxAge)
		print @tdiRowSql
		end
	else
		select @tdiRowSql = tdiSrc
		from metaSchoolTableItems
		WHERE tdiCode = @tdiRow

	if (@tdiCol like 'AGE(%,%)') begin
		select @tdiColSql = tdiSrc
		from metaSchoolTableItems
		WHERE tdiCode = 'AGEPARAM'	-- hardcoded, must exists
		select @comma = charindex(',', @tdiCol)
		, @b = charindex(')', @tdiCol)


		select @paramMinAge = substring(@tdiCol, 5, @comma - 5)
		select @paramMaxAge = substring(@tdiCol, @comma+1, @b - @comma - 1)
		print @paramMaxAge
		Select @tdiColSql = replace(@tdiColSql, '[paramMin]',@paramMinAge)
		Select @tdiColSql = replace(@tdiColSql, '[paramMax]',@paramMaxAge)
		print @tdiColSql
		end
	else
	select @tdiColSql = tdiSrc
	from metaSchoolTableItems
	WHERE tdiCode = @tdiCol


	Select @tdiRowSql = replace(@tdiRowSql, '[paramSchoolType]',QUOTENAME(@paramSchoolType,''''))
	select @tdiRowSql = replace(@tdiRowSql,'[paramYear]',@SurveyYear)
	print @tdiRowSql

	Select @tdiColSql = replace(@tdiColSql, '[paramSchoolType]',QUOTENAME(@paramSchoolType,''''))
	select @tdiColSql = replace(@tdiColSql,'[paramYear]',@SurveyYear)

	Select @schoolNo schoolNo, @SurveyYear year, @paramSchoolType schoolType, @schName schoolName, @tableName tableName,
			@ssID surveyID

	exec sp_sqlexec @tdiRowSql
	exec sp_sqlexec @tdiColSql

	if (@tableName like 'ENROL%') begin
		Select enID id
		, enAge row
		, enLevel col
		, enM M
		, enF F
		from Enrollments
		WHERE ssID = @ssID
		end
	else begin
		if @tdiRow like 'AGE%'
			Select ptID id
			, ptAge [row]  -- need this to be numeric
			, case when @tdiCol like 'LEVEL%' then ptLevel else ptCol end col
			, ptM M
			, ptF F
			from PupilTables
			WHERE ssID = @ssID
			AND ptTable = @tableName
		else
			Select ptID id
			, ptRow [row]
			, case when @tdiCol like 'LEVEL%' then ptLevel else ptCol end col
			, ptM M
			, ptF F
			from PupilTables
			WHERE ssID = @ssID
			AND ptTable = @tableName

	end


END
GO

