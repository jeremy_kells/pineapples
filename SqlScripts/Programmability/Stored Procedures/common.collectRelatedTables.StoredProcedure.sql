SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 20 11 2007
-- Description:	return information about related tables
-- =============================================
CREATE PROCEDURE [common].[collectRelatedTables]
	-- Add the parameters for the stored procedure here
	@TableName nvarchar(50)
AS
-- thankyou to pinalkumar dave
--http://blog.sqlauthority.com/2006/11/01/sql-server-query-to-display-foreign-key-relationships-and-name-of-the-constraint-for-each-table-in-database/
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;
SELECT
		FK_Table  = FK.TABLE_NAME,
		FK_Column = CU.COLUMN_NAME,
		PK_Table  = PK.TABLE_NAME,
		PK_Column = PT.COLUMN_NAME,
		Constraint_Name = C.CONSTRAINT_NAME,
		C.UPDATE_RULE,
		C.DELETE_RULE,
		case C.UPDATE_RULE when 'CASCADE' then -1 else 0 end CascadeUpdate,
		case C.DELETE_RULE when 'CASCADE' then -1 else 0 end CascadeDelete

FROM    	INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS C
INNER JOIN 	INFORMATION_SCHEMA.TABLE_CONSTRAINTS FK ON C.CONSTRAINT_NAME = FK.CONSTRAINT_NAME
INNER JOIN    	INFORMATION_SCHEMA.TABLE_CONSTRAINTS PK ON C.UNIQUE_CONSTRAINT_NAME = PK.CONSTRAINT_NAME
INNER JOIN    	INFORMATION_SCHEMA.KEY_COLUMN_USAGE CU ON C.CONSTRAINT_NAME = CU.CONSTRAINT_NAME
INNER JOIN	(
		SELECT 		i1.TABLE_NAME, i2.COLUMN_NAME
		FROM   		INFORMATION_SCHEMA.TABLE_CONSTRAINTS i1
	        INNER JOIN      INFORMATION_SCHEMA.KEY_COLUMN_USAGE i2 ON i1.CONSTRAINT_NAME = i2.CONSTRAINT_NAME
            	WHERE 		i1.CONSTRAINT_TYPE = 'PRIMARY KEY'
		) PT ON PT.TABLE_NAME = PK.TABLE_NAME

WHERE PT.TABLE_NAME = @TableName
    -- Insert statements for procedure here
END
GO

