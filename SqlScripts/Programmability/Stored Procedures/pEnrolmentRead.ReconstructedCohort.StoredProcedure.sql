SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 12/11/2010
-- Description:	Underlying data for calculation of efa12
-- =============================================
-- summary level
-- 0 = by school and level
-- 1 = national by level
CREATE PROCEDURE [pEnrolmentRead].[ReconstructedCohort]
	@SummaryLevel int
	, @SchoolNo nvarchar(50) = null
	, @SurveyYear int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
-- build estimate best survey enrolments as a temp table


CREATE TABLE #data
(
	schNo nvarchar(50)
	, svyYear int
	, YearOfEd int
	, LevelCode nvarchar(10)
	, EnrolM int  NULL
	, enrolF int NULL
	, EnrolNYM int NULL
	, EnrolNYF int NULL
	, EnrolNYNextLevelM int  NULL
	, EnrolNYNextLevelF int NULL
	, RepM int NULL
	, RepF  int NULL
	, RepNYM int NULL
	, RepNYF int NULL
	, RepNYNextLevelM int NULL
	, RepNYNextLevelF int NULL


	, Estimate smallint
	, [Year Of Data] int
	, [Age of Data] int
	, surveyDimensionssID int
	, [Estimate NY] smallint
	, [Year Of Data NY] int
	, [Age of Data NY] int
	, surveyDimensionssIDNY int

	, Rectype int

)

CREATE TABLE #data2
(
	schNo nvarchar(50)
	, svyYear int
	, YearOfEd int
	, LevelCode nvarchar(10)

	, EnrolM int  NULL
	, enrolF int NULL
	, Enrol int NULL

	, EnrolNYM int NULL
	, EnrolNYF int NULL
	, EnrolNY int NULL

	, EnrolNYNextLevelM int  NULL
	, EnrolNYNextLevelF int NULL
	, EnrolNYNextLevel int NULL

	, RepM int NULL
	, RepF  int NULL
	, Rep  int NULL

	, RepNYM int NULL
	, RepNYF int NULL
	, RepNY int NULL


	, RepNYNextLevelM int NULL
	, RepNYNextLevelF int NULL
	, RepNYNextLevel int NULL


	--------, SlopedEnrolM int NULL
	--------, SlopedEnrolF int NULL
	--------, SlopedEnrol int NULL

	, Estimate smallint
	, [Year Of Data] int
	, [Age of Data] int
	, surveyDimensionssID int
	, [Estimate NY] smallint
	, [Year Of Data NY] int
	, [Age of Data NY] int
	, surveyDimensionssIDNY int
	, Rectype int

)
Select *
into #ebse
from dbo.tfnESTIMATE_BestSurveyenrolments()
WHERE (schNo = @schoolNo or @SchoolNo is null)
AND (LifeYear between @SurveyYear and @SurveyYEar + 1
		or @SurveyYEar is null)


-- current years enrolments by level and gender


-----------------------------------------------------------
-- Enrolments
-- 3 groups- This Year, NextYear, Next Year Next Level
-----------------------------------------------------------
-- can we do all the enrolments at once?
INSERT INTO #data
(
	schNo
	, svyYear
	, YearOfEd
	, LevelCode
	, repM
	, repF
	, repNYM
	, repNYF

)

Select EBSE.schNo
	, EBSE.LifeYEar - num
	, 	lvlYear
	, PT.ptLevel
	, case when num = 0 then PT.repM end
	, case when num = 0 then PT.repF end
	, case when num = 1 then PT.repM end
	, case when num = 1 then PT.repF end

from #ebse EBSE
INNER JOIN metaNumbers M
on M.num in (0,1)
inner join ssIDRepeatersLevel PT
on EBSE.bestssID = PT.ssID
INNER JOIN lkpLevels
	ON lkpLevels.codeCode = PT.ptLevel


INSERT INTO #data
(
	schNo
	, svyYear
	, YearOFEd
	, LevelCode
	, enrolM
	, enrolF
	, EnrolNYM
	, EnrolNYF
	, Estimate
	, [Year Of Data]
	, [Age of Data]
	, surveyDimensionssID
	, [Estimate NY]
	, [Year Of Data NY]
	, [Age of Data NY]
	, surveyDimensionssIDNY
)

Select EBSE.schNo
	, EBSE.LifeYEar - num
	, 	lvlYear
	, EL.LevelCode
	, case when num = 0 then EL.EnrolM end
	, case when num = 0 then EL.enrolF end
	, case when num = 1 then EL.EnrolM end
	, case when num = 1 then EL.enrolF end

	, case when num = 0 then EBSE.Estimate end
	, case when num = 0 then EBSE.bestYear end
	, case when num = 0 then EBSE.Offset end
	, case when num = 0 then EBSE.surveyDimensionssID end

	, case when num = 1 then EBSE.Estimate end
	, case when num = 1 then EBSE.bestYear end
	, case when num = 1 then EBSE.Offset end
	, case when num = 1 then EBSE.surveyDimensionssID end


from
metaNumbers M
INNER JOIN #ebse EBSE
ON M.num in (0,1)
left join pEnrolmentRead.ssidEnrolmentLevel EL
on EBSE.bestssID = El.ssID
left JOIN lkpLevels
	ON lkpLevels.codeCode = EL.levelCode


--
INSERT INTO #data
(
	schNo
	, svyYear
	, YearOfEd
	, EnrolNYNextLevelM
	, EnrolNYNextLevelF
)

Select EBSE.schNo
	, EBSE.LifeYEar - 1
	, lvlYEar - 1
	, EL.EnrolM
	, EL.enrolF

from
#ebse EBSE

inner join pEnrolmentRead.ssidEnrolmentLevel EL
on EBSE.bestssID = El.ssID
INNER JOIN lkpLevels
	ON lkpLevels.codeCode = EL.levelCode


INSERT INTO #data
(
	schNo
	, svyYear
	, YearOfEd
	, repNYNextLevelM
	, repNYNextLevelF
)

Select EBSE.schNo
	, EBSE.LifeYEar - 1
	, 	lvlYear - 1
	, PT.repM
	, PT.repF

from #ebse EBSE
inner join ssIDRepeatersLevel PT
on EBSE.bestssID = PT.ssID
INNER JOIN lkpLevels
	ON lkpLevels.codeCode = PT.ptLevel

-- when all ou've got is a sledgehammer.....
-- The subtle problem with the above is that Estimate Estimate NY may not get set
-- If there is a survey in the year, but no t in the particular level
-- So pick up the sledgehammer and update everythng now


INSERT INTO #Data2
(
	schNo
	, svyYear
	, YearOfEd
	, LevelCode
	, EnrolM
	, enrolF
	, Enrol
	, EnrolNYM
	, EnrolNYF
	, EnrolNY
	, EnrolNYNextLevelM
	, EnrolNYNextLevelF
	, EnrolNYNextLevel
	, RepM
	, RepF
	, Rep
	, RepNYM
	, RepNYF
	, RepNY
	, RepNYNextLevelM
	, RepNYNextLevelF
	, RepNYNextLevel
	, Estimate
	, [Year Of Data]
	, [Age of Data]
	, surveyDimensionssID
	, [Estimate NY]
	, [Year Of Data NY]
	, [Age of Data NY]

)

SELECT
	sub.schNo
	, svyYear
	, YearOfEd
	, LevelCode
	, EnrolM
	, enrolF
	, Enrol
	, EnrolNYM
	, EnrolNYF
	, EnrolNY
	, EnrolNYNextLevelM
	, EnrolNYNextLevelF
	, EnrolNYNextLevel
	, RepM
	, RepF
	, Rep
	, RepNYM
	, RepNYF
	, RepNY
	, RepNYNextLevelM
	, RepNYNextLevelF
	, RepNYNextLevel
	, Estimate
	, [Year Of Data]
	, [Age of Data]
	, surveyDimensionssID
	, [Estimate NY]
	, [Year Of Data NY]
	, [Age of Data NY]


from
(
SELECT
	schNo,
	svyYear,
	-- we have to make sure there is a surveydimensionssid, even if there is only NY data
	--isnull(max(surveyDimensionssID),max(surveyDimensionssIDNY)) surveyDimensionssID,
	--LevelCode,
	YearOfEd,
	min(LevelCode) as LevelCode,
	cast(isnull(sum(enrolM),0) as float) as EnrolM,
	cast(isnull(sum(enrolF),0) as float)  as enrolF,
	cast(isnull(sum(enrolM),0) + isnull(sum(enrolF),0) as float)  as Enrol,


	isnull(sum(EnrolNYM),0) AS EnrolNYM,
	isnull(sum(EnrolNYF),0) AS EnrolNYF,
	isnull(sum(EnrolNYM),0) + isnull(sum(EnrolNYF),0) AS EnrolNY,


	isnull(sum(EnrolNYNextLevelM),0) AS EnrolNYNextLevelM,
	isnull(sum(EnrolNYNextLevelF),0) AS EnrolNYNextLevelF,
	isnull(sum(EnrolNYNextLevelM),0) + isnull(sum(EnrolNYNextLevelF),0) AS EnrolNYNextLevel,


	isnull(sum(RepM),0) AS RepM,
	isnull(sum(RepF),0)  AS RepF,
	isnull(sum(RepM),0) + isnull(sum(RepF),0)  AS Rep,

	isnull(sum(RepNYM),0) AS RepNYM,
	isnull(sum(RepNYF),0) AS RepNYF,
	isnull(sum(RepNYM),0) + isnull(sum(RepNYF),0) AS RepNY,

	isnull(sum(RepNYNextLevelM),0)  AS RepNYNextLevelM,
	isnull(sum(RepNYNextLevelF),0)  AS RepNYNextLevelF,
	isnull(sum(RepNYNextLevelM),0) + isnull(sum(RepNYNextLevelF),0)  AS RepNYNextLevel,

	max(Estimate) as Estimate,
	max([Year of Data]) as [Year of Data],
	max([Age of Data]) as [Age of Data],
	isnull(max(surveyDimensionssID),max(surveyDimensionssIDNY)) surveyDimensionssID,
	max([Estimate NY]) as [Estimate NY],
	max([Year of Data NY]) as [Year of Data NY],
	max([Age of Data NY]) as [Age of Data NY]

	FROM #data
	GROUP BY schNo, svyYear, YearOfEd
) sub


CREATE NONCLUSTERED INDEX myIndex
ON #data2 (schNo, svyYear, YearOfEd)
INCLUDE (Estimate, [Estimate NY])


UPDATE #data2
SET Estimate = EBSE.Estimate
, [Age Of data] = EBSE.Offset
, [Year of Data] = EBSE.bestYear
, [surveyDimensionssID] = EBSE.surveyDimensionssID

FROM #data2 D2
INNER JOIN #ebse EBSE
	ON D2.schNo = EBSE.schNo
	AND D2.svyYEar = EBSE.LifeYEar
WHERE D2.Estimate is null


UPDATE #data2
SET [Estimate NY] = EBSE.Estimate
, [Age Of data NY] = EBSE.Offset
, [Year of Data NY] = EBSE.bestYear
, [surveyDimensionssIDNY] = EBSE.surveyDimensionssID


FROM #data2 D2
INNER JOIN #ebse EBSE
	ON D2.schNo = EBSE.schNo
	AND D2.svyYEar = EBSE.LifeYEar - 1
WHERE [Estimate NY] is null

UPDATE #data2
SET levelCode = DPL.levelCode
FROM #data2 D2
	INNER JOIN common.ListDefaultPathLevels DPL
		ON D2.YearOfEd = DPL.YearOfEd
WHERE D2.levelCode is null


----UPDATE #data2

----SET SlopedEnrolM = round(isnull(case when [Age Of Data] > 0 and ([Estimate NY] = 0)
----				then ([Age Of Data]   * isnull(EnrolNYM,0) + isnull(EnrolM,0)) / cast(([Age Of Data]   + 1) as float)
----										else enrolM end ,0),0)
----, SlopedEnrolF = round(isnull(case when [Age Of Data] > 0 and ([Estimate NY] = 0)
----						then ([Age Of Data] * isnull(EnrolNYF,0) + isnull(EnrolF,0)) / cast(([Age Of Data]  + 1) as float)
----										else enrolF end ,0) ,0)
----, SlopedEnrol = round(isnull(case when [Age Of Data] > 0 and ([Estimate NY] = 0)
----				then ([Age Of Data]   * isnull(EnrolNYM,0) + isnull(EnrolM,0)) / cast(([Age Of Data]   + 1) as float)
----										else enrolM end ,0),0)
----				+
----				round(isnull(case when [Age Of Data] > 0 and ([Estimate NY] = 0)
----						then ([Age Of Data] * isnull(EnrolNYF,0) + isnull(EnrolF,0)) / cast(([Age Of Data]  + 1) as float)
----										else enrolF end ,0) ,0)


IF @SummaryLevel = 0 begin

	SELECT *
	, case when PRM = 0 then 0 when RRM = 1 then null else PRM / (1 - RRM) end SRM
	, case when PRF = 0 then 0 when RRF = 1 then null else PRF / (1 - RRF) end SRF
	, case when PR = 0 then 0 when RR = 1 then null else PR / (1 - RR) end SR
	FROM
	(
	SELECT
		*
		, case when SlopedenrolM = 0 then 0 else RepNYM / SlopedenrolM end RRM
		, case when SlopedenrolF = 0 then 0 else RepNYF / SlopedenrolF end RRF
		, case when SlopedEnrol = 0 then 0 else RepNY /SlopedEnrol end RR

	, case when SlopedenrolM = 0 then 0 else (EnrolNYNextLevelM - RepNYNextLevelM)/ SlopedenrolM end PRM
		, case when SlopedenrolF = 0 then 0 else (EnrolNYNextLevelF - RepNYNextLevelF)/ SlopedenrolF end PRF
		, case when Slopedenrol = 0 then 0 else (EnrolNYNextLevel - RepNYNextLevel)/ Slopedenrol end PR
		, row_number() OVER (PARTITION BY schNo, svyYear ORDER BY yearOfEd) seq
		FROM
		(
		SELECT
			schNo
			, svyYear
			, YearOfEd
			, LevelCode
			, EnrolM
			, enrolF
			, Enrol
			, EnrolNYM
			, EnrolNYF
			, EnrolNY
			, EnrolNYNextLevelM
			, EnrolNYNextLevelF
			, EnrolNYNextLevel
			, RepM
			, RepF
			, Rep
			, RepNYM
			, RepNYF
			, RepNY
			, RepNYNextLevelM
			, RepNYNextLevelF
			, RepNYNextLevel
			, Estimate
			, [Year Of Data]
			, [Age of Data]
			, isnull(surveyDimensionssID , surveyDimensionssIDNY) surveyDimensionssID
			, [Estimate NY]
			, [Year Of Data NY]
			, [Age of Data NY]
		, round(isnull(case when [Age Of Data] > 0 and ([Estimate NY] = 0)
					then ([Age Of Data]   * isnull(EnrolNYM,0) + isnull(EnrolM,0)) / cast(([Age Of Data]   + 1) as float)
											else enrolM end ,0),0) SlopedEnrolM
		, round(isnull(case when [Age Of Data] > 0 and ([Estimate NY] = 0)
							then ([Age Of Data] * isnull(EnrolNYF,0) + isnull(EnrolF,0)) / cast(([Age Of Data]  + 1) as float)
											else enrolF end ,0) ,0) SlopedEnrolF
		, round(isnull(case when [Age Of Data] > 0 and ([Estimate NY] = 0)
					then ([Age Of Data]   * isnull(EnrolNYM,0) + isnull(EnrolM,0)) / cast(([Age Of Data]   + 1) as float)
											else enrolM end ,0),0)
					+
					round(isnull(case when [Age Of Data] > 0 and ([Estimate NY] = 0)
							then ([Age Of Data] * isnull(EnrolNYF,0) + isnull(EnrolF,0)) / cast(([Age Of Data]  + 1) as float)
											else enrolF end ,0) ,0)  SlopedEnrol

		FROM #data2
		where (svyYear = @SurveyYear or @SurveyYear is null)
		) D2
	) sub
end


IF @SummaryLevel = 1 begin -- national

	SELECT *
	, case when PRM = 0 then 0 when RRM = 1 then null else PRM / (1 - RRM) end SRM
	, case when PRF = 0 then 0 when RRF = 1 then null else PRF / (1 - RRF) end SRF
	, case when PR = 0 then 0 when RR = 1 then null else PR / (1 - RR) end SR
	FROM
	(
	SELECT
		*
		, case when SlopedenrolM = 0 then 0 else RepNYM / SlopedenrolM end RRM
		, case when SlopedenrolF = 0 then 0 else RepNYF / SlopedenrolF end RRF
		, case when SlopedEnrol = 0 then 0 else RepNY /SlopedEnrol end RR

	, case when SlopedenrolM = 0 then 0 else (EnrolNYNextLevelM - RepNYNextLevelM)/ SlopedenrolM end PRM
		, case when SlopedenrolF = 0 then 0 else (EnrolNYNextLevelF - RepNYNextLevelF)/ SlopedenrolF end PRF
		, case when Slopedenrol = 0 then 0 else (EnrolNYNextLevel - RepNYNextLevel)/ Slopedenrol end PR
		, row_number() OVER (PARTITION BY svyYear ORDER BY yearOfEd) seq
		FROM
		(
		SELECT


			svyYear,
			YearOfEd,
			min(LevelCode) as LevelCode,
			cast(isnull(sum(enrolM),0) as float) as EnrolM,
			cast(isnull(sum(enrolF),0) as float)  as enrolF,
			cast(isnull(sum(enrolM),0) + isnull(sum(enrolF),0) as float)  as Enrol,


			isnull(sum(EnrolNYM),0) AS EnrolNYM,
			isnull(sum(EnrolNYF),0) AS EnrolNYF,
			isnull(sum(EnrolNYM),0) + isnull(sum(EnrolNYF),0) AS EnrolNY,


			isnull(sum(EnrolNYNextLevelM),0) AS EnrolNYNextLevelM,
			isnull(sum(EnrolNYNextLevelF),0) AS EnrolNYNextLevelF,
			isnull(sum(EnrolNYNextLevelM),0) + isnull(sum(EnrolNYNextLevelF),0) AS EnrolNYNextLevel,


			isnull(sum(RepM),0) AS RepM,
			isnull(sum(RepF),0)  AS RepF,
			isnull(sum(RepM),0) + isnull(sum(RepF),0)  AS Rep,

			isnull(sum(RepNYM),0) AS RepNYM,
			isnull(sum(RepNYF),0) AS RepNYF,
			isnull(sum(RepNYM),0) + isnull(sum(RepNYF),0) AS RepNY,

			isnull(sum(RepNYNextLevelM),0)  AS RepNYNextLevelM,
			isnull(sum(RepNYNextLevelF),0)  AS RepNYNextLevelF,
			isnull(sum(RepNYNextLevelM),0) + isnull(sum(RepNYNextLevelF),0)  AS RepNYNextLevel
		, sum(
			round(isnull(case when [Age Of Data] > 0 and ([Estimate NY] = 0)
					then ([Age Of Data]   * isnull(EnrolNYM,0) + isnull(EnrolM,0)) / cast(([Age Of Data]   + 1) as float)
											else enrolM end ,0),0)
			 ) SlopedEnrolM
		, sum (
				round(isnull(case when [Age Of Data] > 0 and ([Estimate NY] = 0)
							then ([Age Of Data] * isnull(EnrolNYF,0) + isnull(EnrolF,0)) / cast(([Age Of Data]  + 1) as float)
											else enrolF end ,0) ,0)
			 ) SlopedEnrolF

		, sum (
				round(isnull(case when [Age Of Data] > 0 and ([Estimate NY] = 0)
					then ([Age Of Data]   * isnull(EnrolNYM,0) + isnull(EnrolM,0)) / cast(([Age Of Data]   + 1) as float)
											else enrolM end ,0),0)
					+
					round(isnull(case when [Age Of Data] > 0 and ([Estimate NY] = 0)
							then ([Age Of Data] * isnull(EnrolNYF,0) + isnull(EnrolF,0)) / cast(([Age Of Data]  + 1) as float)
											else enrolF end ,0) ,0)
			 ) SlopedEnrol

		FROM #data2
		GROUP BY svyYear, YearOfEd
		) D2
	) sub
end


IF @SummaryLevel = 2 begin -- school rates across levels

	SELECT
	schNo
	, svyYear
	, RRM
	, RRF
	, RR
	, PRM
	, PRF
	, PR

	, (1 - RRM - PRM) DRM
	, (1 - RRF - PRF) DRF
	, (1 - RR - PR) DR

	FROM
	(
	SELECT
		*
		, case when SlopedenrolM = 0 then 0 else RepNYM / SlopedenrolM end RRM
		, case when SlopedenrolF = 0 then 0 else RepNYF / SlopedenrolF end RRF
		, case when SlopedEnrol = 0 then 0 else RepNY /SlopedEnrol end RR

	, case when SlopedenrolM = 0 then 0 else (EnrolNYNextLevelM - RepNYNextLevelM)/ SlopedenrolM end PRM
		, case when SlopedenrolF = 0 then 0 else (EnrolNYNextLevelF - RepNYNextLevelF)/ SlopedenrolF end PRF
		, case when Slopedenrol = 0 then 0 else (EnrolNYNextLevel - RepNYNextLevel)/ Slopedenrol end PR

		FROM
		(
		SELECT
			schNo,

			svyYear,
			min(LevelCode) as LevelCode,
			cast(isnull(sum(enrolM),0) as float) as EnrolM,
			cast(isnull(sum(enrolF),0) as float)  as enrolF,
			cast(isnull(sum(enrolM),0) + isnull(sum(enrolF),0) as float)  as Enrol,


			isnull(sum(EnrolNYM),0) AS EnrolNYM,
			isnull(sum(EnrolNYF),0) AS EnrolNYF,
			isnull(sum(EnrolNYM),0) + isnull(sum(EnrolNYF),0) AS EnrolNY,


			isnull(sum(EnrolNYNextLevelM),0) AS EnrolNYNextLevelM,
			isnull(sum(EnrolNYNextLevelF),0) AS EnrolNYNextLevelF,
			isnull(sum(EnrolNYNextLevelM),0) + isnull(sum(EnrolNYNextLevelF),0) AS EnrolNYNextLevel,


			isnull(sum(RepM),0) AS RepM,
			isnull(sum(RepF),0)  AS RepF,
			isnull(sum(RepM),0) + isnull(sum(RepF),0)  AS Rep,

			isnull(sum(RepNYM),0) AS RepNYM,
			isnull(sum(RepNYF),0) AS RepNYF,
			isnull(sum(RepNYM),0) + isnull(sum(RepNYF),0) AS RepNY,

			isnull(sum(RepNYNextLevelM),0)  AS RepNYNextLevelM,
			isnull(sum(RepNYNextLevelF),0)  AS RepNYNextLevelF,
			isnull(sum(RepNYNextLevelM),0) + isnull(sum(RepNYNextLevelF),0)  AS RepNYNextLevel
		, sum(
			round(isnull(case when [Age Of Data] > 0 and ([Estimate NY] = 0)
					then ([Age Of Data]   * isnull(EnrolNYM,0) + isnull(EnrolM,0)) / cast(([Age Of Data]   + 1) as float)
											else enrolM end ,0),0)
			 ) SlopedEnrolM
		, sum (
				round(isnull(case when [Age Of Data] > 0 and ([Estimate NY] = 0)
							then ([Age Of Data] * isnull(EnrolNYF,0) + isnull(EnrolF,0)) / cast(([Age Of Data]  + 1) as float)
											else enrolF end ,0) ,0)
			 ) SlopedEnrolF

		, sum (
				round(isnull(case when [Age Of Data] > 0 and ([Estimate NY] = 0)
					then ([Age Of Data]   * isnull(EnrolNYM,0) + isnull(EnrolM,0)) / cast(([Age Of Data]   + 1) as float)
											else enrolM end ,0),0)
					+
					round(isnull(case when [Age Of Data] > 0 and ([Estimate NY] = 0)
							then ([Age Of Data] * isnull(EnrolNYF,0) + isnull(EnrolF,0)) / cast(([Age Of Data]  + 1) as float)
											else enrolF end ,0) ,0)
			 ) SlopedEnrol

		FROM #data2
		GROUP BY svyYear, schNo
		) D2
	) sub
end

RETURN 0


DROP TABLE #ebse

END
GO

