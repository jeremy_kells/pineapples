SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2/11/2015
-- Description:	Read a performance assessment by ID
-- =============================================
CREATE PROCEDURE [pPARead].[PerfAssessTemplate]
@framework nvarchar(10)
, @teacherID int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

if (@teacherID is not null) begin
-- need to check if this teacher id is valid too
	Select TOP 1 TI.tID
	, TI.tFullName
	, SS.schNo			paSchNo
	, S.schName
	, @framework		pafrmCode
	, common.Today()	paDate
	FROM paFrameworks_
		LEFT JOIN TeacherIdentity TI
			ON TI.tID = @teacherID
		LEFT JOIN TeacherSurvey TS
			ON TI.tID = TS.tID
		LEFT JOIN SchoolSurvey SS
			ON Ts.ssID = SS.ssID
		LEFT JOIN Schools S
			ON SS.schNo = S.schNo
	WHERE pafrmCode = @framework
	ORDER BY SS.svyYEar DESC
end
else begin
	 -- we can only return the data and the framework
	Select null		tID
	, null			FullName
	, null			paSchNo
	, null			schName
	, @framework  pafrmCode
	, common.Today()	paDate
end


-- outer join all the indicators for this assessment type

Select indFullID
, I.paindID
, null paplCode
, null palComment
from paIndicators I
WHERE pafrmCode = @framework


Select L.*
from paIndicators I
INNER JOIN paIndicatorLevels_ L
	ON I.paindID = L.paIndID
WHERE pafrmCode = @framework
ORDER BY paIndID, paplSort, paplValue


END
GO

