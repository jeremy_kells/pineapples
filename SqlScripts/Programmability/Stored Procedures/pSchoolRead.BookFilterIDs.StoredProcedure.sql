SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 18 6 2016
-- Description:	Filter book IDs
-- =============================================
CREATE PROCEDURE [pSchoolRead].[BookFilterIDs]
	-- Add the parameters for the stored procedure here

	@NumMatches int OUTPUT,
	@PageSize int = 0,
	@PageNo int = 1,
	@SortColumn nvarchar(30) = null,
	@SortDir int = 0,

	@BookCode nvarchar(20) = null,
    @Title nvarchar(100) = null,
    @PublisherCode  nvarchar(20) = null,
    @ISBN  nvarchar(20) = null,
	@TG nvarchar(10) = null,

	@Subject nvarchar(20) = null,
	@Level nvarchar(20) = null,
	@EdLevelCode nvarchar(10) = null,
	@SchoolType nvarchar(20) = null,
	@CurriculumYear int = null,


	@xmlFilter xml = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

	SET NOCOUNT ON;

if (@xmlFilter is not null) begin

	declare @idoc int
	EXEC sp_xml_preparedocument @idoc OUTPUT, @xmlFilter

	Select
		@Subject = isnull(@Subject, Subject),
		@Level = isnull(@Subject, Level)
	FROM OPENXML(@idoc,'//Filter',2)
	WITH
	(
	subject    nvarchar(50)     '@Subject'
	,level    nvarchar(50)     '@Level'
	)
end

	DECLARE @keysAll TABLE
	(
	ID nvarchar(50)
	, recNo int IDENTITY PRIMARY KEY
	)

-- prepare the like search on title
if (@title is not null) begin
	if charindex('%', @Title) = 0 and charindex('_', @Title) = 0
		Select @Title = '%' + @Title + '%'
end

INSERT INTO @KeysAll (ID)
Select bkCode from Books
WHERE
(bkCode = @BookCode or @BookCode is null)
AND (bkSubject = @Subject or @subject is null)
AND (bkPublisher = @PublisherCode or @PublisherCode is null)
AND (bkISBN like @ISBN or @ISBN is null)			-- allow but don;t mandate a search pattern
AND (bkTitle like @Title or @Title is null)
AND (bkTG = @TG or @TG is null)
AND ((@Level is null and @SchoolType is null and @CurriculumYear is null and @EdLevelCode is null)
or bkCode in (Select bkCode from SetBooks
				WHERE
				(cuLevel = @Level or @Level is null)
				AND (cuYEar = @CurriculumYear or @CurriculumYear is null)
				AND (@SchoolType is null
					OR cuLevel in ( select tlmLevel from metaSchoolTypeLevelMap WHERE stCode = @SchoolType)
					)
				AND (@EdLevelCode is null
					OR cuLevel in ( select levelCode from DimensionLevel WHERE edLevelCode = @edLevelCode)
					)
			)
)
OPTION(RECOMPILE)


SELECT @NumMatches = @@ROWCOUNT

-- now return the page, sorted in the right sequence and direction

If @SortDir = 1 begin


		SELECT ID
		, RecNo
		FROM
		(
			Select ID
			, @NumMatches - RecNo + 1 RecNo
			FROM @KeysAll
		) S
		WHERE (@PageSize = 0
				or
					@PageSize >= @NumMatches
				or
						RecNo between @PageSize * (@PageNo - 1) + 1
						and @PageSize * (@PageNo)

				)
		ORDER BY RecNo

	end
	else begin


		SELECT ID
		, RecNo
		FROM @KeysAll
		WHERE (@PageSize = 0
				or
					@PageSize >= @NumMatches
				or
						RecNo between @PageSize * (@PageNo - 1) + 1
						and @PageSize * (@PageNo)

				)
	end

END
GO

