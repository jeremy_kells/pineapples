SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 4 5 2010
-- Description:	Summary of current establishment and plan, by school.
-- =============================================
CREATE PROCEDURE [pEstablishmentRead].[EstablishmentSchoolSummary]
	-- Add the parameters for the stored procedure here
	@EstYear int
	, @AsAtDate datetime = null
	, @SchoolNo nvarchar(50) = null
	, @Authority nvarchar(10) = null
	, @SchoolType nvarchar(10) = null
	, @Sector nvarchar(10) = null
	, @HasPositions int = null
	, @HasProjection int = null
	, @HasPlan int = null
	, @AuthorityChange int = 0
	, @SchoolTypeChange int = 0
	, @ParentStatusChange int = 0
	, @ExtensionStatusChange int = 0


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	If (@AsAtDate is null)
		Select @AsAtDate = estcSnapshotDate
		From EstablishmentControl
		WHERE estcYear = @EstYear


	Select *
	INTO #ebse
	FROM dbo.tfnESTIMATE_BestSurveyEnrolments()
	WHERE LifeYear = year(@AsAtDate)


Select sub.*
, EBSE.bestEnrol Enrolment
, EBSE.bestYear enrolYear
, EBSEX.EnrolmentX
, case when sub.NumExtensions > 0 then EBSEX.EnrolmentX else null end EnrolmentXDisplay
, case when sub.PlanExtensions > 0 then sub.ProjectionX else null end ProjectionXDisplay

from
(
    -- Insert statements for procedure here
	Select Schools.*
	, EP.Projection
	, EPX.ProjectionX
	, case when EP.schNo is not null then 1 else 0 end HasProjection
	, POS.Positions
	, case when POS.schNo is not null then 1 else 0 end HasPositions
	, EPLAN.PlanPositions
	, case when EPLAN.schNo is not null then 1 else 0 end HasEstablishmentPlan
	, EPLAN.estFlag
	, EPLAN.HasNote
	, eststCode
	, case when isnull(eststCode, schType) = schType then 0 else 1 end ChangeType
	, estAuth
	, case when isnull(estAuth, schAuth) = schAuth then 0 else 1 end ChangeAuthority
	, SE.estParent
	, case when schEstablishmentPoint = estEstablishmentPoint  then 0
			when estEstablishmentPoint is null then 0		-- don;t oinclude those with missing establishment
			else 1 end ChangeParent
	, case when PlanPArents.estParent is null then 0 else 1 end PlanIsParent
	, case when Parents.schParent is null then 0 else 1 end IsParent
	, SE.syDormant
	, case when syDormant = schDormant then 0 else 1 end ChangeDormant
	, Parents.NumExtensions
	, PlanPArents.NumExtensions PlanExtensions
	, case when isnull(Parents.NumExtensions,0)
			= isnull(PlanParents.NumExtensions,0) then 0 else 1 end ChangeExtensions

	, case when
		(case when PlanPArents.estParent is null then 0 else 1 end)
		<>
		(case when Parents.schParent is null then 0 else 1 end )
	then 1 else 0 end ParentStatusChanged

from Schools
	LEFT JOIN
		(Select schNo , sum(epdSum) Projection
			from EstablishmentControl
			INNER JOIN EnrolmentProjection
				ON EstablishmentControl.escnID = EnrolmentProjection.escnID
					AND EnrolmentProjection.epYear = @EstYear
			INNER JOIN EnrolmentProjectionData
				ON EnrolmentProjection.epID = EnrolmentProjectionData.epID
			WHERE estcYear = @EstYEar
			GROUP BY SchNo) EP
		ON Schools.schNo = EP.schNo
	LEFT JOIN
		(Select estEstablishmentPoint estSchX, sum(epdSum) ProjectionX
			from EstablishmentControl
			INNER JOIN EnrolmentProjection
				ON EstablishmentControl.escnID = EnrolmentProjection.escnID
					AND EnrolmentProjection.epYear = @EstYear
			INNER JOIN EnrolmentProjectionData
				ON EnrolmentProjection.epID = EnrolmentProjectionData.epID
			INNER JOIN SchoolEstablishment SE
				ON EnrolmentProjection.schNo = SE.schNo
				AND SE.estYear = @EstYear
			WHERE estcYear = @EstYEar
			GROUP BY SE.estEstablishmentPoint) EPX
		ON Schools.schNo = EPX.estSchX
	LEFT JOIN
		(Select  schNo , count(schNo) Positions
			from Establishment
				INNER JOIN RoleGrades RG
					ON Establishment.estpRoleGrade = RG.rgCode
				INNER JOIN lkpTeacherRole R
					ON R.codeCode = RG.roleCode

			WHERE  estpActiveDate <= @AsAtDate
				and ( estpClosedDate >= @AsAtDate or estpClosedDate is null)
				AND ( @sector is null or @Sector = R.secCode)
			GROUP BY SchNo) POS
		ON Schools.schNo = POS.schNo
	LEFT JOIN
		(Select schNo , estFlag
			, min(case when estNote is null then 0 else 1 end) HasNote
			, sum(estrCount) PlanPositions
			from SchoolEstablishment
				INNER JOIN SchoolEstablishmentRoles
					ON SchoolEstablishment.estID = SchoolEstablishmentRoles.estID
				INNER JOIN RoleGrades RG
					ON SchoolEstablishmentRoles.estrRoleGrade = RG.rgCode
				INNER JOIN lkpTeacherRole R
					ON R.codeCode = RG.roleCode
			WHERE estYEar = @estYear
				AND ( @sector is null or @Sector = R.secCode)

			GROUP BY SchNo, estFlag ) EPLAN
		ON Schools.schNo = EPLAN.schNo
	LEFT JOIN
		(Select DISTINCT
			schNo
			, eststCode
			, estAuth
			, estParent
			, estEstablishmentPoint
			, syDormant
			from SchoolEstablishment
			WHERE estYEar = @estYear) SE
		ON Schools.Schno = SE.schNo
	LEFT JOIN
		(Select schParent, count(schNo) NumExtensions
			FROM Schools
			GROUP BY schParent
		) Parents
		On Parents.schParent = Schools.schNo
	LEFT JOIN
		(Select estParent, count(schNo) NumExtensions
			from SchoolEstablishment
			WHERE estYEar = @estYear
			GROUP BY estParent) PlanParents
		On PlanParents.estParent = Schools.schNo
) sub
LEFT JOIN #ebse EBSE
	ON Sub.schNo = EBSE.schNo
LEFT JOIN
	(
		Select schEstablishmentPoint, sum(BestEnrol) EnrolmentX
		FROM #ebse EBSE
			INNER JOIN Schools
			ON EBSE.schNo 	= Schools.schNo
		GROUP BY schEstablishmentPoint
	) EBSEX
	ON EBSEX.schEstablishmentPoint = sub.schNo
WHERE
	 (schClosed = 0 or schClosed >= @EstYear)
	 AND (sub.SchNo = @SchoolNo or @SchoolNo is null)
	 AND (@Authority is null
			or @Authority = schAuth
			or @Authority = estAuth
		 )
	 AND (@SchoolType is null
			or @SchoolType = schType
			or @SchoolType = eststCode
		 )
	 AND (@HasPositions = HasPositions or @HasPositions is null)
	 AND (@HasProjection = HasProjection or @HasProjection is null)
	 AND (@HasPlan = HasEstablishmentPlan or @HasPlan is null)

	 AND (@AuthorityChange = 0
			or ChangeAuthority = 1
		 )
	 AND (@SchoolTypeChange = 0
			or ChangeType = 1
		 )
	 AND (@ParentStatusChange = 0
			or ChangeParent = 1
		 )
	 AND (@ExtensionStatusChange = 0
			or ChangeExtensions = 1
		 )

	SELECT @@ROWCOUNT
END
GO

