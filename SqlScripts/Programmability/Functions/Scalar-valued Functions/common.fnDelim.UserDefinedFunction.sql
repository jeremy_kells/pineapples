SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [common].[fnDelim]
(
	-- Add the parameters for the function here
	@str nvarchar(2000),
	@delim nvarchar(1)
)
RETURNS nvarchar(2000)
AS
BEGIN

	DECLARE @Result nvarchar(2000)

	-- Add the T-SQL statements to compute the return value here
	SELECT @Result = case @delim
		when '(' then '(' + @str + ')'
		when '[' then '[' + @str + ']'
		when '"' then '"' + @str + '"'
		else
				 '''' + @str + ''''
		end
	-- Declare the return variable here@str

	-- Return the result of the function
	RETURN @Result

END
GO

