SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date:
-- Description:
-- =============================================
CREATE FUNCTION [dbo].[fnMakeBestDataStr]
(
	-- Add the parameters for the function here
	@LifeYear int,
	@DataYear int,
	@DatassID int,
	@ssqLevel int
)
RETURNS nvarchar(50)
AS
/* MakeBestDatastr

Created by : Brian Lewis 21 8 2007
Last Modified :
---------------------------------------------------------------------------
 This string is used to determine the best available data in a surveyyear, by ranking
as follows - 0 survey year equal or earlier than life year 1 otherwise
then difference in years
by taking the minimum of this field, we get the best survey year
the year itself and the relevant ssID are included in this string, so they can be extracted
without another data read
Thei si the format of best data string:
1234567890123456789012345678901234567890
S_xxx_yyyy_Q_iiiiiiiiiiiiiiiiiii
where
S = sort indicator 0/1		1-1
xxx = differnece in years	3-5
yyyy = data year			7-10
Q = survey year quality indicator  12
iiii = ssID of data 14-32
------------------------------------------------------------------------------
*/
BEGIN
	-- Declare the return variable here
	DECLARE @Result nvarchar(50)

	-- Add the T-SQL statements to compute the return value here
	SELECT @Result =
			case
				when @DataYear is null then 	null
			else
				case
					when @LifeYear < @DataYear then '1'
					else '0'
				end + '_' +
					right('000' + convert(nvarchar(3) ,abs(@LifeYear - @DataYear)),3) + '_' +
					convert(nchar(4),@DataYear) + '_' +
					isnull(convert(nchar(1),@ssqLevel),'x') + '_' +
					convert(nchar(20),@DatassID)
			end

	-- Return the result of the function
	RETURN @Result

END
GO
GRANT EXECUTE ON [dbo].[fnMakeBestDataStr] TO [public] AS [dbo]
GO

