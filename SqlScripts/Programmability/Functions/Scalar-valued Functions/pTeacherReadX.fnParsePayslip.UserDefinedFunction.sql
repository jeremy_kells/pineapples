SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [pTeacherReadX].[fnParsePayslip]
(
	-- Add the parameters for the function here
	@Payslip nvarchar(max)
)
RETURNS xml

AS
BEGIN
	-- Fill the table variable with the rows for your result set
	declare @xmlPS xml


	declare @rows TABLE
	(
		ID int IDENTITY
		, rowData nvarchar(200)
	)


	Select @XMLPS = '<ps><row>' + replace(replace(@Payslip,'&','_'), char(13) + char(10), '</row><row>') + '</row></ps>'


	INSERT INTO @Rows
	(rowData)
	Select cast( r.query('./text()') as nvarchar(400))
	from
	@xmlps.nodes('/ps/row') as F(r)


	declare @ThisPayStart int
	declare @ThisPayEnd int


	declare @Tag nvarchar(100)
	declare @Pos bigint
	Select @Tag = ' This Pay                      Multiplier      Rate  Hours/Units       Amount'

	Select @ThisPayStart = ID+2
	from @rows
	WHERE rowData like @Tag

	Select @Tag = ' ----%'

	Select top 1 @ThisPayEnd = ID - 3
	from @rows
	WHERE rowData like @Tag
	AND ID > @ThisPayStart


		Select @xmlPS =
		(
		Select  rtrim(substring(rowData, 4,12)) [@pc]
		, cast( substring(rowData, 70,10) as money) [@amt]

		from @rows
		WHERE ID between @ThisPayStart and @ThisPayEnd
		FOR XML PATH('Item'), root( 'ThisPay')
		)

declare @xmlPos xml

		Select @xmlPos =
		(
		Select rtrim(substring(rowData,15,30)) [@title]
		from @rows
		WHERE rowData like '%Job Title%'
		FOR XML PATH('Position')
		)


		Select @xmlPS =
		(
		Select @XMLPS
		, @xmlPos
		FOR XML PATH('Payslip')
		)

	RETURN  @XMLPS
END
GO

