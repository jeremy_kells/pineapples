CREATE ROLE [pTeacherRead]
GO
EXEC sys.sp_addextendedproperty @name=N'Comment', @value=N'Permission to read the basic data about a teacher.' , @level0type=N'USER',@level0name=N'pTeacherRead'
GO
EXEC sys.sp_addextendedproperty @name=N'PineapplesUser', @value=N'CoreRole' , @level0type=N'USER',@level0name=N'pTeacherRead'
GO

