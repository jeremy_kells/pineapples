SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PreSchoolSurvey](
	[presID] [int] IDENTITY(1,1) NOT NULL,
	[ssID] [int] NULL,
	[presSeq] [int] NULL,
	[presName] [nvarchar](50) NULL,
	[presFunded] [nvarchar](20) NULL,
	[presYearsOffered] [smallint] NULL,
	[presPupils] [smallint] NULL,
	[presM] [int] NULL,
	[presF] [int] NULL,
	[presM5] [int] NULL,
	[presF5] [int] NULL,
	[presM6] [int] NULL,
	[presF6] [int] NULL,
	[presM7] [int] NULL,
	[presF7] [int] NULL,
	[presM8] [int] NULL,
	[presF8] [int] NULL,
	[presM9] [int] NULL,
	[presF9] [int] NULL,
	[presM10] [int] NULL,
	[presF10] [int] NULL,
 CONSTRAINT [aaaaaPreSchoolSurvey1_PK] PRIMARY KEY NONCLUSTERED 
(
	[presID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[PreSchoolSurvey] TO [pSchoolRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[PreSchoolSurvey] TO [pSchoolWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[PreSchoolSurvey] TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[PreSchoolSurvey] TO [pSchoolWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[PreSchoolSurvey] TO [public] AS [dbo]
GO
CREATE NONCLUSTERED INDEX [IX_PreSchoolSurvey_SSID] ON [dbo].[PreSchoolSurvey]
(
	[ssID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PreSchoolSurvey]  WITH CHECK ADD  CONSTRAINT [PreSchoolSurvey_FK00] FOREIGN KEY([ssID])
REFERENCES [dbo].[SchoolSurvey] ([ssID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PreSchoolSurvey] CHECK CONSTRAINT [PreSchoolSurvey_FK00]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Used in Kiribati to collect ECE data from Primary schools, since no ECE survey is conducted. One row for each preschool at which there is enrolments. ssID is the foreign key. 
Not to be confused with PreschoolAttenders, which is the new intake into first Year of Primary who have attended ECE. PreschoolAttenders is stored on PupilTables.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PreSchoolSurvey'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Enrolment' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PreSchoolSurvey'
GO

