SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EducationPathLevels](
	[plID] [int] IDENTITY(1,1) NOT NULL,
	[pathCode] [nvarchar](10) NULL,
	[levelCode] [nvarchar](10) NULL,
 CONSTRAINT [aaaaaEducationPathLevels1_PK] PRIMARY KEY NONCLUSTERED 
(
	[plID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[EducationPathLevels] TO [pAdminWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[EducationPathLevels] TO [pAdminWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[EducationPathLevels] TO [pAdminWrite] AS [dbo]
GO
GRANT SELECT ON [dbo].[EducationPathLevels] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[EducationPathLevels] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[EducationPathLevels]  WITH CHECK ADD  CONSTRAINT [EducationPathLevels_FK00] FOREIGN KEY([pathCode])
REFERENCES [dbo].[EducationPaths] ([pathCode])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[EducationPathLevels] CHECK CONSTRAINT [EducationPathLevels_FK00]
GO
ALTER TABLE [dbo].[EducationPathLevels]  WITH CHECK ADD  CONSTRAINT [EducationPathLevels_FK01] FOREIGN KEY([levelCode])
REFERENCES [dbo].[lkpLevels] ([codeCode])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[EducationPathLevels] CHECK CONSTRAINT [EducationPathLevels_FK01]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Set of class Levels that make an Education Path. Middle table between EducationPaths and lkpLevels.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EducationPathLevels'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Enrolment' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EducationPathLevels'
GO

