SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StudentEnrolment_](
	[stueID] [int] IDENTITY(1,1) NOT NULL,
	[stuID] [uniqueidentifier] NULL,
	[schNo] [nvarchar](50) NULL,
	[stueYear] [int] NULL,
	[stueClass] [nvarchar](10) NULL,
	[stueFrom] [nvarchar](20) NULL,
	[stueFromSchool] [nvarchar](50) NULL,
	[stueFromDate] [date] NULL,
	[stueSpEd] [nvarchar](1) NULL,
	[stueDaysAbsent] [int] NULL,
	[stueCompleted] [nvarchar](1) NULL,
	[stueOutcome] [nvarchar](20) NULL,
	[stueOutcomeReason] [nvarchar](50) NULL,
 CONSTRAINT [PK_StudentEnrolment_] PRIMARY KEY CLUSTERED 
(
	[stueID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'How the student arrived in this class Promoted Repeater - from Ece - Transfer In' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StudentEnrolment_', @level2type=N'COLUMN',@level2name=N'stueFrom'
GO

