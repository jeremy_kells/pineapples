SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SchoolEstablishmentRoles](
	[estrID] [int] IDENTITY(1,1) NOT NULL,
	[estID] [int] NOT NULL,
	[estrRoleGrade] [nvarchar](20) NOT NULL,
	[spCode] [nvarchar](10) NULL,
	[estrQuota] [int] NULL,
	[estrCountUser] [int] NULL,
	[estrCount]  AS (isnull([estrCountUser],[estrQuota])),
	[estrNote] [nvarchar](200) NULL,
 CONSTRAINT [aaaaaSchoolEstablishmentRoles1_PK] PRIMARY KEY NONCLUSTERED 
(
	[estrID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[SchoolEstablishmentRoles] TO [pEstablishmentRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[SchoolEstablishmentRoles] TO [pEstablishmentWriteX] AS [dbo]
GO
GRANT INSERT ON [dbo].[SchoolEstablishmentRoles] TO [pEstablishmentWriteX] AS [dbo]
GO
GRANT UPDATE ON [dbo].[SchoolEstablishmentRoles] TO [pEstablishmentWriteX] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[SchoolEstablishmentRoles] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[SchoolEstablishmentRoles] ADD  CONSTRAINT [DF__SchoolEst__estID__640DD89F]  DEFAULT ((0)) FOR [estID]
GO
ALTER TABLE [dbo].[SchoolEstablishmentRoles]  WITH CHECK ADD  CONSTRAINT [FK_SchoolEstablishmentRoles_SchoolYearHistory] FOREIGN KEY([estID])
REFERENCES [dbo].[SchoolYearHistory] ([syID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SchoolEstablishmentRoles] CHECK CONSTRAINT [FK_SchoolEstablishmentRoles_SchoolYearHistory]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'calculated from formula' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SchoolEstablishmentRoles', @level2type=N'COLUMN',@level2name=N'estrQuota'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Manually entered by user' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SchoolEstablishmentRoles', @level2type=N'COLUMN',@level2name=N'estrCountUser'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Specific rolegrades to add to the establishment. Foreign key estId which links these records to the school/establishment year header SchoolEstbalishment.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SchoolEstablishmentRoles'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Establishment' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SchoolEstablishmentRoles'
GO

