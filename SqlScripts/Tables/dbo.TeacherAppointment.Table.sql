SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TeacherAppointment](
	[taID] [int] IDENTITY(1,1) NOT NULL,
	[tID] [int] NOT NULL,
	[taDate] [datetime] NOT NULL,
	[taType] [nvarchar](20) NULL,
	[SchNo] [nvarchar](50) NULL,
	[taRole] [nvarchar](50) NULL,
	[spCode] [nvarchar](10) NULL,
	[taSalary] [money] NULL,
	[estrID] [int] NULL,
	[estpNo] [nvarchar](20) NOT NULL,
	[taEndDate] [datetime] NULL,
	[taFlag] [nvarchar](10) NULL,
	[taNote] [nvarchar](400) NULL,
	[taHasNote]  AS (case when [taNote] IS NULL then (0) else (1) end),
	[taRoleGrade] [nvarchar](20) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditContext] [nvarchar](50) NULL,
	[taSendError] [nvarchar](200) NULL,
	[pRowversion] [timestamp] NOT NULL,
 CONSTRAINT [PK_TeacherAppointment] PRIMARY KEY CLUSTERED 
(
	[taID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[TeacherAppointment] TO [pEstablishmentRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[TeacherAppointment] TO [pEstablishmentWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[TeacherAppointment] TO [pEstablishmentWrite] AS [dbo]
GO
GRANT SELECT ON [dbo].[TeacherAppointment] TO [pTeacherRead] AS [dbo]
GO
GRANT INSERT ON [dbo].[TeacherAppointment] TO [pTeacherWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherAppointment] TO [pTeacherWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[TeacherAppointment] TO [public] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherAppointment] ([taID]) TO [pEstablishmentWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherAppointment] ([tID]) TO [pEstablishmentWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherAppointment] ([taDate]) TO [pEstablishmentWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherAppointment] ([taType]) TO [pEstablishmentWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherAppointment] ([SchNo]) TO [pEstablishmentWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherAppointment] ([taRole]) TO [pEstablishmentWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherAppointment] ([spCode]) TO [pEstablishmentWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherAppointment] ([taSalary]) TO [pEstablishmentWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherAppointment] ([estrID]) TO [pEstablishmentWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherAppointment] ([estpNo]) TO [pEstablishmentWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherAppointment] ([taEndDate]) TO [pEstablishmentWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherAppointment] ([taFlag]) TO [pEstablishmentWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherAppointment] ([taNote]) TO [pEstablishmentWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherAppointment] ([taHasNote]) TO [pEstablishmentWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherAppointment] ([taRoleGrade]) TO [pEstablishmentWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherAppointment] ([pCreateDateTime]) TO [pineapples] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherAppointment] ([pCreateUser]) TO [pineapples] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherAppointment] ([pEditDateTime]) TO [pineapples] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherAppointment] ([pEditUser]) TO [pineapples] AS [dbo]
GO
CREATE NONCLUSTERED INDEX [IX_TeacherAppointment_Date] ON [dbo].[TeacherAppointment]
(
	[taDate] ASC,
	[taID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_TeacherAppointment_estpNo] ON [dbo].[TeacherAppointment]
(
	[estpNo] ASC,
	[taDate] ASC
)
INCLUDE ( 	[taEndDate],
	[tID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_TeacherAppointment_SchNo] ON [dbo].[TeacherAppointment]
(
	[SchNo] ASC,
	[taDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_TeacherAppointment_tID] ON [dbo].[TeacherAppointment]
(
	[tID] ASC,
	[taDate] ASC,
	[taEndDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TeacherAppointment]  WITH CHECK ADD  CONSTRAINT [FK_TeacherAppointment_Establishment] FOREIGN KEY([estpNo])
REFERENCES [dbo].[Establishment] ([estpNo])
GO
ALTER TABLE [dbo].[TeacherAppointment] CHECK CONSTRAINT [FK_TeacherAppointment_Establishment]
GO
ALTER TABLE [dbo].[TeacherAppointment]  WITH CHECK ADD  CONSTRAINT [FK_TeacherAppointment_RoleGrades] FOREIGN KEY([taRoleGrade])
REFERENCES [dbo].[RoleGrades] ([rgCode])
GO
ALTER TABLE [dbo].[TeacherAppointment] CHECK CONSTRAINT [FK_TeacherAppointment_RoleGrades]
GO
ALTER TABLE [dbo].[TeacherAppointment]  WITH CHECK ADD  CONSTRAINT [TeacherAppointment_FK00] FOREIGN KEY([spCode])
REFERENCES [dbo].[lkpSalaryPoints] ([spCode])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[TeacherAppointment] CHECK CONSTRAINT [TeacherAppointment_FK00]
GO
ALTER TABLE [dbo].[TeacherAppointment]  WITH CHECK ADD  CONSTRAINT [TeacherAppointment_FK03] FOREIGN KEY([SchNo])
REFERENCES [dbo].[Schools] ([schNo])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[TeacherAppointment] CHECK CONSTRAINT [TeacherAppointment_FK03]
GO
ALTER TABLE [dbo].[TeacherAppointment]  WITH CHECK ADD  CONSTRAINT [TeacherAppointment_FK04] FOREIGN KEY([tID])
REFERENCES [dbo].[TeacherIdentity] ([tID])
GO
ALTER TABLE [dbo].[TeacherAppointment] CHECK CONSTRAINT [TeacherAppointment_FK04]
GO
ALTER TABLE [dbo].[TeacherAppointment]  WITH CHECK ADD  CONSTRAINT [CK_TeacherAppointment] CHECK  (([taSalary]>=(0)))
GO
ALTER TABLE [dbo].[TeacherAppointment] CHECK CONSTRAINT [CK_TeacherAppointment]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 14 10 2009
-- Description:	
-- =============================================
CREATE TRIGGER [dbo].[AppointmentDateCheck] 
   ON  [dbo].[TeacherAppointment] 
   AFTER INSERT, UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @count int

--------	select @count = count(*) 
--------	from INSERTED
--------
--------	if (@Count > 1)
--------		begin
--------			rollback transaction
--------			print 'after rollback'
--------		end
	Select @count = count(INSERTED.taID)
	from INSERTED
	inner join TeacherAppointment TA2 on INSERTED.tID = TA2.tID
	WHERE (INSERTED.taDate < isnull(TA2.taEndDate,'2999-12-31')
		and  TA2.taDate < isnull(INSERTED.taEndDate,'2999-12-31') 
			)
		AND INSERTED.taID <> TA2.taID
    -- Insert statements for trigger here
	if (@Count > 0)
		begin
			raiserror ( 'overlaps on TeacherIdentity',16,1)
			rollback transaction
			 
		end
	Select @count = count(INSERTED.taID)
	from INSERTED
	inner join TeacherAppointment TA2 on INSERTED.estpNo = TA2.estpNo
	WHERE (INSERTED.taDate < isnull(TA2.taEndDate,'2999-12-31')
		and  TA2.taDate < isnull(INSERTED.taEndDate,'2999-12-31') 
			)
		AND INSERTED.taID <> TA2.taID
    -- Insert statements for trigger here
	if (@Count > 0)
		begin
			raiserror ( 'overlaps on position',16,1)
			rollback transaction
			 
		end


END
GO
ALTER TABLE [dbo].[TeacherAppointment] ENABLE TRIGGER [AppointmentDateCheck]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 17 05 2010
-- Description:	
-- =============================================
CREATE TRIGGER [dbo].[TeacherAppointment_AuditDelete] 
   ON  [dbo].[TeacherAppointment] 
   
   WITH EXECUTE as 'pineapples'
   AFTER DELETE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @NumChanges int
	declare @AuditID int
	
	Select @NumChanges = count(*) from DELETED
	
	
	exec @AuditID =  audit.LogAudit 'TeacherAppointment', null, 'D', @Numchanges
	
	INSERT INTO audit.AuditRow
	(auditID, arowKey, arowName)
	SELECT DISTINCT @AuditID
	, taID
	, isnull(cast(DELETED.tID as nvarchar(15)),'')
	 + ' ' + isnull(TI.tFullName,'') 
	 + ' ' + isnull(TI.tPayroll,'') 
	 + ' ' + isnull(DELETED.estpNo,'') 
 
	FROM DELETED	
	LEFT JOIN TeacherIdentity TI
	ON DELETED.tID = TI.tID



END
GO
ALTER TABLE [dbo].[TeacherAppointment] ENABLE TRIGGER [TeacherAppointment_AuditDelete]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 17 05 2010
-- Description:	
-- =============================================
CREATE TRIGGER [dbo].[TeacherAppointment_AuditInsert] 
   ON  [dbo].[TeacherAppointment] 
    
   WITH EXECUTE as 'pineapples'

   AFTER INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @NumChanges int
	declare @AuditID int
	
	Select @NumChanges = count(*) from INSERTED
	
	
	exec @AuditID =  audit.LogAudit 'TeacherAppointment', null, 'I', @Numchanges
	
	INSERT INTO audit.AuditRow
	(auditID, arowKey)
	SELECT DISTINCT @AuditID
	, taID
	FROM INSERTED	

-- update the created and edited fields
	UPDATE TeacherAppointment
		SET pCreateDateTime = aLog.auditDateTime
			, pCreateUser = alog.auditUser

			, pEditDateTime = aLog.auditDateTime
			, pEditUser = alog.auditUser
	FROM INSERTED I
		CROSS JOIN Audit.auditLog alog
	WHERE TeacherAppointment.taID = I.taID
	AND aLog.auditID = @auditID


END
GO
ALTER TABLE [dbo].[TeacherAppointment] ENABLE TRIGGER [TeacherAppointment_AuditInsert]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2009-0912
-- Description:	Logging trigger
-- =============================================
CREATE TRIGGER [dbo].[TeacherAppointment_AuditUpdate] 
   ON  [dbo].[TeacherAppointment] 

	-- pineapples has access to write to TeacherIdentityAudit
	WITH EXECUTE AS 'pineapples'
   AFTER UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @AuditID int



		declare @NumChanges int
		
		Select @NumChanges = count(*) from INSERTED
		
		If (XACT_STATE() = -1)
			return
		if (@NumChanges = 0)
			return
		
		exec @AuditID =  audit.LogAudit 'TeacherAppointment', null, 'U', @Numchanges
		
		INSERT INTO audit.AuditRow
		(auditID, arowKey)
		SELECT DISTINCT @AuditID
		, taID
		FROM INSERTED	



		
		INSERT INTO audit.AuditColumn
		(arowID, acolName, acolBefore, acolAfter)	
		
		SELECT arowID
		, ColumnName
		, DValue
		, IValue
		FROM audit.AuditRow Row
		INNER JOIN 
		(
				SELECT 
			I.taID
			, case num
				when 1 then 'taID'
				when 2 then 'tID'
				when 3 then 'taDate'
				when 4 then 'taType'
				when 5 then 'SchNo'
				when 6 then 'taRole'
				when 7 then 'spCode'
				when 8 then 'taSalary'
				when 9 then 'estrID'
				when 10 then 'estpNo'
				when 11 then 'taEndDate'
				when 12 then 'taFlag'
				when 13 then 'taNote'
				when 14 then 'taHasNote'
				when 15 then 'taRoleGrade'
			end ColumnName
			, case num
				when 1 then cast(D.taID as nvarchar(50))
				when 2 then cast(D.tID as nvarchar(50))
				when 3 then cast(D.taDate as nvarchar(50))
				when 4 then cast(D.taType as nvarchar(50))
				when 5 then cast(D.SchNo as nvarchar(50))
				when 6 then cast(D.taRole as nvarchar(50))
				when 7 then cast(D.spCode as nvarchar(50))
				when 8 then cast(D.taSalary as nvarchar(50))
				when 9 then cast(D.estrID as nvarchar(50))
				when 10 then cast(D.estpNo as nvarchar(50))
				when 11 then cast(D.taEndDate as nvarchar(50))
				when 12 then cast(D.taFlag as nvarchar(50))
				when 13 then cast(D.taNote as nvarchar(50))
				when 14 then cast(D.taHasNote as nvarchar(50))
				when 15 then cast(D.taRoleGrade as nvarchar(50))
			end dValue
				 
			, case num
				when 1 then cast(I.taID as nvarchar(50))
				when 2 then cast(I.tID as nvarchar(50))
				when 3 then cast(I.taDate as nvarchar(50))
				when 4 then cast(I.taType as nvarchar(50))
				when 5 then cast(I.SchNo as nvarchar(50))
				when 6 then cast(I.taRole as nvarchar(50))
				when 7 then cast(I.spCode as nvarchar(50))
				when 8 then cast(I.taSalary as nvarchar(50))
				when 9 then cast(I.estrID as nvarchar(50))
				when 10 then cast(I.estpNo as nvarchar(50))
				when 11 then cast(I.taEndDate as nvarchar(50))
				when 12 then cast(I.taFlag as nvarchar(50))
				when 13 then cast(I.taNote as nvarchar(50))
				when 14 then cast(I.taHasNote as nvarchar(50))
				when 15 then cast(I.taRoleGrade as nvarchar(50))
			end IValue

		FROM INSERTED I 
		INNER JOIN DELETED D
			ON I.taID = D.taID
		CROSS JOIN metaNumbers
		WHERE num between 1 and 15
		) Edits
		ON Edits.taID = Row.arowKey
		WHERE Row.auditID = @AuditID
		AND isnull(DVAlue,'') <> isnull(IValue,'')

		
		-- update the last edited field	
		UPDATE TeacherAppointment
			SET pEditDateTime = aLog.auditDateTime
				, pEditUser = alog.auditUser
		FROM INSERTED I
			CROSS JOIN Audit.auditLog alog
		WHERE TeacherAppointment.taID = I.taID
		AND aLog.auditID = @auditID
	
END
GO
ALTER TABLE [dbo].[TeacherAppointment] ENABLE TRIGGER [TeacherAppointment_AuditUpdate]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 14 10 2009
-- Description:	
-- =============================================
CREATE TRIGGER [dbo].[TeacherAppointment_Validations] 
   ON  [dbo].[TeacherAppointment] 
   AFTER INSERT, UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @count int

--------	select @count = count(*) 
--------	from INSERTED
--------
--------	if (@Count > 1)
--------		begin
--------			rollback transaction
--------			print 'after rollback'
--------		end
	Select @count = count(INSERTED.taID)
	from INSERTED
	inner join TeacherAppointment TA2 on INSERTED.tID = TA2.tID
	WHERE (INSERTED.taDate < isnull(TA2.taEndDate,'2999-12-31')
		and  TA2.taDate < isnull(INSERTED.taEndDate,'2999-12-31') 
			)
		AND INSERTED.taID <> TA2.taID
    -- Insert statements for trigger here
	if (@Count > 0)
		begin
			raiserror ( 'overlaps on TeacherIdentity',16,1)
			rollback transaction
			 
		end
	Select @count = count(INSERTED.taID)
	from INSERTED
	inner join TeacherAppointment TA2 on INSERTED.estpNo = TA2.estpNo
	WHERE (INSERTED.taDate < isnull(TA2.taEndDate,'2999-12-31')
		and  TA2.taDate < isnull(INSERTED.taEndDate,'2999-12-31') 
			)
		AND INSERTED.taID <> TA2.taID
    -- Insert statements for trigger here
	if (@Count > 0)
		begin
			raiserror ( 'overlaps on position',16,1)
			rollback transaction
			 
		end

	
END
GO
ALTER TABLE [dbo].[TeacherAppointment] ENABLE TRIGGER [TeacherAppointment_Validations]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Simplify display of note indicator' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TeacherAppointment', @level2type=N'COLUMN',@level2name=N'taHasNote'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Represents the appointment of a teacher to an establishment position at a given date. Date may be in the future. May also have an end date. Pay scale is alos on the appointment, and should correspond to the Payrange of the position''s RoleGRade. Foreign keys are tID (TeacherIdentity) estpNo (Establishment).' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TeacherAppointment'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Establishment' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TeacherAppointment'
GO

