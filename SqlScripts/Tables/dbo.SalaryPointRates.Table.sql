SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SalaryPointRates](
	[spID] [int] IDENTITY(1,1) NOT NULL,
	[spCode] [nvarchar](10) NULL,
	[spEffective] [datetime] NULL,
	[spSalaryFN] [money] NULL,
	[spHAllowFN] [money] NULL,
	[spSalary] [money] NULL,
	[spHAllow] [money] NULL,
 CONSTRAINT [aaaaaSalaryPointRates1_PK] PRIMARY KEY NONCLUSTERED 
(
	[spID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[SalaryPointRates] TO [pTeacherAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[SalaryPointRates] TO [pTeacherAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[SalaryPointRates] TO [pTeacherAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[SalaryPointRates] TO [pTeacherAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[SalaryPointRates] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[SalaryPointRates] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[SalaryPointRates]  WITH CHECK ADD  CONSTRAINT [SalaryPointRates_FK00] FOREIGN KEY([spCode])
REFERENCES [dbo].[lkpSalaryPoints] ([spCode])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[SalaryPointRates] CHECK CONSTRAINT [SalaryPointRates_FK00]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pay rates associated to a salry point. Records are grouped into sets by applicable date; for each applicabe date, there should be one record for each salary point., Pineapples can determine the pay rate in effect at any date fro the history of these.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SalaryPointRates'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Teachers' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SalaryPointRates'
GO

