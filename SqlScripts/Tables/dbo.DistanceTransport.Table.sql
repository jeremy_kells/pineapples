SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DistanceTransport](
	[dtID] [int] IDENTITY(1,1) NOT NULL,
	[ssID] [int] NULL,
	[dtCode] [int] NULL,
	[dtFoot] [int] NULL,
	[dtTransport] [int] NULL,
 CONSTRAINT [aaaaaDistanceTransport1_PK] PRIMARY KEY NONCLUSTERED 
(
	[dtID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[DistanceTransport] TO [pSchoolRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[DistanceTransport] TO [pSchoolWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[DistanceTransport] TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[DistanceTransport] TO [pSchoolWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[DistanceTransport] TO [public] AS [dbo]
GO
CREATE NONCLUSTERED INDEX [IX_DistanceTransport_SSID] ON [dbo].[DistanceTransport]
(
	[ssID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DistanceTransport] ADD  CONSTRAINT [DF__DistanceTr__ssID__29AC2CE0]  DEFAULT ((0)) FOR [ssID]
GO
ALTER TABLE [dbo].[DistanceTransport]  WITH CHECK ADD  CONSTRAINT [DistanceTransport_FK00] FOREIGN KEY([ssID])
REFERENCES [dbo].[SchoolSurvey] ([ssID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[DistanceTransport] CHECK CONSTRAINT [DistanceTransport_FK00]
GO
ALTER TABLE [dbo].[DistanceTransport]  WITH CHECK ADD  CONSTRAINT [DistanceTransport_FK01] FOREIGN KEY([dtCode])
REFERENCES [dbo].[lkpDistanceCodes] ([codeNum])
GO
ALTER TABLE [dbo].[DistanceTransport] CHECK CONSTRAINT [DistanceTransport_FK01]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Holds data about the distance of pupils fro school, and the transport they use to get there.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DistanceTransport'
GO
EXEC sys.sp_addextendedproperty @name=N'pDataAccessDB', @value=N'bound/linked table' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DistanceTransport'
GO
EXEC sys.sp_addextendedproperty @name=N'pDataAccessUI', @value=N'Survey' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DistanceTransport'
GO
EXEC sys.sp_addextendedproperty @name=N'pDiagramName', @value=N'School Survey 2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DistanceTransport'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Enrolment' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DistanceTransport'
GO

