SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Streams](
	[stmID] [int] IDENTITY(1,1) NOT NULL,
	[ssID] [int] NULL,
	[stmLevel] [nvarchar](10) NULL,
	[stmNum] [smallint] NULL,
	[stmPupils] [int] NULL,
 CONSTRAINT [aaaaaStreams1_PK] PRIMARY KEY NONCLUSTERED 
(
	[stmID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[Streams] TO [pSchoolRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[Streams] TO [pSchoolWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[Streams] TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Streams] TO [pSchoolWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[Streams] TO [public] AS [dbo]
GO
CREATE NONCLUSTERED INDEX [IX_Streams_SSID] ON [dbo].[Streams]
(
	[ssID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Streams] ADD  CONSTRAINT [DF__Streams__ssID__047AA831]  DEFAULT ((0)) FOR [ssID]
GO
ALTER TABLE [dbo].[Streams] ADD  CONSTRAINT [DF__Streams__stmNum__056ECC6A]  DEFAULT ((0)) FOR [stmNum]
GO
ALTER TABLE [dbo].[Streams] ADD  CONSTRAINT [DF__Streams__stmPupi__0662F0A3]  DEFAULT ((0)) FOR [stmPupils]
GO
ALTER TABLE [dbo].[Streams]  WITH CHECK ADD  CONSTRAINT [Streams_FK00] FOREIGN KEY([stmLevel])
REFERENCES [dbo].[lkpLevels] ([codeCode])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Streams] CHECK CONSTRAINT [Streams_FK00]
GO
ALTER TABLE [dbo].[Streams]  WITH CHECK ADD  CONSTRAINT [Streams_FK01] FOREIGN KEY([ssID])
REFERENCES [dbo].[SchoolSurvey] ([ssID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Streams] CHECK CONSTRAINT [Streams_FK01]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of streams, or class groups, at each class level in primary school. To be replaced with individual records in Classes.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Streams'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Survey' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Streams'
GO

