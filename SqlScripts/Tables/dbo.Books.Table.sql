SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Books](
	[bkCode] [nvarchar](20) NOT NULL,
	[bkPublisher] [nvarchar](10) NULL,
	[bkISBN] [nvarchar](20) NULL,
	[bkSubject] [nvarchar](10) NULL,
	[bkTitle] [nvarchar](100) NULL,
	[bkVol] [nchar](10) NULL,
	[bkTG] [nvarchar](10) NULL,
	[bkEdition] [nvarchar](10) NULL,
	[bkYear] [int] NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pRowversion] [timestamp] NULL,
 CONSTRAINT [PK_Books] PRIMARY KEY CLUSTERED 
(
	[bkCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[Books] TO [pSchoolRead] AS [dbo]
GO
GRANT INSERT ON [dbo].[Books] TO [pSchoolRead] AS [dbo]
GO
GRANT SELECT ON [dbo].[Books] TO [pSchoolRead] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Books] TO [pSchoolRead] AS [dbo]
GO
ALTER TABLE [dbo].[Books]  WITH CHECK ADD  CONSTRAINT [FK_Books_lkpBookTG] FOREIGN KEY([bkTG])
REFERENCES [dbo].[lkpBookTG] ([codeCode])
GO
ALTER TABLE [dbo].[Books] CHECK CONSTRAINT [FK_Books_lkpBookTG]
GO
ALTER TABLE [dbo].[Books]  WITH CHECK ADD  CONSTRAINT [FK_Books_lkpSubjects] FOREIGN KEY([bkSubject])
REFERENCES [dbo].[lkpSubjects] ([subjCode])
GO
ALTER TABLE [dbo].[Books] CHECK CONSTRAINT [FK_Books_lkpSubjects]
GO
ALTER TABLE [dbo].[Books]  WITH CHECK ADD  CONSTRAINT [FK_Books_Publishers] FOREIGN KEY([bkPublisher])
REFERENCES [dbo].[Publishers] ([pubCode])
GO
ALTER TABLE [dbo].[Books] CHECK CONSTRAINT [FK_Books_Publishers]
GO

