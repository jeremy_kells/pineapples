SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lkpTeacherStatus](
	[statusCode] [nvarchar](5) NOT NULL,
	[statusDesc] [nvarchar](200) NULL,
	[statusDescL1] [nvarchar](200) NULL,
	[statusDescL2] [nvarchar](200) NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pRowversion] [timestamp] NULL,
 CONSTRAINT [aaaaalkpTeacherStatus1_PK] PRIMARY KEY NONCLUSTERED 
(
	[statusCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[lkpTeacherStatus] TO [pTeacherAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[lkpTeacherStatus] TO [pTeacherAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpTeacherStatus] TO [pTeacherAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[lkpTeacherStatus] TO [pTeacherAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpTeacherStatus] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[lkpTeacherStatus] TO [public] AS [dbo]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'User configurable code field for Teacher Status.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpTeacherStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Teachers' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpTeacherStatus'
GO

