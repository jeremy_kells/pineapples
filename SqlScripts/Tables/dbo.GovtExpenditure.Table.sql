SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GovtExpenditure](
	[fnmYear] [int] NOT NULL,
	[fnmExpTotA] [money] NULL,
	[fnmExpTotB] [money] NULL,
	[fnmGNP] [money] NULL,
	[fnmGNPCapita] [money] NULL,
	[fnmSource] [nvarchar](100) NULL,
	[fnmNote] [ntext] NULL,
	[fnmGNPCurrency] [nvarchar](3) NULL,
	[fnmGNPXchange] [float] NULL,
 CONSTRAINT [aaaaaGovtExpenditure1_PK] PRIMARY KEY NONCLUSTERED 
(
	[fnmYear] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[GovtExpenditure] TO [pFinanceWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[GovtExpenditure] TO [pFinanceWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[GovtExpenditure] TO [pFinanceWrite] AS [dbo]
GO
GRANT SELECT ON [dbo].[GovtExpenditure] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[GovtExpenditure] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[GovtExpenditure] ADD  CONSTRAINT [DF__GovtExpen__fnmEx__1920BF5C]  DEFAULT ((0)) FOR [fnmExpTotA]
GO
ALTER TABLE [dbo].[GovtExpenditure] ADD  CONSTRAINT [DF__GovtExpen__fnmEx__1A14E395]  DEFAULT ((0)) FOR [fnmExpTotB]
GO
ALTER TABLE [dbo].[GovtExpenditure] ADD  CONSTRAINT [DF__GovtExpen__fnmGN__1B0907CE]  DEFAULT ((0)) FOR [fnmGNP]
GO
ALTER TABLE [dbo].[GovtExpenditure] ADD  CONSTRAINT [DF__GovtExpen__fnmGN__1BFD2C07]  DEFAULT ((0)) FOR [fnmGNPCapita]
GO
ALTER TABLE [dbo].[GovtExpenditure] ADD  CONSTRAINT [DF__GovtExpen__fnmGN__1CF15040]  DEFAULT ((1)) FOR [fnmGNPXchange]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Macroeconomic date per year, such as government expenditure, GNP GNP per capita.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GovtExpenditure'
GO
EXEC sys.sp_addextendedproperty @name=N'pDiagramName', @value=N'Education Expenditure' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GovtExpenditure'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Finance' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GovtExpenditure'
GO

