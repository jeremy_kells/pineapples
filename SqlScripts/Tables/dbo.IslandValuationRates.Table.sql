SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IslandValuationRates](
	[imultID] [int] IDENTITY(1,1) NOT NULL,
	[imultYear] [int] NOT NULL,
	[iCode] [nvarchar](2) NOT NULL,
	[imultFactor] [decimal](6, 4) NOT NULL,
 CONSTRAINT [PK_IslandValuationRates] PRIMARY KEY CLUSTERED 
(
	[imultID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[IslandValuationRates] TO [pInfrastructureAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[IslandValuationRates] TO [pInfrastructureAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[IslandValuationRates] TO [pInfrastructureAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[IslandValuationRates] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[IslandValuationRates] TO [public] AS [dbo]
GO
SET ANSI_PADDING ON
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_IslandValuationRates] ON [dbo].[IslandValuationRates]
(
	[iCode] ASC,
	[imultYear] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[IslandValuationRates]  WITH CHECK ADD  CONSTRAINT [FK_IslandValuationRates_Islands] FOREIGN KEY([iCode])
REFERENCES [dbo].[Islands] ([iCode])
GO
ALTER TABLE [dbo].[IslandValuationRates] CHECK CONSTRAINT [FK_IslandValuationRates_Islands]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Island Multipliers. In a given year, for an island, the multiplier to apply to Estimated costs on Work Orders, and to Building Valuations. If not defined in any year for an island, defaults to 1. iCode - the primary key of Islands - is foreign key.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'IslandValuationRates'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Inrastructure' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'IslandValuationRates'
GO

