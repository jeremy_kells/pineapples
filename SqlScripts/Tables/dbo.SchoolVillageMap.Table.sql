SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SchoolVillageMap](
	[svmID] [int] IDENTITY(1,1) NOT NULL,
	[vilID] [int] NULL,
	[stCode] [nvarchar](3) NULL,
	[schNo] [nvarchar](50) NULL,
 CONSTRAINT [aaaaaSchoolVillageMap1_PK] PRIMARY KEY NONCLUSTERED 
(
	[svmID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON [dbo].[SchoolVillageMap] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[SchoolVillageMap] ADD  CONSTRAINT [DF__SchoolVil__vilID__3F115E1A]  DEFAULT ((0)) FOR [vilID]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cacthment area of school. Not used.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SchoolVillageMap'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Census' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SchoolVillageMap'
GO

