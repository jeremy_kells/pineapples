SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lkpAuthorityGovt](
	[codeCode] [nvarchar](1) NOT NULL,
	[codeDescription] [nvarchar](50) NULL,
	[codeSeq] [int] NULL,
	[codeDescriptionL1] [nvarchar](50) NULL,
	[codeDescriptionL2] [nvarchar](50) NULL,
 CONSTRAINT [aaaaalkpAuthorityGovt1_PK] PRIMARY KEY NONCLUSTERED 
(
	[codeCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[lkpAuthorityGovt] TO [pSchoolAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[lkpAuthorityGovt] TO [pSchoolAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpAuthorityGovt] TO [pSchoolAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[lkpAuthorityGovt] TO [pSchoolAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpAuthorityGovt] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[lkpAuthorityGovt] TO [public] AS [dbo]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Top level consolidatin for Authorities is Gov or Non-Govt. Effectively, system defined. 
Appears as foerign key on AuthorityTypes.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpAuthorityGovt'
GO
EXEC sys.sp_addextendedproperty @name=N'pDiagramName', @value=N'Authority Hierarchy' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpAuthorityGovt'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Authorities' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpAuthorityGovt'
GO

