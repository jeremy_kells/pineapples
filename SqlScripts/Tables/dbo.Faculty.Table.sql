SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Faculty](
	[facID] [int] IDENTITY(1,1) NOT NULL,
	[ssID] [int] NULL,
	[facName] [nvarchar](50) NULL,
	[facLocn] [nvarchar](200) NULL,
	[facM] [int] NULL,
	[facF] [int] NULL,
	[facTchrM] [int] NULL,
	[facTchrF] [int] NULL,
 CONSTRAINT [aaaaaFaculty1_PK] PRIMARY KEY NONCLUSTERED 
(
	[facID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[Faculty] TO [pSchoolRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[Faculty] TO [pSchoolWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[Faculty] TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Faculty] TO [pSchoolWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[Faculty] TO [public] AS [dbo]
GO
CREATE NONCLUSTERED INDEX [IX_Faculty_SSID] ON [dbo].[Faculty]
(
	[ssID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Faculty] ADD  CONSTRAINT [DF__Faculty__ssID__2F650636]  DEFAULT ((0)) FOR [ssID]
GO
ALTER TABLE [dbo].[Faculty]  WITH CHECK ADD  CONSTRAINT [Faculty_FK00] FOREIGN KEY([ssID])
REFERENCES [dbo].[SchoolSurvey] ([ssID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Faculty] CHECK CONSTRAINT [Faculty_FK00]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Used in Tertiary survey. Faculty records the name, and sumary of pupil and teachers for each faculty of a tertiary insitution.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Faculty'
GO
EXEC sys.sp_addextendedproperty @name=N'pDiagramName', @value=N'Tertiary Surveys' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Faculty'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Tertiary Sector' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Faculty'
GO

