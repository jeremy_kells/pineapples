SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserInfo](
	[uLogin] [nvarchar](30) NOT NULL,
	[uName] [nvarchar](50) NULL,
	[uLang] [smallint] NULL,
	[uEMail] [nvarchar](200) NULL,
	[uActive] [bit] NOT NULL,
	[uLoginTime] [datetime] NULL,
	[uLoginMachine] [nvarchar](50) NULL,
	[uLoginIP] [nvarchar](25) NULL,
	[uSystem] [nvarchar](50) NULL,
	[uContext] [nvarchar](2000) NULL,
 CONSTRAINT [aaaaaUserInfo1_PK] PRIMARY KEY NONCLUSTERED 
(
	[uLogin] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT INSERT ON [dbo].[UserInfo] TO [public] AS [dbo]
GO
GRANT SELECT ON [dbo].[UserInfo] TO [public] AS [dbo]
GO
GRANT UPDATE ON [dbo].[UserInfo] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[UserInfo] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[UserInfo] ADD  CONSTRAINT [DF__UserInfo__uLang__0A9D95DB]  DEFAULT ((0)) FOR [uLang]
GO
ALTER TABLE [dbo].[UserInfo] ADD  CONSTRAINT [DF_UserInfo_uActive]  DEFAULT ((0)) FOR [uActive]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'List of system users. Maintained when the user logs in, records date /time of last login/logout and language preference. 
Login events are now also recrded in ActivityLog, which should be used for this information.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserInfo'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Users' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserInfo'
GO

