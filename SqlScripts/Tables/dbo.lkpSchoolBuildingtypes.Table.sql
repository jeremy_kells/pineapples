SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lkpSchoolBuildingTypes](
	[codeCode] [nvarchar](10) NOT NULL,
	[codeDescription] [nvarchar](50) NULL,
	[codeSort] [nvarchar](50) NULL,
	[codeDescriptionL1] [nvarchar](50) NULL,
	[codeDescriptionL2] [nvarchar](50) NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pRowversion] [timestamp] NULL,
 CONSTRAINT [aaaaalkpSchoolBuildingtypes1_PK] PRIMARY KEY NONCLUSTERED 
(
	[codeCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[lkpSchoolBuildingTypes] TO [pAdminWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[lkpSchoolBuildingTypes] TO [pAdminWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[lkpSchoolBuildingTypes] TO [pAdminWrite] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpSchoolBuildingTypes] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[lkpSchoolBuildingTypes] TO [public] AS [dbo]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Used only for building types on ECE survey. May be removed in future revisions' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpSchoolBuildingTypes'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'ECE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpSchoolBuildingTypes'
GO

