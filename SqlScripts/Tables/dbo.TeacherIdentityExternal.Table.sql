SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TeacherIdentityExternal](
	[xtID] [int] IDENTITY(1,1) NOT NULL,
	[xtSrcID] [int] NULL,
	[xtSrc] [nvarchar](255) NULL,
	[xtSrcFile] [nvarchar](255) NULL,
	[xtRegister] [nvarchar](50) NULL,
	[xtPayroll] [nvarchar](50) NULL,
	[xtProvident] [nvarchar](50) NULL,
	[xtDOB] [datetime] NULL,
	[xtDOBEst] [bit] NULL,
	[xtSex] [nvarchar](1) NULL,
	[xtFullname] [nvarchar](200) NULL,
	[xtSurname] [nvarchar](50) NULL,
	[xtGiven] [nvarchar](50) NULL,
	[xtYearStarted] [int] NULL,
	[xtYearFinished] [int] NULL,
	[xtDatePSAppointed] [datetime] NULL,
	[xtDatePSClosed] [datetime] NULL,
	[xtCloseReason] [nvarchar](50) NULL,
	[xtSalaryCode] [nvarchar](20) NULL,
	[xtSalary] [money] NULL,
	[xtSalaryPointDate] [datetime] NULL,
	[xtSalaryPointNextIncr] [datetime] NULL,
	[xtError] [bit] NULL,
	[xtErrorUser] [nvarchar](50) NULL,
	[xtErrorDate] [datetime] NULL,
	[xtMatchID] [int] NULL,
	[xtSchNo] [nvarchar](50) NULL,
	[xtSchName] [nvarchar](100) NULL,
	[xtRole] [nvarchar](50) NULL,
	[xtRoleDesc] [nvarchar](100) NULL,
	[xtApptDate] [datetime] NULL,
	[xtpayptCode] [nvarchar](10) NULL,
	[xtActive] [bit] NOT NULL,
	[xtSalaryPoint] [nvarchar](20) NULL,
	[xtNameFirst] [nvarchar](50) NULL,
	[xtNameFirstSoundex] [nchar](4) NULL,
	[xtNameLast] [nvarchar](50) NULL,
	[xtNameLastSoundex] [nchar](4) NULL,
	[xtNameMiddle] [nvarchar](100) NULL,
	[xtNamePrefix] [nvarchar](50) NULL,
	[xtNameSuffix] [nvarchar](20) NULL,
	[xtMatchType] [nchar](2) NULL,
 CONSTRAINT [aaaaaTeacherIdentityExternal1_PK] PRIMARY KEY NONCLUSTERED 
(
	[xtID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[TeacherIdentityExternal] TO [pTeacherOps] AS [dbo]
GO
GRANT INSERT ON [dbo].[TeacherIdentityExternal] TO [pTeacherOps] AS [dbo]
GO
GRANT SELECT ON [dbo].[TeacherIdentityExternal] TO [pTeacherOps] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherIdentityExternal] TO [pTeacherOps] AS [dbo]
GO
GRANT SELECT ON [dbo].[TeacherIdentityExternal] TO [pTeacherReadX] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[TeacherIdentityExternal] TO [public] AS [dbo]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_TeacherIdentityExternal] ON [dbo].[TeacherIdentityExternal]
(
	[xtSrc] ASC,
	[xtMatchID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TeacherIdentityExternal] ADD  CONSTRAINT [DF__TeacherId__xtDOB__778AC167]  DEFAULT ((0)) FOR [xtDOBEst]
GO
ALTER TABLE [dbo].[TeacherIdentityExternal] ADD  CONSTRAINT [DF__TeacherId__xtErr__787EE5A0]  DEFAULT ((0)) FOR [xtError]
GO
ALTER TABLE [dbo].[TeacherIdentityExternal] ADD  CONSTRAINT [DF_TeacherIdentityExternal_xtActive]  DEFAULT ((1)) FOR [xtActive]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Holds teacher data imported from an external source, such as payroll. Payroll reconcile compare this to the live TeacherIdentity data, to fill missing fields on both sides.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TeacherIdentityExternal'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Teachers' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TeacherIdentityExternal'
GO

