SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Services](
	[svcID] [int] IDENTITY(1,1) NOT NULL,
	[svcName] [nvarchar](50) NULL,
	[svcType] [nvarchar](10) NULL,
	[svcAddr1] [nvarchar](50) NULL,
	[svcAddr2] [nvarchar](50) NULL,
	[svcAddr3] [nvarchar](50) NULL,
	[svcAddr4] [nvarchar](50) NULL,
 CONSTRAINT [aaaaaServices1_PK] PRIMARY KEY NONCLUSTERED 
(
	[svcID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[Services] TO [pSchoolRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[Services] TO [pSchoolWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[Services] TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Services] TO [pSchoolWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[Services] TO [public] AS [dbo]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Services - airports , clinics etc - that may service a school area.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Services'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Schools' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Services'
GO

