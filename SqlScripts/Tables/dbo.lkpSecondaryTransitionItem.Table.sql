SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lkpSecondaryTransitionItem](
	[codeCode] [nvarchar](10) NOT NULL,
	[codeDescription] [nvarchar](50) NULL,
	[codeSeq] [int] NULL,
	[codeDescriptionL1] [nvarchar](50) NULL,
	[codeDescriptionL2] [nvarchar](50) NULL,
 CONSTRAINT [aaaaalkpSecondaryTransitionItem1_PK] PRIMARY KEY NONCLUSTERED 
(
	[codeCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[lkpSecondaryTransitionItem] TO [pAdminWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[lkpSecondaryTransitionItem] TO [pAdminWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[lkpSecondaryTransitionItem] TO [pAdminWrite] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpSecondaryTransitionItem] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[lkpSecondaryTransitionItem] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[lkpSecondaryTransitionItem] ADD  CONSTRAINT [DF__lkpSecond__codeS__3C34F16F]  DEFAULT ((0)) FOR [codeSeq]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Secondary transaitions grid is made of these items, and related Secondary Transisitions code. These appear on PupilTables for the Transitions.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpSecondaryTransitionItem'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Enrolments' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpSecondaryTransitionItem'
GO

