SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TertiarySurveyExtAssess](
	[tassID] [int] IDENTITY(1,1) NOT NULL,
	[ssID] [int] NOT NULL,
	[tassDuration] [int] NULL,
	[tassDurationUnit] [nvarchar](10) NULL,
	[tassAssessor] [nvarchar](50) NULL,
	[tassType] [nvarchar](50) NULL,
 CONSTRAINT [PK_TertiarySurveyExtAssess] PRIMARY KEY CLUSTERED 
(
	[tassID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[TertiarySurveyExtAssess] TO [pSchoolRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[TertiarySurveyExtAssess] TO [pSchoolWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[TertiarySurveyExtAssess] TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TertiarySurveyExtAssess] TO [pSchoolWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[TertiarySurveyExtAssess] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[TertiarySurveyExtAssess]  WITH CHECK ADD  CONSTRAINT [FK_TertiarySurveyExtAssess_SchoolSurvey] FOREIGN KEY([ssID])
REFERENCES [dbo].[SchoolSurvey] ([ssID])
GO
ALTER TABLE [dbo].[TertiarySurveyExtAssess] CHECK CONSTRAINT [FK_TertiarySurveyExtAssess_SchoolSurvey]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Solomon Islands Tertiary Survey form only - records of external assessments' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TertiarySurveyExtAssess'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Tertiary' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TertiarySurveyExtAssess'
GO

