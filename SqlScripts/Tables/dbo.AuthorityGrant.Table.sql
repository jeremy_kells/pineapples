SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AuthorityGrant](
	[agID] [int] IDENTITY(1,1) NOT NULL,
	[authCode] [nvarchar](10) NOT NULL,
	[agYear] [int] NOT NULL,
	[agPaymentNo] [int] NOT NULL,
	[agEstimate] [int] NULL,
	[agRecalculated] [int] NULL,
	[agStatus] [nvarchar](10) NULL,
 CONSTRAINT [AuthorityGrant_PK] PRIMARY KEY NONCLUSTERED 
(
	[agID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[AuthorityGrant] TO [pFinanceReadX] AS [dbo]
GO
GRANT DELETE ON [dbo].[AuthorityGrant] TO [pFinanceWriteX] AS [dbo]
GO
GRANT INSERT ON [dbo].[AuthorityGrant] TO [pFinanceWriteX] AS [dbo]
GO
GRANT UPDATE ON [dbo].[AuthorityGrant] TO [pFinanceWriteX] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[AuthorityGrant] TO [public] AS [dbo]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [authCode] ON [dbo].[AuthorityGrant]
(
	[authCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AuthorityGrant] ADD  CONSTRAINT [DF__Authority__agEst__38079B34]  DEFAULT ((0)) FOR [agEstimate]
GO
ALTER TABLE [dbo].[AuthorityGrant] ADD  CONSTRAINT [DF__Authority__agRec__38FBBF6D]  DEFAULT ((0)) FOR [agRecalculated]
GO
ALTER TABLE [dbo].[AuthorityGrant]  WITH CHECK ADD  CONSTRAINT [AuthoritiesAuthorityGrant] FOREIGN KEY([authCode])
REFERENCES [dbo].[Authorities] ([authCode])
GO
ALTER TABLE [dbo].[AuthorityGrant] CHECK CONSTRAINT [AuthoritiesAuthorityGrant]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Record of a grant payment to an Authority.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AuthorityGrant'
GO
EXEC sys.sp_addextendedproperty @name=N'pDataAccessDB', @value=N'linked table/bound form' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AuthorityGrant'
GO
EXEC sys.sp_addextendedproperty @name=N'pDataAccessUI', @value=N'Authority Grants form' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AuthorityGrant'
GO
EXEC sys.sp_addextendedproperty @name=N'pDiagramName', @value=N'School Grants' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AuthorityGrant'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'School Grants' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AuthorityGrant'
GO

