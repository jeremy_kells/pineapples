SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[schoolSurveyQualityRooms]
AS
SELECT SchoolSurveyQuality.*
FROM SchoolSurveyQuality
WHERE (((SchoolSurveyQuality.ssqDataItem)='Rooms'))
GO

