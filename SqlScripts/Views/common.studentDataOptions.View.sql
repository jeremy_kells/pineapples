SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 2018
-- Description:	student data options
--
--- Placeholder view to get Filter to work
--- Pivot table and chart feature not priority
-- =============================================
CREATE VIEW [common].[studentDataOptions]
AS
Select C, N, seq
from
(
Select 'total' c
, 'Total' N
, 0 seq
UNION
Select 'enrol' C
, 'Enrol' N
, 1 seq
UNION
Select 'percF' C
, 'Female %' N
, 2 seq

) S
GO

