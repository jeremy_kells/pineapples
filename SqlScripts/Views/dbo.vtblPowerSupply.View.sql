SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vtblPowerSupply]
AS
SELECT     resID, ssID, resName, resSplit AS PowerSupplyType, resAvail, resNumber, resCondition
FROM         dbo.Resources
WHERE     (resName = 'Power Supply')
GO

