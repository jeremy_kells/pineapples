SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Brian Lewis
-- Create date: 2017
-- Description:
-- Fundamental list of enrolment at lowest aggregation level - school, year classLevel age gender
-- the purpose of this view is to Include estimates.
-- Normalised by Gender (cf measureEnrol)
-- Used in the creation of data warehouse
-- Related higher aggregates are:
--   -- measureEnrolLevelAgeG (national totals)
--   -- measureEnrolSchoolG ( school totals)
-- =============================================
CREATE VIEW [dbo].[measureEnrolG]
AS
SELECT schNo
, SurveyYear
, Estimate

, SurveyDimensionID
, ClassLevel
, Age
, G.genderCode
, case G.genderCode
	when 'M' then EnrolM
	when 'F' then EnrolF
end Enrol
from measureEnrol E
	CROSS JOIN DimensionGender G
GO

