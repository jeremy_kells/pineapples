SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Brian Lewis
-- Create date: 2017
-- Description:
-- Counts Boys and Girls toilets, and includes estimates
-- Normalised by gender
-- Used in the creation of data warehouse
-- =============================================
CREATE VIEW [dbo].[measureToiletsG]
AS
SELECT
schNo
, SurveyYear
, Estimate
, SurveyDimensionID
, YearOfData
, G.genderCode
, case G.genderCode
	when 'M' then NumToiletsM
	when 'F' then NumToiletsF
end NumToilets
from measureToilets
	CROSS JOIN DimensionGender G
GO

