SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vtblDormitories]
AS
SELECT
Rooms.ssID,
Rooms.rmType,
Rooms.rmSize,
Rooms.rmYear,
Rooms.rmMaterials,
Rooms.rmCapM,
Rooms.rmCapF,
Rooms.rmShowersM,
Rooms.rmShowersF,
Rooms.rmToiletsM AS toiletsM,
Rooms.rmToiletsF AS toiletsF,
(Isnull([rmtoiletsM],0) + IsNull([rmtoiletsF],0)) as Toilets
FROM
dbo.SchoolSurvey INNER JOIN dbo.Rooms ON dbo.SchoolSurvey.ssID = dbo.Rooms.ssID
WHERE (((dbo.Rooms.rmType)='DORM'))
GO

