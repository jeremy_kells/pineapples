SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[TRSchoolRegistration]
AS
SELECT     rxCode,
isnull(CASE dbo.userLanguage()
	WHEN 0 THEN rxDescription
	WHEN 1 THEN rxDescriptionL1
	WHEN 2 THEN rxDescriptionL2

END,rxDescription) AS rxDescription
FROM         dbo.lkpSchoolRegistration
GO
GRANT SELECT ON [dbo].[TRSchoolRegistration] TO [public] AS [dbo]
GO

