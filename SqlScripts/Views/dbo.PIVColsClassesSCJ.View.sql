SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 5 11 2013
-- Description:	Report on composite and joint teacher classes
--
-- see PIVClasses_SCJ
-- =============================================
CREATE VIEW [dbo].[PIVColsClassesSCJ]
AS

---------------------------------------
Select DISTINCT
ssID
, C.subjCode
, COUNT(DISTINCT pclLevel) NumLevels
, COUNT(DISTINCT tID) NumTeachers
, case when COUNT(DISTINCT pclLevel)  > 1 then 'C'
		when COUNT(DISTINCT pclLevel) is null then null
		else 'S' end Composite
, case when COUNT(DISTINCT pclLevel)  > 1 then 'J'
		when COUNT(DISTINCT pclLevel) is null then null
		else 'S' end JointTeacher
,  min(lvlYear) MinYearOfEd
,  max(lvlYear) MaxYearOfEd
from Classes C
	LEft Join ClassLevel CL
		ON C.pcID = CL.pcID
	LEFT JOIN lkpLevels L
		ON L.codeCode  = CL.pclLevel
	LEft Join ClassTeacher CT
		ON C.pcID = CT.pcID

GROUP BY C.pcID
, C.ssID
, C.subjCode
GO

