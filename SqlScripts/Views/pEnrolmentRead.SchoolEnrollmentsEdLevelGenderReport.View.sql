SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 20 5 2013
-- Description:	View to show estimates enrolments by school by year crafted for School Reports
-- =============================================
CREATE VIEW [pEnrolmentRead].[SchoolEnrollmentsEdLevelGenderReport]
WITh VIEW_METADATA
AS
select schNo
, LifeYear AS Year
, E.enLevel
, E.enM
, E.enF
from dbo.tfnESTIMATE_BestSurveyEnrolments() EBSE
INNER JOIN Enrollments E
	ON EBSE.bestssID = E.ssID
GO

