SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ISCEDLevelSchoolTypeMap]
AS
SELECT     TOP 100 PERCENT dbo.DimensionLevel.[ISCED Level], dbo.metaSchoolTypeLevelMap.stCode, MIN(dbo.lkpLevels.lvlYear) AS MinOflvlYear,
                      MAX(dbo.lkpLevels.lvlYear) AS MaxOflvlYear
FROM         dbo.lkpLevels INNER JOIN
                      dbo.metaSchoolTypeLevelMap ON dbo.lkpLevels.codeCode = dbo.metaSchoolTypeLevelMap.tlmLevel INNER JOIN
                      dbo.metaSchoolTypeLevelMap AS metaSchoolTypeLevelMap_1 ON dbo.metaSchoolTypeLevelMap.stCode = metaSchoolTypeLevelMap_1.stCode INNER JOIN
                      dbo.DimensionLevel ON metaSchoolTypeLevelMap_1.tlmLevel = dbo.DimensionLevel.LevelCode
GROUP BY dbo.DimensionLevel.[ISCED Level], dbo.metaSchoolTypeLevelMap.stCode
ORDER BY dbo.DimensionLevel.[ISCED Level], MinOflvlYear, MaxOflvlYear
GO
GRANT DELETE ON [dbo].[ISCEDLevelSchoolTypeMap] TO [pAdminAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[ISCEDLevelSchoolTypeMap] TO [pAdminAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[ISCEDLevelSchoolTypeMap] TO [pAdminAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[ISCEDLevelSchoolTypeMap] TO [public] AS [dbo]
GO

