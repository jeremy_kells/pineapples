SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date:
-- Description:	Nation Flow
--
-- Wrapper around warehouse.nationCohort to include flow calculations
-- ie PromoteRate, RepeatRate, SurvivalRate (single year survival aka "transition rate")
-- This version is intended to be used in reports, use nationCohort for "cubes"
-- (pivot tables, tableau) where ratios should be calculated based on current aggregations.
-- TRANSFER versions:
-- field named xxxxTR calculate the same ratios but attempt to take into account internal transfers
-- These should be used with caution as internal transfer data may not be very reliable
-- See related views:
-- warehouse.districtCohort
-- warehouse.districtFlow
-- warehouse.nationCohort
-- warehouse.cohort (table)
-- warehouse.schoolFlow
-- =============================================
CREATE VIEW [warehouse].[NationFlow]
WITH VIEW_METADATA
AS

Select S.*
, 1 - RepeatRate - PromoteRate DropoutRate
, 1 - RepeatRateTR - PromoteRateTR DropoutRateTR
, case when RepeatRate = 1 then null else PromoteRate / (1 - RepeatRate) end SurvivalRate
, case when RepeatRateTR = 1 then null else PromoteRateTR / (1 - RepeatRateTR) end SurvivalRateTR
FROM
(
	Select C.*
	, convert(float,RepNY) /Enrol RepeatRate
	, convert(float,RepNY) / (Enrol - isnull(TroutNY,0)) RepeatRateTR
	, ( convert(float,EnrolNYNextLevel) - RepNYNextLevel)  / Enrol PromoteRate
	, ( convert(float,EnrolNYNextLevel) - RepNYNextLevel - - isnull(TrinNYNextLevel,0))  / ( Enrol - isnull(TroutNY,0)) PromoteRateTR
	from warehouse.NationCohort C

) S
GO

