SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	  Brian Lewis
-- Mopdified: 26 6 2018
-- Description:	Wrapper around Islands, to provide the Distrct full name
-- Used for Island lookup in web site
-- =============================================
CREATE VIEW [dbo].[lkpIslands]
AS
SELECT TOP 100 PERCENT  -- top clause allows View to sort
iCode
, iName
, dName
, iGroup
, iOuter
FROM  Islands
	LEFT JOIN Districts
		ON Districts.dID = dbo.Islands.iGroup
ORDER BY iName
GO
GRANT DELETE ON [dbo].[lkpIslands] TO [pAdminAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[lkpIslands] TO [pAdminAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[lkpIslands] TO [pAdminAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpIslands] TO [public] AS [dbo]
GO

