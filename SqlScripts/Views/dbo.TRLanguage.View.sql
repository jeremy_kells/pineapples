SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[TRLanguage]
AS
SELECT     langCode, langName, langGrp, langSystem
FROM         dbo.lkpLanguage
GO
GRANT SELECT ON [dbo].[TRLanguage] TO [public] AS [dbo]
GO

