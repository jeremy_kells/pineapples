SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [pInfrastructureRead].[PIVWorkItemsView]
AS
SELECT
	WI.witmID
	, WI.schNo
	, WO.woRef
	, WO.woStatus [WO Status]
	, WI.witmType [Work Type]
	, WIT.witGRoup [Work Class]

	, plannedStart
	, year(plannedStart) PlannedStartYear
	, common.formatDateTime(plannedStart,'YYYY MM') PlannedStartYYYYMM

	, witmProgressDate ProgressDate
	, common.FormatDateTime(witmProgressDate,'YYYY MM') ProgressYYYYMM
	, year(witmprogressDate) ProgressYear

	, PlannedCompletion
	, year(plannedCompletion) PlannedCompletionYear
	, common.FormatDateTime(PlannedCompletion,'YYYY MM') PlannedCompletionYYYYMM

	,witmEstCost EstCost
	,witmActualCost ActualCost
	, witmContractValue ContractValue
	,[witmRoomsClass] [NewClassroom]
      ,[witmRoomsOHT] [NewOHT]
      ,[witmRoomsStaff] NewStaffroom
      ,[witmRoomsAdmin] NewOffice
      ,[witmRoomsStorage] NewStoreroom
      ,[witmRoomsDorm]	NewDormRoom
      ,[witmRoomsKitchen] NewKitchen
      ,[witmRoomsDining] NewDining
      ,[witmRoomsLibrary] NewLibrary
      ,[witmRoomsSpecialTuition] NewSpecialTuitionRoom
      ,[witmRoomsHall] NewManeaba
      ,[witmRoomsHall] NewHall
      ,[witmRoomsOther] NewOtherRoom
FROM

	(
	Select WI.witmID
		, isnull(witmScheduled, woPlanned) PlannedStart
		, case when witmEstDuration is null then woPlannedCompletion else dateadd(d,witmEstDuration,isnull(witmScheduled, woPlanned)) end PlannedCompletion
	FROM WorkItems WI
		INNER JOIN WorkORders WO
			ON WO.wOID = WI.woID
	) ITOT
INNER JOIN WorkItems WI
	ON ITOT.witmID = WI.witmID
INNER JOIN WorkORders WO
	ON Wo.woID = WI.woID
 LEFT JOIN TRWorkItemProgress WIP
	ON WI.witmProgress = WIP.codeCode
 LEFT JOIN TRWorkITemType WIT
	ON WI.witmType = WIT.codeCode
INNER JOIN Schools
	ON WI.schNo = Schools.SchNo
GO

