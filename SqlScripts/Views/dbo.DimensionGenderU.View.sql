SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date:
-- Description:	Dimension Gender (including not specified)
--
-- Simple dimension to all gender name to be used in place of gender code
-- Can handle cases where gender is not specified, is null is replaced with '<>'
-- Select isnull(gender, '<>') Gender .....
-- =============================================
CREATE VIEW [dbo].[DimensionGenderU]
AS
SELECT     codeCode AS GenderCode, codeDescription AS Gender
FROM         dbo.TRGender
UNION Select '<>', '<not specified>'
GO

