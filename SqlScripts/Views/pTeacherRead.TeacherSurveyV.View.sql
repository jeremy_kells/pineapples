SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [pTeacherRead].[TeacherSurveyV]
WITH VIEW_METADATA
AS
	SELECT TS.*
		, TI.tRegister
		, TI.tPayroll
		, TI.tProvident
		, TI.tDOB
		, TI.tSex
		, TI.tNamePrefix
		, TI.tGiven
		, TI.tMiddleNames
		, TI.tSurname
		, TI.tNameSuffix
		, TI.tYearStarted
		, TI.tSrcID
		, TI.tSrc
		, TI.tComment
		, TI.tDOBEst
		, TI.pEditContext
		, SS.svyYear
		, SS.ssSchType
		, Schools.schNo
		, Schools.schName
		, ST.statusDesc
		, RL.codeDescription RoleDesc
		, convert(int,
			case when isnull(tSurname,'')  <> isnull(tchFamilyName,'') then 1
			when isnull(tGiven,'')  <> isnull(tchFirstName,'') then 1
			when isnull(tSex,'') <> isnull(tchGender,'') then 1
			when isnull(tDob,'1901-01-01') <> tchDoB then 1
			when isnull(tPayroll,'') <> tchEmplNo then 1
			when isnull(tProvident,'') <> tchProvident then 1
			when isnull(tRegister,'') <> tchRegister then 1
			when isnull(tYearStarted,0) <> tchYearStarted then 1
			else 0
			end
			) IdentityConflict
		, convert(int,
				case when SS.svyYear = MaxYear then 1 else 0 end
			) LatestSurvey
	FROM
		TeacherSurvey TS
			INNER JOIN TeacherIdentity TI
				ON TS.tID = TI.tID
			INNER JOIN SchoolSurvey SS
				ON TS.ssID = SS.ssID
			INNER JOIN 	Schools
				ON Schools.schNo = SS.schNo
			LEFT JOIN TRTeacherStatus ST
				ON ST.statusCode = TS.tchStatus
			LEFT JOIN TRTeacherRole RL
				ON RL.codeCode = TS.tchRole

			LEFT JOIN
			(Select tID,  max(svyYear) MaxYear
				FROM TeacherSurvey TS
					INNER JOIN SchoolSurvey SS
						ON TS.ssID = SS.ssID
				GROUP BY tID
			) MAXY
			ON MAXY.tID = TS.tID
				AND MAXY.MaxYear = SS.svyYear
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 29 3 2014
-- Description:	Update the TEacherSurveyV
-- =============================================
CREATE TRIGGER [pTeacherRead].[TeacherSurveyV_Update]
   ON  [pTeacherRead].[TeacherSurveyV]
   INSTEAD OF Update, Insert, Delete
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @identity int = null
	-- first write teacher identity records if needed
	
    -- updates on TeacherSurvey
    UPDATE TeacherSurvey
		SET tchSort = I.tchSort
           ,tchEmplNo = I.tchEmplNo
           ,tchRegister = I.tchREgister
           ,tchProvident = I.tchProvident
           ,tchUnion = I.tchUnion
           ,tchNamePrefix = I.tchNamePrefix
           ,tchFirstName = I.tchFirstName
           ,tchMiddleNames = I.tchMiddleNames
           ,tchFamilyName = I.tchFamilyName
           ,tchNameSuffix = I.tchNameSuffix
           ,tchSalaryScale = I.tchSalaryScale
           ,tchSalary = I.tchSalary
           ,tchDOB = I.tchDoB
           ,tchGender = I.tchGender
           ,tchCitizenship = I.tchCitizenship
           ,tchIsland = I.tchIsland
           ,tchDistrict = I.tchDistrict
           ,tchSponsor = I.tchSponsor
           ,tchFTE = I.tchFTE
           ,tchFullPart = I.tchFullPart
           ,tchYears = I.tchYears
           ,tchYearsSchool = I.tchYearsSchool
           ,tchTrained = I.tchTrained
           ,tchEdQual = I.tchEdQual
           ,tchQual = I.tchQual
           ,tchSubjectTrained = I.tchSubjectTrained

           ,tchCivilStatus = I.tchCivilStatus
           ,tchNumDep = I.tchNumDep
           ,tchHouse = I.tchHouse
           ,tchSubjectMajor = I.tchSubjectMajor
           ,tchSubjectMinor = I.tchSubjectMinor
           ,tchSubjectMinor2 = I.tchSubjectMinor2
           ,tchClass = I.tchClass
           ,tchClassMax = I.tchClassMax
           ,tchJoint = I.tchJoint
           ,tchComposite = I.tchComposite
           ,tchStatus = I.tchStatus
           ,tchRole = I.tchRole
           ,tchTAM = I.tchTAM
           ,tID = I.tID
           ,tchECE = I.tchECE
           ,tchInserviceYear = I.tchInserviceYear
           ,tchInservice = I.tchInService
           ,tchSector = I.tchSector
           ,tchYearStarted = I.tchYearStarted
           ,tchSpouseFirstName = I.tchSpouseFirstName
           ,tchSpouseFamilyName = I.tchSpouseFamilyName
           ,tchSpouseOccupation = I.tchSpouseOccupation
           ,tchSpouseEmployer = I.tchSpouseEmployer
           ,tchSpousetID = I.tchSpousetID
           ,tchLangMajor = I.tchLangMajor
           ,tchLangMinor = I.tchLangMinor
           ,tchsmisID = I.tchsmisID
           ,tchApptTerms = I.tchApptTerms
           ,tchSalaryCurr = I.tchSalaryCurr
           ,tchSalaryPeriod = I.tchSalaryPeriod

    
		
		FROM TeacherSurvey
			INNER JOIN INSERTED I
				ON TeacherSurvey.tchsID = I.tchsID
				
	INSERT INTO TEacherSurvey
	(
			ssID
           ,tchSort
           ,tchEmplNo
           ,tchRegister
           ,tchProvident
           ,tchUnion
           ,tchNamePrefix
           ,tchFirstName
           ,tchMiddleNames
           ,tchFamilyName
           ,tchNameSuffix
           ,tchSalaryScale
           ,tchSalary
           ,tchDOB
           ,tchGender
           ,tchCitizenship
           ,tchIsland
           ,tchDistrict
           ,tchSponsor
           ,tchFTE
           ,tchFullPart
           ,tchYears
           ,tchYearsSchool
           ,tchTrained
           ,tchEdQual
           ,tchQual
           ,tchSubjectTrained
           ,tchCivilStatus
           ,tchNumDep
           ,tchHouse
           ,tchSubjectMajor
           ,tchSubjectMinor
           ,tchSubjectMinor2
           ,tchClass
           ,tchClassMax
           ,tchJoint
           ,tchComposite
           ,tchStatus
           ,tchRole
           ,tchTAM
           ,tID
           ,tchECE
           ,tchInserviceYear
           ,tchInservice
           ,tchSector
           ,tchYearStarted
           ,tchSpouseFirstName
           ,tchSpouseFamilyName
           ,tchSpouseOccupation
           ,tchSpouseEmployer
           ,tchSpousetID
           ,tchLangMajor
           ,tchLangMinor
           ,tchsmisID
           ,tchApptTerms
           ,tchSalaryCurr
           ,tchSalaryPeriod
           )
    
	SELECT
	       ssID
           ,tchSort
           ,tchEmplNo
           ,tchRegister
           ,tchProvident
           ,tchUnion
           ,tchNamePrefix
           ,tchFirstName
           ,tchMiddleNames
           ,tchFamilyName
           ,tchNameSuffix
           ,tchSalaryScale
           ,tchSalary
           ,tchDOB
           ,tchGender
           ,tchCitizenship
           ,tchIsland
           ,tchDistrict
           ,tchSponsor
           ,tchFTE
           ,tchFullPart
           ,tchYears
           ,tchYearsSchool
           ,tchTrained
           ,tchEdQual
           ,tchQual
           ,tchSubjectTrained
           ,tchCivilStatus
           ,tchNumDep
           ,tchHouse
           ,tchSubjectMajor
           ,tchSubjectMinor
           ,tchSubjectMinor2
           ,tchClass
           ,tchClassMax
           ,tchJoint
           ,tchComposite
           ,tchStatus
           ,tchRole
           ,tchTAM
           ,tID
           ,tchECE
           ,tchInserviceYear
           ,tchInservice
           ,tchSector
           ,tchYearStarted
           ,tchSpouseFirstName
           ,tchSpouseFamilyName
           ,tchSpouseOccupation
           ,tchSpouseEmployer
           ,tchSpousetID
           ,tchLangMajor
           ,tchLangMinor
           ,tchsmisID
           ,tchApptTerms
           ,tchSalaryCurr
           ,tchSalaryPeriod
       FROM INSERTED I
       WHERE I.tchsID is null
       
       Select @identity = SCOPE_IDENTITY()
       
       DELETE FRom TEacherSurvey
       WHERE TchsID in (Select tchsID from DELETED)
       AND tchsID not in (select tchsID from INSERTED WHERE tchsID is not null)
       
       -- update the teacher identity, if this is the most recent survey
       
  
 /*
 
 '
' if this is the most recent survey, prepare to update the name
' STATEMENT OF RULES for Updating TeacherIdentity:-
' The value must not be null
' AND
'    - the current value in TeacherIdentity IS Null
' OR
'    - must prompt

 
 */     
       UPDATE TeacherIdentity
			set tSurname = coalesce(TeacherIdentity.tSurname,tchFamilyName)
			, tGiven = coalesce(TeacherIdentity.tGiven,tchFirstName)
			, tDoB = coalesce(TeacherIdentity.tDoB,tchDOB )
			, tSex = coalesce(TeacherIdentity.tSex,tchGender)
			, tPayroll = coalesce(TeacherIdentity.tPayroll,tchEmplNo)
			, tProvident = coalesce(TeacherIdentity.tProvident,tchProvident)
			, tRegister = coalesce(TeacherIdentity.tRegister,tchRegister)
			, tYearStarted = coalesce(TeacherIdentity.tYearStarted,tchYearStarted )
       FROM TeacherIdentity
			INNER JOIN INSERTED 
				ON TeacherIdentity.tID = INSERTED.tID
			INNER JOIN 
			(Select tID,  max(svyYear) MaxYear
				FROM TeacherSurvey TS
					INNER JOIN SchoolSurvey SS
						ON TS.ssID = SS.ssID
				GROUP BY tID
			) MAXY
			ON MAXY.tID = INSERTED.tID
				AND MAXY.MaxYear = INSERTED.svyYear
		
		-- restore the identity from SCOPE_IDENTITY	if necessary	
		exec common.ForgeIdentity @identity
				
END

GO

