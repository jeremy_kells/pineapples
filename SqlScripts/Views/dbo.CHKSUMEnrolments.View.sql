SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CHKSUMEnrolments]
AS
SELECT     svyYear, schNo, ssSchType, ssEnrolM AS M, ssEnrolF AS F, ssEnrol AS Total
FROM         dbo.SchoolSurvey
GO
GRANT DELETE ON [dbo].[CHKSUMEnrolments] TO [pAdminAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[CHKSUMEnrolments] TO [pAdminAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[CHKSUMEnrolments] TO [pAdminAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[CHKSUMEnrolments] TO [public] AS [dbo]
GO

