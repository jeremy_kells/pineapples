SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 5 11 2013
-- Description:	Report on subjects offered from the classes / class level table
--
-- see PIVSurvey_SubjectsOffered
-- =============================================
CREATE VIEW [dbo].[PIVColsSubjectOffered]
AS
---------------------------------------
-- repairs and maintenance decoded
---------------------------------------
Select SS.ssID
, SS.subjCode subjectCode
, S.subjName SubjectName
, DL.*
, 1 as SubjectTaught
FROM
(
Select DISTINCT
ssID
, C.subjCode
, pclLevel
from Classes C
INNER JOIN ClassLevel CL
	ON C.pcID = CL.pcID
) SS
INNER JOIN lkpSubjects S
	ON SS.subjCode = S.subjCode
INNER JOIN DimensionLevel DL
	ON SS.pclLevel =  DL.levelCode
GO

