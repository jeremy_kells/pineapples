SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[qrySchoolSurveyNumbers]
as

SELECT
Schools.schNo,
Schools.schType,
Schools.schName,
Schools.schVillage,
Islands.iName,
Districts.dName,
Schools.schLang,
Schools.schAuth,
Max(SchoolSurvey.ssPrinSurname) AS ssPrinSurname,
Min(SchoolSurvey.svyYear) AS MinYear,
Max(SchoolSurvey.svyYear) AS MaxYear,
Count(SchoolSurvey.ssID) AS NumSurveys
FROM Districts RIGHT JOIN ((Islands RIGHT JOIN Schools ON Islands.iCode = Schools.iCode) LEFT JOIN SchoolSurvey ON Schools.schNo = SchoolSurvey.schNo) ON Districts.dID = Islands.iGroup
GROUP BY Schools.schNo, Schools.schType, Schools.schName, Schools.schVillage, Islands.iName, Districts.dName, Schools.schLang, Schools.schAuth;
GO

