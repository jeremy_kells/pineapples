SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[DimensionSchoolSurvey]
AS
SELECT
DimensionSchoolSurveyNoYear.[Survey Data Year] AS [Survey Year],
DimensionSchoolSurveyNoYear.*
FROM
DimensionSchoolSurveyNoYear
GO

