SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Brian Lewis
-- Create date: 2017
-- Description:
-- Counts Boys and Girls toilets, and includes estimates
-- Used in the creation of data warehouse
-- Normalised by gender in measureToiletsG
-- =============================================
CREATE VIEW [dbo].[measureToilets]
AS
SELECT
schNo
, LifeYear SurveyYear
, Estimate
, SurveyDimensionssID SurveyDimensionID
, ET.bestYear YearOfData
, sum(case toiUse WHEN 'Boys' then toiNum else null end) NumToiletsM
, sum(case toiUse WHEN 'Girls' then toiNum else null end) NumToiletsF

from dbo.tfnESTIMATE_BestSurveyToilets() ET
INNER JOIN Toilets T
ON ET.bestssID = T.ssID

GROUP BY
schNo, LifeYear, Estimate
, SurveyDimensionssID , bestYear
GO

