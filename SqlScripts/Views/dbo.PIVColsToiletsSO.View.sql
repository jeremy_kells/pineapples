SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[PIVColsToiletsSO]
AS
SELECT
	ssID,
	toiNum Number,

	case toiCondition
		when 'A' then 'Y'
		when 'B' then 'N'
		when 'C' then 'N'
		else toiCondition
	end as CleanYN,
	case when toiCondition in ('Y', 'A') then 1 end Clean,
	toiYN as HasWaterYN,
	case when toiYN = 'Y' then 1 end HasWater,

	lkpToiletTypes.ttypName AS [Type],
	Toilets.toiUse AS UsedBy
	-- yn is not included
FROM Toilets
INNER JOIN lkpToiletTypes
	ON lkpToiletTypes.ttypName = Toilets.toiType
GO

