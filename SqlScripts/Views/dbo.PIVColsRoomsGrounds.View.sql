SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
------------------------------------------------------
CREATE VIEW [dbo].[PIVColsRoomsGrounds]
AS
SELECT
R.rmType [Code],
lkpRoomTypes.codeDescription AS [Type],
R.rmID,
R.rmYear AS YearBuilt,

R.rmSize AS Area,
case when (rmSize > 0 ) then 1 else 0 end AS AreaSupplied,
R.rmCondition AS Condition,
1 AS Number,

ssID [ssID]

FROM Rooms as R
	INNER JOIN lkpRoomTypes
		ON lkpRoomTypes.codeCode = R.rmType
GO

