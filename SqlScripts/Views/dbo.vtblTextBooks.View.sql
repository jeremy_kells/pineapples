SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vtblTextBooks]
AS
-- 22 03 2009
-- some aliases created
SELECT
Resources.resID,
Resources.ssID,
Resources.resName,
Resources.resLevel,
Resources.resSplit,
Resources.resNumber,
Resources.resCondition,
Resources.resAvail,
Resources.resAdequate

FROM dbo.Resources
WHERE (((Resources.resName)='Text books') or Resources.resName = 'Readers')
GO

