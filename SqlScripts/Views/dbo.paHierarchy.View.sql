SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[paHierarchy]
WITH VIEW_METADATA
AS
Select *
FROM
(

select pafrmCode
, indFullID FullID
, paIndID ID
, paindText [Text]
, 'I' Level
from paIndicators
UNION

select pafrmCode
, elmFullID
, paelmID
, paelmDescription Text
, 'E'
from paElements
UNION

select pafrmCode
, comFullID
, pacomID
, paComDescription
, 'C'
from paCompetencies

UNION
Select pafrmCode
, pafrmCode
, null
, pafrmDescription
, 'F'
from paFrameworks
) U
GO

