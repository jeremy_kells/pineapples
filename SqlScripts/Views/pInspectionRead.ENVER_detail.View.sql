SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW pInspectionREad.[ENVER_detail]
WITH VIEW_METADATA
AS
Select *

FROM
(
Select I.schNo
, L.codeCode clsLevel
, lvlYear YearOfEd
, case lvlYear
	when 0 then I.M0
	when 1 then I.M1
	when 2 then I.M2

end M
, case lvlYear
	when 0 then I.F0
	when 1 then I.F1
	when 2 then I.F2

end F
from pInspectionRead.ENVER I
CROSS JOIN lkpLevels L
) SUB
WHERE coalesce(SUB.M,SUB.F) is not null
GO

