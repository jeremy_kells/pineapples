﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;
using System.Data;
using System.Data.SqlClient;
using System.Security.Claims;

namespace Pineapples.Data
{
    public interface IDSTeacher : IDSCrud<TeacherBinder, int>
    {
        IDataResult Filter(TeacherFilter fltr);
        IDataResult Table(string rowsplit, string colsplit, TeacherFilter fltr);
        IDataResult Geo(string geoType, TeacherFilter fltr);

        // Teacher has a specialised Read
        IDataResult Read(int teacherID, bool readX);
        // access control
        void AccessControl(int teacherID, ClaimsIdentity identity);
    }
}
