﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pineapples.Data.DB;
using Softwords.DataTools;
using System.Data;
using System.Data.SqlClient;

namespace Pineapples.Data
{
    public interface IDSPerfAssess
    {
        Models.PerfAssessment Template(string framework, int? teacherId);
        IDataResult Filter(PerfAssessFilter fltr);
        IDataResult Table(PerfAssessTableFilter fltr);
        
        // crud methods
        IDataResult Save(Models.PerfAssessment pf);
        IDataResult Read(int Id);
    }
}
