using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
    [Table("lkpSubjects")]
    [Description(@"Subjects taught on schools. Classes may refer to a subject, as well as recources. Teacher are trained to teach a subject, and Teacher Survey may record up to 3 subject taught by the teacher. Teachers can be searched by subject. Subject withy a non-zero sort code appear in the Enrolments by Subject grid.")]
    public partial class Subject : SequencedCodeTable
    {
        [Key]

        [Column("subjCode", TypeName = "nvarchar")]
        [MaxLength(10)]
        [StringLength(10)]
        [Required(ErrorMessage = "subj Code is required")]
        [Display(Name = "subj Code")]
        public override string Code { get; set; }

        [Column("subjName", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Required(ErrorMessage = "subj Name is required")]
        [Display(Name = "subj Name")]
        public override string Description { get; set; }

        [Column("subjSort", TypeName = "int")]
        [Display(Name = "subj Sort")]
        public override int? Seq { get; set; }

        [Column("ifID", TypeName = "int")]
        [Display(Name = "if ID")]
        public int? ifID { get; set; }

        [Column("subjNameL1", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "subj Name L1")]
        public override string DescriptionL1 { get; set; }

        [Column("subjNameL2", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "subj Name L2")]
        public override string DescriptionL2 { get; set; }
    }
}
