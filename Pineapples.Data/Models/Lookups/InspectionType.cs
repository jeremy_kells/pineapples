using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
    [Table("lkpInspectionTypes")]
    [Description(@"Types of school inspections. An inspection type may have a customised display form. BLDG - BuildingReview - is system defined. Foreign key on InspectionSet, which in turn is foreign key on SchoolInspections.")]
    public partial class InspectionType : SimpleCodeTable
    {
        [Key]

        [Column("intyCode", TypeName = "nvarchar")]
        [MaxLength(10)]
        [StringLength(10)]
        [Required(ErrorMessage = "inty Code is required")]
        [Display(Name = "inty Code")]
        public override string Code { get; set; }

        [Column("intyDesc", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "inty Desc")]
        public override string Description { get; set; }

        [Column("intyForm", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "inty Form")]
        public string intyForm { get; set; }

        [Column("intyDescL1", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "inty Desc L1")]
        public override string DescriptionL1 { get; set; }

        [Column("intyDescL2", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "inty Desc L2")]
        public override string DescriptionL2 { get; set; }
    }
}
