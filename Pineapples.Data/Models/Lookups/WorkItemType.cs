using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
    [Table("lkpWorkItemType")]
    [Description(@"Type of work that may be done on a work item. The work item type defines a range of characteristic of the work: how to cost it, whether a building is required, whether a new building may be created, how to count the buildings that will be created if any. Foreign key on WorkITems - mandatory.")]
    public partial class WorkItemType : SimpleCodeTable
    {
        [Key]

        [Column("codeCode", TypeName = "nvarchar")]
        [MaxLength(15)]
        [StringLength(15)]
        [Required(ErrorMessage = "code Code is required")]
        [Display(Name = "code Code")]
        public override string Code { get; set; }

        [Column("codeDescription", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "code Description")]
        public override string Description { get; set; }

        [Column("codeDescriptionL1", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "code Description L1")]
        public override string DescriptionL1 { get; set; }

        [Column("codeDescriptionL2", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "code Description L2")]
        public override string DescriptionL2 { get; set; }

        [Column("witUseBuilding", TypeName = "int")]
        [Required(ErrorMessage = "wit Use Building is required")]
        [Display(Name = "wit Use Building")]
        [Description(@"0=dont prompt for building,1=prompt for building,2=building is mandatory;9=building not allowed")]
        public int witUseBuilding { get; set; }

        [Column("witBuildingType", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "wit Building Type")]
        [Description(@"default building type for this type of work")]
        public string witBuildingType { get; set; }

        [Column("witCreateBuilding", TypeName = "int")]
        [Required(ErrorMessage = "wit Create Building is required")]
        [Display(Name = "wit Create Building")]
        [Description(@"0=don't prompt to create building;1=prompt to create building;2=bulding mandatory at completion")]
        public int witCreateBuilding { get; set; }

        [Column("witEstUnitCost", TypeName = "money")]
        [Display(Name = "wit Est Unit Cost")]
        public decimal? witEstUnitCost { get; set; }

        [Column("witEstUOM", TypeName = "nvarchar")]
        [MaxLength(10)]
        [StringLength(10)]
        [Display(Name = "wit Est UOM")]
        public string witEstUOM { get; set; }

        [Column("witEstFormula", TypeName = "nvarchar")]
        [MaxLength(100)]
        [StringLength(100)]
        [Display(Name = "wit Est Formula")]
        public string witEstFormula { get; set; }

        [Column("witEstIM", TypeName = "bit")]
        [Required(ErrorMessage = "wit Est IM is required")]
        [Display(Name = "wit Est IM")]
        public bool witEstIM { get; set; }

        [Column("witGroup", TypeName = "nvarchar")]
        [MaxLength(10)]
        [StringLength(10)]
        [Display(Name = "wit Group")]
        public string witGroup { get; set; }
    }
}
