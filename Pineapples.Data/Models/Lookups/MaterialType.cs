using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
    [Table("lkpMaterialTypes")]
    [Description(@"Original broad classificaiton of material type - Permanent, Semi{Permanent, Traditional. lkpBuildingMaterials is more detailed for BuildingInventory.")]
    public partial class MaterialType : SequencedCodeTable
    {
        [Key]

        [Column("codeCode", TypeName = "nvarchar")]
        [MaxLength(1)]
        [StringLength(1)]
        [Required(ErrorMessage = "code Code is required")]
        [Display(Name = "code Code")]
        public override string Code { get; set; }

        [Column("codeDescription", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "code Description")]
        public override string Description { get; set; }

        [Column("codeSort", TypeName = "int")]
        [Display(Name = "code Sort")]
        public override int? Seq { get; set; }

        [Column("codeDescriptionL1", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "code Description L1")]
        public override string DescriptionL1 { get; set; }

        [Column("codeDescriptionL2", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "code Description L2")]
        public override string DescriptionL2 { get; set; }
    }
}
