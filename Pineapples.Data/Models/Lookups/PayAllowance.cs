using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
    [Table("lkpPayAllowance")]
    public partial class PayAllowance : SimpleCodeTable
    {

        [Column("payItemType", TypeName = "nvarchar")]
        [MaxLength(20)]
        [StringLength(20)]
        [Display(Name = "pay Item Type")]
        public string PayItemType { get; set; }
    }
}
