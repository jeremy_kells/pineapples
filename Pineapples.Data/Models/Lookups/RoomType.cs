using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
    [Table("lkpRoomTypes")]
    [Description(@"Type of rooms. Some are system defined - e.g. CLASS.")]
    public partial class RoomType : SequencedCodeTable
    {

        [Column("codeGroup", TypeName = "nvarchar")]
        [MaxLength(1)]
        [StringLength(1)]
        [Display(Name = "code Group")]
        public string codeGroup { get; set; }

        [Column("codeSort", TypeName = "int")]
        [Display(Name = "code Sort")]
        public override int? Seq { get; set; }

        [Column("rmresCat", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "rmres Cat")]
        public string rmresCat { get; set; }

        [Column("rfcnCode", TypeName = "nvarchar")]
        [MaxLength(10)]
        [StringLength(10)]
        [Display(Name = "rfcn Code")]
        public string rfcnCode { get; set; }
 
    }
}
