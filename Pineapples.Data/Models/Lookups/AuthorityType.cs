﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{


    [Table("lkpAuthorityType")]
    [Description(@"Classification of Authorities. Middle tier of authority reporting. 
    Authority Gov is foreign key, while the authority type is foreign key in turn on Authorities.")]
    public partial class AuthorityType: SimpleCodeTable
    {
        [Key]
        [Column("codeCode", TypeName = "nvarchar")]
        [MaxLength(1)]
        [StringLength(1)]
        [Required(ErrorMessage = "code Code is required")]
        [Display(Name = "code Code")]
        public override string Code { get; set; }

        [Column("codeGroup", TypeName = "nvarchar")]
        [MaxLength(1)]
        [StringLength(1)]
        [Display(Name = "code Group")]
        public string Group { get; set; }

    }



}
