﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pineapples.Data.DB;
using Pineapples.Data;
using System.Data.Entity.Infrastructure; // some lower-level ef contsructs in here
using Softwords.Web;
using Softwords.DataTools;
using System.Data;
using System.Data.SqlClient;
using Pineapples.Data.Models;


namespace Pineapples.Data.DataLayer
{
    internal class DSPerfAssess : BaseRepository, IDSPerfAssess
    {
        public DSPerfAssess(DB.PineapplesEfContext cxt) : base(cxt) { }

        public IDataResult Read(int perfAssessId)
        {
            //3 8 2017 this is done more simply using the StructuredResult appraoch introduced e.g. in Schools
            SqlCommand cmd = readCmd(perfAssessId);
            string[] tablenames = new string[] { "Indicators", "IndicatorLevels" };
            return structuredResult(cmd, "perfassess", tablenames);        // the tag allows Restangular to identify this and process it on the client

            //DataSet ds = cmdToDataset(cmd);
            //PerfAssess pf = new PerfAssess();
            //DataTable pa = ds.Tables[0];
            //if (pa.Rows.Count == 0)
            //{
            //  // return 
            //}
            //DataRow r = pa.Rows[0];
            //pf.Id = (int)r["paID"];
            //pf.Framework = r["pafrmCode"].ToString();
            //pf.TeacherId = (int)r["tID"] ;
            //pf.TeacherName = r["tFullName"].ToString();
            //pf.SchoolNo = r["paSchNo"].ToString();
            //pf.SchoolName = r["schName"].ToString();
            //pf.Date = (DateTime)(r["paDate"]);
            //pf.ConductedBy = db2str(r["paConductedBy"]);
            //pf.Rowversion = Convert.ToBase64String((byte[])r["pRowversion"]);

            ////http://stackoverflow.com/questions/3392612/convert-datatable-to-ienumerablet
            //pf.Indicators = ds.Tables[1].AsEnumerable().Select( row =>
            //  {
            //    return new PerfAssessIndicator
            //    {
            //      IndicatorCode = row["indFullID"].ToString(),
            //      IndicatorId = Convert.ToInt32(row["paindID"]),
            //      PerformanceLevel = db2str(row["paplCode"])


            //    };
            //  }).ToList();

            //pf.IndicatorLevels = ds.Tables[2].AsEnumerable().Select(row =>
            //{
            //  return new PerfAssessIndicatorLevel
            //  {
            //    IndicatorId = Convert.ToInt32(row["paindID"]),
            //    Level = db2str(row["paplCode"]),
            //    Value = db2int(row["paplValue"]),
            //    Description = db2str(row["paplDescription"]),
            //    Criterion = db2str(row["paplCriterion"])


            //  };
            //}).ToList();

            //return pf;
        }

        // this method will return a new perfaccess, given the framework and optionally teacherid
        public PerfAssessment Template(string framework, int? teacherId)
        {
            SqlCommand cmd = templateCmd(framework, teacherId);
            DataSet ds = cmdToDataset(cmd);
            PerfAssessment pf = new PerfAssessment();
            DataTable pa = ds.Tables[0];
            if (pa.Rows.Count == 0)
            {
                // return 
            }
            DataRow r = pa.Rows[0];
            pf.Framework = r["pafrmCode"].ToString();
            if (r["tID"].ToString() == string.Empty)
            {
                pf.TeacherId = null;
            }
            else
            {
                pf.TeacherId = Convert.ToInt32(r["tID"]);
            }

            pf.Date = System.DateTime.Now.ToUniversalTime();
            pf.TeacherName = r["tFullName"].ToString();
            pf.SchoolNo = r["paSchNo"].ToString();
            pf.SchoolName = r["schName"].ToString();
            //http://stackoverflow.com/questions/3392612/convert-datatable-to-ienumerablet
            pf.Indicators = ds.Tables[1].AsEnumerable().Select(row =>
            {
                return new PerfAssessmentIndicator
                {
                    IndicatorCode = row["indFullID"].ToString(),
                    IndicatorId = Convert.ToInt32(row["paindID"]),
                    PerformanceLevel = null // may as well initialize all these as null here?


          };
            }).ToList();
            pf.IndicatorLevels = ds.Tables[2].AsEnumerable().Select(row =>
            {
                return new PerfAssessmentIndicatorLevel
                {
                    IndicatorId = Convert.ToInt32(row["paindID"]),
                    Level = db2str(row["paplCode"]),
                    Value = db2int(row["paplValue"]),
                    Description = db2str(row["paplDescription"]),
                    Criterion = db2str(row["paplCriterion"])


                };
            }).ToList();
            return pf;
        }

        public IDataResult Save(PerfAssessment pf)
        {
            // insert
            using (cxt)
            {
                Models.PerfAssessment dbpf;
                if (pf.Id == null)
                {
                    dbpf = new Models.PerfAssessment();
                    // need to add to the context, becuase we dod not create this object from the cxt
                    cxt.PerfAssessments.Add(dbpf);
                    // some lower level EF manipulations - this link goes through various ways...
                    // https://stackoverflow.com/questions/25894587/how-to-update-record-using-entity-framework-6
                    DbEntityEntry ee = cxt.Entry(dbpf);
                    DbPropertyValues eepv = ee.CurrentValues;
                    eepv.SetValues(pf);
                    cxt.SaveChanges();
                }
                else
                {
                    dbpf = (from p in cxt.PerfAssessments
                            where p.Id == pf.Id
                            select p)
                            .First();
                    if (dbpf == null)
                    {
                        // not found 
                        throw new ArgumentException();

                    }
                    //////if (Convert.ToBase64String(dbpf.Rowversion.ToArray()) != pf.Rowversion)
                    //////{
                    //////  // optimistic concurreny error
                    //////  // we return the most recent version of the record
                    //////  var newVersion = Read((int)pf.Id);
                    //////  throw new ConcurrencyException(newVersion);

                    //////}

                    DbEntityEntry ee = cxt.Entry(dbpf);
                    DbPropertyValues eepv = ee.CurrentValues;
                    eepv.SetValues(pf);
                    cxt.SaveChanges();
                    // by defining the Indicators collection as 'virtual'
                    // it is 'lazy loaded' Therefore we don't have to explicitly load before using it -
                    // it is loaded on first access
                    //ee.Collection("Indicators").Load();
                    cxt.PerfAssessmentIndicators.RemoveRange(dbpf.Indicators);
                }
                foreach (PerfAssessmentIndicator pi in pf.Indicators)
                {
                    if (!string.IsNullOrEmpty(pi.PerformanceLevel))
                    {
                        Models.PerfAssessmentIndicator pal = new Models.PerfAssessmentIndicator()
                        {
                            IndicatorId = pi.IndicatorId,
                            PerformanceLevel = pi.PerformanceLevel
                        };
                        dbpf.Indicators.Add(pal);
                    }
                }
                cxt.SaveChanges();
                return Read((int)dbpf.Id);
            }
        }

        public IDataResult Filter(PerfAssessFilter fltr)
        {
            SqlCommand cmd = filterCmd(fltr);
            return sqlExec(cmd);
        }
        public IDataResult Table(PerfAssessTableFilter fltr)
        {
            SqlCommand cmd = filterCmd(fltr.filter);
            cmd.Parameters.Add("@row", SqlDbType.NVarChar, 50)
              .Value = fltr.row;
            cmd.CommandText = "pPARead.PerfAssessTable";
            return sqlExec(cmd);
        }
        #region commands


        private SqlCommand filterCmd(PerfAssessFilter fltr)
        {
            SqlCommand cmd = new SqlCommand();
            // generated by a tool
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "pPARead.PerfAssessFilterPaged";

            cmd.Parameters.Add("@ColumnSet", SqlDbType.Int).Value = (object)(fltr.ColumnSet) ?? DBNull.Value;
            cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = (object)(fltr.PageSize) ?? DBNull.Value;
            cmd.Parameters.Add("@PageNo", SqlDbType.Int).Value = (object)(fltr.PageNo) ?? DBNull.Value;
            cmd.Parameters.Add("@SortColumn", SqlDbType.NVarChar, 128).Value = (object)(fltr.SortColumn) ?? DBNull.Value;
            cmd.Parameters.Add("@SortDir", SqlDbType.Int).Value = (object)(fltr.SortDesc) ?? DBNull.Value;

            cmd.Parameters.Add("@PerfAssessID", SqlDbType.Int).Value = (object)(fltr.PerfAssessID) ?? DBNull.Value;
            cmd.Parameters.Add("@TeacherID", SqlDbType.Int).Value = (object)(fltr.TeacherID) ?? DBNull.Value;
            cmd.Parameters.Add("@ConductedBy", SqlDbType.NVarChar, 50).Value = (object)(fltr.ConductedBy) ?? DBNull.Value;
            cmd.Parameters.Add("@SchoolNo", SqlDbType.NVarChar, 50).Value = (object)(fltr.SchoolNo) ?? DBNull.Value;
            cmd.Parameters.Add("@District", SqlDbType.NVarChar, 10).Value = (object)(fltr.District) ?? DBNull.Value;
            cmd.Parameters.Add("@Island", SqlDbType.NVarChar, 10).Value = (object)(fltr.Island) ?? DBNull.Value;
            cmd.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = (object)(fltr.StartDate) ?? DBNull.Value;
            cmd.Parameters.Add("@EndDate", SqlDbType.DateTime).Value = (object)(fltr.EndDate) ?? DBNull.Value;
            cmd.Parameters.Add("@SubScoreCode", SqlDbType.NVarChar, 10).Value = (object)(fltr.SubScoreCode) ?? DBNull.Value;
            cmd.Parameters.Add("@SubScoreMin", SqlDbType.Float).Value = (object)(fltr.SubScoreMin) ?? DBNull.Value;
            cmd.Parameters.Add("@SubScoreMax", SqlDbType.Float).Value = (object)(fltr.SubScoreMax) ?? DBNull.Value;
            cmd.Parameters.Add("@XmlFilter", SqlDbType.Xml).Value = (object)(fltr.XmlFilter) ?? DBNull.Value;

            return cmd;
        }

        /// <summary>
        /// read command read a single applicant
        /// </summary>
        private SqlCommand readCmd(int perfAssessID)
        {
            SqlCommand cmd = new SqlCommand();
            // generated by a tool
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "pPARead.PerfAssessRead";
            cmd.Parameters.Add("@paID", SqlDbType.Int)
              .Value = perfAssessID;
            return cmd;
        }

        private SqlCommand templateCmd(string framework, int? teacherId)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "pPARead.PerfAssessTemplate"; ;
            cmd.Parameters.Add("@framework", SqlDbType.NVarChar, 10)
              .Value = framework;
            cmd.Parameters.Add("@teacherID", SqlDbType.Int)
              .Value = teacherId;

            return cmd;
        }
        #endregion
    }
}

