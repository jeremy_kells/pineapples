﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Softwords.Web;
using System.Security.Claims;

namespace Pineapples.Data
{
    [JsonConverter(typeof(Softwords.Web.Models.ModelBinderConverter<Models.SchoolLink>))]
    public class SchoolLinkBinder : Softwords.Web.Models.Entity.ModelBinder<Models.SchoolLink>
    {
        public int? ID
        {
            get
            {
                return (int?)getProp("lnkID");
            }
            set
            {
                definedProps.Add("lnkID", value);
            }
        }

        public void Update(Data.DB.PineapplesEfContext cxt, ClaimsIdentity identity)
        {
            Models.SchoolLink e = null;

            IQueryable<Models.SchoolLink> qry = from schoollink in cxt.SchoolLinks
                                                 where schoollink.lnkID == ID
                                        select schoollink;

            if (qry.Count() == 0)
            {
                throw RecordNotFoundException.Make(ID);
            }

            e = qry.First();
            if (Convert.ToBase64String(e.pRowversion.ToArray()) != this.RowVersion)
            {
                // optimistic concurrency error
                // we return the most recent version of the record
                FromDb(e);
                throw ConcurrencyException.Make(this.rowVersionProp, this.definedProps);
            }
            ToDb(cxt, identity, e);


        }

        public void Create(Data.DB.PineapplesEfContext cxt, ClaimsIdentity identity)
        {
            Models.SchoolLink e = null;

            IQueryable<Models.SchoolLink> qry = from schoollink in cxt.SchoolLinks
                                      where schoollink.lnkID == ID
                                      select schoollink;

            if (qry.Count() != 0)
            {
                throw DuplicateKeyException.Make(this.keyProp, ID);
            }

            e = new Models.SchoolLink();
            cxt.SchoolLinks.Add(e);
            ToDb(cxt, identity, e);
        }
    }
}
