﻿/// <binding BeforeBuild='_build_deploy' />
/*eslint-env node */

module.exports = function (grunt) {
  "use strict";

  // Loads the tasks specified in package.json
  require("load-grunt-tasks")(grunt);
  var nodePath = require("path");   // the node path module
  var bowerfiles = require('bower-files')(); // an array of the bower dependencies

  // get the various pointers to folders
  var appPath = nodePath.resolve();
  var appRoot = nodePath.resolve("../");
  var appName = "pineapples";
  var deployPath = nodePath.join(appRoot, appName);

  /**
   * typescript compile options for src and test
   * using tsconfig in each case
   */
  var tsConfig = {
    app: {
      tsconfig: "./src",
      options: {
        failOnTypeErrors: true
      }
    },
    test: {
      tsconfig: "./test"
    }
  };

  var concatConfig = {
    js: {
      src: [
        "license_header.txt", "src/common/js/swcommon.js", "src/common/js/**/*.js", "src/app/js/" + appName + ".js", "src/tscompiled.js", "src/app/js/**/*.js"
      ],
      dest: nodePath.join("dist/scripts/app/", appName + ".js")
    },
    css: {
      src: [
      "src/common/css/*.css",
      "src/app/css/indicators.css",
      "src/app/css/font-awesome.css",
      "src/app/css/theme.css",
      "src/app/css/tables-and-grids.css",
      "src/app/css/custom.css",
      "src/app/css/ui-grid-customise.css",
      "src/app/css/navigator.css",
      "src/app/css/material-fixes.css",
      "src/app/css/dashboard.css",
      ],
      dest: "dist/styles/app/" + appName + ".css"
    },
    theme: {
      // ensure this list lines up with the dependencies in the definition of the application module
      src: "src/theme/js/**/*.js",
      dest: "dist/scripts/app/theme.js"
    },
    pnotify: {
      src: [
        "bower_components/pnotify/pnotify.core.js",
        "bower_components/pnotify/pnotify.buttons.js",
        "bower_components/pnotify/pnotify.callbacks.js",
        "bower_components/pnotify/pnotify.confirm.js",
        "bower_components/pnotify/pnotify.desktop.js",
        "bower_components/pnotify/pnotify.history.js",
        "bower_components/pnotify/pnotify.nonblock.js"
      ],
      dest: "bower_components/pnotify/pnotify.js"
    }
  };

  /* helper function to separate remotable components, by looking in bower.json */
  var remotables = function(ext, include) {
    var b = bowerfiles.ext(ext).relative("bower_components").files;
    var bower = grunt.file.readJSON("bower.json")
    var output = [];
    b.forEach(function (boweritem) {
      //grunt.log.writeln(boweritem);
      var path = boweritem.substring(0, boweritem.indexOf("\\"));
      //grunt.log.writeln(path);
      //grunt.log.writeln(bower.remotable[path]);
      var inout = ((bower.remotable && bower.remotable[path])? true: false);
      if (include == inout) {
        output.push(boweritem);
      }

    });
    return output;
  };

  var copyConfig = {
    // Copy first to dist/
    remotablejs: {
      files: [
        {
          expand: true,
          cwd: "bower_components",
          src: remotables("js", true), //bowerfiles.ext("js").relative("bower_components").files,
          dest: "dist/scripts/remotable/",
          filter: 'isFile'
        }
      ]
    },
    localjs: {
      files: [
        {
          expand: true,
          cwd: "bower_components",
          src: remotables("js", false),   //bowerfiles.ext("js").relative("bower_components").files,
          dest: "dist/scripts/vendor/",
          filter: 'isFile'
        }
      ]
    },
    remotablecss: {
      files: [
        {
          expand: true,
          cwd: "bower_components",
          src: remotables("css", true),
          dest: "dist/styles/remotable/",
          filter: 'isFile'
        }
      ]
    },
    localcss: {
      files: [
        {
          expand: true,
          cwd: "bower_components",
          src: remotables("css", false),
          dest: "dist/styles/vendor/",
          filter: 'isFile'
        }
      ]
    },
    vendorjs: {
      files: [
        {
          expand: true,
          cwd: "src/vendor",
          src: "**/*.js",
          dest: "dist/scripts/vendor/",
          filter: 'isFile'
        }
      ]
    },
    vendorcss: {
      files: [ // any 3rd party css not available from bower
        {
          expand: true,
          cwd: "src/vendor",
          src: "**/*.css",
          dest: "dist/styles/vendor/",
          filter: 'isFile'
        }
      ]
    },
    fonts: {
      files: [ // Serve fonts locally instead of from CDN on unreliable Internet
        {
          expand: true,
          cwd: "src/app/css",
          src: "fonts/**/*",
          dest: "dist/styles/app/"
        }
      ]
    },
    // And then to deployPath where client files are served by ASN.NET
    assets: {
      files: [
        {
          expand: true,
          cwd: "assets",
          src: "**/*",
          dest: deployPath + "/assets/",
          filter: 'isFile'
        }
      ]
    },
    assets_local: {
      files: [
        {
          expand: true,
          cwd: "assets_local",
          src: "**/*",
          dest: deployPath + "/assets_local/",
          filter: 'isFile'
        }
      ]
    },
    scripts: {
      files: [
        {
          expand: true,
          cwd: "dist/scripts",
          src: "**/*",
          dest: deployPath + "/scripts/",
          filter: 'isFile'
        }
      ]
    },
    styles: {
      files: [
        {
          expand: true,
          cwd: "dist/styles",
          src: "**/*",
          dest: deployPath + "/styles/",
          filter: 'isFile'
        }
      ]
    }
  };

  var lessConfig = {
    theme: {
      options: {
        paths: ["src/app/less/_theme"],
        cleancss: true,
        modifyVars: {
          imgPath: '"http://mycdn.com/path/to/images"',
          bgColor: 'red'
        }
      },
      files: [
        {
          // this less bundles up all the forza configurables AS WELL as font-awesome
          src: "src/app/less/_theme/styles.less",
          dest: "src/app/css/theme.css"
        }
      ]
    },
    fa: {
      // not used becuase we are including font-awesome in the main theme build
      options: {
        paths: ["assets/fonts/font-awesome/less"],
        cleancss: true
      },
      files: [
        {
          // this less bundles up font-awesome
          src: "assets/fonts/font-awesome/less/font-awesome.less",
          dest: "assets/fonts/font-awesome/css/font-awesome.css"
        }
      ]
    }
  }

  var karmaConfig = {
    unit: {
      configFile: "karma.conf.js"
    },
    allbrowser: {
      configFile: "karma.conf.js",
      browsers: ['Chrome', 'PhantomJS', 'Firefox']
    }
  };

  var watchConfig = {
    //run unit tests with karma (server needs to be already running)
    karma: {
      files: ['dist/scripts/app/pineapples.js', 'test/tests.js'],
      tasks: ["karma:unit:run"] //future use?
    },
    ts: {
      files: ['src/tsconfig.json', 'src/app/ts/**/*.ts'],
      tasks: ["ts:app"] //compile the typescript
    }

  };

  /**
   * start and run iis
   */
  var iisConfig = {
    server: {
      options: {
        port: 53600,
        open: true,
        openPath: '/testpages/testpage.html',
        killOnExit: false,
        killOn: "iis.done"
      }
    }
  };

  var injectorConfig = {
    testpage: {
      options: { relative: true },
      files: {
        'testpages/testpage.html': ['dist/scripts/vendor/**/*.js', 'dist/scripts/app/*.js']
      }
    },
    testpage2: {
      options: { relative: true },
      files: {
        'testpages/testpage.html': bowerfiles.ext("js").files.concat(['dist/scripts/app/*.js'])
      }
    }
  };

  var shellConfig = {
    bowerinstall: {
      command: "bower install --force-latest"
    }

  }

  // lints
  var tslintConfig = {
    options: {
      configuration: grunt.file.readJSON(".tslintrc")
    },
    all: {
      src: ["src/app/ts/**/*.ts", "src/common/ts/**/*.ts"]
    }
  };

  var jscsConfig = {
    files: ["Gruntfile.js", "src/app/js/**/*.js"],
    options: {
      config: ".jscsrc"
    }
  };

  var eslintConfig = {
    target: ["Gruntfile.js", "src/app/js/**/*.js"],
    options: {
      configFile: ".eslintrc"
    }
  };

  var csslintConfig = {
    target: ["src/app/css/**/*.css", "src/common/css/**/*.css"],
    options: {
      csslintrc: ".csslintrc"
    }
  };

  /**
   * Config
   */
  grunt.initConfig({
    copy: copyConfig,
    concat: concatConfig,
    less: lessConfig,
    ts: tsConfig,
    karma: karmaConfig,
    iisexpress: iisConfig,
    injector: injectorConfig,
    shell: shellConfig,
    tslint: tslintConfig,
    jscs: jscsConfig,
    eslint: eslintConfig,
    csslint: csslintConfig,
    watch: watchConfig

  });

  /**
   *
   * Karma Unit Testing
   *
   */
  grunt.registerTask("karma_start", ["karma:unit", "watch:karma"]);
  grunt.registerTask("karma_run", ["karma:unit:run"]);
  grunt.registerTask("karma_allbrowser_run", ["karma:allbrowser:run"]);
  grunt.registerTask("karma_compile", ["ts:test", "karma:unit", "watch:karma"]);

  /**
   *
   * Testing in IIS
   *
   */
  grunt.registerTask("iisexpress-kill", function () {
    grunt.event.emit("iis.done");
  });

  grunt.registerTask("remotables", function () { remotables("js", false); });
  /**
   *
   * Build and Deployment
   * - produce a single js for the app from its js and ts code
   * - produce a css for the app
   * - vendor files go to dist/scripts/vendor
   * - vendor css go to dist/styles/vendor
   */

  var clear = function (path) {
    if (grunt.file.exists(path)) {
      grunt.file.delete(path, { force: true });
    }
  }
  var clearDeploy = function (path) {
    clear(nodePath.join(deployPath, path));
  }

  /**
   * build steps
   */
  grunt.registerTask("clearvendorscripts:local", function () {
    clear("dist/scripts/vendor");
    clear("dist/scripts/remotable");
  });

  grunt.registerTask("clearappscripts:local", function () {
    clear("dist/scripts/app");
  });
  grunt.registerTask("clearvendorstyles:local", function () {
    clear("dist/styles/vendor");
    clear("dist/styles/remotable");
  });

  grunt.registerTask("clearappstyles:local", function () {
    clear("dist/styles/app");
  });


  /**
   * build app js
   */
  grunt.registerTask("build:js:app", ["clearappscripts:local", "ts:app", "concat:js", "concat:theme"]);
  grunt.registerTask("build:js:vendor", ["clearvendorscripts:local", "copy:remotablejs", "copy:localjs", "copy:vendorjs"]);
  grunt.registerTask("build:js", ["build:js:app", "build:js:vendor"]);

  /**
  * build app css
  */
  grunt.registerTask("build:css:app", ["clearappstyles:local", "csslint", "concat:css"]);
  grunt.registerTask("build:css:vendor", ["clearvendorstyles:local", "copy:remotablecss", "copy:localcss", "copy:vendorcss", "copy:fonts"]);
  grunt.registerTask("build:css", ["build:css:app", "build:css:vendor"]);
  grunt.registerTask("build", ["build:js", "build:css"]);

  /**
   * deploy steps
   */
  grunt.registerTask("clearscripts", function () {
    clearDeploy("scripts");
  });

  grunt.registerTask("clearstyles", function () {
    clearDeploy("styles");
  });

  grunt.registerTask("clearassets", function () {
    clearDeploy("assets");
  });
  grunt.registerTask("clearassets_local", function () {
    clearDeploy("assets_local");
  });
  grunt.registerTask("deploy:app:js", [
    "clearappscripts", "concat:jsdeploy", "concat:themedeploy"
  ]);

  grunt.registerTask("deploy:vendor:js", [
    "clearvendorscripts", "copy:remotablejs", "copy:localjs", "copy:vendorjs"
  ]);

  grunt.registerTask("deploy:assets", [
    "clearassets", "copy:assets", "clearassets_local", "copy:assets_local"
  ]);
  grunt.registerTask("deploy:scripts", ["clearscripts", "copy:scripts"]);
  grunt.registerTask("deploy:styles", ["clearstyles", "copy:styles"]);

  /**
   * run all deploy tasks
   * js - "build" the application .js
   *    - deploy that
   *    -
   */
  grunt.registerTask("deploy", ["showDeployInfo", "deploy:assets", "deploy:scripts", "deploy:styles"]);
  grunt.registerTask("_build_deploy", ["build", "deploy"]);

  grunt.registerTask("dependencies", function () {
    var bf = bowerfiles.ext("js").relative("bower_components").files;
    bf.forEach(function (f) {
      grunt.log.writeln(f);
    });
  });

  grunt.registerTask("bowerAudit", function () {
    var bf = bowerfiles.relative("bower_components");
    bf.forEach(function (f) {
      grunt.log.writeln(f);
    });
  });
  grunt.registerTask("showDeployInfo", function () {
    grunt.log.writeln(appPath);
    grunt.log.writeln("From: " + appPath);
    grunt.log.writeln("To: " + deployPath);
  });
};
