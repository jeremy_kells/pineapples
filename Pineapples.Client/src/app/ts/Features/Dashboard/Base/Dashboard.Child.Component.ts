﻿namespace Pineapples.Dashboards {

  // this is a widget that implements some particular view of 
  // data provided by a dashboard
  // Communication between the child and its parent dashboard occurs in these 3 ways:
  // 1) require: 
  // The child component sets up a 'require' on its parent controller
  // This means the child cannot be instantiated outside its parent controller
  // and, the child has a stringly typed reference to the services (including data)
  // provided by its parent
  // 2) Bindings
  // The child components are children in the UI of the dashboard. So - the dashboard
  // can provide notifications to the children by component bindings
  // For example, the dashboardReady binding is set from the dashboardReady property of the dashboard
  // notifying children that the data is ready, or has been modified
  // 3) publish/subscribe
  // 
  export interface IDashboardChild {

    dashboard: IDashboard; // set up via a 'require' in the component definition

    // bindings
    //----------------------------------------------
    // the dashboard is ready
    dashboardReady: number;
    // one of the children can mark itself as 'selected' ie given prominence in the dashboard
    // the dashboard notifies all it children of this change of status
    selectedChild: string;

    // a unique id to identify the instance of the component
    componentId: string

    // check if the current component is the 'selected' component
    // these delegate to the relvant members of the dashboard
    isSelected(): boolean;
    isUnselected(): boolean;
    setSelected();
    toggleSelected();

    // reporting
    // 
    // true if there is an assoicated report
    hasReport(): boolean
    // generate the associated report
    runReport(): void

    // virtuals
    onDashboardReady(): void; //function to call when dashboardReady changes;

    // function to call when the selected child changes
    // the currently selected child has been transmitted via bindings to selectedChild - this is the 
    // componentId of that child
    // 
    onChildSelected(): void; 

    // called when the options assoicated to the dashboard have changed
    // this is implemented with a binding, rather than a public subscribe
    // the data is the change descri[tion packaed by the options object
    // Note that most clients will not need to access this becuase:
    // bindings to optionvalues in the html will update anyway e.g.
    // {{vm.dashboard.optoins.selectedYear}} 
    // will automatically refresh
    // as well, as Crossfilter dashboard should respond to the option changes by applying
    // the appropriate filters to its dimensions. Any clients built on these dimensions will respond appropriately.
    // if the client does want to respond to this, it can access the optionChange object,
    // which is the description of the change, or else just work with dashboard.options, which holds all the current settings
    onOptionChange(): void;

  }

  // Controller for a dashboard child component
  // extends the ReportRunner class to allow the dashboard object to rpint reports
  export class DashboardChild extends Pineapples.Reporting.ReportRunner implements IDashboardChild {
    // injections and constructor
    static $inject = ["$timeout", "$mdDialog", "$state", "Lookups", "reportManager"];
    constructor(
      protected timeout: ng.ITimeoutService
      , mdDialog: ng.material.IDialogService
      , protected $state: ng.ui.IStateService
      , public lookups: Sw.Lookups.LookupService
      , rmgr: Pineapples.Reporting.IReportManagerService) {
      super(mdDialog, rmgr);
      this.componentId = this.uniqueId();
    }

    // require
    dashboard: IDashboard;      // derived class must override this to strongly type it
    // --- bindings ---
    dashboardReady: number;
    selectedChild: string;
    // this is a data packet describing an option change
    // that is relayed to the child component
    optionChange: any       
    //
    // path in the jasper report hierarchy to the associated report
    // this can be set in code to provide a default
    // otherwise can be overwritten by binding report-path
    protected defaultReportPath: string

    public cht: any;

    public afterChartInit(cht: any) {
      this.cht = cht;
    }

    // the value of the selected item, which is derived from the onOptionChanged
    public selectedKey: any;

    protected get reportPath(): string {
      return this.defaultReportPath;
    }

    protected set reportPath(newValue) {
      if (newValue) {
        this.defaultReportPath = newValue;
      }
    }


    public $onChanges(changes) {
      if (changes.dashboardReady && changes.dashboardReady.currentValue > 0) {
        this.onDashboardReady();
      }
      if (changes.selectedChild) {
        this.onChildSelected();
      }
      if (changes.optionChange) {
        this.onOptionChange();
      }
    }

    // ------------------------------------
    // reporting
    //-------------------------------------

    
    // true if there is a report
    public hasReport(): boolean {
      return (this.reportPath != null);
    }

    // for example...
    public getReportContext() {
      return {
        surveyYear: this.dashboard.options.selectedYear,
        district: this.dashboard.options.selectedDistrict
      }
    }
    // setParams is noop - construct reportContext as the parameters
    public setParams = () => { }


    // execute the associated report
    public runReport() {
      // first step is to get a Report object from the definition
      this.report = new Pineapples.Reporting.Report();
      this.report.path = this.reportPath;
      this.report.promptForParams = "always";
      this.report.canRenderPdf = true;
      this.report.canRenderExcel = true;
      this.report.format = "PDF";
      this.reportContext = this.getReportContext();
      this.run();
    }


    public onDashboardReady() { }

    // called whenever the selectedChild in the group changes
    // often it wont be necessary to do anything in here,
    // because UI effects will be manage by use of vm.isSelected(...)
    // e.g. in ng-show or ng-class
    public onChildSelected() { }

    // called when the dashboard has recorded a change in the options
    // 
    public onOptionChange() {
    }

   /**
    * unique identifier for this instance of the component
    **/
    public componentId: string

    //-----------------------------------------------
    // selected child impementation
    // this delegates to the dashboard
    //-----------------------------------------------
    public isSelected(): boolean {
      return (this.componentId === this.selectedChild);
    }
    public isUnselected(): boolean {
      return (this.selectedChild != this.componentId && this.selectedChild !== "");
    }

    // these methods just pass through to the dashboard
    // but it is convenient to have them here so that markup in the ui can refer to them 
    // directly: e.g. ng-click="vm.toggleSelected()"
    // this will change the value in the dashboard, which will get relayed to every child
    // via the binding, forcing the call to onChildSelected()
    public setSelected() {
      this.dashboard.setSelected(this.componentId);
    }
    public toggleSelected() {
      this.dashboard.toggleSelected(this.componentId);
    }


    // implementation
    private uniqueId() {
      // https://gist.github.com/gordonbrander/2230317
      // Math.random should be unique because of its seeding algorithm.
      // Convert it to base 36 (numbers + letters), and grab the first 9 characters
      // after the decimal.
      return '_' + Math.random().toString(36).substr(2, 9);
    };
  }
}