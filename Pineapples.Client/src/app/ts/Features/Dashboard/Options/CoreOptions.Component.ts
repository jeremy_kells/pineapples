﻿namespace Pineapples.Dashboard {


  class Controller {
    public options: any

    static $inject = ["Lookups"]
    constructor(public lookups: Sw.Lookups.LookupService) { }

    public resetAll = () => this.options.resetAll();

  }

  class Component implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;

    
    constructor() {
      this.bindings = {
        options: "<"
      };
      this.controller = Controller;
      this.controllerAs = "vm";
      this.templateUrl = "dashboard/options/core";
    }
  }

  angular
    .module("pineapples")
    .component("coreOptionsEditor", new Component());
}