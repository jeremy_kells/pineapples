﻿namespace Pineapples.Dashboards {

  class Controller extends DashboardChild implements IDashboardChild {

    public dashboard: EnrolDashboard;      
    public grpDistrictCode: CrossFilter.Group<Enrolments.IxfData, string, any>;

    public onDashboardReady() {
      let xFilter = this.dashboard.xFilter;
      this.grpDistrictCode = xFilter.xReduce(this.dashboard.dimDistrictCode, xFilter.getPropAccessor("SurveyYear"), Enrolments.vaGendered);

      this.layers = [{
        name: 'F',
        accessor: (d) => d.value.Tot.Female
      },
        {
          name: 'M',
          accessor: (d) => d.value.Tot.Male
        }
      ];
     }

    public onOptionChange() {
      console.log("onOptionschanged", this.optionChange);
      if (this.optionChange.selectedDistrict) {
        if (this.optionChange.selectedDistrict.new) {
          this.selectedKey = this.optionChange.selectedDistrict.new;
        } else {
          this.selectedKey = null;
        }
      }
    }

    public afterChartInit(chart) {
      chart.legend(dc.legend().x(350).y(10).itemHeight(13).gap(5));
    }

    public onChildSelected() {
      // this forces a delayed digest after the child selection is applied - this
      // gives the component a chart to know its new size
      // before it is redrawn
      this.timeout(1000, true);
    }

    public onClick(key) {
      this.dashboard.options.toggleSelectedDistrict(key);
    }

    // this is a value accessor for the chart. It receives a key value pair
    public totAccessor(d) {
      return d.value.Tot.Total;
    }

    public layers: any[];
  }

  angular
    .module("pineapples")
    .component("enrolmentDistrictChart", new EnrolComponent(Controller, "EnrolmentDistrictChart"));
}
