﻿namespace Pineapples.Dashboards {

  class Controller extends DashboardChild implements IDashboardChild {

    // reference to the host page is from the 'require' on the componentOptions
    public dashboard: EnrolDashboard;      

    // groups 
    public grpSchoolType: CrossFilter.Group<Enrolments.IxfData, string, number>;

    public onOptionsChanged(data, sender) {
    }
 
    public onDashboardReady() {
      this.grpSchoolType = this.dashboard.dimSchoolTypeCode.group().reduceSum(d => d.EnrolM + d.EnrolF);
    }
  }

  angular
    .module("pineapples")
    .component("enrolmentLevelBySchooltype", new EnrolComponent(Controller, "EnrolmentLevelBySchooltype"));
}
