﻿namespace Pineapples.Dashboards {

  class Controller extends DashboardChild implements IDashboardChild {

    public dashboard: EnrolDashboard;      

    public xf: CrossFilter.CrossFilter<Enrolments.IxfData>;

    public grpAuthorityCode: CrossFilter.Group<Enrolments.IxfData, string, number>;
    public grpAuthorityGovt: CrossFilter.Group<Enrolments.IxfData, string, number>;

    public onDashboardReady() {
      this.grpAuthorityCode = this.dashboard.dimAuthorityCode.group().reduceSum(d => d.EnrolM + d.EnrolF);
      this.grpAuthorityGovt = this.dashboard.dimAuthorityGovt.group().reduceSum(d => d.EnrolM + d.EnrolF);
    }

    public onOptionChange() {
      console.log("onOptionchanged", this.optionChange);
      if (!this.optionChange) return;
      if (this.isSelected() && this.optionChange.selectedAuthority) {
        this.cht.filterAll();
        this.cht.filter(this.dashboard.options.selectedAuthority);
        this.cht.redraw();
      }
      if (!this.isSelected() && this.optionChange.selectedAuthorityGovt) {
        this.cht.filterAll();
        this.cht.filter(this.dashboard.options.selectedAuthorityGovt);
        this.cht.redraw();
      }
    }

    public onChildSelected() {
      this.timeout(() => {
        this.cht.filterAll();
        this.cht.filter(this.dashboard.options[(this.isSelected() ? 'selectedAuthority' : 'selectedAuthorityGovt')] || null);
        this.cht.redraw();
      }, 0, true);

      this.timeout(1000, true);
    }

    public onClick(datum) {
      this.isSelected()
        ? this.dashboard.options.toggleSelectedAuthority(datum.key)
        : this.dashboard.options.toggleSelectedAuthorityGovt(datum.key);
    }

    // this is a value accessor for the chart. It receives a key value pair
    public totAccessor(d) {
      return d.value;
    }
  }

  angular
    .module("pineapples")
    .component("enrolmentAuthorityChart", new EnrolComponent(Controller, "EnrolmentAuthorityChart"));
}
