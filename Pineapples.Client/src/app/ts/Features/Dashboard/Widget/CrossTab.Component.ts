﻿namespace Pineappples.Dashboards {
  const memoizeKeys = _.memoize(_.keys);
  
  class Controller {

    headingTitle: string = 'You forgot to pass heading-title';
    headingFilters: string = 'You forgot to pass heading-filters';
    selectedChild: any;
    toggleSelected: any;
    dimensions: string;
    reportPath: string;

    headingOptions: any;
    gridData: any; //Pineapples.Dashboards.IKeyValuePair<any, any>[];
    firstDimName: string = 'You forgot to pass first-dim-name';
    rowLabels: any;
    colLabelsLookup: string;

    detailGridData: any; // Pineapples.Dashboards.IKeyValuePair<any, any>[];
    detailColLabelsLookup: string;
    detailRowLabels: any;

    hasColumnTotals: boolean = true;
    hasRowTotals: boolean = true; s
    indicatorCalculators: any;

    onRowSelect: (key: any) => void;

    selectedViewOption: string;

    // This is a bit ugly, but fits with Dashboards not defining componentIds - Componenent Design recommends pushing state (like this) up. 
    componentId: string;
    isSelected = () => this.componentId == this.selectedChild;
    anotherComponentSelected = () => this.selectedChild != '' && this.componentId != this.selectedChild;

    static $inject = ['Lookups'];
    constructor(public lookups) {
      this.componentId = uniqueId();
    }


    selectedColumns = () => this.headingOptions[this.selectedViewOption]; 
    viewOptions = () => memoizeKeys(this.headingOptions);

    $onInit(changesObj) {
      if (this.headingOptions) {
        this.selectedViewOption = _.keys(this.headingOptions)[0]; 
      }
    }

  }

  class Component implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;

    constructor() {
      this.bindings = {
        // For <dashboard-child>
        dimensions: "@",
        selectedChild: "<",
        headingTitle: "@?",
        headingFilters: "<?",
        headingOptions: "<?",
        reportPath: "@",
        toggleSelected: "<",

        indicatorCalculators: "<?",
        hasColumnTotals: "<?",
        hasRowTotals: "<?",
        firstDimName: "@?",
        colLabelsLookup: "@?",
        rowLabels: "<?", 
        gridData: "<",

        detailGridData: "<?",
        detailColLabelsLookup: "@?",
        detailRowLabels: "<?",

        onRowSelect: "&"

      };
      this.controller = Controller;
      this.controllerAs = "vm";
      this.templateUrl = `dashboard/widget/CrossTab`;
    }
  }

  angular
    .module("pineapples")
    .component("crossTab", new Component());
}