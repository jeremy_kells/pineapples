﻿namespace Pineappples.Dashboards {

  const memoizeKeys = _.memoize(_.keys)

  class Controller {

    headingTitle: string = 'You forgot to pass heading-title';
    headingFilters: string = 'You forgot to pass heading-filters';
    selectedChild: any;
    toggleSelected: any;
    dimensions: string;
    reportPath: string;



    defaultValueAccessor = (x) => x.EnrolF + x.EnrolM; //(x) => x.value;

    headingOptions: any;
    selectedViewOption: string;
    viewOptions = () => memoizeKeys(this.headingOptions);
    indicatorCalculators: any;

    va = _.memoize((opt) => this.indicatorCalculators[this.headingOptions[opt][0]]);


    dim: any;
    selectedKey: any;

    onClick: any;

    viewOptionSelected = (opt) => opt === this.selectedViewOption;

    //$onChanges(changes) {
    //  console.log('PieChart.$onChanges', changes);
    //}

    $onInit(changesObj) {
      if (this.headingOptions) {
        this.selectedViewOption = _.keys(this.headingOptions)[0];
      }
    }

    // This is a bit ugly, but fits with Dashboards not defining componentIds - Componenent Design recommends pushing state (like this) up. 
    componentId: string;
    isSelected = () => this.componentId == this.selectedChild;
    anotherComponentSelected = () => this.selectedChild != '' && this.componentId != this.selectedChild;

    static $inject = ['Lookups'];
    constructor(public lookups) {
      this.componentId = uniqueId();
    }
  }

  class Component implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;

    constructor() {
      this.bindings = {
        // For <dashboard-child>
        dimensions: "@",
        selectedChild: "<",
        headingTitle: "@?",
        headingFilters: "<?",
        reportPath: "@",
        toggleSelected: "<",

        indicatorCalculators: "<?",
        headingOptions: "<?",
        dim: "<",
        selectedKey: "<",
        // Initially these will show Enrolment totals by default - extend with function binding to pass to group().reduceSum
        valueAccessor: "<?",
        onClick: "<",

      };
      this.controller = Controller;
      this.controllerAs = "vm";
      this.templateUrl = `dashboard/widget/PieChart`;
    }
  }

  angular
    .module("pineapples")
    .component("pieChart", new Component());
}