﻿namespace Pineapples.Students {

  interface IBindings {
    model: Student;
  }

  class Controller extends Sw.Component.ComponentEditController implements IBindings {
    public model: Student;

    static $inject = ["ApiUi", "studentsAPI"];
    constructor(apiui: Sw.Api.IApiUi, api: any) {
      super(apiui, api);
    }

    public $onChanges(changes) {
      super.$onChanges(changes);
    }

  }

  angular
    .module("pineapples")
    .component("componentStudent", new Sw.Component.ItemComponentOptions("student", Controller));
}