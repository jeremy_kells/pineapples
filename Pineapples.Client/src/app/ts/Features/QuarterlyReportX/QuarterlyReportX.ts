﻿namespace Pineapples.QuarterlyReportsX {

  export class QuarterlyReportX extends Pineapples.Api.Editable implements Sw.Api.IEditable {

    constructor(qrData) {
      super();
      angular.extend(this, qrData);
    }

    // create static method returns the object constructed from the resultset object from the server
    public static create(resultSet: any) {
      let qr = new QuarterlyReportX(resultSet);
      return qr;
    }

    // IEditable implementation
    public _name() {
      return (<any>this).schNo + '-' + (<any>this).InspYear;
    }

    public _type() {
      return "quarterlyreportx";
    }

    public _id() {
      return (<any>this).qrID
    }

  }
}
