namespace Pineappples.QuarterlyReportsX {

  let routes = function ($stateProvider) {
    var featurename = 'QuarterlyReportsx';
    var filtername = 'QuarterlyReportXFilter';
    var templatepath = "quarterlyreportx";
    var tableOptions = "quarterlyreportXFieldOptions";
    var url = "quarterlyreportsx";
    var usersettings = null;
    //var mapview = 'SchoolMapView';

    // root state for 'quarterlyreport' feature
    let state: ng.ui.IState = Sw.Utils.RouteHelper.featureState(featurename, filtername, templatepath);

    // default 'api' in this feature is quarterlyReportsXAPI
    state.resolve = state.resolve || {};
    state.resolve["api"] = "quarterlyReportsXAPI";

    let statename = "site.quarterlyreportsx";
    $stateProvider.state("site.quarterlyreportsx", state);

    // takes an editable list state
    state = Sw.Utils.RouteHelper.editableListState("QuarterlyReportX");
    statename = "site.quarterlyreportsx.list";
    state.url = "^/quarterlyreportsx/list";
    $stateProvider.state(statename, state);
    console.log("state:" + statename);
    console.log(state);

    state = {
      url: '^/quarterlyreportsx/reload',
      onEnter: ["$state", "$templateCache", function ($state, $templateCache) {
        $templateCache.remove("quarterlyreportx/item");
        $templateCache.remove("quarterlyreportx/searcher");
        $state.go("site.quarterlyreportsx.list");
      }]
    };
    statename = "site.quarterlyreportsx.reload";
    $stateProvider.state(statename, state);

    state = {
      url: "^/quarterlyreportsx/dashboard",
      data: {
        permissions: {
          only: 'SchoolReadX'
        }
      },
      views: {
        "@": {
          component: "quarterlyReportsXDashboardComponent"
        }

      },
    };
    $stateProvider.state("site.quarterlyreportsx.dashboard", state);

    // chart table and map
    Sw.Utils.RouteHelper.addChartState($stateProvider, featurename);
    Sw.Utils.RouteHelper.addTableState($stateProvider, featurename);
    // Sw.Utils.RouteHelper.addMapState($stateProvider, featurename, mapview);

    // new - state with a custom url route
    state = {
      url: "^/quarterlyreportsx/new",
      params: { id: null, columnField: null, rowData: {} },
      data: {
        permissions: {
          only: 'SchoolWriteX'
        }
      },
      views: {
        "actionpane@site.quarterlyreportsx.list": {
          component: "componentQuarterlyReportX"
        }

      },
      resolve: {
        model: ['quarterlyReportsXAPI', '$stateParams', function (api, $stateParams) {
          return api.new();
        }]
      }
    };
    $stateProvider.state("site.quarterlyreportsx.list.new", state);

    state = {
      url: "^/quarterlyreportsx/reports",
      views: {
        "@": "reportPage"       // note this even more shorthand syntax for a component based view
      },
      resolve: {
        folder: () => "Quarterly_Reports_X",  // actually Quarterly Reports folder in Jasper but URI is underscore
        promptForParams: () => "always"
      }
    }
    $stateProvider.state("site.quarterlyreportsx.reports", state);

    // item state
    //$resolve is intriduced in ui-route 0.3.1
    // it allows bindings from the resolve directly into the template
    // injections are replaced with bindings when using a component like this
    // the component definition takes care of its required injections - and its controller
    state = {
      url: "^/quarterlyreportsx/{id}",
      params: { id: null, columnField: null, rowData: {} },
      views: {
        "actionpane@site.quarterlyreportsx.list": {
          component: "componentQuarterlyReportX"
        }
      },
      resolve: {
        model: ['quarterlyReportsXAPI', '$stateParams', function (api, $stateParams) {
          return api.read($stateParams.id);
        }]
      }
    };
    $stateProvider.state("site.quarterlyreportsx.list.item", state);
  }

  angular
    .module('pineapples')
    .config(['$stateProvider', routes])

}
