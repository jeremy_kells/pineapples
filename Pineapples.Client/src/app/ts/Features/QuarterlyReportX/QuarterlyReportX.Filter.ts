﻿module Pineapples {

  let viewDefaults = {
    columnSet: 0,
    columnDefs: [
      {
        field: 'qrID',
        name: 'QRID',
        displayName: 'QRID',
        editable: false,
        width: 60,
        pinnedLeft: true,
        cellTemplate: '<div class="ngCellText ui-grid-cell-contents" ng-class="col.colIndex()"><a  ng-click="grid.appScope.listvm.action(\'item\',col.field, row.entity);">{{row.entity[col.field]}}</a></div>'
      },
      {
        field: 'InspectedBy',
        name: 'Completed By',
        displayName: 'Completed By',
        editable: false,
        width: 120,
        pinnedLeft: true
      },
      {
        field: 'ApprovedBy',
        name: 'Approved By',
        displayName: 'Approved By',
        editable: false,
        width: 120,
        pinnedLeft: true
      },
      {
        field: 'InspYear',
        name: 'Year',
        displayName: 'Year',
        editable: false,
        width: 80,
        pinnedLeft: true
      },
      {
        field: 'schNo',
        name: 'School',
        displayName: 'School',
        editable: false,
        width: 260,
        pinnedLeft: true,
        lookup: 'schoolCodes'
      }
    ]
  };

  class QuarterlyReportXParamManager extends Sw.Filter.FilterParamManager implements Sw.Filter.IFilterParamManager {

    constructor(lookups: any) {
      super(lookups);
    };

    //public toFlashString(params: any) {
    //  var tt = ['SchoolType', 'District',
    //    'Authority', 'SchoolName'
    //  ];
    //  return this.flashStringBuilder(params, tt);
    //}

    //public fromFlashString(flashstring: string) {
    //  if (flashstring.trim().length === 0) {
    //    return;
    //  }
    //  let params: any = {};
    //  var parts = this.tokenise(flashstring);         // 8 3 2015 smarter Tokenise
    //  var parsed = [];
    //  for (var i = 0; i < parts.length; i++) {
    //    var s = parts[i].trim();
    //    var S = s.toUpperCase();

    //    if (this.cacheFind(S, params, "schoolTypes", "SchoolType")) {
    //      continue;
    //    }
    //    if (this.cacheFind(S + ' SCHOOL', params, "schoolTypes", "SchoolType")) {
    //      continue;
    //    }
    //    if (this.cacheFind(S, params, "districts", "District")) {
    //      continue;
    //    }
    //    if (this.cacheFind(S, params, "authorities", "Authority")) {
    //      continue;
    //    }
    //    if (s.indexOf('*') >= 0 || s.indexOf('?') >= 0 || s.indexOf('%') >= 0) {
    //      if (!params.SchoolName) {
    //        params.SchoolName = s;
    //        parsed.push(s);
    //        continue;
    //      }
    //    }
    //    if (parts.length === 1) {
    //      params.schNo = s;
    //      parsed.push(s);
    //    }
    //  }

    //  return params;
    //}

    //protected getParamString(name, value) {
    //  switch (name) {
    //    case 'AwardType':
    //      return this.lookups.findByID('awardTypes', value).C;
    //    default:
    //      return value.toString();
    //  }
    //}

  }

  class QuarterlyReportXFilter extends Sw.Filter.Filter implements Sw.Filter.IFilter {

    static $inject = ["$rootScope", "$state", "$q", "Lookups", "quarterlyReportsXAPI", "identity"];
    constructor(protected $rootScope: ng.IRootScopeService, protected $state: ng.ui.IStateService, protected $q: ng.IQService,
      protected lookups: Sw.Lookups.LookupService, protected api: any, protected identity: Sw.Auth.IIdentity) {
      super();
      this.ViewDefaults = viewDefaults;
      this.entity = "quarterlyreportx";
      this.ParamManager = new QuarterlyReportXParamManager(lookups);
    }

    protected identityFilter() {
      let fltr: any = {};
      // not required
      return fltr;
    }

    //public createTableCalculator() {
    //  return new SchoolTableCalculator();
    //}

    public createFindConfig() {
      let config = new Sw.Filter.FindConfig();
      let d = this.$q.defer<Sw.Filter.FindConfig>();
      config.defaults.paging.pageNo = 1;
      config.defaults.paging.pageSize = 50;
      config.defaults.paging.sortColumn = "qrID";
      config.defaults.paging.sortDirection = "asc";
      config.defaults.table.row = "School";
      config.defaults.table.col = "InspYear";
      config.defaults.viewMode = this.ViewModes[0].key;
      config.current = angular.copy(config.defaults);
      //config.tableOptions = "schoolFieldOptions";
      //config.dataOptions = "schoolDataOptions";

      config.identity.filter = this.identityFilter();
      config.reset();
      d.resolve(config);
      return d.promise;
    }

  }
  angular
    .module("pineapples")
    .service("QuarterlyReportXFilter", QuarterlyReportXFilter);
}
