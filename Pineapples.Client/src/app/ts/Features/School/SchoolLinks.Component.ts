﻿namespace Pineapples.Schools {

  interface IBindings {
    links: any[];
  }

  class Controller extends Pineapples.Documents.DocumentRenderer implements IBindings {
    public links: any[];
    public school: School;

    static $inject = ["identity", "$state", "$mdDialog", "Restangular", "documentsAPI"]
    constructor(public identity: Sw.Auth.IIdentity, public state: ng.ui.IStateService
      , public mdDialog: ng.material.IDialogService
      , public Restangular: restangular.IService
      , docAPI) {
      super(docAPI, "");
    }

    public $onChanges(changes) {
    }
    public newDoc() {
      this._uploadDialog().then(() => {
      }, (error) => {
      });
    }

    private _uploadDialog() {
      let options: ng.material.IDialogOptions = {
        locals: { school: this.school },
        controller: SchoolLinkUploadController,
        controllerAs: 'vm',
        bindToController: true,
        templateUrl: "schoollink/uploaddialog"
      }
      return this.mdDialog.show(options);
    }

    private doEdit(link, event) {
      let tlink = new Pineapples.Documents.DocumentLink(link);
      // this has cloned the data...
      this.Restangular.restangularizeElement(null, tlink, "schoollinks");
      this._editDialog(tlink, link);
    }

    private _editDialog(schlink: Pineapples.Documents.DocumentLink, link: any) {
      let options: ng.material.IDialogOptions = {
        locals: { model: schlink, modelInit: link, school: this.school },
        controller: editController,
        controllerAs: 'vm',
        bindToController: true,
        templateUrl: "schoollink/editdialog"
      }
      return this.mdDialog.show(options);

    }

    // to do refactor this to somewhere else its duplicated at the moment with teacher links
    private doDelete(link, event) {
      let confirm = this.mdDialog.confirm()
        .title('Delete Document')
        .textContent('Delete the document ' + link.docTitle + ' from the document library?')
        .targetEvent(event)
        .ok('Delete')
        .cancel('Cancel');
      this.mdDialog.show(confirm).then(() => {
        let schlink = new Pineapples.Documents.DocumentLink(link);
        // this has cloned the data...
        this.Restangular.restangularizeElement(null, schlink, "schoollinks");
        schlink.remove().then(() => {
          // find the item by the deleted key
          // let wonderful lodash do the heavy lifting
          _.remove(this.links, { lnkID: link.lnkID });
        }, 
          () => { }
          // remove failed
        );
      });
    }

  }

  /**
   * Controller for the edit Dialog
   */
  class editController extends Sw.Component.ComponentEditController {
    static $inject = ["$mdDialog", "ApiUi"];
    constructor(public mdDialog: ng.material.IDialogService
      , apiUi: Sw.Api.IApiUi) {
      super(apiUi, null);
      this.isEditing = true;
    }
    public school: School;

    public onModelUpdated(newData: any) {
      super.onModelUpdated(newData);
      this.school.schPhoto = newData.schPhoto;
    }
    public documentChanged(document) {
      // the document was edited in the thumbnail viewer....
      // this is the same object as model, but, the change is not relayed to the modelInit...
      this.pristine();
    }

    public closeDialog() {
      this.mdDialog.cancel();
    }
  }

  class Component implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;

    constructor() {
      this.bindings = {
        links: '<',
        school: "<"
      };
      this.controller = Controller;
      this.controllerAs = "vm";
      this.templateUrl = "school/linklist";
    }
  }
  angular
    .module("pineapples")
    .component("componentSchoolLinks", new Component());

}