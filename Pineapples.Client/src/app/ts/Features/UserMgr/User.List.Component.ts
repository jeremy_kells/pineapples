﻿namespace Sw.Auth {

  class Controller extends Sw.Component.ListMonitor {
    public users: any[];
    public isWaiting: boolean;

    public monitoredList() {
      return this.users;
    }
    // can be directly invoked from the row containing the user
    public showUser(user) {
      this.state.go("site.users.list.item", { id: user.Id });
    };

    public newUser() {
      this.state.go("site.users.list.item", { id: "0" });
    }
    public deleteUser(user) {
      // display a dialog to confirm

      this.mdDialog.show(this.confirmDeleteDlg(user))
        .then(() => {
              this.isWaiting = true;
              // clone is a bit of overkill here
              // but it does allow restangular to now handle the deletion
              let tu = this.clone(user);
              tu.remove()
                .then(() => {
                  this.isWaiting = false;
                })
                .catch((error) => {
                  this.isWaiting = false;
                  this.apiUi.showErrorResponse(user, error);
                })
            });
    }

    private confirmDeleteDlg(user) {
      return this.mdDialog.confirm()
        .title('Delete User')
        .textContent("Once the account is deleted, this user will no longer be able to access the system")
        .ok('Delete')
        .cancel('Cancel');
    }

     private clone(user) {
      
      // drop any reqstangular stuff
      let u = angular.copy(user.plain ? user.plain() : user);
      // create the new object

      //restangularize it
      this.restangular.restangularizeElement(null, u, "umgr/users");
      return u;
    }
     static $inject = ["$scope", "$mdDialog", "$location", "$state", "Restangular", "ApiUi", "Lookups"];
     constructor(
       private scope: Sw.IScopeEx
       , public mdDialog: ng.material.IDialogService
       , private $location: ng.ILocationService
       , public state: ng.ui.IStateService
       , public restangular: restangular.IService
       , private apiUi: Sw.Api.IApiUi
       , public lookups: Sw.Lookups.LookupService) {
       super();
      // umgr/users is the route for managing users
      // Id is a string representation of a guid - note capitalisation from ASP.NET Identity
      this.monitor(scope, "umgr/users", "Id");
      this.multiSelect = false;

    }

  }
  class Component implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;

    constructor() {
      this.bindings = {
        users: "<"
      };
      this.controller = Controller;
      this.controllerAs = "vm";
      this.templateUrl = "usermgr/userlist";
    }
  }

  //---------------------------------------------------
  // delete confirm dialog
  //---------------------------------------------------


  angular
    .module("sw.common")
    .component("userList", new Component());
}