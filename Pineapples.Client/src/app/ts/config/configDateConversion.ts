﻿namespace Pineapples {
  /*
  This convertDateStringsToDates is an http interceptor that scans any returned json looking for string
  representations of dates, and then converts those to Date objects.
  it reconginizes the date strings based on the regex shown below, which will specifically match the date strings we expect to see returned
  from web api / newtonsoft json
  */
  //http://aboutcode.net/2013/07/27/json-date-parsing-angularjs.html
  //var regexIso8601 = /^(\d{4}|\+\d{6})(?:-(\d{2})(?:-(\d{2})(?:T(\d{2}):(\d{2}):(\d{2})(\.(\d{1,}))?(Z|([\-+])(\d{2})(?:)(\d{2}))?)?)?)?$/;
  // this much simpler regex expects only the patterns that may come from the server
  // 2017-05-20T00:00:00
  // 2017-05-20T00:00:00z

  var regexDateTime = /^[1-2]\d{3}-[0-1]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d(Z|\+\d{2}:\d{2})?$/;

  let convertDateStringsToDates = (input) => {
    // Ignore things that aren't objects.
    if (typeof input !== "object") return input;

    for (var key in input) {
      if (!input.hasOwnProperty(key)) continue;

      var value = input[key];
      var match;
      // Check for string properties which look like dates.
      if (typeof value === "string" && (match = value.match(regexDateTime))) {
        var milliseconds = Date.parse(match[0])
        if (!isNaN(milliseconds)) {
          input[key] = new Date(milliseconds);
        }
      } else if (typeof value === "object") {
        // Recurse into object
        convertDateStringsToDates(value);
      }
    }
  }

  let config = ($httpProvider) => {
    $httpProvider.defaults.transformResponse.push((responseData) => {
      convertDateStringsToDates(responseData);
      return responseData;
    });
  }

  angular
    .module("pineapples")
    .config(["$httpProvider", config]);

}