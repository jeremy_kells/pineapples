﻿namespace Pineapples.Reporting {
  class Controller {

    public jasperdefs: jasper.IResource[];
    public reports: Report[];
    public folder: string;      // the root folder for this tree of reports
    public promptForParams: string;
    public hostSetParams: any;

    static $inject = ["$mdDialog", "reportManager"];
    constructor(
      private mdDialog: ng.material.IDialogService,
      private rmgr: Pineapples.Reporting.IReportManagerService) { }

    public $onChanges(changes) {
      if (changes.folder) {
        this.reports = [];
        this.rmgr.loadJasperReportDefs(this.folder)
          .then((data) => {
            this.jasperdefs = data;
            this.jasperdefs.forEach((j) => {
              this.reports.push(this.getReport(j));
            });
          })
          .catch((error) => {
            this.jasperdefs = undefined;
          });
      }
    }

    public getReport(j: jasper.IResource) {
      let r: Report = new Report;
      r.description = j.description;
      r.path = j.uri;
      r.name = j.label;
      r.promptForParams = this.promptForParams;
      r.canRenderExcel = true;
      r.canRenderPdf = true;
      r.format = "PDF";
      return r;
    }

    // relay this to the container
    private setParams(report, context, instanceInfo) {
      this.hostSetParams({ report: report, context: context, instanceInfo: instanceInfo })
    }
  }


  class ComponentOptions implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;

    constructor() {
      this.bindings = {
        folder: '@',
        promptForParams: "@",
        reportContext: '<context',
        hostSetParams: "&setParams"
      };
      this.controller = Controller;
      this.controllerAs = "vm";
      this.templateUrl = "reports/jasperList";
    }
  }
  angular
    .module("pineapples")
    .component("reportList", new ComponentOptions());
}