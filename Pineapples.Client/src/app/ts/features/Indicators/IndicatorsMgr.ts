﻿namespace Pineapples.Indicators {

  /**
    represents a data point that can be split by gender
    and the various representations of that
    not all values are meaningful in any given context; for example, generally only one of F or I is of use.
  **/
  export interface IGenderedValue {
    T: number;    // value for all - not necessarily M + F ; for example, if this is an indicator such as GIR
    M: number;    // values for Males
    F: number;    // Value for Females
    P: number;    // Percent Females = F/T     
    I: number;     // GPI ie F/M
  }
  export class GenderedValue implements IGenderedValue{
    constructor(m: number, f: number, t?: number) {
      this.M = m;
      this.F = f;
      if (t === undefined) {
        this.T = m + f;
      } else {
        this.T = t;
      }

    }
    public T: number;    // value for all - not necessarily M + F ; for example, if this is an indicator such as GIR
    public M: number;    // values for Males
    public F: number;    // Value for Females
    public get P() { return this.F / this.T };    // Percent Females = F/T     
    public get I() { return !this.M ? undefined : this.F / this.M };     // GPI ie F/M

  }
  /**
   * @class IndicatorSet
   * @description A plain object to hold precalculated indicators values.
   * The purpose is so that we do not do the expensive vermdata loookups on each digest cycle. i.e. a 'facade'
   * The constructor takes an indicatorCalc, en the parameters that define the set
   * IndicatorMgs caches these for reuse
   * @see IndicatorsMgr, IndicatorsData, IndicatorsController, IndicatorsFactory
   */

  export class IndicatorSet {

    public Population: IGenderedValue;
    public Enrolment: IGenderedValue;
    public NetEnrolment: IGenderedValue;
    public Repeaters: IGenderedValue;
    public GER: IGenderedValue;
    public NER: IGenderedValue;
    public Intake: IGenderedValue;
    public NetIntake: IGenderedValue;
    public GIR: IGenderedValue;
    public NIR: IGenderedValue;
    public YearsOfSchooling: number;
    public OfficialStartAge: number;
    public AgeRange: string;

    // The teacher related indicators below are in two groups (Teachers/Teachers2, etc.). They are the same data but from
    // a different location in the XML file and come from a different source in the DB
    // TeacherTraining vs SurveyYearTeacherQual
    // the source of this data is set with the TEACHER_QUALCERT_SOURCE flag in sysParam table and care must be taken to use
    // the correct indicator depending on your teacher qualifications data source.
    public Teachers: IGenderedValue;
    public TeachersCert: IGenderedValue;
    public TeachersCertP: IGenderedValue;
    public TeachersQual: IGenderedValue;
    public TeachersQualP: IGenderedValue;
    public PTR: any;
    public CertPTR: any;
    public QualPTR: any;   
    public Teachers2: IGenderedValue;
    public TeachersCert2: IGenderedValue;
    public TeachersCertP2: IGenderedValue;
    public TeachersQual2: IGenderedValue;
    public TeachersQualP2: IGenderedValue;
    public PTR2: any;
    public CertPTR2: any;
    public QualPTR2: any;

    public RepRate: IGenderedValue;
    public Survival4: IGenderedValue;
    public Survival6: IGenderedValue;
    public Survival8: IGenderedValue;
    public Survival12: IGenderedValue;
    public GIRLast: IGenderedValue;

    public constructor(protected p: IndicatorCalc, protected year: number, protected edLevel: string, protected edSector: string) {

      let edLevelNode = p.getEdLevelNode(year, edLevel);

      this.Population = p.getMFT(edLevelNode, 'pop');
      this.Enrolment = p.getMFT(edLevelNode, 'enrol');
      this.NetEnrolment = p.getMFT(edLevelNode, 'nenrol');
      this.Repeaters = p.getMFT(edLevelNode, 'rep');
      this.GER = p.getMFT(edLevelNode, 'ger');
      this.NER = p.getMFT(edLevelNode, 'ner');
      this.Intake = p.getMFT(edLevelNode, 'intake');
      this.NetIntake = p.getMFT(edLevelNode, 'nIntake');
      this.GIR = p.getMFT(edLevelNode, 'gir');
      this.NIR = p.getMFT(edLevelNode, 'nir');
      this.YearsOfSchooling = p.edLevelNumYears(year, edLevel);
      this.OfficialStartAge = p.edLevelStartAge(year, edLevel);
      this.AgeRange = (this.OfficialStartAge).toString() + "-" + (this.OfficialStartAge + this.YearsOfSchooling - 1).toString() + " yo";

      let teacherNode = p.getSectorTeacherNode(year, edSector);

      this.Teachers = p.getMFT(teacherNode, "teachers");
      this.TeachersCert = p.getMFT(teacherNode, "cert");
      this.TeachersCertP = p.getMFT(teacherNode, "certperc");
      this.TeachersQual = p.getMFT(teacherNode, "cert");
      this.TeachersQualP = p.getMFT(teacherNode, "certperc");
      this.PTR = p.sectorPTR(year, edSector);
      this.CertPTR = p.sectorCertPTR(year, edSector);
      this.QualPTR = p.sectorQualPTR(year, edSector);

      let teacherNode2 = p.getSectorTeacherNode2(year, edSector);

      this.Teachers2 = p.getMFT(teacherNode2, "Teachers");
      this.TeachersCert2 = p.getMFT(teacherNode2, "Cert");
      this.TeachersCertP2 = p.getMFT(teacherNode2, "CertPerc");
      this.TeachersQual2 = p.getMFT(teacherNode2, "Qual");
      this.TeachersQualP2 = p.getMFT(teacherNode2, "QualPerc");
      this.PTR2 = p.get(teacherNode2, "PTR");
      this.CertPTR2 = p.get(teacherNode2, "CertPTR");
      this.QualPTR2 = p.get(teacherNode2, "QualPTR");

      // get last year's ed level node
      edLevelNode = p.getEdLevelNode(year - 1, edLevel);
      let LYE = p.getMFT(edLevelNode, 'enrol')       // last year's enrol

      this.RepRate = new GenderedValue(
        this.Repeaters.M / LYE.M,
        this.Repeaters.F / LYE.F,
        this.Repeaters.T / LYE.T
      );

      this.Survival4 = new GenderedValue(
        p.yoeSurvivalTo(year - 1, 4, 'M'),
        p.yoeSurvivalTo(year - 1, 4, 'F'),
        p.yoeSurvivalTo(year - 1, 4, '')
      );

      this.Survival6 = new GenderedValue(
        p.yoeSurvivalTo(year - 1, 6, 'M'),
        p.yoeSurvivalTo(year - 1, 6, 'F'),
        p.yoeSurvivalTo(year - 1, 6, '')
      );

      this.Survival8 = new GenderedValue(
        p.yoeSurvivalTo(year - 1, 8, 'M'),
        p.yoeSurvivalTo(year - 1, 8, 'F'),
        p.yoeSurvivalTo(year - 1, 8, '')
      );

      this.Survival12 = new GenderedValue(
        p.yoeSurvivalTo(year - 1, 12, 'M'),
        p.yoeSurvivalTo(year - 1, 12, 'F'),
        p.yoeSurvivalTo(year - 1, 12, '')
      );

      // gir into last year
      let lyoe = p.edLevelLastYoE(year, edLevel);
      let yoeNode = p.getYoENode(year, lyoe);

      this.GIRLast = p.getMFT(yoeNode, 'gir');
    }

  }

  /**
   * @class IndicatorsMgr
   * @description A service to construct the Indicators object required by the particular page.
   * @see Indicators, IndicatorsData, IndicatorsController, IndicatorsFactory
   */
  export class IndicatorsMgr {

    public setcache: any;
    public calccache: any
    //public p: any;

    static $inject = ["IndicatorsAPI", "$q"];
    constructor(public api: Pineapples.Api.IIndicatorsApi
    , private q: ng.IQService) {
  
      this.initialise();
    }

    /**
     * return a promise to a calculator
     * this may created here, or retrived from the IndicatorMgr's cache
     * @param districtCode
     * @param xml
     */
    public getCalculator(districtCode: string = ""): ng.IPromise<IndicatorCalc> {
      let cachekey = districtCode;
      if (!this.calccache[cachekey]) {
        // read the required data ( may already be cached at the http level )
        // then use it to construct the IndicatorSet
        let promise: ng.IPromise<any>;
        if (districtCode == "") {
          promise = this.api.vermdata();
        } else {
          // new method in api class to get district version
          promise = this.api.vermdataDistrict(districtCode);
        }
        return promise.then((xml) => {
          this.calccache[cachekey] = new IndicatorCalc(xml);
          return this.calccache[cachekey];
        });
      } else {
        // return a resolved promise
        let d = this.q.defer<IndicatorCalc>();
        d.resolve(this.calccache[cachekey]);
        return d.promise;
      }
    }

    /**
     * get an IndicatorSet from the cache, or if not in the cache, construct it cache it and return it
     * @param indicatorCalc
     * @param surveyYear
     * @param edLevelCode
     * @param edSector
     * @param districtCode
     */
    public get(indicatorCalc: IndicatorCalc, surveyYear: number, edLevelCode: string = null, edSector: string = null, districtCode: string = null) {
      let cachekey = [surveyYear, edLevelCode, edSector, districtCode].join("_");
      if (!this.setcache[cachekey]) {
        // read the required data ( may already be cached at the http level )
        // then use it to construct the IndicatorSet
        let indicatorSet = new IndicatorSet(indicatorCalc, surveyYear, edLevelCode, edSector);
        this.setcache[cachekey] = indicatorSet;
      }
      return this.setcache[cachekey];
    }

    public selectedYear: number;      // place to hold the comparison year in a two year array
    public baseYear: number;

    public initialise() {
      this.setcache = {};
      // this is a cache of calculators
      // these are indicatorcalc objects; populated with either the national data,
      // or district level data
      this.calccache = {};
      this.selectedYear = 2015;       // to do 
      this.baseYear = 2014;
    }
  }

  angular
    .module('pineapples')
    .service('IndicatorsMgr', IndicatorsMgr);

}