﻿
namespace Pineapples.Indicators {

   function format(str: string, ...col: any[]) {
    col = typeof col === 'object' ? col : Array.prototype.slice.call(arguments, 1);

    return str.replace(/\{\{|\}\}|\{(\w+)\}/g, function (m: string, n: string) {
      if (m == "{{") { return "{"; }
      if (m == "}}") { return "}"; }
      return col[n];
    });
  }
  /**
   * @class IndicatorCalc (previously Pineapples)
   * @description A data service (previously named pineapple) with responsibility of holding all indicators
   * and related data coming from XML served through an backend API endpoint. It provides the Indicators API.
   * For each indicator, it constructs the XPath into the vermdata to get the required value.
   * It also does basic calculations such as getting ratios, sums, etc.
   * This class is no longer accessed as a service, but is directly instantiated by IndicatorsMgr
   * @see IndicatorsMgr, Indicators, IndicatorsController, IndicatorsFactory
   */
   export class IndicatorCalc {

     public xml: JQuery;

     constructor(data) {
       this.xml = $(data);
    }
    // some high level identification

    public title() {
      return this.getAppNameFull();
    };
    public source() {
      return this.getSourceServer() + ':' + this.getSourceDB() + ' snapshot created ' + this.getCreateDate();
    };

    /**
     * Retrieve a value from the xml
     * @param xpath xpath to the node
     * @param xattr optional attribute of that node
     */
    public domvalue(xpath, xattr?) {
      if (xattr) {
        return this.xml.find(xpath).attr(xattr);
      } else {
        return this.xml.find(xpath).text();
      }
    }

    public domsum(xpath, xattr?) {
      let tot = 0;
      this.xml.find(xpath).each(function (ix) {
        if (xattr) {
          tot += parseFloat($(this).attr(xattr));
        } else {
          tot += parseFloat($(this).text());
        }
      });
      return tot;
    }

    public domset(xpath, xattr?) {
      let vals = [];
      let part = this.xml.find(xpath);
      part.each(function (ix) {
        if (xattr) {
          vals.push(parseFloat($(this).attr(xattr)));
        } else {
          vals.push(parseFloat($(this).text()));
        }
      });
      return _.uniq(_.sortBy(vals, function (i) { return i; })); // TODO Change to sortedUniq with lodash v4
    }

    public schoolCount(year: number, schoolType: string) {
      let xpath: string;
      if (year) {
        xpath = format('SchoolCounts > SchoolCount[year="{0}"][schoolType="{1}"] > count', year, schoolType);
      }
      else {
        xpath = format('SchoolCounts > SchoolCount[schoolType="{0}"] > count', schoolType);
      }
      return this.domsum(xpath);
    }

    public getYearsWithData () {
      let xpath = 'ERs > ER';
      return this.domset(xpath, 'year');
    }

    public ERTable(surveyYear, edLevelCode, gender = "", dataPoint) {      
      let xpath = format('ERs > ER[year="{0}"][edLevelCode="{1}"] > {3}{2}', surveyYear, edLevelCode, gender, dataPoint);
      let f: any;
      let m: any;
      let tot: any;

      switch (gender) {
        case 'I':
        case 'GPI':
        case 'F/M':
          f = this.ERTable(surveyYear, edLevelCode, "F", dataPoint);
          m = this.ERTable(surveyYear, edLevelCode, "M", dataPoint);
          return f / m;

        case "M%":
          tot = this.ERTable(surveyYear, edLevelCode, "", dataPoint);
          m = this.ERTable(surveyYear, edLevelCode, "M", dataPoint);
          return m / tot;

        case "F%":
          tot = this.ERTable(surveyYear, edLevelCode, "", dataPoint);
          f = this.ERTable(surveyYear, edLevelCode, "F", dataPoint);
          return f / tot;

        case 'M':
        case 'F':
        case '':
          return this.domsum(xpath);
      }
    }

    public edLevelHeader(surveyYear, edLevelCode, item) {
      let xpath = format('ERs > ER[year="{0}"][edLevelCode="{1}"]', surveyYear, edLevelCode);
      let xattr = item
      return this.domsum(xpath, xattr);
    }

    public edLevelEnrol(surveyYear, edLevelCode, gender) {
      return this.ERTable(surveyYear, edLevelCode, gender, "enrol");
    }

    public edLevelNetEnrol(surveyYear, edLevelCode, gender) {
      return this.ERTable(surveyYear, edLevelCode, gender, "nEnrol");
    }

    public edLevelPop(surveyYear, edLevelCode, gender) {
      return this.ERTable(surveyYear, edLevelCode, gender, "pop");
    }

    public edLevelGER(surveyYear, edLevelCode, gender) {
      return this.ERTable(surveyYear, edLevelCode, gender, "ger");
    }

    public edLevelNER(surveyYear, edLevelCode, gender) {
      return this.ERTable(surveyYear, edLevelCode, gender, "ner");
    }

    public edLevelGIR(surveyYear, edLevelCode, gender) {
      return this.ERTable(surveyYear, edLevelCode, gender, "gir");
    }

    public edLevelNIR(surveyYear, edLevelCode, gender) {
      return this.ERTable(surveyYear, edLevelCode, gender, "nir");
    }

    public edLevelGIRLast(surveyYear, edLevelCode, gender) {
      // find the last year of ed
      let yoeLast = this.edLevelLastYoE(surveyYear, edLevelCode)
      return this.yoeGIR(surveyYear, yoeLast, gender);
    }

    public edLevelNIRLast(surveyYear, edLevelCode, gender) {
      // find the last year of ed
      let yoeLast = this.edLevelLastYoE(surveyYear, edLevelCode)
      return this.yoeNIR(surveyYear, yoeLast, gender);
    }

    public edLevelIntake(surveyYear, edLevelCode, gender) {
      return this.ERTable(surveyYear, edLevelCode, gender, "intake");
    }

    public edLevelNetIntake(surveyYear, edLevelCode, gender) {
      return this.ERTable(surveyYear, edLevelCode, gender, "nIntake");
    }

    public edLevelRep(surveyYear, edLevelCode, gender) {
      return this.ERTable(surveyYear, edLevelCode, gender, "rep");
    }

    public edLevelRepRate(referenceYear, edLevelCode, gender) {
      switch (gender) {
        case "I":
        case "GPI":
        case "F/M":
          let m = this.edLevelRepRate(referenceYear, edLevelCode, "M");
          let f = this.edLevelRepRate(referenceYear, edLevelCode, "F");
          return f / m;
        case "F":
        case "M":
        case "":
          let r = this.ERTable(referenceYear + 1, edLevelCode, gender, 'rep');
          let e = this.ERTable(referenceYear, edLevelCode, gender, 'enrol');
          return r / e;
      }
    }

    // Education Level parameters
    public edLevelNumYears(surveyYear, edLevelCode) {
      return this.edLevelHeader(surveyYear, edLevelCode, "numYears");
    }

    public edLevelStartAge(surveyYear, edLevelCode) {
      return this.edLevelHeader(surveyYear, edLevelCode, "startAge");
    }

    public edLevelFirstYoE(surveyYear, edLevelCode) {
      return this.edLevelHeader(surveyYear, edLevelCode, "firstYear");
    }

    public edLevelLastYoE(surveyYear, edLevelCode) {
      return this.edLevelHeader(surveyYear, edLevelCode, "lastYear");
    }

    // Class Level enrolment
    public itemTableBase(xpathTable, surveyYear, itemCode, gender = "", dataPoint) {
      //let xpath = 'ERs > ER[year="{0}"][edLevelCode="{1}"]'.format(surveyYear, levelCode, gender, dataPoint);

      let xpath = format(xpathTable, surveyYear, itemCode, gender, dataPoint);
      let f: any;
      let m: any;
      let tot: any;

      switch (gender) {
        case 'I':
        case 'GPI':
        case 'F/M':
          f = this.itemTableBase(xpathTable, surveyYear, itemCode, "F", dataPoint);
          m = this.itemTableBase(xpathTable, surveyYear, itemCode, "M", dataPoint);
          return f / m;

        case "M%":
          tot = this.itemTableBase(xpathTable, surveyYear, itemCode, "", dataPoint);
          m = this.itemTableBase(xpathTable, surveyYear, itemCode, "M", dataPoint);
          return m / tot;

        case "F%":
          tot = this.itemTableBase(xpathTable, surveyYear, itemCode, "", dataPoint);
          f = this.itemTableBase(xpathTable, surveyYear, itemCode, "F", dataPoint);
          return f / tot;

        case 'M':
        case 'F':
        case '':
          return this.domsum(xpath);
      }

    }

    public classLevelTable(surveyYear, levelCode, gender, dataPoint) {
      let xpath = 'LevelERs > LevelER[year="{0}"][levelCode="{1}"] > {3}{2}';
      return this.itemTableBase(xpath, surveyYear, levelCode, gender, dataPoint);
    }

    public classLevelEnrol(surveyYear, levelCode, gender) {
      return this.classLevelTable(surveyYear, levelCode, gender, "enrol");
    }

    public classLevelNetEnrol(surveyYear, levelCode, gender) {
      return this.classLevelTable(surveyYear, levelCode, gender, "nEnrol");
    }

    public classLevelPop(surveyYear, levelCode, gender) {
      return this.classLevelTable(surveyYear, levelCode, gender, "pop");
    }

    public classLevelNER(surveyYear, levelCode, gender) {
      return this.classLevelTable(surveyYear, levelCode, gender, "ner");
    }

    public classLevelGER(surveyYear, levelCode, gender) {
      return this.classLevelTable(surveyYear, levelCode, gender, "ger");
    }

    // single year versions - by year of education
    public yoeTable(surveyYear, yoe, gender, dataPoint) {
      let xpath = 'LevelERs > LevelER[year="{0}"][yearOfEd="{1}"] > {3}{2}';
      return this.itemTableBase(xpath, surveyYear, yoe, gender, dataPoint);
    }

    public yoeEnrol(surveyYear, yoe, gender) {
      return this.yoeTable(surveyYear, yoe, gender, "enrol");
    }

    public yoeNetEnrol(surveyYear, yoe, gender) {
      return this.yoeTable(surveyYear, yoe, gender, "nEnrol");
    }

    public yoePop(surveyYear, yoe, gender) {
      return this.yoeTable(surveyYear, yoe, gender, "pop");
    }

    public yoeRep(surveyYear, yoe, gender) {
      return this.yoeTable(surveyYear, yoe, gender, "rep");
    }

    public yoeNER(surveyYear, yoe, gender) {
      return this.yoeTable(surveyYear, yoe, gender, "ner");
    }

    public yoeGER(surveyYear, yoe, gender) {
      return this.yoeTable(surveyYear, yoe, gender, "ger");
    }

    public yoeNIR(surveyYear, yoe, gender) {
      return this.yoeTable(surveyYear, yoe, gender, "nir");
    }

    public yoeGIR(surveyYear, yoe, gender) {
      return this.yoeTable(surveyYear, yoe, gender, "gir");
    }

    // Sectors, including teacher numbers
    public sectorTable(surveyYear, sectorCode, gender, dataPoint) {
      // changed to sector 30 12 2016
      let xpath = 'Sectors > Sector[year="{0}"][sectorCode="{1}"] > {3}{2}';
      return this.itemTableBase(xpath, surveyYear, sectorCode, gender, dataPoint);
    }

    public sectorTeachers(surveyYear, sectorCode, gender) {
      return this.sectorTable(surveyYear, sectorCode, gender, "teachers");
    }

    public sectorTeachersQual(surveyYear, sectorCode, gender) {
      return this.sectorTable(surveyYear, sectorCode, gender, "qual");
    }

    public sectorTeachersQualP(surveyYear, sectorCode, gender) {
      return this.sectorTable(surveyYear, sectorCode, gender, "qualPerc");
    }

    public sectorTeachersCert(surveyYear, sectorCode, gender) {
      return this.sectorTable(surveyYear, sectorCode, gender, "cert");
    }

    public sectorTeachersCertP(surveyYear, sectorCode, gender) {
      return this.sectorTable(surveyYear, sectorCode, gender, "certPerc");
    }

    public sectorTeachersCertQual(surveyYear, sectorCode, gender) {
      return this.sectorTable(surveyYear, sectorCode, gender, "certQual");
    }

    public sectorPTR(surveyYear, sectorCode) {
      return this.sectorTable(surveyYear, sectorCode, "", "PTR");
    }

    public sectorCertPTR(surveyYear, sectorCode) {
      return this.sectorTable(surveyYear, sectorCode, "", "certPTR");
    }

    public sectorQualPTR(surveyYear, sectorCode) {
      return this.sectorTable(surveyYear, sectorCode, "", "qualPTR");
    }

    public sectorEnrol(surveyYear, sectorCode) {
      return this.sectorTable(surveyYear, sectorCode, "", "enrol");
    }

    // Survival Rates
    public yoeSurvivalTable(year, yearOfEd, gender, dataPoint) {
      let xpath = 'Survivals > Survival[year="{0}"][yearOfEd="{1}"] > {3}{2}';

      return this.itemTableBase(xpath, year, yearOfEd, gender, dataPoint);
    }

    public yoePromotionFrom(referenceYear, yearOfEd, gender) {
      return this.yoeSurvivalTable(referenceYear, yearOfEd, gender, "PR")
    }

    public yoeRepetitionRate(referenceYear, yearOfEd, gender) {
      return this.yoeSurvivalTable(referenceYear, yearOfEd, gender, "RR")
    }

    public yoeTransitionFrom(referenceYear, yearOfEd, gender) {
      return this.yoeSurvivalTable(referenceYear, yearOfEd, gender, "TR")
    }

    public yoeSurvivalTo(referenceYear, toYearOfEd, gender, fromYearOfEd = 1) {
      switch (gender) {
        case 'I':
        case 'GPI':
        case 'F/M':
          let f = this.yoeSurvivalTo(referenceYear, toYearOfEd, 'F', fromYearOfEd);
          let m = this.yoeSurvivalTo(referenceYear, toYearOfEd, 'M', fromYearOfEd);
          return f / m;


        case 'M':
        case 'F':
        case '':
          if (fromYearOfEd == 1)
            return this.yoeSurvivalTable(referenceYear, toYearOfEd, gender, "SR");
          else
            return this.yoeSurvivalTable(referenceYear, toYearOfEd, gender, "SR") /
              this.yoeSurvivalTable(referenceYear, fromYearOfEd, gender, "SR");
      }
    }

    // construction details
    public vermTable(item) {
      // this seems not to work any more??
      //return this.xml.attr(item);
      // this does ... in absence of a better idea
      return this.xml.find("Params").parent().attr(item);
    }

    public getSourceServer() {
      return this.vermTable("server");
    }

    public getSourceDB() {
      return this.vermTable("database");
    }

    public getCreateDate() {
      return this.vermTable("createdate");
    }

    // system details
    public paramTable(item) {
      return this.xml.find("Params").attr(item);
    }

    public getNation() {
      return this.paramTable("nation");
    }

    public getAppName() {
      return this.paramTable("appname");
    }

    public getAppNameFull() {
      return this.paramTable("appnamefull");
    }

    public getNode(xpath) {
      return this.xml.find(xpath);
    }

    public getEdLevelNode(surveyYear, edLevelCode) {
      let xpath = format('ERs > ER[year="{0}"][edLevelCode="{1}"]', surveyYear, edLevelCode);
      return this.getNode(xpath);
    }

    public getSectorTeacherNode(surveyYear, sectorCode) {
      let xpath = format('Sectors > Sector[year="{0}"][sectorCode="{1}"]', surveyYear, sectorCode);
      return this.getNode(xpath);
    }

    public getSectorTeacherNode2(surveyYear, sectorCode) {
      let xpath = format('TeacherQCs > TeacherQC[year="{0}"][sectorCode="{1}"]', surveyYear, sectorCode);
      return this.getNode(xpath);
    }

    public getYoENode(surveyYear, yoe) {
      let xpath = format('LevelERs > LevelER[year="{0}"][yearOfEd="{1}"]', surveyYear, yoe);
      return this.getNode(xpath);
    }

    public getMFT(node, dataPoint): IGenderedValue {
      let t: number = parseFloat(node.find(dataPoint).text());
      let m: number = parseFloat(node.find(dataPoint + 'M').text());
      let f: number = parseFloat(node.find(dataPoint + 'F').text());

      return { T: t, M: m, F: f, P: f / t, I: f / m };
    }

    public get(node, dataPoint) {
      let v = parseFloat(node.find(dataPoint).text());
      return v;
    }

    public getEdLevelYearSeries(startYear, endYear, edLevelCode, dataPoint) {
      let ret: any = {};
      let data: any = {};

      dataPoint.forEach(function (dp) {
        data[dp] = { M: [], F: [], T: [] };
      });
      for (let i = startYear; i <= endYear; i++) {

        let edLevelNode = this.getEdLevelNode(i, edLevelCode);
        if (edLevelNode) {
          dataPoint.forEach(function (dp) {
            let mft = this.getMFT(edLevelNode, dp);

            data[dp].M.push({ yr: i, v: mft.M });
            data[dp].F.push({ yr: i, v: mft.F });
            data[dp].T.push({ yr: i, v: mft.T });
          });
        }
      }
      ret = {};
      dataPoint.forEach(function (dp) {
        let d = [{ key: 'M', values: data[dp].M },
        { key: 'F', values: data[dp].F },
        { key: 'all', values: data[dp].T }
        ];
        ret[dp] = d;
      });
      return ret;
    }

  }
}