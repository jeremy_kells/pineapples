﻿module Pineapples.Api {

  let factory = ($q: ng.IQService, restAngular: restangular.IService, errorService) => {

    let svc: any = Sw.Api.ApiFactory.getApi(restAngular, errorService, "schoolaccreditations");
    svc.read = (id) => svc.get(id).catch(errorService.catch);

    svc.new = () => {

      let e = new Pineapples.SchoolAccreditations.SchoolAccreditation({
        saID: null
      });
      restAngular.restangularizeElement(null, e, "schoolaccreditations");
      var d = $q.defer();
      d.resolve(e);
      return d.promise;
    }
    return svc;
  };

  angular.module("pineapplesAPI")
    .factory("schoolAccreditationsAPI", ['$q', 'Restangular', 'ErrorService', factory]);
}
