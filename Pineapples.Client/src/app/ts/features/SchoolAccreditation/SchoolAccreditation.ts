﻿namespace Pineapples.SchoolAccreditations {

  export class SchoolAccreditation extends Pineapples.Api.Editable implements Sw.Api.IEditable {

    //private _scores: Array<Number>;

    constructor(saData) {
      super();
      angular.extend(this, saData);


      // This does NOT work. 
      //this._scores = [
      //  (<any>this).L1, (<any>this).L2, (<any>this).L3, (<any>this).L4,
      //  (<any>this).T1, (<any>this).T2, (<any>this).T3, (<any>this).T4,
      //  (<any>this).D1, (<any>this).D2, (<any>this).D3, (<any>this).D4,
      //  (<any>this).N1, (<any>this).N2, (<any>this).N3, (<any>this).N4,
      //  (<any>this).F1, (<any>this).F2, (<any>this).F3, (<any>this).F4,
      //  (<any>this).S1, (<any>this).S2, (<any>this).S3, (<any>this).S4,
      //  (<any>this).CO1, (<any>this).CO2
      //];

    }

    // create static method returns the object constructed from the resultset object from the server
    public static create(resultSet: any) {
      let sa = new SchoolAccreditation(resultSet);
      return sa;
    }

    // IEditable implementation
    public _name() {
      return (<any>this).schNo + '-' + (<any>this).InspYear;
    }

    public _type() {
      return "schoolaccreditation";
    }

    public _id() {
      return (<any>this).saID
    }

    // SchoolAccreditation specific

    // Bind the level tally's to their automatically calculated values
    // I tried not repeating this below somehow but could not get it to work
    public get LT1() {
      return _.filter([
        (<any>this).L1, (<any>this).L2, (<any>this).L3, (<any>this).L4,
        (<any>this).T1, (<any>this).T2, (<any>this).T3, (<any>this).T4,
        (<any>this).D1, (<any>this).D2, (<any>this).D3, (<any>this).D4,
        (<any>this).N1, (<any>this).N2, (<any>this).N3, (<any>this).N4,
        (<any>this).F1, (<any>this).F2, (<any>this).F3, (<any>this).F4,
        (<any>this).S1, (<any>this).S2, (<any>this).S3, (<any>this).S4,
        (<any>this).CO1, (<any>this).CO2
      ], function (o) { return o == 1; }).length; }
    public set LT1(newLT1) { }

    public get LT2() {
      return _.filter([
        (<any>this).L1, (<any>this).L2, (<any>this).L3, (<any>this).L4,
        (<any>this).T1, (<any>this).T2, (<any>this).T3, (<any>this).T4,
        (<any>this).D1, (<any>this).D2, (<any>this).D3, (<any>this).D4,
        (<any>this).N1, (<any>this).N2, (<any>this).N3, (<any>this).N4,
        (<any>this).F1, (<any>this).F2, (<any>this).F3, (<any>this).F4,
        (<any>this).S1, (<any>this).S2, (<any>this).S3, (<any>this).S4,
        (<any>this).CO1, (<any>this).CO2
      ], function (o) { return o == 2; }).length; }
    public set LT2(newLT2) { }

    public get LT3() {
      return _.filter([
        (<any>this).L1, (<any>this).L2, (<any>this).L3, (<any>this).L4,
        (<any>this).T1, (<any>this).T2, (<any>this).T3, (<any>this).T4,
        (<any>this).D1, (<any>this).D2, (<any>this).D3, (<any>this).D4,
        (<any>this).N1, (<any>this).N2, (<any>this).N3, (<any>this).N4,
        (<any>this).F1, (<any>this).F2, (<any>this).F3, (<any>this).F4,
        (<any>this).S1, (<any>this).S2, (<any>this).S3, (<any>this).S4,
        (<any>this).CO1, (<any>this).CO2
      ], function (o) { return o == 3; }).length; }
    public set LT3(newLT3) { }

    public get LT4() {
      return _.filter([
        (<any>this).L1, (<any>this).L2, (<any>this).L3, (<any>this).L4,
        (<any>this).T1, (<any>this).T2, (<any>this).T3, (<any>this).T4,
        (<any>this).D1, (<any>this).D2, (<any>this).D3, (<any>this).D4,
        (<any>this).N1, (<any>this).N2, (<any>this).N3, (<any>this).N4,
        (<any>this).F1, (<any>this).F2, (<any>this).F3, (<any>this).F4,
        (<any>this).S1, (<any>this).S2, (<any>this).S3, (<any>this).S4,
        (<any>this).CO1, (<any>this).CO2
      ], function (o) { return o == 4; }).length; }
    public set LT4(newLT4) {}

    public get T() { return this.LT1 + this.LT2 + this.LT3 + this.LT4; }
    public set T(newT) {}

    // Yeah...I just literally translated the Excel formula from the accreditation
    // department in FSM
    public get SchLevel() {
      if (this.LT1 >= 4) {
        return "Level 1";
      } else {
        if (this.LT4 >= this.LT3 && this.LT4 >= this.LT2 && this.LT4 >= this.LT1) {
          return "Level 4";
        } else {
          if (this.LT3 > this.LT4 && this.LT3 >= this.LT2 && this.LT3 >= this.LT1) {
            return "Level 3";
          } else {
            if (this.LT2 > this.LT3 && this.LT2 > this.LT4 && this.LT2 >= this.LT1) {
              return "Level 2";
            } else {
              if (this.LT1 > this.LT2 && this.LT1 > this.LT3 && this.LT1 > this.LT4) {
                return "Level 1";
              }
            }
          }
        }
      }    
    }
    public set SchLevel(newSchLevel) { }

  }
}
