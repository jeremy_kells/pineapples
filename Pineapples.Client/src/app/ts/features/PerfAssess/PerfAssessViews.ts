﻿namespace Pineapples.PerfAssess {

  var modes = [
        {
          key: "Perf Assess",
          columnSet: 1,
          gridOptions: {
            columnDefs: []
          }
        }
  ];
  var pushModes = function (filter) {
    filter.PushViewModes(modes);
  };

  angular
      .module('pineapples')
      .run(['PerfAssessFilter', pushModes])
}
