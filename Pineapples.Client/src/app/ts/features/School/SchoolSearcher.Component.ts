﻿namespace Pineapples.Schools {

  interface IBindings {
    filtervm: any;
  }

  class Controller implements IBindings {
    public filtervm: any;
    public filter: any;

    static $inject = ["$scope", "$controller"];

    constructor(protected $scope: ng.IScope, protected $controller: ng.IControllerService) {
      let scope = $scope.$new();
      this.filter = $controller('FilterController', scope);
    }

    public $onChanges(changes) {
      if (changes.filtervm) {
        console.log('$onChanges: ', this.filtervm);
      }
    }

  }

  class Component implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;

    constructor() {
      this.bindings = {
        filtervm: '<'
      };
      this.controller = Controller;
      this.controllerAs = "vm";
      this.templateUrl = "school/SearcherComponent";
    }
  }

  angular
    .module("pineapples")
    .component("schoolSearcherComponent", new Component());
}