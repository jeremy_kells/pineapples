// School Routes
module Pineappples.Schools {

  let routes = function ($stateProvider) {
    var featurename = 'Schools';
    var filtername = 'SchoolFilter';
    var templatepath = "school";
    var tableOptions = "schoolFieldOptions";
    var url = "schools";
    var usersettings = null;
    var mapview = 'SchoolMapView';

    // root state for 'school' feature
    let state: ng.ui.IState = Sw.Utils.RouteHelper.featureState(featurename, filtername, templatepath, url, usersettings, tableOptions, mapview);

    // default 'api' in this feature is schoolsAPI
    state.resolve = state.resolve || {};
    state.resolve["api"] = "schoolsAPI";

    let statename = "site.schools";
    $stateProvider.state("site.schools", state); // why not use addFeatureState?

    // state for a high level dashboard
    // This is where I wanted the dashboards state for schools (same goes for other entities) - GH
    state = {
      url: "^/schools/dashboard",
      data: {
        permissions: {
          only: 'SchoolReadX'
        }
      },
      views: {
        "@": {
          component: "schoolsDashboardComponent" // temporary dashboard to be replace by new stuff
        }

      },
    };
    $stateProvider.state("site.schools.dashboard", state);
    
    // takes an editable list state
    state = Sw.Utils.RouteHelper.editableListState("School");
    statename = "site.schools.list";
    state.url = "^/schools/list";
    $stateProvider.state(statename, state);
    console.log("state:" + statename);
    console.log(state);

    // chart table and map
    Sw.Utils.RouteHelper.addChartState($stateProvider, featurename);
    Sw.Utils.RouteHelper.addTableState($stateProvider, featurename);
    Sw.Utils.RouteHelper.addMapState($stateProvider, featurename, mapview);

    // new - state with a custom url route
    state = {
      url: "^/schools/new",
      params: { id: null, columnField: null, rowData: {} },
      data: {
        permissions: {
          only: 'SchoolWriteX'
        }
      },
      views: {
        "actionpane@site.schools.list": {
          component: "componentSchool"
        }
      },
      resolve: {
        model: ['schoolsAPI', '$stateParams', function (api, $stateParams) {
          return api.new();
        }]
      }
    };
    $stateProvider.state("site.schools.list.new", state);

    state = {
      url: "^/schools/reports",
      views: {
        "@": "reportPage"       // note this even more shorthand syntax for a component based view
      },
      resolve: {
        folder: () => "Schools",           // not a promise, but to get the automatic binding to the component, make a resolve for folder
        promptForParams: () => "always"
      }
    }
    $stateProvider.state("site.schools.reports", state);

    // item state
    // This is where I wanted the dashboards for individual schools (same goes for other entities) - GH
    state = {
      url: "^/schools/{id}",
      params: { id: null, columnField: null, rowData: {} },
      views: {
        "actionpane@site.schools.list": {
          component: "componentSchool" // down the line contains some old individual school dashboard bits - GH
        }
      },
      // ui-router 1.x automatically binds the component bindings to a resolve of the same name
      // otherwise you can add a 'bindings' property to the state to map rsolve names to component inputs 
      //see
      //https://ui-router.github.io/guide/ng1/route-to-component
      resolve: {
        model: ['schoolsAPI', '$stateParams', function (api, $stateParams) {
          return api.read($stateParams.id);
        }]
      }
    };
    $stateProvider.state("site.schools.list.item", state);

    // unique to schools
    $stateProvider.state("site.schools.list.latlng", {
      url: "/latlng",
      params: { id: null, columnField: null, rowData: {} },
      views: {
        "actionpane@site.schools.list": {
          templateUrl: "school/latlng",
          controller: 'SchoolLatLng',
          controllerAs: 'vm'
        }

      },
      resolve: {
        data: ['$stateParams', function (params) { return params.rowData; }]

      }
    });

  }

  angular
    .module('pineapples')
    .config(['$stateProvider', routes])

}
