﻿namespace Pineapples.Schools {

  /**
   * @interface IBindings
   * @description What bindings are expected in the component's controller
   * @see IChartMetric
   */
  interface IBindings {
    quarterlyreports: any;
    title: string;
    meta: Array<IChartMetric>;
    ngDisabled: boolean;
  }

  /**
   * @interface IChartUnitData
   * @description A single atomic point on the chart. A Javascript item would look like
   * ```javascript
   * {
   *   svyYear: 2015,
   *   value: 0.98
   * }
   * ```
   */
  interface IChartUnitData {
    period: string;
    value: number;
  }

  /**
   * @interface IChartMetric
   * @description A description of the metric for a single dataset in a chart. A Javascript item would look like
   * ```javascript
   * {
   *   metric: 'AggDaysAbs',
   *   name: 'Agg. Days of Absence'
   * }
   * ```
   * where the metric is am actual metric coming from a School quaterly report list and
   * name simply a more verbose name for it.
   */
  interface IChartMetric {
    metric: string;
    name: number;
  }

  /**
   * @interface IChartDataset
   * @description A given dataset with associated metadata. It is is packaged in a way ready to be passed
   * to a Plottable.Dataset easily. A single item would look like
   * ```javascript
   * {
   *   ds: [{period: "2015/Q3", value: 0.98}, {period: "2015/Q4", value: 0.985}]
   *   meta: {metric: 'AvgDailyAtt', name: 'Avg. Daily Attendance'}
   * }
   * ```  
   * @see IChartUnitData, IChartMetric
   * @example new Plottable.Dataset(chartData.ds[0]) where chartData: IChartDataset
   */
  interface IChartDataset {
    ds: Array<IChartUnitData>;
    meta: IChartMetric;
  }

  /**
   * @interface IChartData
   * @description Contains the chart datasets and some other information for the chart
   * @see IChartDataset
   */
  interface IChartData {
    title: string;
    datasets: Array<IChartDataset>;
  }

  class Controller extends Sw.Charts.SizeableComponentController implements IBindings {

    // bindings
    public quarterlyreports: any;
    public title: string;
    public meta: Array<IChartMetric>;
    public ngDisabled: boolean;

    private _chartData: IChartData;
    private _tip;
    private chart: Plottable.Component;

    private datasets: any;
    private sortedQrs: any;

    // injections
    static $inject = ["$compile", "$timeout", "$scope", "$element"];

    constructor($compile: ng.ICompileService, $timeout: ng.ITimeoutService, scope: ng.IScope, element) {
      super($compile, $timeout, scope, element);
    }

    public $onChanges(changes: any) {
      console.log('$onChanges quarterlyreport chart: ', changes);

      // what sort of chart datasets we dealing with (but no data yet)
      this.datasets = _.map(this.meta, function (i) {
        return { ds: [], meta: i };
      });

      // Sort our quarterly reports by period
      this.sortedQrs = this.quarterlyreports.sort(function (a, b) {
        let aYearAndQuarter: [number, string] = a.InspQuarterlyReport.split("/");
        let bYearAndQuarter: [number, string] = b.InspQuarterlyReport.split("/");

        if (aYearAndQuarter[0] < bYearAndQuarter[0]) {
          return -1;
        }
        if (aYearAndQuarter[0] > bYearAndQuarter[0]) {
          return 1;
        }
        if (aYearAndQuarter[0] === bYearAndQuarter[0]) {
          let periodIndex = { 'Q3': 1, 'Q4': 2, 'Q1': 3, 'Q2': 4 };
          if (periodIndex[aYearAndQuarter[1]] < periodIndex[bYearAndQuarter[1]]) {
            return -1;
          }
          if (periodIndex[aYearAndQuarter[1]] > periodIndex[bYearAndQuarter[1]]) {
            return 1;
          }
        }

      });

      // For each school's quarterly reports extract data of interest into datasets
      for (let qr of this.sortedQrs) {
        for (let dataset of this.datasets) {
          // What metric is this? and get its quarterly report data
          let metric = dataset.meta.metric;
          let period = qr.InspQuarterlyReport;
          let value = qr[metric];
          dataset.ds.push({ period: period, value: value });
        }
      }

      // Compile our chart data
      this._chartData = {
        title: this.title,
        datasets: this.datasets
      };
    }

    /**
     * @method newSize
     * @override Sw.Charts.SizeableComponentController's newSize
     * @description applies some rounding so this is not responding to tiny fluctuations
     */
    public setSize(newSize: Sw.Charts.Size) {
      let scrollwidth = ((window.innerWidth - $(window).width())) || 0;
      this.size.width = Math.floor(newSize.width / 10) * 10 - (20 - scrollwidth);
      this.size.height = Math.floor(this.size.width / 3); // 3:1 aspect ratio is the default
    }

    /**
     * @method onResize
     * @override Sw.Charts.SizeableComponentController's onResize
     */
    public onResize(element) {
      if (this.size.height === 0) {
        return;
      }
      if (this.chart) {
        this.chart.redraw();
      } else {
        this.makeChart(element);
      }
    }

    public clickHandler(d) {
      alert(d.period);
    }

    public makeChart = (element) => {
      let chartData: IChartData = this._chartData;
      let lineChart = new Plottable.Plots.Line();
      let categoryScaleV = new Plottable.Scales.Category();
      let valueScaleV = new Plottable.Scales.Linear();
      let colorScaleV = new Plottable.Scales.Color();
      let categoryAxisV = new Plottable.Axes.Category(categoryScaleV, "bottom");
      categoryAxisV.tickLabelAngle(-90);
      let valueAxisV = new Plottable.Axes.Numeric(valueScaleV, "left");
      let legendV = new Plottable.Components.Legend(colorScaleV)
        .maxEntriesPerRow(3)
        .xAlignment('left');

      let plottableDatasets = _.map(chartData.datasets, function (dataset) {
        return new Plottable.Dataset(dataset.ds).metadata(dataset.meta.name);
      });

      let tip = d3.tip()
        .attr('class', 'd3-tip')
        .offset([-10, 0])
        .html(function (d, i) {
          return d[i].value;
        });
      this._tip = tip;      // set aside so we can destroy it later...

      let compile = this.$compile;
      let scope = this.scope;
      let self = this;
      let callbackAnimate = function (jr: Plottable.Drawers.JoinResult, attr, drawer) {
        (<d3.selection.Update<any>>jr.merge)
          .on("mouseover", tip.show)
          .on("mouseout", tip.hide)
          .on("click", self.clickHandler)
          .call(function () {
            compile(this[0].parentNode)(scope);
          });

        this.innerAnimate(jr, attr, drawer);
      };
      let innerAnimator = new Plottable.Animators.Easing()
        .stepDuration(500)
        .stepDelay(0);
      let animator = new Plottable.Animators.Callback()
        .callback(callbackAnimate)
        .innerAnimator(innerAnimator);

      plottableDatasets.forEach(function (plottableDs) {
        lineChart.addDataset(plottableDs);
      });

      lineChart
        .x((d) => d.period, categoryScaleV)
        .y((d) => d.value, valueScaleV)
        .attr("stroke", (d, i, dataset) => dataset.metadata(), colorScaleV)
        .attr("opacity", 1)
        .animated(true)
        .animator(Plottable.Plots.Animator.MAIN, animator);

      this.chart = new Plottable.Components.Table([
        [valueAxisV, lineChart],
        [null, categoryAxisV],
        [null, legendV]
      ]);

      let div = $(element).find("div")
      let svg = $(element).find("div>svg");
      let d3svg = d3.selectAll(svg.toArray());
      //d3svg.call(<any>tip);  // Deactivate tooltip and add this in new issue
      this.chart.renderTo(d3svg);
    }

  }

  class QuarterlyReportChart implements ng.IComponentOptions {
    public bindings: any = {
      quarterlyreports: "<",
      title: "@",
      meta: "<",
      ngDisabled: "="
    };
    public controller: any = Controller;
    public controllerAs: string = "vm";

    public template = `<div><svg width="{{vm.size.width}}" height="{{vm.size.height}}"></svg></div>`;
  }

  angular
    .module("pineapples")
    .component("quarterlyReportChart", new QuarterlyReportChart());

}