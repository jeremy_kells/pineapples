﻿namespace Pineapples.Survey {

  interface IBindings {
    schoolSurvey: any;
  }

  class Controller implements IBindings {
    public schoolSurvey: any;

    public $onChanges(changes) {
      if (changes.schoolSurvey) {
        console.log("changes.schoolSurvey: ", this.schoolSurvey);
      }
    }

  }

  class Component implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;

    constructor() {
      this.bindings = {
        schoolSurvey: '<'
      };
      this.controller = Controller;
      this.controllerAs = "vm";
      this.templateUrl = "survey/Survey";
    }
  }

  angular
    .module("pineapples")
    .component("survey", new Component());
}
