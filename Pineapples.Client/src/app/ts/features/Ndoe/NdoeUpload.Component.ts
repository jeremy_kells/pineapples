﻿namespace Pineapples.Ndoe {

  interface INdoeProcessClient {
    connectFile(fileId: string);
    disconnectFile(fileId: string);
  }
  class ndoeProgress { 
    public schools: string;
    public students: string;
    public staff: string;
    public wash: string;
  }

  /**
   * the response on upload identifies the data, and also validates that school nos are OK
   * across each sheet. Bad or missing schoool numbers are returned in the vaidations field
   */
  class ndoeUploadResponse {

    constructor(responseData: any) {
      angular.extend(this, responseData);
    }

    public validations: any[];
    public staffValidations: any[];
    public get hasValidations(): boolean {
      return (this.validations && this.validations.length > 0);
    }
    public get hasStaffValidations(): boolean {
      return (this.staffValidations && this.staffValidations.length > 0);
    }

  }

  /**
   * the first recordset return is the validations array
   * if this is empty, then the upload will take place and the
   * pupil table is delivered in the last 4 recordsets
   * the second recordset is Warnings related to conflicts of National ID number.
   * These will not prevent the upload if present.
   * see the stored proc NdoeLoadStudents
   */
  class ndoeProcessResponse {

    constructor(private responseData: any[]) {

    }
    public get hasValidations(): boolean {
      return (this.responseData[0].length > 0);
    }
    public get validations() {
      return (this.responseData[0]);
    }

    public get hasWarnings(): boolean {
      return (this.responseData[1].length > 0);
    }
    public get warnings() {
      return (this.responseData[1]);
    }
    public get hasEnrolmentPupilTable(): boolean {
      return (this.responseData.length >= 6);
    }
    public get enrolmentPupilTable(): Pineapples.PupilTable {
      if (!this.hasEnrolmentPupilTable) {
        return null;
      }

      return new Pineapples.PupilTable(this.responseData[2][0],
        this.responseData[3],
        this.responseData[4],
        this.responseData[5]);
    }
    public get hasRepeaterPupilTable(): boolean {
      return (this.responseData.length >= 8);
    }

    public get repeaterPupilTable(): Pineapples.PupilTable {
      if (!this.hasRepeaterPupilTable) {
        return null;
      }

      return new Pineapples.PupilTable(this.responseData[6][0],
        this.responseData[3], // reusing the rows (schools)
        this.responseData[4], // resuing the columns (levels)
        this.responseData[7]);
    }

    public get hasPSAPupilTable(): boolean {
      return (this.responseData.length >= 10);
    }

    public get psaPupilTable(): Pineapples.PupilTable {
      if (!this.hasPSAPupilTable) {
        return null;
      }

      return new Pineapples.PupilTable(this.responseData[8][0],
        this.responseData[3], // reusing the rows (schools)
        this.responseData[9], // age in the columns
        this.responseData[10]);
    }
  }

  class Controller extends Pineapples.Documents.DocumentUploader {

    public doc: any;      // the document record representing the photo

    public model: any;      // teacher link, bound from the ui

    public imageHeight: number;

    public document: any;     // this is the document object
    public allowUpload: boolean;

    public docPath: string;

    public isProcessing: boolean;
    public uploadResponseData: ndoeUploadResponse;
    public processResponseData: ndoeProcessResponse;

    public progressData: ndoeProgress;
    public enrolmentSummary: Pineapples.PupilTable;
    public repeaterSummary: Pineapples.PupilTable;
    public psaSummary: Pineapples.PupilTable;

    public hub: INdoeProcessClient;          // the ndoeprocess signalr hub

    static $inject = ["identity", "documentsAPI", "FileUploader", "$mdDialog", "Hub", "$rootScope", "$http"];
    constructor(public identity: Sw.Auth.IIdentity, docApi: any, FileUploader
      , mdDialog: ng.material.IDialogService
      , public Hub: ngSignalr.HubFactory
      , public rootScope: ng.IRootScopeService
      , public http: ng.IHttpService) {
      super(identity, docApi, FileUploader, mdDialog);
      this.uploader.url = "api/ndoe/upload";
      
      this.uploader.filters.push({
        name: "openxml", fn: (file) => this.isExcelOpenXml(file.name),
        msg: "The NDOE Survey Workbook must be an Excel 2007 Workbook with extension .xslm"
      });
      let options: ngSignalr.HubOptions;
      options = {
        listeners: {
          "progress": (fileId: string, state: ndoeProgress) => {
            console.log(state);
            this.progressData = state;
          }
        },
        methods: ["connectFile", "disconnectFile"],
        errorHandler: (error: string) => {
        },
        logging: true,
        useSharedConnection: false,
        rootPath: "./signalR",        // needs to be relative to the site path in general
        stateChanged: (state: SignalR.StateChanged) => {
          switch (state.newState) {
            case SignalR.ConnectionState.Connected:
              break;
            case SignalR.ConnectionState.Connecting:
              break;
            case SignalR.ConnectionState.Disconnected:
              this.hub = <any>(new Hub("ndoeprocess", options));
              break;
            case SignalR.ConnectionState.Reconnecting:
              break;
          }
        }
      };
      this.hub = <any>(new Hub("ndoeprocess", options));
    };


    protected onAfterAddingFile(fileItem: angularFileUpload.FileItem) {
      super.onAfterAddingFile(fileItem);
      // if we have started to process a new file, clear all the state
      this.uploadResponseData = null;
      this.progressData = null;
      this.processResponseData = null;
      this.enrolmentSummary = null;
      this.repeaterSummary = null;
      this.psaSummary = null;
      this.isProcessing = false; // probably unnecessary, anticipating a missed change of state through an error?

    };

    protected onCompleteItem(fileItem: angularFileUpload.FileItem, response, status , headers) {
      this.uploadResponseData = new ndoeUploadResponse(response);    // note slightly different format - not an array item
      
      // show the dislog for confirmation to continue;
      this._confirmationDialog(this.uploadResponseData).then(() => {
        // connect to signalR
        let fileId = response.id;
        this.hub.connectFile(fileId);
        this.isProcessing = true;

        
        this.http.get("api/ndoe/process/" + fileId).then((processResponse) => {
          this.hub.disconnectFile(fileId);
          console.log(processResponse);
          this.isProcessing = false;
          this.processResponseData = new ndoeProcessResponse(<any[]>(<any>processResponse.data).ResultSet);
          this.enrolmentSummary = this.processResponseData.enrolmentPupilTable;
          this.repeaterSummary = this.processResponseData.repeaterPupilTable;
          this.psaSummary = this.processResponseData.psaPupilTable;
          this.uploader.queue.forEach((item) => {
            item.remove();
          });
        },
          // an error response has come back from the processing
          this.processError
        );
      }, () => {
        // cancel the upload from the confirm dialog
        // you may not be given the choice to continue if there are validation errors
        this.progressData = null;
        this.enrolmentSummary = null;
        this.repeaterSummary = null;
        this.psaSummary = null;
        this.isProcessing = false;
        this.uploader.queue.forEach((item) => {
          item.remove();
        });
      });
   
    };

    protected onBeforeUploadItem(item) {
      super.onBeforeUploadItem(item);
    }

    private _confirmationDialog(info) {
      let options: ng.material.IDialogOptions = {
        locals: { info: info },
        controller: confirmController,
        controllerAs: 'dvm',
        bindToController: true,
        templateUrl: "ndoe/uploadconfirm"

      }
      return this.mdDialog.show(options);
    }
    // life cycyle hooks
    public $onChanges(changes) {
    }

    public $onInit() {
    }

    // promise error handlers
    // this is a callback - so use lambda format to bind to this
    // (otherwise, this is 'Window' )
    private processError = (errorResponse) => {
      //this.hub.disconnectFile(fileId);

      let title = "Error Processing File";
      let msg = errorResponse.data.message;
      let ariaLabel = "Processing error";
      this.mdDialog.show(
        this.mdDialog.alert()
          .clickOutsideToClose(true)
          .title(title)
          .textContent(msg)
          .ariaLabel(ariaLabel)
          .multiple(true)
          .ok('Close')
      );
    }
  }

  class ComponentOptions implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;

    constructor() {
      this.bindings = {
      };
      this.controller = Controller;
      this.controllerAs = "vm";
      this.templateUrl = "ndoe/Upload";
    }
  }
  angular
    .module("pineapples")
    .component("ndoeUploadComponent", new ComponentOptions());

  class confirmController extends Sw.Component.MdDialogController {
    public info: ndoeUploadResponse;
  }
}