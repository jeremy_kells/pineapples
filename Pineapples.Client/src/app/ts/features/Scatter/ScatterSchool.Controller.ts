﻿module Pineapples {

  class SchoolScatterController {
    public theData: any;
    public resultset: any;
    public isWaiting: boolean;
    public colorBy: string;

    static $inject = ["$scope", "SchoolScatterFilter"];
    constructor($scope: ng.IScope, public theFilter: Sw.Filter.IFilter) {

      this.colorBy = "District";
      $scope.$on('SearchComplete', (event, resultpack) => {
        if (resultpack.entity === this.theFilter.entity) {
          this.resultset = resultpack.resultset;
          // 3 recordsets come back - first is the scatter data

          this.theData = d3.nest()
            .key(function (d: any) { return d.District; })
            .entries(this.resultset[0]);

        }
      });

      $scope.$on('FindNow', (event, data) => {
        if (data.filter.entity === this.theFilter.entity) {
          data.actions.push(Sw.Filter.Request.search);
          this.isWaiting = true;
        }
      });
    }
  }

angular
  .module('pineapples')
  .controller('SchoolScatterController', SchoolScatterController);
}
