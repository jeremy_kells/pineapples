﻿namespace Pineapples.Teachers {

  // Not in used yet. Here as placeholder for future work
  export class TeacherLink extends Pineapples.Documents.Document implements Sw.Api.IEditable {

    constructor(linkData) {
      super(linkData);
     }

    // create static method returns the object constructed from the resultset object from the server
    public static create(resultSet: any) {
      let teacherlink = new TeacherLink(resultSet);
      return teacherlink;
    }

    // IEditable implementation
    public _name() {
      return (<any>this).docDescr;
    }
    public _type() {
      return "teacherlink";
    }
    public _id() {
      return (<any>this).lnkID
    }

    public _transform(newData) {
      // convert these incoming data values
      return super._transform(newData);
    }

  }
}