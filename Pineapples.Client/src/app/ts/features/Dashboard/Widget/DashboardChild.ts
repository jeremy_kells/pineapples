﻿namespace Pineappples.Dashboards {

  // Move to utils somewhere?
  export const uniqueId = () => '_' + Math.random().toString(36).substr(2, 9);


  class Controller extends Pineapples.Reporting.ReportRunner {


    isSelected: boolean;
    anotherComponentSelected: boolean;
    componentId: string;
    toggleSelected: any;

    reportPath: string;

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /* Reporting
       =========

     Composition over inheritance - Rather than extending Pineapples.Reporting.ReportRunner, 
     It would be better to hold (or create on demand) something that does.
     Or better yet, could .run() be a static method? with parameters? or not quite as good, 
     Is this because of the need to prompt for params?   

     i.e. static method:

     Pineapples.Reporting.ReportRunner.run({
       reportPath: this.reportPath,
       promptForParams: "always",
       canRenderPdf: true,
       canRenderExcel: true,
       format: "PDF",
       context: {
         surveyYear: this.dashboard.options.selectedYear,
         district: this.dashboard.options.selectedDistrict
       },
     });
    */

    public report: any;
    public reportContext: any;

    public getReportContext() {
      // Pass options down in binding to get this.options
      return {
        //  surveyYear: this.options.selectedYear,
        //  district: this.options.selectedDistrict
      }
    }

    // setParams is noop - construct reportContext as the parameters
    public setParams = () => { }

    public runReport() {
      // first step is to get a Report object from the definition 
      this.report = new Pineapples.Reporting.Report();
      this.report.path = this.reportPath;
      this.report.promptForParams = "always";
      this.report.canRenderPdf = true;
      this.report.canRenderExcel = true;
      this.report.format = "PDF";
      this.reportContext = this.getReportContext();
      this.run();
    }

    static $inject = [
      //"$timeout",
      "$mdDialog",
      //"$state",
      "Lookups",
      "reportManager",
    ];
    constructor(
      //protected timeout: ng.ITimeoutService,
      mdDialog: ng.material.IDialogService,
      //protected $state: ng.ui.IStateService,
      public lookups: Sw.Lookups.LookupService,
      rmgr: Pineapples.Reporting.IReportManagerService,
    ) {
      super(mdDialog, rmgr);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /* Without extending ReportRunner */

    //static $inject = ["Lookups",];
    //constructor(public lookups: Sw.Lookups.LookupService) {
    //  this.componentId = uniqueId();
    //}

  }

  class Component implements ng.IComponentOptions {
    public bindings: any;
    public transclude: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;

    constructor() {
      this.bindings = {
        reportPath: '@',
        isSelected: "<",
        anotherComponentSelected: "<",
        toggleSelected: "<",
        componentId: "<",
      };
      this.transclude = {
        title: "?headingTitle",
        filters: "?headingFilters",
        body: "?childBody",
        selectDisplayOptions: "?headingOptions"
      };

      this.controller = Controller;
      this.controllerAs = "vm";
      this.templateUrl = `dashboard/widget/DashboardChild`;
    }
  }

  angular
    .module("pineapples")
    .component("dashboardChild", new Component());
}
