﻿namespace Sw.Api {
  export interface IEditable {
    get?(): ng.IPromise<any>;
    put?(): ng.IPromise<any>;
    post?(): ng.IPromise<any>;
    remove?(): ng.IPromise<any>;
    plain?(): any;

    _rowversion: string; // implement with getter setter
    _rowversionProperty(): string;

    _id(): any;
    _type(): string;
    _name(): string;

    // called when an object is updated, and fresh data is returned from the server on response
    _onUpdated(newData: any): void;

    // called from restangular interceptor when a data object is about to be saved
    _beforeSave(data: any): any
  }
}
