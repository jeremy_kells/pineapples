﻿namespace Pineapples {

  class Controller {
    public nullmenu = [
      {
        label: 'Sign In',
        iconClasses: 'fa fa-home',
        state: 'signin'
      },
    ];

    public isAuthenticated: boolean;
    public currentMenu;

    private openItems = [];
    private selectedItems = [];
    private selectedFromNavMenu = false;

    static $inject = ["$scope", "$location", "$timeout", "identity", "$transitions","GlobalSettings"];
    constructor(scope, location, timeout, private identity: Sw.Auth.Identity, public transition, public global) {
      // set up watches for menu changes, including user logins
      scope.$on('auth:login-success', (ev, user) => {
        // grab the menu from the user object
        this.setMenu(user.menu);
      });
      scope.$on('auth:login-error', (ev, user) => {
        this.setMenu(this.nullmenu);

      });
      scope.$on('auth:logout-success', (ev, user) => {
        this.setMenu(this.nullmenu);

      });
      scope.$on('auth:validation-success', (ev, user) => {
        this.setMenu(user.menu);
      });
      scope.$on('auth:validation-error', (ev, user) => {
        this.setMenu(this.nullmenu);
      });

      transition.onSuccess({}, (trans) => {
        this.highlightCurrent(trans.targetState().name());
      });

      scope.$on('$stateChangeSuccess', (event, toState, toStateParams, fromState, fromStateParams) => {
        this.highlightCurrent(toState.name);

      });

      scope.$on('$stateChangeError', (event, toState, toStateParams, fromState, fromStateParams, error) => {
        this.highlightCurrent(fromState.name);
      });
    }

    public $onChanges(changes) {
      if (changes.isAuthenticated) {
        this.setMenu(this.identity.menu)
      } else {
        this.setMenu(this.nullmenu);
      }
    }
    private setParent(children: any[], parent) {
      children.forEach((child) => {
        child.parent = parent;
        if (child.children !== undefined) {
          this.setParent(child.children, child);
        }
      });
    };

    public setMenu(menu) {
      this.currentMenu = angular.copy(menu);
      this.setParent(this.currentMenu, null);
    }

    public findItemByState(children, state) {
      for (var i = 0, length = children.length; i < length; i++) {
        if (children[i].state == state) return children[i];
        if (children[i].children !== undefined) {
          var item = this.findItemByState(children[i].children, state);
          if (item) return item;
        }
      }
    };

    public collapsed() {
      return this.global.get("navbarCollapsed");
    }

    public select(item) {
      // close open nodes
      if (item.open) {
        item.open = false;
        return;
      }
      for (var i = this.openItems.length - 1; i >= 0; i--) {
        this.openItems[i].open = false;
      };
      this.openItems = [];
      var parentRef = item;
      while (parentRef !== null) {
        parentRef.open = true;
        this.openItems.push(parentRef);
        parentRef = parentRef.parent;
      }

      // handle leaf nodes
      if (!item.children || (item.children && item.children.length < 1)) {
        this.selectedFromNavMenu = true;
        for (var j = this.selectedItems.length - 1; j >= 0; j--) {
          this.selectedItems[j].selected = false;
        };
        this.selectedItems = [];
        var parentRef = item;
        while (parentRef !== null) {
          parentRef.selected = true;
          this.selectedItems.push(parentRef);
          parentRef = parentRef.parent;
        }
        // we are nivigating so we don;t need the navigator anymore if it is not locked
        this.global.set("navbarShown", false);      // has no effect in large screen mode
      };
    };

    private highlightCurrent(newVal) {
      if (this.selectedFromNavMenu == false) {
        var item = this.findItemByState(this.currentMenu, newVal);
        if (item)
          // timeout means select is run after exiting, so slectedFromNavMenu gets left on
          // $timeout (function () { $scope.select (item); });
          this.select(item);
      }
      this.selectedFromNavMenu = false;
    };
  }

  class Component implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public template: string;

    constructor() {
      this.bindings = {
        isAuthenticated: "<"
      };
      this.controller = Controller;
      this.controllerAs = "vm";
      this.template = `
        <ul class="navigator" ng-class="{'collapsed': vm.collapsed()}" >
          <li ng-repeat="item in vm.currentMenu" class="nav-topitem nav-item"
            ng-class="{ hasChild: (item.children!==undefined && item.children.length!=0),
              active: item.selected,
              open: (item.children!==undefined && item.children.length!=0) && item.open }"
              ng-include="'generic/navigator'"></li>
        </ul>`;
    }
  }

  angular
    .module("pineapples")
    .component("navigator", new Component());

}