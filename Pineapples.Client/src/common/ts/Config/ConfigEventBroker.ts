﻿// http://simplyaprogrammer.com/2015/05/angular-message-bus-and-smart-watch.html

namespace Sw {
  // these interfaces allow us to assume that publish and subscribe aare part of Scope

  export interface IHub {
    publish: (message, data, sender?) => void;
    subscribe: (message: string, func: (data, sender) => void) => void;
  }
  export interface IScopeEx extends ng.IScope {
    hub: IHub;
  }

  export interface IRootScopeEx extends ng.IRootScopeService {
    hub: IHub;
  }

  export interface IDataChangePublication {
    what: string;
    data: any;
  }

  let decorator = delegate => {
    var rootScope: ng.IRootScopeService = delegate;

    Object.defineProperty(rootScope.constructor.prototype, 'hub', {
      get: function () {
        return {
          publish: (msg, data, sender) => {
            data = data || {};
            // emit goes to parents, broadcast goes down to children
            // since rootScope has no parents, this is the least noisy approach
            // however, with the caveat mentioned below
            rootScope.$emit(msg, data, sender);
          },
          subscribe: (msg, func) => {
            // ignore the event.  Just want the data
            var unbind = rootScope.$on(msg, (event, data, sender) => func(data, sender));
            // being able to enforce unbinding here is why decorating rootscope
            // is preferred over DI of an explicit bus service
            this.$on('$destroy', unbind);
          }
        };
      }
    });
    return delegate;
  };
  function eventBroker($provide: any) {
    $provide.decorator("$rootScope", ["$delegate", decorator]);
  };

  angular
    .module("sw.common")
    .config(["$provide", eventBroker]);

}


