﻿namespace Sw.Auth {



  class Controller {
    // bindings
    public userId: string;
    public code: string;
    public email: string;
    public passwordValidator: IPasswordValidator;

    public newPassword: string;
    public confirmPassword: string;

    public form: ng.IFormController;

    public messageText: string = "";
    public buttonState: string;
    public initialButtonState = "Set your password";
    public inProcess: boolean = false;

    public ModelState: any;
    static $inject = ["$scope", "$state", "$http", "$mdDialog", "authenticationService", "GlobalSettings"];
    constructor(private _scope,
      private $state: ng.ui.IStateService,
      private http: ng.IHttpService,
      private $mdDialog: ng.material.IDialogService,
      private _auth: Sw.Auth.IAuthenticationService,
      public global) {
      this.buttonState = this.initialButtonState;
    }

    public $onInit() {
      this.global.set('fullscreen', true);
    }
    public $onDestroy() {
      this.global.set('fullscreen', false);
    }

    public $onChanges(changes) {
      // log changes to PasswordValidator
      console.log(changes);
    }
    public resetPassword() {
      // this.Form.$setUntouched();

      let data = {
        Code: this.code,
        UserId: this.userId,
        NewPassword: this.newPassword,
        ConfirmPassword: this.confirmPassword
      }
      this.http.post(
        "_api/Account/confirminvite",
        data,
        {
          headers: {
            'Content-Type': 'application/json'
          }
        }).then((response) => {
          let msg = "Congratulations! Your password is recorded and your account is activated.";
          let returnUrl = this.$state.params["back"];
          if (!this.email && !returnUrl) {
            msg += " Log in using your new password.";
          }

          let okAlert = this.$mdDialog.alert()
            .title('Welcome to FEDEMIS!')
            .textContent(msg)
            .ok('Close');
          this.$mdDialog.show(okAlert).finally(() => {
            this.$mdDialog.hide();
            if (returnUrl) {
              window.location.href = returnUrl;
            } else {
              if (this.email) {
                this._auth.login(this.email, this.newPassword, false)
                  .then(() => {
                    this.$state.go('site.home');
                  },
                  (response) => {
                    // should be no reason for this to happen
                    this.$state.go("signin");
                  });
              } else {
                this.$state.go("signin");
              }
            }
          });
        },
        // error setting password
        // if the format of the password is correclt validated, the ooly reason this should happen is if 
        // the password reset token has expired, is invalid - or has been faked
        (response) => {
          this.buttonState = this.initialButtonState;
          console.log("Set password fail");
          console.log(response);
          let msg = "Your password could not be set.";
          switch (response.data.Message) {
            case "Invalid token.":
              msg = msg + " Your Set Password link has expired, or has already been used. From the login page, select FORGOT PASSWORD to set your password again";
              break;
          }
          let okAlert = this.$mdDialog.alert()
            .title("Error")
            .textContent(msg)
            .ok('Close');
          this.$mdDialog.show(okAlert)
            .finally(() => {
              this.$state.go("signin");
            });
        });
    }
  }

  class Component implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;

    constructor() {
      this.bindings = {
        userId: "<",
        code: "<",
        email: "<",
        passwordValidator: "<"
      };
      this.controller = Controller;
      this.controllerAs = "vm";
      this.templateUrl = "user/confirmInvite";
    }
  }
  angular
    .module("sw.common")
    .component("confirmInvite", new Component());
}
