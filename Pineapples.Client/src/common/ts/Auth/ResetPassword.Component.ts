﻿namespace Sw.Auth {



  class Controller {
    // bindings
    public userId: string;
    public code: string;
    public email: string;
    public passwordValidator: IPasswordValidator;

    public newPassword: string;
    public confirmPassword: string;

    public form: ng.IFormController;

    public messageText: string = "";
    public buttonState: string;
    public initialButtonState = "Reset your password";
    public inProcess: boolean = false;

    public ModelState: any;
    static $inject = ["$scope", "$state", "$http", "$mdDialog", "authenticationService", "GlobalSettings"];
    constructor(private _scope,
      private $state: ng.ui.IStateService,
      private http: ng.IHttpService,
      private $mdDialog: ng.material.IDialogService,
      private _auth: Sw.Auth.IAuthenticationService,
      public global) {
      this.buttonState = this.initialButtonState;
    }

    public $onInit() {
      this.global.set('fullscreen', true);
    }
    public $onDestroy() {
      this.global.set('fullscreen', false);
    }

    public $onChanges(changes) {
      // log changes to PasswordValidator
      console.log(changes);
    }
    public resetPassword() {
      // this.Form.$setUntouched();

      let data = {
        Code: this.code,
        UserId: this.userId,
        NewPassword: this.newPassword,
        ConfirmPassword: this.confirmPassword
      }
      this.http.post(
        "_api/Account/resetPassword",
        data,
        {
          headers: {
            'Content-Type': 'application/json'
          }
        }).then((response) => {
          let okAlert = this.$mdDialog.alert()
            .title('Success!')
            .textContent('Your password has been reset. Log in using your new password.')
            .ok('Close');
          this.$mdDialog.show(okAlert).finally(() => {
            this.$mdDialog.hide();
            if (this.email) {
              this._auth.login(this.email, this.newPassword, false)
                .then(() => {
                  this.$state.go('site.home');
                },
                (response) => {
                  // should be no reason for this to happen
                  this.$state.go("signin");
                });
            } else {
              this.$state.go("signin");
            }
          });
        }, (response) => {
          this.buttonState = this.initialButtonState;
          console.log("Reset Password fail");
          console.log(response);
          let okAlert = this.$mdDialog.alert()
            .title("Error")
            .textContent("Your password could not be reset.")
            .ok('Close');
          this.$mdDialog.show(okAlert);
        });
    }
  }

  class Component implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;

    constructor() {
      this.bindings = {
        userId: "<",
        code: "<",
        email: "<",
        passwordValidator: "<"
      };
      this.controller = Controller;
      this.controllerAs = "vm";
      this.templateUrl = "user/resetpassword";
    }
  }
  angular
    .module("sw.common")
    .component("resetPassword", new Component());
}
