﻿namespace Sw.Auth {



  class Controller {

    public email: string;

    public messageText: string = "";
    public inProcess: boolean = false;

    private baseState: string = "Submit Request";
    private processState;

    public ModelState: any;
    static $inject = ["$scope", "$state", "$http", "$mdDialog",  "authenticationService", "GlobalSettings"];
    constructor(private _scope, 
      private $state: ng.ui.IStateService, 
      private http: ng.IHttpService,
      private $mdDialog: ng.material.IDialogService,
      private _auth: Sw.Auth.IAuthenticationService,
      public global) {
      this.processState = this.baseState;
    }

    public $onInit() {
      this.global.set('fullscreen', true);
    }
    public $onDestroy() {
      this.global.set('fullscreen', false);
    }

    public gotoSignin() {
      this.$state.go("signin");
    }
    public doForgotPassword() {
      // this.Form.$setUntouched();

      let data = {      // this object matches ForgotPasswordBindingModel
        email: this.email
      }
      this.inProcess = true;
      this.processState = "Sending request...";
      this.http.post(
        "_api/Account/ForgotPassword",
        data,
        {
          headers: {
            'Content-Type': 'application/json'
          }
        }).then((response) => {
          this.inProcess = false;
          this.processState = "Request sent";

          let okAlert = this.$mdDialog.alert()
            .title('Success!')
            .htmlContent(`<p>Your request has been processed.<br/>
You will receive an email from Australia Awards Africa with instructions on how to reset your password.<br/><br/>
If you do not receive this email, check your spam or junk folders.</p>`)
            .ok('Close');
          this.$mdDialog.show(okAlert).finally(() => {
            this.$mdDialog.hide();
            this.$state.go("signin");
          });
        }, (response) => {
          this.inProcess = false;
          this.processState = this.baseState;
          this.ModelState = response.data.ModelState;
          console.log("Request failed");
          let alert = this.$mdDialog.alert()
            .title('Request Failed')
            .textContent('An error occurred processing your request. Please check your email address, and try again.')
            .ok('Close')
            .multiple(true);
          this.$mdDialog.show(alert);
          console.log(response);
        });
    }
  }

  class Component implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;

    constructor() {

      this.controller = Controller;
      this.controllerAs = "vm";
      this.templateUrl = "user/forgotpassword";
    }
  }
  angular
    .module("sw.common")
    .component("forgotPassword", new Component());
}
