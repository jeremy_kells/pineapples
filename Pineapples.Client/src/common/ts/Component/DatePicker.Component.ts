﻿namespace Sw.Components {
  interface IBindings {
   
  }
  class Controller implements IBindings {

    public ngModel: any;
    public ngDisabled: boolean;
    public ngRequired: boolean;

    public isOpen: boolean
    static $inject = [];
    constructor() { };

    public open() {
      this.isOpen = true;
    }
    public formats = ["dd/MM/yyyy", "d/M/yyyy", "yyyy-MM-dd", "dd-MM-yyyy", "d-M-yyyy"];
  }

  class Component implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public template: string;

    constructor() {
      this.bindings = {
        ngModel: '=',
        ngDisabled: "<",
        ngRequired: "<",
      };

      this.controller = Controller;
      this.controllerAs = "vm";
      this.template = `<div class="input-group">
                        <input type="text" class="form-control" id="inputCode"
                          ng-model="vm.ngModel"
                          ng-disabled="vm.ngDisabled" 
                          ng-required="vm.ngRequired" 
                          uib-datepicker-popup="dd-MMM-yyyy" close-text="Close" 
                          is-open="vm.isOpen"
                          alt-input-formats="vm.formats"
                        />
                        <span class="input-group-btn">
                        <button type="button" class="btn btn-default" ng-disabled="vm.ngDisabled" ng-click="vm.open()"><i class="fa fa-calendar"></i></button>
                        </span></div>`
    }

  }
  angular
    .module("sw.common")
    .component("swDateInput", new Component());
}



