﻿namespace Sw.Component {
  // simple base class for controllers that allow you to select one item from an ng-repeat (e.g. table rows)
  export class SelectableList {
    public multiSelect: boolean = false;
    public selected = null;
    public selectAs = null;

    // get the token to store in the selection array
    // by default, the whole object, but may be a property or a function returning a value
    // storing a unique property is useful when the component may refresh the data
    // the selection will still be in place after the refresh
    private t(p) {
      if (!this.selectAs) {
        return p;
      }
      if (_.isFunction(this.selectAs)) {
        return this.selectAs.call(this, p);
      }
      return p[this.selectAs];
    }
    public toggleSelect(p) {
      if (this.isSelected(p)) {
        this.unselect(p);
      } else {
        this.select(p);
      }
    }

    public selectionMap(mapfcn: (item: any, index?: number, arr?: any[]) => any) {
      if (this.multiSelect) {
        return (<any[]>this.selected).map(mapfcn);
      }
    }

    public select(p) {
      if (!this.multiSelect) {
        this.selected = this.t(p);
      } else {
        if (!this.isSelected(p)) {
          if (this.selected == null) {
            this.selected = [this.t(p)];
          } else {
            this.selected.push(this.t(p));
          }
        }
      }
    }
    public selectAll(list: any[]) {
      list.forEach((item) => {this.select(item)});
    }

    public unselect(p) {
      if (!this.multiSelect) {
        this.selected = null;
      } else {
        let k = this.selectionIndex(p);
        if (k >= 0) {
          (<any[]>this.selected).splice(k, 1);
        }
      }
    }

    public unselectAll(list?: any[]) {
      if (!list) {
        this.selected = null;
      } else {
        let newSelected = _.reject(this.selected, o => {
          return _.findIndex(list, p => { return this.t(p) === o; }) >= 0;
        });
        this.selected = newSelected;
      }
    }

    public selectionCount() {
      if (this.selected === null)
        return 0;
      if (Array.isArray(this.selected))
        return this.selected.length;
      return 1;
    }
    public isSelected(p): boolean {
      return (this.selectionIndex(p) >= 0);
    }

    private selectionIndex(p): number {
      if (!this.multiSelect) {
        return (this.selected === this.t(p)) ? 0 : -1;
      }
      if (this.selected === null) {
        return -1;
      }
      return _.findIndex(this.selected, (s) => (s === this.t(p)));
    }

    public hasSelection(): boolean {
      return (this.selectionCount() > 0);
    }

    // return the set of elements in list that are selected
    // this is useful if the selectAs token stores only a single property - 
    // this method will return the full array 
    // note that for flexibility, list may be a subarray, or even an entirely different array, 
    // from the array used to generate or manage the list
    public getSelectedFrom(list: any[]) {
      // _ elegance :)
      return _.filter(list, (s) => (this.isSelected(s)));
    }

    // remove any items in selected that do not correspond to items in list
    // this doe not add every item in list to selected, if that is the result you need
    // you can unselectAll then selectAll(list)
    public trimSelectedTo(list: any[]) {
      let trimmed = _.filter(this.selected, (t) => {
        let idx = _.findIndex(list, (s) => (this.t(s) === t));
        return (idx >= 0);
      });
      this.selected = trimmed;
    }
  }
}