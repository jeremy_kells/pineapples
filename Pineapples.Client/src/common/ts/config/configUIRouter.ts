﻿module Sw {
  //
  // ui.Router configure
  //
  function urlRouterConfig($urlRouterProvider: ng.ui.IUrlRouterProvider) {

    // ui-router
    $urlRouterProvider.when('', '/home');
  };

  function urlMatcherFactorConfig(urlMatcherFactory: ng.ui.IUrlMatcherFactory) {
    // this is the better way to set case insensitivity
    urlMatcherFactory.caseInsensitive(true);
  }

  function standardRoutes($stateProvider) {

    $stateProvider
         // 'site' is the parent to manage authorized access
    .state('site', {
      abstract: true,
      data: {
        permissions: {
          only: ["authenticated"],
          redirectTo: 'signin'
        }
      },
      resolve: {
        lookupsResolver: ['Lookups', function (Lookups) {
          return Lookups.init();
        }]
      }
    })
    .state('site.home', {
      url: '/home',
      // moving away from ng-token-auth
      //onEnter: ['$auth', '$state', function ($auth, $state) {
      //    $state.go($auth.user.home);
      onEnter: ["identity", "$state", function (identity, $state) {
        if (identity.isAuthenticated === false) {
          // how can this be?????
        } else {
          $state.go(identity.home);
        }
      }]
    })
    .state('signin', {

      url: '/signin',
      data: {
        roles: []
      },
      views: {
        '@': {
          templateUrl: 'user/login',
          controller: 'LoginController',
          controllerAs: 'vm'
        }
      },
      resolve: {
        lookupCache: ['Lookups', function (Lookups) {
          return Lookups.init();
        }]
      }
    })
.state('site.changepassword', {
  url: '/changepassword',
  views: {
    '@': {
      templateUrl: 'user/ChangePassword',
      controller: 'AuthorizationController',
      controllerAs: 'vm'

    }
  }
})
.state('site.register', {
  url: '/register',
  views: {
    '@': {
      templateUrl: 'user/Register',
      controller: 'LoginController',
      controllerAs: 'vm'
    }
  }
})

.state('restricted', {
  parent: 'site',
  url: '/restricted',
  data: {
    roles: ['Admin']
  },
  views: {
    'content@': {
      templateUrl: 'user/accessrestricted'
    }
  }
}).state('accessdenied', {

  url: '/denied',
  data: {
    roles: []
  },
  views: {
    '@': {
      templateUrl: 'user/accessdenied'
    }
  }
});

    $stateProvider
    .state('x', {
      url: '/x',
      abstract: true
    })
    .state('x.applications', {
      url: '/apps',
      abstract: true
    });
  };
  angular
    .module('sw.common')
    .config(['$urlRouterProvider', urlRouterConfig])
    .config(['$stateProvider', standardRoutes])
    .run(["$urlMatcherFactory", urlMatcherFactorConfig]);

}