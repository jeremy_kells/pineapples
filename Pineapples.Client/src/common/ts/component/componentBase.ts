﻿namespace Sw.Component {

  //standard options for an 'item' component
  export class ItemComponentOptions implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;

    constructor(itemName: string, controller) {
      this.bindings = {
        model: "<" ,
        api: "<"        // api is only need when we allow 'new'
      };
      this.controller = controller;
      this.controllerAs = "vm";
      this.templateUrl = itemName + "/Item";
    }
  }


}
