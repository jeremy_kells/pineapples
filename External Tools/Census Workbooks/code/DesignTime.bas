' DesignTime
' Version date: 20181019
' Version msg: Applied changes to Merge and Rollover from RMI

Sub ColLockCell()
Dim cll As Object
ActiveSheet.Unprotect
For Each cll In Selection
    If cll.Locked = True Then
        cll.Interior.ColorIndex = 39
End If
Next cll
If Selection.Locked = False Then
MsgBox "There is no locked cell in range that you chose", vbOKOnly, "Locked Cell Checker"
End If
End Sub



'
' load a book, and extract each school in that book into a separate workbook
'
Sub AutoExport()

Dim i As Integer
Dim numSrcSchools As Integer

numSrcSchools = ThisWorkbook.MergeSchools.Rows.Count
ThisWorkbook.SelectedListName = "(all)"
For i = 1 To numSrcSchools
    ' select the next school....
    ThisWorkbook.MergeSelectedSchool = i
    ClearAllData
    doMerge
    Sheets("Schools").Activate
    Range("nm_state") = schoolInfo(ThisWorkbook.MergeSelectedSchoolName, "state")
    
    ThisWorkbook.SaveAs ThisWorkbook.BulkExportName
Next
MsgBox "Export Completed"
End Sub
