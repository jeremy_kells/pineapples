' EventHandlersMerge
' Version date: 20181019
' Version msg: Applied changes to Merge and Rollover from RMI

Option Explicit

'
' event handlers for dropdowns and command buttons on the Merge page


Sub doGoToList()
Dim SelectedList As String
SelectedList = Replace(Range("selectedListName"), " ", "")
If SelectedList <> "(all)" Then
    Sheets(SelectedList).Activate
End If

End Sub



Sub ddMergeSheet_Change()
shtMerge.setButtonVisibility
End Sub



Sub doClearList()

On Error GoTo ErrorHandler

Dim SelectedList As String
SelectedList = Replace(Range("selectedListName"), " ", "")
Dim lo As ListObject


If SelectedList = "(all)" Then
    Exit Sub
End If

Dim wk As Worksheet
Set wk = Sheets(SelectedList)
If wk.ListObjects.Count Then
    Set lo = wk.ListObjects(1)
Else
    Exit Sub
End If

Dim ok As Integer
ok = MsgBox("Remove all rows from " & SelectedList & " before merging?", vbQuestion + vbDefaultButton2 + vbOKCancel, "Merge- Clear Rows")
If ok = vbOK Then
    DeleteListObjectRow lo, lo.DataBodyRange.EntireRow
End If
Application.CalculateFull
Exit Sub

ErrorHandler:
    MsgBox "Error clearing list " & SelectedList & ": " & Err.Description, vbExclamation, "Merge Error"
    
End Sub

' if the merge workbook changes - we need to refresh the dropdown list of merge schools
Sub ddMergeWorkbook_Change()
refreshMergeSchoolList
End Sub
Sub ddAllOrOneMerge_Change()
refreshMergeSchoolList
End Sub


Sub refreshMergeSchoolList()

On Error GoTo ErrorHandler

Dim wkName As Variant
Dim r As Range
 Set r = Application.Range("AllOrOneSchool")
 
Dim ddshp As Shape
Dim ddshpR As Shape
Dim dd As ControlFormat
Set ddshp = ThisWorkbook.Sheets("Merge").Shapes("ddMergeSelectedSchool")
Set ddshpR = ThisWorkbook.Sheets("Rollover").Shapes("ddMergeSelectedSchool")
Set dd = ddshp.ControlFormat
 
' clear the current value
Range("mergeSelectedSchool") = -1       ' works better than clear for some reason

Select Case r.Value
    Case 1
        '' all schools
        '' nothing to do
        ddshp.Visible = msoFalse
        ddshpR.Visible = msoFalse
    Case 2
        ddshp.Visible = msoTrue
        ddshpR.Visible = msoTrue
End Select
wkName = Application.Range("selectedWorkbookName")
If wkName > "" Then
Else
    MsgBox "Select the source workbook for the merge"
    Exit Sub
End If

Select Case r.Value
    Case 2
        '' a selected school, we have to get the list
        Dim lo As ListObject

        Set lo = getListObject((wkName), "schools")
        If lo Is Nothing Then
            MsgBox "Could not get schools list from the source workbook. Make sure it is still open", vbExclamation
            doRecalcAll
            Exit Sub
        End If
        If lo.ListRows.Count = 0 Or lo.ListRows.Count = 1 And IsEmpty(lo.ListColumns("School Name").DataBodyRange.Cells(1, 1)) Then
            MsgBox "The School list in the source workbook has no entries", vbExclamation
            doRecalcAll
            Exit Sub
        End If
        ' now to get the schools that are in that list, and we'll assume they are unique
        ddshp.ControlFormat.ListFillRange = ""
        ddshpR.ControlFormat.ListFillRange = ""
        
        ''ddshp.ControlFormat.List = lo.ListColumns("School Name").DataBodyRange
        ''ddshpR.ControlFormat.List = lo.ListColumns("School Name").DataBodyRange
        '' using the variables to set the list doesn;t work - have to path them down the hierarchy from the sheet :(
        ThisWorkbook.Sheets("Merge").Shapes("ddMergeSelectedSchool").ControlFormat.List = lo.ListColumns("School Name").DataBodyRange
        ThisWorkbook.Sheets("Rollover").Shapes("ddMergeSelectedSchool").ControlFormat.List = lo.ListColumns("School Name").DataBodyRange
End Select

Exit Sub

ErrorHandler:
    MsgBox Err.Description
End Sub


Function mergeSchoolList(ByVal wkName As Variant)
  Dim lo As ListObject

        Set lo = getListObject((wkName), "schools")

wkName = Application.Range("selectedWorkbookName")

If wkName > "" Then
Set mergeSchoolList = lo.ListColumns("School Name").DataBodyRange
Else
    Exit Function
End If

End Function

