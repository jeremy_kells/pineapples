﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Pineapples.Controllers
{
    public class PivoterController : ApiController
    {
        [HttpPost]
        [ActionName("enrolment")]
        public object Enrolment(Data.Pivoter fltr)
        {
            return fltr.Enrolment();
        }
    }
}
