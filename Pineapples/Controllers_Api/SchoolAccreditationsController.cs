﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Pineapples.Data;
using DataLayer = Pineapples.Data.DataLayer;
using System.Security.Claims;
using Pineapples.Models;

namespace Pineapples.Controllers
{
    [RoutePrefix("api/schoolaccreditations")]
    public class SchoolAccreditationsController : PineapplesApiController
    {
        public SchoolAccreditationsController(DataLayer.IDSFactory factory) : base(factory) { }

        #region SchoolAccreditation methods
        [HttpPost]
        [Route(@"")]
        [Route(@"{saID:int}")]
        [PineapplesPermission(PermissionTopicEnum.School, Softwords.Web.PermissionAccess.WriteX)]
        public HttpResponseMessage Create(SchoolAccreditationBinder sa)
        {
            // validate that we are allowed to act on this school
            Factory.School().AccessControl((string)sa.definedProps["schNo"], (ClaimsIdentity)User.Identity);

            try
            {
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created,
                    Ds.Create(sa, (ClaimsIdentity)User.Identity));
                return response;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                // return the object as the body
                var resp = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
                throw new HttpResponseException(resp);
            }
        }

        [HttpGet]
        [Route(@"{saID:int}")]
        [PineapplesPermission(PermissionTopicEnum.School, Softwords.Web.PermissionAccess.ReadX)]
        public object Read(int saID)
        {
            return Ds.Read(saID);
        }
                
        [HttpPut]
        [Route(@"{saID:int}")]
        [PineapplesPermission(PermissionTopicEnum.School, Softwords.Web.PermissionAccess.WriteX)]
        public object Update(SchoolAccreditationBinder sa)
        {
            try
            {
                return Ds.Update(sa, (ClaimsIdentity)User.Identity).definedProps;
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                // return the object as the body
                var resp = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
                throw new HttpResponseException(resp);
            }
        }

        [HttpDelete]
        [Route(@"{saID:int}")]
        [PineapplesPermission(PermissionTopicEnum.School, Softwords.Web.PermissionAccess.WriteX)]
        public object Delete([FromBody] SchoolAccreditationBinder sa)
        {
            // TO DO
            return sa.definedProps;
        }
        #endregion

        #region SchoolAccreditation Collection methods
        [HttpPost]
        [ActionName("filter")]
        public object Filter(SchoolAccreditationFilter fltr)
        {
            fltr.ApplyUserFilter(User.Identity as ClaimsIdentity);
            return Ds.Filter(fltr);
        }
        #endregion

        #region Access Control

        private void AccessControl(int qrID)
        {
            Factory.QuarterlyReport().AccessControl(qrID, (ClaimsIdentity)User.Identity);
        }
        #endregion

        private IDSSchoolAccreditation Ds
        {
            get { return Factory.SchoolAccreditation(); }
        }

    }
}
