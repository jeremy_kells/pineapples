﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DataLayer = Pineapples.Data.DataLayer;


using Newtonsoft.Json.Linq;

namespace Pineapples.Controllers
{
    [RoutePrefix("api/schoolscatter")]
    public class SchoolScatterController : PineapplesApiController
    {
    public SchoolScatterController(DataLayer.IDSFactory factory) : base(factory) { }

  
        [HttpPost]
        [Route("getScatter")]
        public Object getSchoolScatter(Data.SchoolScatterOptions opts)
        {
            return Factory.SchoolScatter().getScatter(opts);
        }
    }
}
