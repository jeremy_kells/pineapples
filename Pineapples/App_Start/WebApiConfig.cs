﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

using System.Net.Http.Headers;
using System.Net.Http.Formatting;
using System.Web.Http.Routing;

namespace Pineapples
{
    public class CustomDirectRouteProvider : DefaultDirectRouteProvider
    {
        protected override IReadOnlyList<IDirectRouteFactory>
        GetActionRouteFactories(System.Web.Http.Controllers.HttpActionDescriptor actionDescriptor)
        {
            return actionDescriptor.GetCustomAttributes<IDirectRouteFactory>
            (inherit: true);
        }
    }
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            //   Web API routes
            config.MapHttpAttributeRoutes(new CustomDirectRouteProvider());

            // do this to make json the defualt ... err somehow

            config.Formatters.Add(new BrowserJsonFormatter());

            // register the exception handler filter - this will format the exception appropriately when it falls through
            // http://www.asp.net/web-api/overview/error-handling/exception-handling
            config.Filters.Add(new Softwords.Web.ExceptionFilterAttribute());

            config.Routes.MapHttpRoute(
                 name: "GroupActions",
                 routeTemplate: "api/{controller}/collection/{action}"
             );

            config.Routes.MapHttpRoute(
              name: "ItemActions",
              routeTemplate: "api/{controller}/item/{action}"
            );
            config.Routes.MapHttpRoute(
                name: "selectors",
                routeTemplate: "api/selectors/{action}",
                defaults: new { controller = "selectors" }
            );
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }

    public class BrowserJsonFormatter : System.Net.Http.Formatting.JsonMediaTypeFormatter
    {
        public BrowserJsonFormatter()
        {
            this.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
        }

        public override void SetDefaultContentHeaders(Type type, HttpContentHeaders headers, MediaTypeHeaderValue mediaType)
        {
            base.SetDefaultContentHeaders(type, headers, mediaType);
            headers.ContentType = new MediaTypeHeaderValue("application/json");
        }
    }
}
