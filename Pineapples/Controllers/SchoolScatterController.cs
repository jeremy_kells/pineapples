﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace Pineapples.mvcControllers
{
    public class SchoolScatterController : Softwords.Web.mvcControllers.mvcControllerBase
    {
        // GET: SchoolScatter
        public ActionResult Chart()
        {
            return View("SchoolScatterChart");
        }
        public ActionResult Searcher()
        {
            return View("SchoolScatterSearcher");
        }
    }
}