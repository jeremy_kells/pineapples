﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Softwords.Web.mvcControllers;
using Pineapples.Models;

namespace Pineapples.mvcControllers
{
    public class UserMgrController : Softwords.Web.mvcControllers.mvcControllerBase
    {
        public ActionResult UserList()
        {
            PermissionSet ps = new PermissionSet("Admins");
            return View(ps);
        }
        public ActionResult RoleList()
        {
            return View();
        }
        [LayoutInjector("EditPageLayout")]
        public ActionResult Item()
        {
            ViewBag.EditPermission = "Admins";
            return View();
        }
    }
}