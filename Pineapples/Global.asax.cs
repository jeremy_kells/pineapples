﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.Http;
using System.Web.Optimization;
using System.Web.Mvc;
using ImageProcessor.Web.Services;
using ImageProcessor.Web.Helpers;

namespace Pineapples
{
    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            GlobalConfiguration.Configure(WebApiConfig.Register);
            //support for unity dependency injeciton
            UnityConfig.RegisterComponents();
            // usual bundling and dependencies for angular etc are in here in common bundle config
            // omit this for the time being until it is redesigned for simppler
            // model
            // Softwords.Web.CommonBundleConfig.RegisterBundles(BundleTable.Bundles);

            //any project specific libraries?
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            // need this to get MVC routing
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            // see also
            //http://stackoverflow.com/questions/14228072/app-start-folder-in-asp-4-5-only-in-webapplications-projects

            // finally change the paths where we look for mvc views
            // see http://stackoverflow.com/questions/632964/can-i-specify-a-custom-location-to-search-for-views-in-asp-net-mvc
            // we include a path based on the current 'context' as defined in web.config  see issue111

            string context = System.Web.Configuration.WebConfigurationManager.AppSettings["context"] ?? String.Empty;
            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new Softwords.Web.WebFormViewEngineEx(context));
            ViewEngines.Engines.Add(new Softwords.Web.RazorViewEngineEx(context));

            ImageProcessor.Web.HttpModules.ImageProcessingModule.ValidatingRequest += ImageProcessingModule_ValidatingRequest;

            // configure the razor engine here as well
            Pineapples.RazorEngineConfig.Config(context);

        }

        private void ImageProcessingModule_ValidatingRequest(object sender, ImageProcessor.Web.Helpers.ValidatingRequestEventArgs e)
        {
            const string library = "/library/";
            const string thumb = "/thumb/";
            const string flag = "/flag/";


            // throw away the token now from the querystring so as not to confuse the caching logic
            //t is the token
            int i = e.QueryString.IndexOf("&t=");
            if (i >= 0)
                e.QueryString = e.QueryString.Substring(0, i);

            // get the requestpath
            string requestpath = e.Context.Request.Path;

            if (requestpath.Contains(flag))
            {
                return;
            }
            // requests need authentication
            //////if (!(e.Context.Request.IsAuthenticated))
            //////{
            //////    e.Cancel = true;
            //////    return;
            //////}


            // library requests may be handled either by the imageprocessor module, which can size, and transform an image etc
            // or by the LibController, which will just return the file
            // if there is no query string, let it fall through
            // if there is a query string, remove it and let if fall through if the document is not an image...
            // this means we need to peek at it here
            if (requestpath.Contains(library))
            {
                ValidateLibraryRequest(e);
                return;
            }
            if (requestpath.Contains(thumb))
            {
                ValidateThumbRequest(e);
                return;
            }
        }

        private void ValidateLibraryRequest(ImageProcessor.Web.Helpers.ValidatingRequestEventArgs e)
        {
            // library routes may look like 
            // library/<guid> or library/<guid>/72
            // library/<guid>.ext library/<guid>.ext/48
            // 
            // handled by image processor if its an image, and there is some processing instruction
            // either in the query string, or via the size parameter in the url

            // otherwise handled by the LibraryController, and returne in default size

            char[] separators = new char[] { '/', '\\' };
            string[] parts = e.Context.Request.Path.Split(separators, StringSplitOptions.None);
            int ididx = Array.IndexOf(parts, "library") + 1;
            string id = parts[ididx];

            if (e.QueryString == String.Empty && parts.Length == (ididx + 1))
            {
                // fall through to the default handler
                e.Cancel = true;
                return;
            }
            if (!ImageProcessor.Web.Helpers.ImageHelpers.IsValidImageExtension(Providers.FileDB.GetFilePath(id)))
            {
                e.Cancel = true;
                return;
            }

            string querystring = string.Empty;
            if (parts.Length >= ididx + 2)
            {
                querystring = String.Format("&rotate={0}", parts[ididx + 1]);
            }
            // display full size unless otherwise specified
            if (parts.Length >= ididx + 3)
            {
                string size = parts[ididx + 2];
                if (size != string.Empty && size != "0")
                {
                    querystring += String.Format("&height={0}&width={0}&mode=min", size);
                }
            }
            Array.Resize(ref parts, ididx + 1);
            e.Context.RewritePath(String.Join("/", parts));
            e.QueryString = querystring;
        }

        private void ValidateThumbRequest(ImageProcessor.Web.Helpers.ValidatingRequestEventArgs e)
        {
            // thumbnail routes may look like 
            // thumb/<guid> or thumb/<guid>/72
            // thumb/<guid>.ext thumb/<guid>.ext/48
            // or
            // thumb/pdf thumb/xlsx thumb/zip/24 etc
            // if its not a guid, handle in the controller

            char[] separators = new char[] { '/', '\\' };
            string[] parts = e.Context.Request.Path.Split(separators, StringSplitOptions.None);
            int ididx = Array.IndexOf(parts, "thumb") + 1;
            string id = parts[ididx];

            string ext = System.IO.Path.GetExtension(id);     // drop the dot .
            if (ext == String.Empty)
            {
                Guid gOut = new Guid();
                if (!Guid.TryParse(id, out gOut))
                {
                    // if its not a guid, assume the whole path is just the extension we seek - 
                    // e.g. thumb/pdf
                    // drop through to thumb method of library controller
                    e.Cancel = true;
                    return;
                }
                // it's a guid - assume its pointing into the library, what file is that?
                id = Providers.FileDB.GetFilePath(id);  // id is now the fully qualified path to the file
                ext = System.IO.Path.GetExtension(id);
            }
            // id now contains a path with extension
            ext = ext.Substring(1);   // drop the dot .
            if (!ImageHelpers.IsValidImageExtension(id))
            {
                // not an image, so we just drop through to the controller to get a default thumb for the fie type
                e.Cancel = true;
                // get the extension, and change the url, removing the id and inserting the ext in its place
                parts[ididx] = ext;
                e.Context.RewritePath(String.Join("/", parts));
                return;
            }
            string querystring = string.Empty;          // assemble the imageprocessor directives
            if (parts.Length >= ididx + 2)
            {
                querystring = String.Format("&rotate={0}", parts[ididx + 1]);
            }
            // we are going to create a thumb fro the image - how big?
            // the thumb may have a size argument,if it does we convert here into a query string
            // and strip off the size
            string size = "72";
            if (parts.Length >= ididx + 3)
            {
                size = parts[ididx + 2];
                if (size == string.Empty || size == "0")
                {
                    size = "72";
                }
            }
            querystring += String.Format("&height={0}&width={0}&mode=min", size);
            Array.Resize(ref parts, ididx + 1);
            e.Context.RewritePath(String.Join("/", parts));
            e.QueryString = querystring;
        }



        // this from here:
        //http://stackoverflow.com/questions/47089/best-way-in-asp-net-to-force-https-for-an-entire-site
        protected void Application_BeginRequest(Object sender, EventArgs e)
        {
            switch (Request.Url.Scheme)
            {
                case "https":
                    // put this to one side for a while.... 2015 01 06
                    //Response.AddHeader("Strict-Transport-Security", "max-age=300");
                    break;
                case "http":
                    string useHttps = System.Web.Configuration.WebConfigurationManager.AppSettings["useHttps"];
                    if (useHttps == null || useHttps.ToLower() != "false")
                    {
                        if (Request.Url.Host == "localhost")
                        {
                            // special case for localhost
                            // just hardcode the SSL path that appears in the project properties window -- see
                            //http://www.asp.net/web-api/overview/security/working-with-ssl-in-web-api

                            // allow to proceed with https -- note this hardcoded path has to match the properties for the project
                            var path = "https://localhost:44301" + Request.Url.PathAndQuery;
                            Response.Status = "301 Moved Permanently";
                            Response.AddHeader("Location", path);
                        }
                        else
                        {
                            var path = "https://" + Request.Url.Host + Request.Url.PathAndQuery;
                            Response.Status = "301 Moved Permanently";
                            Response.AddHeader("Location", path);
                        }
                    }
                    break;
            }
        }
    }
}